﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <stdint.h>
#include <limits>


template <typename T1>
struct VirtualActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct VirtualFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct VirtualFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R, typename T1, typename T2>
struct VirtualFuncInvoker2
{
	typedef R (*Func)(void*, T1, T2, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};

// System.Action`1<System.Object>
struct Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC;
// System.Action`1<UnityEngine.InputSystem.InputActionRebindingExtensions/RebindingOperation>
struct Action_1_t712E64B2FB6B72A4E947CBB2975DABB73A06E663;
// System.Action`2<UnityEngine.InputSystem.LowLevel.InputEventPtr,UnityEngine.InputSystem.InputDevice>
struct Action_2_t83E2B2D05A16B14F487969D4D187AEC66413E199;
// System.Action`2<System.Object,UnityEngine.InputSystem.InputActionChange>
struct Action_2_tACB2F8D6B2F70F78DC65A6BC64A3108B0B1AC539;
// System.Action`2<UnityEngine.InputSystem.InputActionRebindingExtensions/RebindingOperation,System.String>
struct Action_2_t1C13FAA0BE04A7EFAC5DDD87B23307535BE59B56;
// System.Dynamic.Utils.CacheDict`2<System.Type,System.Func`5<System.Linq.Expressions.Expression,System.String,System.Boolean,System.Collections.ObjectModel.ReadOnlyCollection`1<System.Linq.Expressions.ParameterExpression>,System.Linq.Expressions.LambdaExpression>>
struct CacheDict_2_t9FD97836EA998D29FFE492313652BD241E48F2C6;
// System.Dynamic.Utils.CacheDict`2<System.Type,System.Reflection.MethodInfo>
struct CacheDict_2_t23833FEB97C42D87EBF4B5FE3B56AA1336D7B3CE;
// System.Runtime.CompilerServices.ConditionalWeakTable`2<System.Linq.Expressions.Expression,System.Linq.Expressions.Expression/ExtensionInfo>
struct ConditionalWeakTable_2_t53315BD762B310982B9C8EEAA1BEB06E4E8D0815;
// System.Collections.Generic.Dictionary`2<UnityEngine.InputSystem.Utilities.InternedString,System.Func`1<UnityEngine.InputSystem.Layouts.InputControlLayout>>
struct Dictionary_2_t73E7A2CC7CD17B4CBC4DBC5B50FC3A085745875D;
// System.Collections.Generic.Dictionary`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString[]>
struct Dictionary_2_t6B8207956EB1FAAEB6DBD37E3FD84A30466F2BA8;
// System.Collections.Generic.Dictionary`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Layouts.InputControlLayout>
struct Dictionary_2_tDCF55C892C8E630B68A32A8B68C04B47E837C8A7;
// System.Collections.Generic.Dictionary`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>
struct Dictionary_2_tBE5A6550789B79F0A18DA883DF48023D1D980096;
// System.Collections.Generic.Dictionary`2<UnityEngine.InputSystem.Utilities.InternedString,System.String>
struct Dictionary_2_tDA5E5646D36E737D207FA28F7AB4CE457B42B367;
// System.Collections.Generic.Dictionary`2<UnityEngine.InputSystem.Utilities.InternedString,System.Type>
struct Dictionary_2_t9AFCC08689B78B2E9999C1E77045DE7FE2E38939;
// System.Collections.Generic.Dictionary`2<System.Object,UniGLTF.glTFExtensions/ComponentVec>
struct Dictionary_2_tB46DDF458408E887F684BF61B389BCF27B59F718;
// System.Collections.Generic.Dictionary`2<System.Linq.Expressions.ParameterExpression,System.Int32>
struct Dictionary_2_t557635FBDBCB4F09E0827F01D69D76FF503D03A7;
// System.Collections.Generic.Dictionary`2<System.Linq.Expressions.ParameterExpression,System.Linq.Expressions.Interpreter.LocalVariable>
struct Dictionary_2_tAE9216CE6245A2FBEA94860E5D55598909B27352;
// System.Collections.Generic.Dictionary`2<System.Type,UniGLTF.glTFExtensions/ComponentVec>
struct Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E;
// UnityEngine.Rendering.DynamicArray`1<UnityEngine.Experimental.Rendering.RenderGraphModule.IRenderGraphResource>
struct DynamicArray_1_tE865ACD9B04344D2C5D88A7A1DDE805DDE7B5616;
// UnityEngine.Rendering.DynamicArray`1<System.Object>
struct DynamicArray_1_tBD159187A396D2634FDBCFD5A8F85F26FA3DF11D;
// System.Linq.Expressions.Expression`1<System.Object>
struct Expression_1_t01B093F396848A065BE827B0C58E6F20E760FB6F;
// System.Func`2<UnityEngine.InputSystem.InputControl,System.String>
struct Func_2_t256D379297E9972A2E59B56A5D0276E9ED789181;
// System.Func`3<UnityEngine.InputSystem.InputControl,UnityEngine.InputSystem.LowLevel.InputEventPtr,System.Single>
struct Func_3_tB2180AFDCB89ECEDE3ED8A30CA81B8E923416ADC;
// System.Collections.Generic.HashSet`1<UnityEngine.InputSystem.Utilities.InternedString>
struct HashSet_1_t68A0DBD19F3F3043FF8AC0D854B63BAA7BEB4064;
// System.Collections.Generic.HashSet`1<System.Object>
struct HashSet_1_t680119C7ED8D82AED56CDB83DF6F0E9149852A9B;
// System.Collections.Generic.HashSet`1<System.Linq.Expressions.ParameterExpression>
struct HashSet_1_t42A3AC337CA15FAC250AA5DA438F909806C72CB0;
// System.Collections.Generic.IEnumerable`1<System.Linq.Expressions.ParameterExpression>
struct IEnumerable_1_tF5978845C2912DCA5471ADD9480357E32BB03D1E;
// System.Collections.Generic.IEqualityComparer`1<System.Linq.Expressions.ParameterExpression>
struct IEqualityComparer_1_t25F6568124205E1DCEEEFEF9FFD485B340114892;
// System.Collections.Generic.IEqualityComparer`1<System.Type>
struct IEqualityComparer_1_t7EEC9B4006D6D425748908D52AA799197F29A165;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,UniGLTF.glTFExtensions/ComponentVec>
struct KeyCollection_t5808F3ACCA0E43AA457FB857458CFB079947CB52;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5;
// System.Collections.Generic.List`1<System.Linq.Expressions.ParameterExpression>
struct List_1_tCDDF33E8793E2DD752E38CC326B13F8F35B1493B;
// System.Collections.Generic.List`1<System.String>
struct List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3;
// System.Collections.Generic.List`1<UniGLTF.glTFAccessor>
struct List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC;
// System.Collections.Generic.List`1<UniGLTF.glTFAnimation>
struct List_1_t0ACBD66ACBCB928B2502E851848F2397C58315E5;
// System.Collections.Generic.List`1<UniGLTF.glTFBuffer>
struct List_1_t4BFB26128F5A4C3E19B9E2DEAB3A142C3EFF8F38;
// System.Collections.Generic.List`1<UniGLTF.glTFBufferView>
struct List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89;
// System.Collections.Generic.List`1<UniGLTF.glTFCamera>
struct List_1_tB0D97651B850CC1988E2E5E04908E28BF5CEF39C;
// System.Collections.Generic.List`1<UniGLTF.glTFImage>
struct List_1_tD99B99E72B26A00EB46A7FD4B4CF8A7737F04CDD;
// System.Collections.Generic.List`1<UniGLTF.glTFMaterial>
struct List_1_tBF560A6A99F5B82BE164503467D8684D672FC119;
// System.Collections.Generic.List`1<UniGLTF.glTFMesh>
struct List_1_t934B70DFA7B8E9278E8936524113CCE5A37F007C;
// System.Collections.Generic.List`1<UniGLTF.glTFNode>
struct List_1_tE044764DB1DBE4A4B6C5DD39C3237D73AD872853;
// System.Collections.Generic.List`1<UniGLTF.glTFSkin>
struct List_1_t24D9800C2C25EC55A7776132DF3A1D018CD28065;
// System.Collections.Generic.List`1<UniGLTF.glTFTexture>
struct List_1_t93D2AEDD3CACD14F2165FD425D3B53F7DC20C9C0;
// System.Collections.Generic.List`1<UniGLTF.glTFTextureSampler>
struct List_1_tD62CE7D871066E026CC03730FED6BDCE71EFCC90;
// System.Collections.Generic.List`1<UniGLTF.gltfScene>
struct List_1_tA6B2AA43D377CA57C52B77094F68B8394D39B3E7;
// System.Collections.Generic.List`1<UnityEngine.InputSystem.Layouts.InputControlLayout/Collection/LayoutMatcher>
struct List_1_tE0D0016948365B54865C0A62B95211A71F2FB9A6;
// System.Collections.Generic.Stack`1<System.Collections.Generic.HashSet`1<System.Linq.Expressions.ParameterExpression>>
struct Stack_1_t438C22E9DF33740A9BDB48CF9504B6E044484958;
// System.Collections.Generic.Stack`1<System.Object>
struct Stack_1_t92AC5F573A3C00899B24B775A71B4327D588E981;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,UniGLTF.glTFExtensions/ComponentVec>
struct ValueCollection_tCE56BF0D3F5B56997BD65FBFB46555F58386A2D4;
// System.Action`1<System.Object>[]
struct Action_1U5BU5D_t45AFA72AB9C17482E4A448FF460696B2CD7F5B30;
// System.Action`2<System.Object,UnityEngine.InputSystem.InputActionChange>[]
struct Action_2U5BU5D_t459BDBFC4C24F188864A38C4CD1D0E76071FEF6D;
// System.Collections.Generic.Dictionary`2/Entry<System.Type,UniGLTF.glTFExtensions/ComponentVec>[]
struct EntryU5BU5D_t37832DB0F69C709421F10CA29BC5FA8F5A8E6A93;
// System.Collections.Generic.HashSet`1<System.Linq.Expressions.ParameterExpression>[]
struct HashSet_1U5BU5D_tE80AA7A58195958A441A5F7F80D36F0F00AD9275;
// System.Collections.Generic.HashSet`1/Slot<System.Linq.Expressions.ParameterExpression>[]
struct SlotU5BU5D_tAF315AD110D3AD4FBD91B25289AFC6FB963DC31E;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// UnityEngine.Color[]
struct ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834;
// System.Runtime.InteropServices.GCHandle[]
struct GCHandleU5BU5D_tE15D27927DA8B3D667EF2723192CDC34B763D2F3;
// UnityEngine.InputSystem.IInputInteraction[]
struct IInputInteractionU5BU5D_tC7710034CF7D51B96E7C1E12F3C7E64AB0B6C3E8;
// UnityEngine.Experimental.Rendering.RenderGraphModule.IRenderGraphResource[]
struct IRenderGraphResourceU5BU5D_tDA418E4DA02A060158EDE9260A8B3305AA1DE7CE;
// UnityEngine.InputSystem.InputActionMap[]
struct InputActionMapU5BU5D_tEE6F572B03BE03B28C7B701C19AE4FD4D5D2E580;
// UnityEngine.InputSystem.InputBindingComposite[]
struct InputBindingCompositeU5BU5D_tC0DEE76C22B9127A27E52487A6F2F5F9CCF483B6;
// UnityEngine.InputSystem.InputControl[]
struct InputControlU5BU5D_t4668D8411829C981759512FED1506B20F902CA67;
// UnityEngine.InputSystem.InputProcessor[]
struct InputProcessorU5BU5D_tC2B97EE395F9CFBD17046BD90DB5A0A3B46EB9D3;
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
// System.IntPtr[]
struct IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6;
// UnityEngine.InputSystem.Utilities.InternedString[]
struct InternedStringU5BU5D_t1B3BD9ED90129E67B58E1681B1944E72F8E0E648;
// UnityEngine.Matrix4x4[]
struct Matrix4x4U5BU5D_tE53F71E9C9110DD439281A6AB8B699F9F85D8F82;
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
// System.Linq.Expressions.ParameterExpression[]
struct ParameterExpressionU5BU5D_tCF3EAA6D8556513C4937E1126F65AA08DF4DB147;
// System.Single[]
struct SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971;
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.UInt32[]
struct UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF;
// UniGLTF.UShort4[]
struct UShort4U5BU5D_tBDA4C224D509794003F0CCB10B200496F61CEA35;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871;
// UniGLTF.glTFAccessor[]
struct glTFAccessorU5BU5D_tB6C25C0019B07C4953F77D1C50A3E47C80F4B3F1;
// UniGLTF.glTFBuffer[]
struct glTFBufferU5BU5D_t650B9B5A1C957CEFFEA63275F01A843E001F365D;
// UniGLTF.glTFBufferView[]
struct glTFBufferViewU5BU5D_t0E07A12470F4D78D9480515FB786D4F57C607A96;
// UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem[]
struct ControlItemU5BU5D_t5AFF1520667DFF9AA06E17887A3D0B77BEB173BB;
// UnityEngine.InputSystem.InputControlScheme/DeviceRequirement[]
struct DeviceRequirementU5BU5D_t78146FB21A5C410CBC1F66A8D380D1E95EB2C045;
// System.Action
struct Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Exception
struct Exception_t;
// System.Linq.Expressions.Expression
struct Expression_t30A004209C10C2D9A9785B2F74EEED431A4D4660;
// UniGLTF.IBytesBuffer
struct IBytesBuffer_t3E01F7DE9D56A7A94BADE70648EA289D789E2592;
// System.Collections.IDictionary
struct IDictionary_t99871C56B8EC2452AC5C4CF3831695E617B89D3A;
// UnityEngine.Experimental.Rendering.RenderGraphModule.IRenderGraphResource
struct IRenderGraphResource_tC6A193679EB59585903B3E0EFC7F5B937C68AAE7;
// UnityEngine.Experimental.Rendering.RenderGraphModule.IRenderGraphResourcePool
struct IRenderGraphResourcePool_t4D3438D3ED4DDEC9E1881C5EEADCD82187BECC7D;
// UnityEngine.InputSystem.InputAction
struct InputAction_tF0C77AF72E0C19608B020E77A2C36C04E9DCBE48;
// UnityEngine.InputSystem.InputActionAsset
struct InputActionAsset_tB1114F77EFFAE61A28A8A559662A6FCCF444F70E;
// UnityEngine.InputSystem.InputActionMap
struct InputActionMap_tBA45B784C5957091166E0007413402A3E83DD226;
// UnityEngine.InputSystem.InputActionState
struct InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C;
// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046;
// System.Linq.Expressions.Interpreter.InterpretedFrame
struct InterpretedFrame_tC7B57503A639148EB56B34F5464120D4B42627E2;
// System.Linq.Expressions.LambdaExpression
struct LambdaExpression_t26BF905E97E6D94F81F17D60F4F4F47F8E93B474;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// System.NotImplementedException
struct NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6;
// System.NotSupportedException
struct NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339;
// System.Linq.Expressions.ParameterExpression
struct ParameterExpression_tA7B24F1DE0F013DA4BD55F76DB43B06DB33D8BEE;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1;
// System.Single
struct Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Type
struct Type_t;
// System.UInt16
struct UInt16_t894EA9D4FB7C799B244E7BBF2DF0EEEDBC77A8BD;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// UniGLTF.glTF
struct glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17;
// UniGLTF.glTFAccessor
struct glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC;
// UniGLTF.glTFAssets
struct glTFAssets_t52C00CC462A2941625CCAB39CFC40E45E49A4986;
// UniGLTF.glTFBuffer
struct glTFBuffer_t840FC6FB6032C82B50388CE51BB1F054475E4BD6;
// UniGLTF.glTFBufferView
struct glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4;
// UniGLTF.glTFSparse
struct glTFSparse_tF8621187A1982375A902F6D7B023E40B72886684;
// UniGLTF.glTFSparseIndices
struct glTFSparseIndices_t332206D5F85C3918BDB6880C545D1B59EF3DE614;
// UniGLTF.glTFSparseValues
struct glTFSparseValues_t482F196AE1519B26241A4BB28B338F2977BC734F;
// UniGLTF.glTF_extensions
struct glTF_extensions_t1CAC5D30CEDEEFCC3C3DDEBF83806FB88490B985;
// UniGLTF.gltf_extras
struct gltf_extras_tC3468176DD822D7D326DB819BA4F115B129E6C94;
// UnityEngine.InputSystem.InputActionRebindingExtensions/RebindingOperation
struct RebindingOperation_t5DA0F3263991E7BED360B8A1ADE014B9B451F970;
// UnityEngine.InputSystem.InputActionState/ActionMapIndices
struct ActionMapIndices_t70234E0132647DCE386300AED2C964F629609245;
// UnityEngine.InputSystem.InputActionState/BindingState
struct BindingState_tE715A30188652BA114CEB7CD0E12A25CFCEF8FC7;
// UnityEngine.InputSystem.InputActionState/InteractionState
struct InteractionState_tC8A885CD98436B1609B0988EFDCCD6BBBCFDD6B7;
// UnityEngine.InputSystem.InputActionState/TriggerState
struct TriggerState_tD1D00F0DE77AE9CA21DE7A2CEDA8D93EED009CBE;
// UnityEngine.InputSystem.Layouts.InputControlLayout/Builder
struct Builder_t4C196179CE57554255EBFB4ECD4D6499ADA974FB;
// System.Linq.Expressions.Interpreter.LightCompiler/QuoteVisitor
struct QuoteVisitor_tFE404B4C826642C3FC245A108AEC9E94D691E627;
// System.Linq.Expressions.Interpreter.QuoteInstruction/ExpressionQuoter
struct ExpressionQuoter_t174D328A07E522775BA6B19ADF09DBEAF13098FE;
// UnityEngine.Experimental.Rendering.RenderGraphModule.RenderGraphResourceRegistry/RenderGraphResourcesData
struct RenderGraphResourcesData_tF2FA3C3EF12CF61BE1A292070F6C8B355CEBAC8D;
// UnityEngine.Experimental.Rendering.RenderGraphModule.RenderGraphResourceRegistry/ResourceCallback
struct ResourceCallback_t7E07CE90DE9FCF190F4C1598056CDEE81573878B;

IL2CPP_EXTERN_C RuntimeClass* Exception_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* HashSet_1_t42A3AC337CA15FAC250AA5DA438F909806C72CB0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* InputControlLayout_t012E8BC3C93CE8A2A35BFC6A70C52530FFB55917_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* InputProcessor_tAC24832406A406286CE4B162D9D2EF5E2E08765A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_tCDDF33E8793E2DD752E38CC326B13F8F35B1493B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Type_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* glTFSparseIndices_t332206D5F85C3918BDB6880C545D1B59EF3DE614_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* glTFSparseValues_t482F196AE1519B26241A4BB28B338F2977BC734F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* glTFSparse_tF8621187A1982375A902F6D7B023E40B72886684_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral02BB463928D4E15BD5ECA84DABC17D6F67D3A89A;
IL2CPP_EXTERN_C String_t* _stringLiteral6DA17EF0CFCB88F3EE460F1488CE83FBB4DE84A3;
IL2CPP_EXTERN_C String_t* _stringLiteral725B1CAFF9B49E1231FDA15B85166BBEFAA36A11;
IL2CPP_EXTERN_C String_t* _stringLiteral7F5DF872FA6CA76650526A1B598A5AE1F16E1FFC;
IL2CPP_EXTERN_C String_t* _stringLiteralAFCE96C2E9CB5FEF65576BADEA096873577F2BF6;
IL2CPP_EXTERN_C String_t* _stringLiteralE3A5647F9D4D7321D0FB2267C5B9C147C222F636;
IL2CPP_EXTERN_C String_t* _stringLiteralEAAF420AB23FFD94B4DF38C14A9A16F0BED2E0F7;
IL2CPP_EXTERN_C String_t* _stringLiteralEF8AE9E6CBCFDABA932FBEB4C85964F450F724F5;
IL2CPP_EXTERN_C const RuntimeMethod* Array_Empty_TisParameterExpression_tA7B24F1DE0F013DA4BD55F76DB43B06DB33D8BEE_m5F471CFF3314A2643178F228536765874B6A83EF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* BindingSyntax_WithInteraction_TisRuntimeObject_mF004EAD1BEFB97AAECF7B3AD3C9856304B302EDC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* BindingSyntax_WithProcessor_TisRuntimeObject_mE7F31DB067FE1D0D8B6CAE2F3A869BB60B4E15ED_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_TryGetValue_m5453E36663BAE277EFCD153F225808627F9570CD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* DynamicArray_1_Resize_mF6CDBE3B7E4118B28D15C20B1CA3DE54A23AB9FC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* DynamicArray_1_get_Item_mA0C22D4E0321EFABE0334B7074D3E0978CE46251_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* DynamicArray_1_get_size_m45BF02C8D0C22798BED29BCA0C5470C01CB2A015_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* HashSet_1_Add_m9CF730E2C32C0613D8089427318C09E55C350DBE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* HashSet_1__ctor_m7E015D0E7832B3967403CAEE703C819D77AE741B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m0B405996905BBAFA7ED5939184F2344559D3400C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m6189B75EF62756B468C403149F4AD081AC68C912_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_mDDEE5E9B13D0FE35D394C910EB8ADDF90244F666_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mE4C9B3F15E5D5168479F4E7227A000B97C871A30_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_m3528BC05F270F79CD2DFF46FFDD7722124617F04_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_mC221419694BDE6FF50C3F68C0964472743ACBA03_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_mC4617D8E0A2C06E1168AC910BDB298A17C8F53FC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Stack_1_Pop_m1B41FE87FFACE09E92B93079328100A46D7A9550_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Stack_1_Push_m40AB4A635E6FAFC5491A227935BF4EB1D070C8E1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* glTFExtensions_GetAccessorType_TisColor_tF40DAF76C04FFECF3FE6024F85A294741C9CC659_mAC298AB0447CC9FB4D019F99B852974CA050283E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* glTFExtensions_GetAccessorType_TisMatrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461_mF397CF1D74549BA7A9BD045FEEB3E6A0831259BD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* glTFExtensions_GetAccessorType_TisRuntimeObject_m86DD1A906539E3063A4EEB8AB82D74ABE1A3EA58_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* glTFExtensions_GetAccessorType_TisUInt32_tE60352A06233E4E69DD198BCC67142159F686B15_mABDD7BBAF69B1BEE6F36115F00EDA0DADAF017BB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* glTFExtensions_GetAccessorType_TisUShort4_tB19A4ECD972B8600C9515187BC86B572B83920DB_mDCBE589FB99CC00854FCD9DE415B16F024B2129E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* glTFExtensions_GetAccessorType_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_mCFFC4EBE31F11816D29C72308AEF1E51A37570EF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* glTFExtensions_GetAccessorType_TisVector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_m9CD508D0E1682A72CF0F38B496C8FBB2E4E0278C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* glTFExtensions_GetAccessorType_TisVector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_m6FCC0143CCE1E49205215AFB4A0C66D6697CEB4A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* glTFExtensions_GetComponentType_TisColor_tF40DAF76C04FFECF3FE6024F85A294741C9CC659_m31B80320403DC6AA259DE27AE6B9AB626B51AD84_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* glTFExtensions_GetComponentType_TisMatrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461_m2C2A005ACBC154DBAB81F23A91AEA84AC2144C08_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* glTFExtensions_GetComponentType_TisRuntimeObject_m401F1575C5C6A2B9B7E42AB3412C6868C2F6F698_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* glTFExtensions_GetComponentType_TisUInt32_tE60352A06233E4E69DD198BCC67142159F686B15_mCB4C70ACCF174504B3EACE9F3249CEDBA3D1FD4A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* glTFExtensions_GetComponentType_TisUShort4_tB19A4ECD972B8600C9515187BC86B572B83920DB_m09407A885CDDA3F95F5C8AAA85A3677E6F0F760B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* glTFExtensions_GetComponentType_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_mE765928E35770B5DF25B89EDEB0CCB98632364A4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* glTFExtensions_GetComponentType_TisVector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_mE483B95CF6753D05452AD675D2ADF98800407FCD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* glTFExtensions_GetComponentType_TisVector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_m196048D2AC77FBEE9F8398F1564F730EBF41A46C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeType* Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* UInt32_tE60352A06233E4E69DD198BCC67142159F686B15_0_0_0_var;
struct DeviceRequirement_t81B74F0AB5D81F4F19AB7A30E422908B6EF74ED3_marshaled_com;
struct DeviceRequirement_t81B74F0AB5D81F4F19AB7A30E422908B6EF74ED3_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
struct Matrix4x4U5BU5D_tE53F71E9C9110DD439281A6AB8B699F9F85D8F82;
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
struct ParameterExpressionU5BU5D_tCF3EAA6D8556513C4937E1126F65AA08DF4DB147;
struct UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF;
struct UShort4U5BU5D_tBDA4C224D509794003F0CCB10B200496F61CEA35;
struct Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA;
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4;
struct Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Collections.Generic.Dictionary`2<System.Type,UniGLTF.glTFExtensions/ComponentVec>
struct Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::buckets
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::entries
	EntryU5BU5D_t37832DB0F69C709421F10CA29BC5FA8F5A8E6A93* ___entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::version
	int32_t ___version_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeList
	int32_t ___freeList_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeCount
	int32_t ___freeCount_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::comparer
	RuntimeObject* ___comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::keys
	KeyCollection_t5808F3ACCA0E43AA457FB857458CFB079947CB52 * ___keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::values
	ValueCollection_tCE56BF0D3F5B56997BD65FBFB46555F58386A2D4 * ___values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E, ___buckets_0)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buckets_0), (void*)value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E, ___entries_1)); }
	inline EntryU5BU5D_t37832DB0F69C709421F10CA29BC5FA8F5A8E6A93* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_t37832DB0F69C709421F10CA29BC5FA8F5A8E6A93** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_t37832DB0F69C709421F10CA29BC5FA8F5A8E6A93* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___entries_1), (void*)value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E, ___freeList_4)); }
	inline int32_t get_freeList_4() const { return ___freeList_4; }
	inline int32_t* get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(int32_t value)
	{
		___freeList_4 = value;
	}

	inline static int32_t get_offset_of_freeCount_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E, ___freeCount_5)); }
	inline int32_t get_freeCount_5() const { return ___freeCount_5; }
	inline int32_t* get_address_of_freeCount_5() { return &___freeCount_5; }
	inline void set_freeCount_5(int32_t value)
	{
		___freeCount_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E, ___comparer_6)); }
	inline RuntimeObject* get_comparer_6() const { return ___comparer_6; }
	inline RuntimeObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(RuntimeObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___comparer_6), (void*)value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E, ___keys_7)); }
	inline KeyCollection_t5808F3ACCA0E43AA457FB857458CFB079947CB52 * get_keys_7() const { return ___keys_7; }
	inline KeyCollection_t5808F3ACCA0E43AA457FB857458CFB079947CB52 ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCollection_t5808F3ACCA0E43AA457FB857458CFB079947CB52 * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___keys_7), (void*)value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E, ___values_8)); }
	inline ValueCollection_tCE56BF0D3F5B56997BD65FBFB46555F58386A2D4 * get_values_8() const { return ___values_8; }
	inline ValueCollection_tCE56BF0D3F5B56997BD65FBFB46555F58386A2D4 ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(ValueCollection_tCE56BF0D3F5B56997BD65FBFB46555F58386A2D4 * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___values_8), (void*)value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E, ____syncRoot_9)); }
	inline RuntimeObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline RuntimeObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(RuntimeObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_9), (void*)value);
	}
};


// UnityEngine.Rendering.DynamicArray`1<UnityEngine.Experimental.Rendering.RenderGraphModule.IRenderGraphResource>
struct DynamicArray_1_tE865ACD9B04344D2C5D88A7A1DDE805DDE7B5616  : public RuntimeObject
{
public:
	// T[] UnityEngine.Rendering.DynamicArray`1::m_Array
	IRenderGraphResourceU5BU5D_tDA418E4DA02A060158EDE9260A8B3305AA1DE7CE* ___m_Array_0;
	// System.Int32 UnityEngine.Rendering.DynamicArray`1::<size>k__BackingField
	int32_t ___U3CsizeU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_m_Array_0() { return static_cast<int32_t>(offsetof(DynamicArray_1_tE865ACD9B04344D2C5D88A7A1DDE805DDE7B5616, ___m_Array_0)); }
	inline IRenderGraphResourceU5BU5D_tDA418E4DA02A060158EDE9260A8B3305AA1DE7CE* get_m_Array_0() const { return ___m_Array_0; }
	inline IRenderGraphResourceU5BU5D_tDA418E4DA02A060158EDE9260A8B3305AA1DE7CE** get_address_of_m_Array_0() { return &___m_Array_0; }
	inline void set_m_Array_0(IRenderGraphResourceU5BU5D_tDA418E4DA02A060158EDE9260A8B3305AA1DE7CE* value)
	{
		___m_Array_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Array_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CsizeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DynamicArray_1_tE865ACD9B04344D2C5D88A7A1DDE805DDE7B5616, ___U3CsizeU3Ek__BackingField_1)); }
	inline int32_t get_U3CsizeU3Ek__BackingField_1() const { return ___U3CsizeU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CsizeU3Ek__BackingField_1() { return &___U3CsizeU3Ek__BackingField_1; }
	inline void set_U3CsizeU3Ek__BackingField_1(int32_t value)
	{
		___U3CsizeU3Ek__BackingField_1 = value;
	}
};


// UnityEngine.Rendering.DynamicArray`1<System.Object>
struct DynamicArray_1_tBD159187A396D2634FDBCFD5A8F85F26FA3DF11D  : public RuntimeObject
{
public:
	// T[] UnityEngine.Rendering.DynamicArray`1::m_Array
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_Array_0;
	// System.Int32 UnityEngine.Rendering.DynamicArray`1::<size>k__BackingField
	int32_t ___U3CsizeU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_m_Array_0() { return static_cast<int32_t>(offsetof(DynamicArray_1_tBD159187A396D2634FDBCFD5A8F85F26FA3DF11D, ___m_Array_0)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_Array_0() const { return ___m_Array_0; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_Array_0() { return &___m_Array_0; }
	inline void set_m_Array_0(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_Array_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Array_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CsizeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DynamicArray_1_tBD159187A396D2634FDBCFD5A8F85F26FA3DF11D, ___U3CsizeU3Ek__BackingField_1)); }
	inline int32_t get_U3CsizeU3Ek__BackingField_1() const { return ___U3CsizeU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CsizeU3Ek__BackingField_1() { return &___U3CsizeU3Ek__BackingField_1; }
	inline void set_U3CsizeU3Ek__BackingField_1(int32_t value)
	{
		___U3CsizeU3Ek__BackingField_1 = value;
	}
};


// System.EmptyArray`1<System.Object>
struct EmptyArray_1_tBF73225DFA890366D579424FE8F40073BF9FBAD4  : public RuntimeObject
{
public:

public:
};

struct EmptyArray_1_tBF73225DFA890366D579424FE8F40073BF9FBAD4_StaticFields
{
public:
	// T[] System.EmptyArray`1::Value
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___Value_0;

public:
	inline static int32_t get_offset_of_Value_0() { return static_cast<int32_t>(offsetof(EmptyArray_1_tBF73225DFA890366D579424FE8F40073BF9FBAD4_StaticFields, ___Value_0)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_Value_0() const { return ___Value_0; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_Value_0() { return &___Value_0; }
	inline void set_Value_0(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___Value_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Value_0), (void*)value);
	}
};


// System.Collections.Generic.HashSet`1<System.Linq.Expressions.ParameterExpression>
struct HashSet_1_t42A3AC337CA15FAC250AA5DA438F909806C72CB0  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.HashSet`1::_buckets
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ____buckets_7;
	// System.Collections.Generic.HashSet`1/Slot<T>[] System.Collections.Generic.HashSet`1::_slots
	SlotU5BU5D_tAF315AD110D3AD4FBD91B25289AFC6FB963DC31E* ____slots_8;
	// System.Int32 System.Collections.Generic.HashSet`1::_count
	int32_t ____count_9;
	// System.Int32 System.Collections.Generic.HashSet`1::_lastIndex
	int32_t ____lastIndex_10;
	// System.Int32 System.Collections.Generic.HashSet`1::_freeList
	int32_t ____freeList_11;
	// System.Collections.Generic.IEqualityComparer`1<T> System.Collections.Generic.HashSet`1::_comparer
	RuntimeObject* ____comparer_12;
	// System.Int32 System.Collections.Generic.HashSet`1::_version
	int32_t ____version_13;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.HashSet`1::_siInfo
	SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1 * ____siInfo_14;

public:
	inline static int32_t get_offset_of__buckets_7() { return static_cast<int32_t>(offsetof(HashSet_1_t42A3AC337CA15FAC250AA5DA438F909806C72CB0, ____buckets_7)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get__buckets_7() const { return ____buckets_7; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of__buckets_7() { return &____buckets_7; }
	inline void set__buckets_7(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		____buckets_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____buckets_7), (void*)value);
	}

	inline static int32_t get_offset_of__slots_8() { return static_cast<int32_t>(offsetof(HashSet_1_t42A3AC337CA15FAC250AA5DA438F909806C72CB0, ____slots_8)); }
	inline SlotU5BU5D_tAF315AD110D3AD4FBD91B25289AFC6FB963DC31E* get__slots_8() const { return ____slots_8; }
	inline SlotU5BU5D_tAF315AD110D3AD4FBD91B25289AFC6FB963DC31E** get_address_of__slots_8() { return &____slots_8; }
	inline void set__slots_8(SlotU5BU5D_tAF315AD110D3AD4FBD91B25289AFC6FB963DC31E* value)
	{
		____slots_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____slots_8), (void*)value);
	}

	inline static int32_t get_offset_of__count_9() { return static_cast<int32_t>(offsetof(HashSet_1_t42A3AC337CA15FAC250AA5DA438F909806C72CB0, ____count_9)); }
	inline int32_t get__count_9() const { return ____count_9; }
	inline int32_t* get_address_of__count_9() { return &____count_9; }
	inline void set__count_9(int32_t value)
	{
		____count_9 = value;
	}

	inline static int32_t get_offset_of__lastIndex_10() { return static_cast<int32_t>(offsetof(HashSet_1_t42A3AC337CA15FAC250AA5DA438F909806C72CB0, ____lastIndex_10)); }
	inline int32_t get__lastIndex_10() const { return ____lastIndex_10; }
	inline int32_t* get_address_of__lastIndex_10() { return &____lastIndex_10; }
	inline void set__lastIndex_10(int32_t value)
	{
		____lastIndex_10 = value;
	}

	inline static int32_t get_offset_of__freeList_11() { return static_cast<int32_t>(offsetof(HashSet_1_t42A3AC337CA15FAC250AA5DA438F909806C72CB0, ____freeList_11)); }
	inline int32_t get__freeList_11() const { return ____freeList_11; }
	inline int32_t* get_address_of__freeList_11() { return &____freeList_11; }
	inline void set__freeList_11(int32_t value)
	{
		____freeList_11 = value;
	}

	inline static int32_t get_offset_of__comparer_12() { return static_cast<int32_t>(offsetof(HashSet_1_t42A3AC337CA15FAC250AA5DA438F909806C72CB0, ____comparer_12)); }
	inline RuntimeObject* get__comparer_12() const { return ____comparer_12; }
	inline RuntimeObject** get_address_of__comparer_12() { return &____comparer_12; }
	inline void set__comparer_12(RuntimeObject* value)
	{
		____comparer_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____comparer_12), (void*)value);
	}

	inline static int32_t get_offset_of__version_13() { return static_cast<int32_t>(offsetof(HashSet_1_t42A3AC337CA15FAC250AA5DA438F909806C72CB0, ____version_13)); }
	inline int32_t get__version_13() const { return ____version_13; }
	inline int32_t* get_address_of__version_13() { return &____version_13; }
	inline void set__version_13(int32_t value)
	{
		____version_13 = value;
	}

	inline static int32_t get_offset_of__siInfo_14() { return static_cast<int32_t>(offsetof(HashSet_1_t42A3AC337CA15FAC250AA5DA438F909806C72CB0, ____siInfo_14)); }
	inline SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1 * get__siInfo_14() const { return ____siInfo_14; }
	inline SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1 ** get_address_of__siInfo_14() { return &____siInfo_14; }
	inline void set__siInfo_14(SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1 * value)
	{
		____siInfo_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____siInfo_14), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____items_1)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields, ____emptyArray_5)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Linq.Expressions.ParameterExpression>
struct List_1_tCDDF33E8793E2DD752E38CC326B13F8F35B1493B  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ParameterExpressionU5BU5D_tCF3EAA6D8556513C4937E1126F65AA08DF4DB147* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tCDDF33E8793E2DD752E38CC326B13F8F35B1493B, ____items_1)); }
	inline ParameterExpressionU5BU5D_tCF3EAA6D8556513C4937E1126F65AA08DF4DB147* get__items_1() const { return ____items_1; }
	inline ParameterExpressionU5BU5D_tCF3EAA6D8556513C4937E1126F65AA08DF4DB147** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ParameterExpressionU5BU5D_tCF3EAA6D8556513C4937E1126F65AA08DF4DB147* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tCDDF33E8793E2DD752E38CC326B13F8F35B1493B, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tCDDF33E8793E2DD752E38CC326B13F8F35B1493B, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tCDDF33E8793E2DD752E38CC326B13F8F35B1493B, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tCDDF33E8793E2DD752E38CC326B13F8F35B1493B_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ParameterExpressionU5BU5D_tCF3EAA6D8556513C4937E1126F65AA08DF4DB147* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tCDDF33E8793E2DD752E38CC326B13F8F35B1493B_StaticFields, ____emptyArray_5)); }
	inline ParameterExpressionU5BU5D_tCF3EAA6D8556513C4937E1126F65AA08DF4DB147* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ParameterExpressionU5BU5D_tCF3EAA6D8556513C4937E1126F65AA08DF4DB147** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ParameterExpressionU5BU5D_tCF3EAA6D8556513C4937E1126F65AA08DF4DB147* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UniGLTF.glTFAccessor>
struct List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	glTFAccessorU5BU5D_tB6C25C0019B07C4953F77D1C50A3E47C80F4B3F1* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC, ____items_1)); }
	inline glTFAccessorU5BU5D_tB6C25C0019B07C4953F77D1C50A3E47C80F4B3F1* get__items_1() const { return ____items_1; }
	inline glTFAccessorU5BU5D_tB6C25C0019B07C4953F77D1C50A3E47C80F4B3F1** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(glTFAccessorU5BU5D_tB6C25C0019B07C4953F77D1C50A3E47C80F4B3F1* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	glTFAccessorU5BU5D_tB6C25C0019B07C4953F77D1C50A3E47C80F4B3F1* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC_StaticFields, ____emptyArray_5)); }
	inline glTFAccessorU5BU5D_tB6C25C0019B07C4953F77D1C50A3E47C80F4B3F1* get__emptyArray_5() const { return ____emptyArray_5; }
	inline glTFAccessorU5BU5D_tB6C25C0019B07C4953F77D1C50A3E47C80F4B3F1** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(glTFAccessorU5BU5D_tB6C25C0019B07C4953F77D1C50A3E47C80F4B3F1* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UniGLTF.glTFBuffer>
struct List_1_t4BFB26128F5A4C3E19B9E2DEAB3A142C3EFF8F38  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	glTFBufferU5BU5D_t650B9B5A1C957CEFFEA63275F01A843E001F365D* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t4BFB26128F5A4C3E19B9E2DEAB3A142C3EFF8F38, ____items_1)); }
	inline glTFBufferU5BU5D_t650B9B5A1C957CEFFEA63275F01A843E001F365D* get__items_1() const { return ____items_1; }
	inline glTFBufferU5BU5D_t650B9B5A1C957CEFFEA63275F01A843E001F365D** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(glTFBufferU5BU5D_t650B9B5A1C957CEFFEA63275F01A843E001F365D* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t4BFB26128F5A4C3E19B9E2DEAB3A142C3EFF8F38, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t4BFB26128F5A4C3E19B9E2DEAB3A142C3EFF8F38, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t4BFB26128F5A4C3E19B9E2DEAB3A142C3EFF8F38, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t4BFB26128F5A4C3E19B9E2DEAB3A142C3EFF8F38_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	glTFBufferU5BU5D_t650B9B5A1C957CEFFEA63275F01A843E001F365D* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t4BFB26128F5A4C3E19B9E2DEAB3A142C3EFF8F38_StaticFields, ____emptyArray_5)); }
	inline glTFBufferU5BU5D_t650B9B5A1C957CEFFEA63275F01A843E001F365D* get__emptyArray_5() const { return ____emptyArray_5; }
	inline glTFBufferU5BU5D_t650B9B5A1C957CEFFEA63275F01A843E001F365D** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(glTFBufferU5BU5D_t650B9B5A1C957CEFFEA63275F01A843E001F365D* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UniGLTF.glTFBufferView>
struct List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	glTFBufferViewU5BU5D_t0E07A12470F4D78D9480515FB786D4F57C607A96* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89, ____items_1)); }
	inline glTFBufferViewU5BU5D_t0E07A12470F4D78D9480515FB786D4F57C607A96* get__items_1() const { return ____items_1; }
	inline glTFBufferViewU5BU5D_t0E07A12470F4D78D9480515FB786D4F57C607A96** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(glTFBufferViewU5BU5D_t0E07A12470F4D78D9480515FB786D4F57C607A96* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	glTFBufferViewU5BU5D_t0E07A12470F4D78D9480515FB786D4F57C607A96* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89_StaticFields, ____emptyArray_5)); }
	inline glTFBufferViewU5BU5D_t0E07A12470F4D78D9480515FB786D4F57C607A96* get__emptyArray_5() const { return ____emptyArray_5; }
	inline glTFBufferViewU5BU5D_t0E07A12470F4D78D9480515FB786D4F57C607A96** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(glTFBufferViewU5BU5D_t0E07A12470F4D78D9480515FB786D4F57C607A96* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.Stack`1<System.Collections.Generic.HashSet`1<System.Linq.Expressions.ParameterExpression>>
struct Stack_1_t438C22E9DF33740A9BDB48CF9504B6E044484958  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.Stack`1::_array
	HashSet_1U5BU5D_tE80AA7A58195958A441A5F7F80D36F0F00AD9275* ____array_0;
	// System.Int32 System.Collections.Generic.Stack`1::_size
	int32_t ____size_1;
	// System.Int32 System.Collections.Generic.Stack`1::_version
	int32_t ____version_2;
	// System.Object System.Collections.Generic.Stack`1::_syncRoot
	RuntimeObject * ____syncRoot_3;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(Stack_1_t438C22E9DF33740A9BDB48CF9504B6E044484958, ____array_0)); }
	inline HashSet_1U5BU5D_tE80AA7A58195958A441A5F7F80D36F0F00AD9275* get__array_0() const { return ____array_0; }
	inline HashSet_1U5BU5D_tE80AA7A58195958A441A5F7F80D36F0F00AD9275** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(HashSet_1U5BU5D_tE80AA7A58195958A441A5F7F80D36F0F00AD9275* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____array_0), (void*)value);
	}

	inline static int32_t get_offset_of__size_1() { return static_cast<int32_t>(offsetof(Stack_1_t438C22E9DF33740A9BDB48CF9504B6E044484958, ____size_1)); }
	inline int32_t get__size_1() const { return ____size_1; }
	inline int32_t* get_address_of__size_1() { return &____size_1; }
	inline void set__size_1(int32_t value)
	{
		____size_1 = value;
	}

	inline static int32_t get_offset_of__version_2() { return static_cast<int32_t>(offsetof(Stack_1_t438C22E9DF33740A9BDB48CF9504B6E044484958, ____version_2)); }
	inline int32_t get__version_2() const { return ____version_2; }
	inline int32_t* get_address_of__version_2() { return &____version_2; }
	inline void set__version_2(int32_t value)
	{
		____version_2 = value;
	}

	inline static int32_t get_offset_of__syncRoot_3() { return static_cast<int32_t>(offsetof(Stack_1_t438C22E9DF33740A9BDB48CF9504B6E044484958, ____syncRoot_3)); }
	inline RuntimeObject * get__syncRoot_3() const { return ____syncRoot_3; }
	inline RuntimeObject ** get_address_of__syncRoot_3() { return &____syncRoot_3; }
	inline void set__syncRoot_3(RuntimeObject * value)
	{
		____syncRoot_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_3), (void*)value);
	}
};


// System.Linq.Expressions.Expression
struct Expression_t30A004209C10C2D9A9785B2F74EEED431A4D4660  : public RuntimeObject
{
public:

public:
};

struct Expression_t30A004209C10C2D9A9785B2F74EEED431A4D4660_StaticFields
{
public:
	// System.Dynamic.Utils.CacheDict`2<System.Type,System.Reflection.MethodInfo> System.Linq.Expressions.Expression::s_lambdaDelegateCache
	CacheDict_2_t23833FEB97C42D87EBF4B5FE3B56AA1336D7B3CE * ___s_lambdaDelegateCache_0;
	// System.Dynamic.Utils.CacheDict`2<System.Type,System.Func`5<System.Linq.Expressions.Expression,System.String,System.Boolean,System.Collections.ObjectModel.ReadOnlyCollection`1<System.Linq.Expressions.ParameterExpression>,System.Linq.Expressions.LambdaExpression>> modreq(System.Runtime.CompilerServices.IsVolatile) System.Linq.Expressions.Expression::s_lambdaFactories
	CacheDict_2_t9FD97836EA998D29FFE492313652BD241E48F2C6 * ___s_lambdaFactories_1;
	// System.Runtime.CompilerServices.ConditionalWeakTable`2<System.Linq.Expressions.Expression,System.Linq.Expressions.Expression/ExtensionInfo> System.Linq.Expressions.Expression::s_legacyCtorSupportTable
	ConditionalWeakTable_2_t53315BD762B310982B9C8EEAA1BEB06E4E8D0815 * ___s_legacyCtorSupportTable_2;

public:
	inline static int32_t get_offset_of_s_lambdaDelegateCache_0() { return static_cast<int32_t>(offsetof(Expression_t30A004209C10C2D9A9785B2F74EEED431A4D4660_StaticFields, ___s_lambdaDelegateCache_0)); }
	inline CacheDict_2_t23833FEB97C42D87EBF4B5FE3B56AA1336D7B3CE * get_s_lambdaDelegateCache_0() const { return ___s_lambdaDelegateCache_0; }
	inline CacheDict_2_t23833FEB97C42D87EBF4B5FE3B56AA1336D7B3CE ** get_address_of_s_lambdaDelegateCache_0() { return &___s_lambdaDelegateCache_0; }
	inline void set_s_lambdaDelegateCache_0(CacheDict_2_t23833FEB97C42D87EBF4B5FE3B56AA1336D7B3CE * value)
	{
		___s_lambdaDelegateCache_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_lambdaDelegateCache_0), (void*)value);
	}

	inline static int32_t get_offset_of_s_lambdaFactories_1() { return static_cast<int32_t>(offsetof(Expression_t30A004209C10C2D9A9785B2F74EEED431A4D4660_StaticFields, ___s_lambdaFactories_1)); }
	inline CacheDict_2_t9FD97836EA998D29FFE492313652BD241E48F2C6 * get_s_lambdaFactories_1() const { return ___s_lambdaFactories_1; }
	inline CacheDict_2_t9FD97836EA998D29FFE492313652BD241E48F2C6 ** get_address_of_s_lambdaFactories_1() { return &___s_lambdaFactories_1; }
	inline void set_s_lambdaFactories_1(CacheDict_2_t9FD97836EA998D29FFE492313652BD241E48F2C6 * value)
	{
		___s_lambdaFactories_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_lambdaFactories_1), (void*)value);
	}

	inline static int32_t get_offset_of_s_legacyCtorSupportTable_2() { return static_cast<int32_t>(offsetof(Expression_t30A004209C10C2D9A9785B2F74EEED431A4D4660_StaticFields, ___s_legacyCtorSupportTable_2)); }
	inline ConditionalWeakTable_2_t53315BD762B310982B9C8EEAA1BEB06E4E8D0815 * get_s_legacyCtorSupportTable_2() const { return ___s_legacyCtorSupportTable_2; }
	inline ConditionalWeakTable_2_t53315BD762B310982B9C8EEAA1BEB06E4E8D0815 ** get_address_of_s_legacyCtorSupportTable_2() { return &___s_legacyCtorSupportTable_2; }
	inline void set_s_legacyCtorSupportTable_2(ConditionalWeakTable_2_t53315BD762B310982B9C8EEAA1BEB06E4E8D0815 * value)
	{
		___s_legacyCtorSupportTable_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_legacyCtorSupportTable_2), (void*)value);
	}
};


// System.Linq.Expressions.ExpressionVisitor
struct ExpressionVisitor_tD098DE8A366FBBB58C498C4EFF8B003FCA726DF4  : public RuntimeObject
{
public:

public:
};


// UnityEngine.Experimental.Rendering.RenderGraphModule.IRenderGraphResource
struct IRenderGraphResource_tC6A193679EB59585903B3E0EFC7F5B937C68AAE7  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Experimental.Rendering.RenderGraphModule.IRenderGraphResource::imported
	bool ___imported_0;
	// System.Boolean UnityEngine.Experimental.Rendering.RenderGraphModule.IRenderGraphResource::shared
	bool ___shared_1;
	// System.Boolean UnityEngine.Experimental.Rendering.RenderGraphModule.IRenderGraphResource::sharedExplicitRelease
	bool ___sharedExplicitRelease_2;
	// System.Boolean UnityEngine.Experimental.Rendering.RenderGraphModule.IRenderGraphResource::requestFallBack
	bool ___requestFallBack_3;
	// System.UInt32 UnityEngine.Experimental.Rendering.RenderGraphModule.IRenderGraphResource::writeCount
	uint32_t ___writeCount_4;
	// System.Int32 UnityEngine.Experimental.Rendering.RenderGraphModule.IRenderGraphResource::cachedHash
	int32_t ___cachedHash_5;
	// System.Int32 UnityEngine.Experimental.Rendering.RenderGraphModule.IRenderGraphResource::transientPassIndex
	int32_t ___transientPassIndex_6;
	// System.Int32 UnityEngine.Experimental.Rendering.RenderGraphModule.IRenderGraphResource::sharedResourceLastFrameUsed
	int32_t ___sharedResourceLastFrameUsed_7;
	// UnityEngine.Experimental.Rendering.RenderGraphModule.IRenderGraphResourcePool UnityEngine.Experimental.Rendering.RenderGraphModule.IRenderGraphResource::m_Pool
	IRenderGraphResourcePool_t4D3438D3ED4DDEC9E1881C5EEADCD82187BECC7D * ___m_Pool_8;

public:
	inline static int32_t get_offset_of_imported_0() { return static_cast<int32_t>(offsetof(IRenderGraphResource_tC6A193679EB59585903B3E0EFC7F5B937C68AAE7, ___imported_0)); }
	inline bool get_imported_0() const { return ___imported_0; }
	inline bool* get_address_of_imported_0() { return &___imported_0; }
	inline void set_imported_0(bool value)
	{
		___imported_0 = value;
	}

	inline static int32_t get_offset_of_shared_1() { return static_cast<int32_t>(offsetof(IRenderGraphResource_tC6A193679EB59585903B3E0EFC7F5B937C68AAE7, ___shared_1)); }
	inline bool get_shared_1() const { return ___shared_1; }
	inline bool* get_address_of_shared_1() { return &___shared_1; }
	inline void set_shared_1(bool value)
	{
		___shared_1 = value;
	}

	inline static int32_t get_offset_of_sharedExplicitRelease_2() { return static_cast<int32_t>(offsetof(IRenderGraphResource_tC6A193679EB59585903B3E0EFC7F5B937C68AAE7, ___sharedExplicitRelease_2)); }
	inline bool get_sharedExplicitRelease_2() const { return ___sharedExplicitRelease_2; }
	inline bool* get_address_of_sharedExplicitRelease_2() { return &___sharedExplicitRelease_2; }
	inline void set_sharedExplicitRelease_2(bool value)
	{
		___sharedExplicitRelease_2 = value;
	}

	inline static int32_t get_offset_of_requestFallBack_3() { return static_cast<int32_t>(offsetof(IRenderGraphResource_tC6A193679EB59585903B3E0EFC7F5B937C68AAE7, ___requestFallBack_3)); }
	inline bool get_requestFallBack_3() const { return ___requestFallBack_3; }
	inline bool* get_address_of_requestFallBack_3() { return &___requestFallBack_3; }
	inline void set_requestFallBack_3(bool value)
	{
		___requestFallBack_3 = value;
	}

	inline static int32_t get_offset_of_writeCount_4() { return static_cast<int32_t>(offsetof(IRenderGraphResource_tC6A193679EB59585903B3E0EFC7F5B937C68AAE7, ___writeCount_4)); }
	inline uint32_t get_writeCount_4() const { return ___writeCount_4; }
	inline uint32_t* get_address_of_writeCount_4() { return &___writeCount_4; }
	inline void set_writeCount_4(uint32_t value)
	{
		___writeCount_4 = value;
	}

	inline static int32_t get_offset_of_cachedHash_5() { return static_cast<int32_t>(offsetof(IRenderGraphResource_tC6A193679EB59585903B3E0EFC7F5B937C68AAE7, ___cachedHash_5)); }
	inline int32_t get_cachedHash_5() const { return ___cachedHash_5; }
	inline int32_t* get_address_of_cachedHash_5() { return &___cachedHash_5; }
	inline void set_cachedHash_5(int32_t value)
	{
		___cachedHash_5 = value;
	}

	inline static int32_t get_offset_of_transientPassIndex_6() { return static_cast<int32_t>(offsetof(IRenderGraphResource_tC6A193679EB59585903B3E0EFC7F5B937C68AAE7, ___transientPassIndex_6)); }
	inline int32_t get_transientPassIndex_6() const { return ___transientPassIndex_6; }
	inline int32_t* get_address_of_transientPassIndex_6() { return &___transientPassIndex_6; }
	inline void set_transientPassIndex_6(int32_t value)
	{
		___transientPassIndex_6 = value;
	}

	inline static int32_t get_offset_of_sharedResourceLastFrameUsed_7() { return static_cast<int32_t>(offsetof(IRenderGraphResource_tC6A193679EB59585903B3E0EFC7F5B937C68AAE7, ___sharedResourceLastFrameUsed_7)); }
	inline int32_t get_sharedResourceLastFrameUsed_7() const { return ___sharedResourceLastFrameUsed_7; }
	inline int32_t* get_address_of_sharedResourceLastFrameUsed_7() { return &___sharedResourceLastFrameUsed_7; }
	inline void set_sharedResourceLastFrameUsed_7(int32_t value)
	{
		___sharedResourceLastFrameUsed_7 = value;
	}

	inline static int32_t get_offset_of_m_Pool_8() { return static_cast<int32_t>(offsetof(IRenderGraphResource_tC6A193679EB59585903B3E0EFC7F5B937C68AAE7, ___m_Pool_8)); }
	inline IRenderGraphResourcePool_t4D3438D3ED4DDEC9E1881C5EEADCD82187BECC7D * get_m_Pool_8() const { return ___m_Pool_8; }
	inline IRenderGraphResourcePool_t4D3438D3ED4DDEC9E1881C5EEADCD82187BECC7D ** get_address_of_m_Pool_8() { return &___m_Pool_8; }
	inline void set_m_Pool_8(IRenderGraphResourcePool_t4D3438D3ED4DDEC9E1881C5EEADCD82187BECC7D * value)
	{
		___m_Pool_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Pool_8), (void*)value);
	}
};


// UnityEngine.Experimental.Rendering.RenderGraphModule.IRenderGraphResourcePool
struct IRenderGraphResourcePool_t4D3438D3ED4DDEC9E1881C5EEADCD82187BECC7D  : public RuntimeObject
{
public:

public:
};


// UniGLTF.JsonSerializableBase
struct JsonSerializableBase_t40934AAB77C4330D240EB9E990699A5DA3EF5C8F  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// UnityEngine.SubsystemsImplementation.SubsystemProvider
struct SubsystemProvider_t39E89FB8DB1EF1D2B0AF93796AEDB19D76A665F9  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.SubsystemsImplementation.SubsystemProvider::m_Running
	bool ___m_Running_0;

public:
	inline static int32_t get_offset_of_m_Running_0() { return static_cast<int32_t>(offsetof(SubsystemProvider_t39E89FB8DB1EF1D2B0AF93796AEDB19D76A665F9, ___m_Running_0)); }
	inline bool get_m_Running_0() const { return ___m_Running_0; }
	inline bool* get_address_of_m_Running_0() { return &___m_Running_0; }
	inline void set_m_Running_0(bool value)
	{
		___m_Running_0 = value;
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// UniGLTF.glTFExtensions
struct glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A  : public RuntimeObject
{
public:

public:
};

struct glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,UniGLTF.glTFExtensions/ComponentVec> UniGLTF.glTFExtensions::ComponentTypeMap
	Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E * ___ComponentTypeMap_0;

public:
	inline static int32_t get_offset_of_ComponentTypeMap_0() { return static_cast<int32_t>(offsetof(glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_StaticFields, ___ComponentTypeMap_0)); }
	inline Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E * get_ComponentTypeMap_0() const { return ___ComponentTypeMap_0; }
	inline Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E ** get_address_of_ComponentTypeMap_0() { return &___ComponentTypeMap_0; }
	inline void set_ComponentTypeMap_0(Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E * value)
	{
		___ComponentTypeMap_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ComponentTypeMap_0), (void*)value);
	}
};


// UnityEngine.Experimental.Rendering.RenderGraphModule.RenderGraphResourceRegistry/RenderGraphResourcesData
struct RenderGraphResourcesData_tF2FA3C3EF12CF61BE1A292070F6C8B355CEBAC8D  : public RuntimeObject
{
public:
	// UnityEngine.Rendering.DynamicArray`1<UnityEngine.Experimental.Rendering.RenderGraphModule.IRenderGraphResource> UnityEngine.Experimental.Rendering.RenderGraphModule.RenderGraphResourceRegistry/RenderGraphResourcesData::resourceArray
	DynamicArray_1_tE865ACD9B04344D2C5D88A7A1DDE805DDE7B5616 * ___resourceArray_0;
	// System.Int32 UnityEngine.Experimental.Rendering.RenderGraphModule.RenderGraphResourceRegistry/RenderGraphResourcesData::sharedResourcesCount
	int32_t ___sharedResourcesCount_1;
	// UnityEngine.Experimental.Rendering.RenderGraphModule.IRenderGraphResourcePool UnityEngine.Experimental.Rendering.RenderGraphModule.RenderGraphResourceRegistry/RenderGraphResourcesData::pool
	IRenderGraphResourcePool_t4D3438D3ED4DDEC9E1881C5EEADCD82187BECC7D * ___pool_2;
	// UnityEngine.Experimental.Rendering.RenderGraphModule.RenderGraphResourceRegistry/ResourceCallback UnityEngine.Experimental.Rendering.RenderGraphModule.RenderGraphResourceRegistry/RenderGraphResourcesData::createResourceCallback
	ResourceCallback_t7E07CE90DE9FCF190F4C1598056CDEE81573878B * ___createResourceCallback_3;
	// UnityEngine.Experimental.Rendering.RenderGraphModule.RenderGraphResourceRegistry/ResourceCallback UnityEngine.Experimental.Rendering.RenderGraphModule.RenderGraphResourceRegistry/RenderGraphResourcesData::releaseResourceCallback
	ResourceCallback_t7E07CE90DE9FCF190F4C1598056CDEE81573878B * ___releaseResourceCallback_4;

public:
	inline static int32_t get_offset_of_resourceArray_0() { return static_cast<int32_t>(offsetof(RenderGraphResourcesData_tF2FA3C3EF12CF61BE1A292070F6C8B355CEBAC8D, ___resourceArray_0)); }
	inline DynamicArray_1_tE865ACD9B04344D2C5D88A7A1DDE805DDE7B5616 * get_resourceArray_0() const { return ___resourceArray_0; }
	inline DynamicArray_1_tE865ACD9B04344D2C5D88A7A1DDE805DDE7B5616 ** get_address_of_resourceArray_0() { return &___resourceArray_0; }
	inline void set_resourceArray_0(DynamicArray_1_tE865ACD9B04344D2C5D88A7A1DDE805DDE7B5616 * value)
	{
		___resourceArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___resourceArray_0), (void*)value);
	}

	inline static int32_t get_offset_of_sharedResourcesCount_1() { return static_cast<int32_t>(offsetof(RenderGraphResourcesData_tF2FA3C3EF12CF61BE1A292070F6C8B355CEBAC8D, ___sharedResourcesCount_1)); }
	inline int32_t get_sharedResourcesCount_1() const { return ___sharedResourcesCount_1; }
	inline int32_t* get_address_of_sharedResourcesCount_1() { return &___sharedResourcesCount_1; }
	inline void set_sharedResourcesCount_1(int32_t value)
	{
		___sharedResourcesCount_1 = value;
	}

	inline static int32_t get_offset_of_pool_2() { return static_cast<int32_t>(offsetof(RenderGraphResourcesData_tF2FA3C3EF12CF61BE1A292070F6C8B355CEBAC8D, ___pool_2)); }
	inline IRenderGraphResourcePool_t4D3438D3ED4DDEC9E1881C5EEADCD82187BECC7D * get_pool_2() const { return ___pool_2; }
	inline IRenderGraphResourcePool_t4D3438D3ED4DDEC9E1881C5EEADCD82187BECC7D ** get_address_of_pool_2() { return &___pool_2; }
	inline void set_pool_2(IRenderGraphResourcePool_t4D3438D3ED4DDEC9E1881C5EEADCD82187BECC7D * value)
	{
		___pool_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pool_2), (void*)value);
	}

	inline static int32_t get_offset_of_createResourceCallback_3() { return static_cast<int32_t>(offsetof(RenderGraphResourcesData_tF2FA3C3EF12CF61BE1A292070F6C8B355CEBAC8D, ___createResourceCallback_3)); }
	inline ResourceCallback_t7E07CE90DE9FCF190F4C1598056CDEE81573878B * get_createResourceCallback_3() const { return ___createResourceCallback_3; }
	inline ResourceCallback_t7E07CE90DE9FCF190F4C1598056CDEE81573878B ** get_address_of_createResourceCallback_3() { return &___createResourceCallback_3; }
	inline void set_createResourceCallback_3(ResourceCallback_t7E07CE90DE9FCF190F4C1598056CDEE81573878B * value)
	{
		___createResourceCallback_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___createResourceCallback_3), (void*)value);
	}

	inline static int32_t get_offset_of_releaseResourceCallback_4() { return static_cast<int32_t>(offsetof(RenderGraphResourcesData_tF2FA3C3EF12CF61BE1A292070F6C8B355CEBAC8D, ___releaseResourceCallback_4)); }
	inline ResourceCallback_t7E07CE90DE9FCF190F4C1598056CDEE81573878B * get_releaseResourceCallback_4() const { return ___releaseResourceCallback_4; }
	inline ResourceCallback_t7E07CE90DE9FCF190F4C1598056CDEE81573878B ** get_address_of_releaseResourceCallback_4() { return &___releaseResourceCallback_4; }
	inline void set_releaseResourceCallback_4(ResourceCallback_t7E07CE90DE9FCF190F4C1598056CDEE81573878B * value)
	{
		___releaseResourceCallback_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___releaseResourceCallback_4), (void*)value);
	}
};


// System.ArraySegment`1<UnityEngine.Color>
struct ArraySegment_1_t8A45E4F401BB0C4F9F8643CCA8706F98DC8057F6 
{
public:
	// T[] System.ArraySegment`1::_array
	ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834* ____array_0;
	// System.Int32 System.ArraySegment`1::_offset
	int32_t ____offset_1;
	// System.Int32 System.ArraySegment`1::_count
	int32_t ____count_2;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(ArraySegment_1_t8A45E4F401BB0C4F9F8643CCA8706F98DC8057F6, ____array_0)); }
	inline ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834* get__array_0() const { return ____array_0; }
	inline ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____array_0), (void*)value);
	}

	inline static int32_t get_offset_of__offset_1() { return static_cast<int32_t>(offsetof(ArraySegment_1_t8A45E4F401BB0C4F9F8643CCA8706F98DC8057F6, ____offset_1)); }
	inline int32_t get__offset_1() const { return ____offset_1; }
	inline int32_t* get_address_of__offset_1() { return &____offset_1; }
	inline void set__offset_1(int32_t value)
	{
		____offset_1 = value;
	}

	inline static int32_t get_offset_of__count_2() { return static_cast<int32_t>(offsetof(ArraySegment_1_t8A45E4F401BB0C4F9F8643CCA8706F98DC8057F6, ____count_2)); }
	inline int32_t get__count_2() const { return ____count_2; }
	inline int32_t* get_address_of__count_2() { return &____count_2; }
	inline void set__count_2(int32_t value)
	{
		____count_2 = value;
	}
};


// System.ArraySegment`1<System.Int32>
struct ArraySegment_1_t9FDA96F04294A918E0DBFCE7CF750DE23A898976 
{
public:
	// T[] System.ArraySegment`1::_array
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ____array_0;
	// System.Int32 System.ArraySegment`1::_offset
	int32_t ____offset_1;
	// System.Int32 System.ArraySegment`1::_count
	int32_t ____count_2;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(ArraySegment_1_t9FDA96F04294A918E0DBFCE7CF750DE23A898976, ____array_0)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get__array_0() const { return ____array_0; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____array_0), (void*)value);
	}

	inline static int32_t get_offset_of__offset_1() { return static_cast<int32_t>(offsetof(ArraySegment_1_t9FDA96F04294A918E0DBFCE7CF750DE23A898976, ____offset_1)); }
	inline int32_t get__offset_1() const { return ____offset_1; }
	inline int32_t* get_address_of__offset_1() { return &____offset_1; }
	inline void set__offset_1(int32_t value)
	{
		____offset_1 = value;
	}

	inline static int32_t get_offset_of__count_2() { return static_cast<int32_t>(offsetof(ArraySegment_1_t9FDA96F04294A918E0DBFCE7CF750DE23A898976, ____count_2)); }
	inline int32_t get__count_2() const { return ____count_2; }
	inline int32_t* get_address_of__count_2() { return &____count_2; }
	inline void set__count_2(int32_t value)
	{
		____count_2 = value;
	}
};


// System.ArraySegment`1<UnityEngine.Matrix4x4>
struct ArraySegment_1_t7FCE6A956BF91EE65591716518285D293D3C5B6B 
{
public:
	// T[] System.ArraySegment`1::_array
	Matrix4x4U5BU5D_tE53F71E9C9110DD439281A6AB8B699F9F85D8F82* ____array_0;
	// System.Int32 System.ArraySegment`1::_offset
	int32_t ____offset_1;
	// System.Int32 System.ArraySegment`1::_count
	int32_t ____count_2;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(ArraySegment_1_t7FCE6A956BF91EE65591716518285D293D3C5B6B, ____array_0)); }
	inline Matrix4x4U5BU5D_tE53F71E9C9110DD439281A6AB8B699F9F85D8F82* get__array_0() const { return ____array_0; }
	inline Matrix4x4U5BU5D_tE53F71E9C9110DD439281A6AB8B699F9F85D8F82** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(Matrix4x4U5BU5D_tE53F71E9C9110DD439281A6AB8B699F9F85D8F82* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____array_0), (void*)value);
	}

	inline static int32_t get_offset_of__offset_1() { return static_cast<int32_t>(offsetof(ArraySegment_1_t7FCE6A956BF91EE65591716518285D293D3C5B6B, ____offset_1)); }
	inline int32_t get__offset_1() const { return ____offset_1; }
	inline int32_t* get_address_of__offset_1() { return &____offset_1; }
	inline void set__offset_1(int32_t value)
	{
		____offset_1 = value;
	}

	inline static int32_t get_offset_of__count_2() { return static_cast<int32_t>(offsetof(ArraySegment_1_t7FCE6A956BF91EE65591716518285D293D3C5B6B, ____count_2)); }
	inline int32_t get__count_2() const { return ____count_2; }
	inline int32_t* get_address_of__count_2() { return &____count_2; }
	inline void set__count_2(int32_t value)
	{
		____count_2 = value;
	}
};


// System.ArraySegment`1<System.UInt32>
struct ArraySegment_1_tE4B89ED4839E563DF2A25436675AC5C5E87B62B9 
{
public:
	// T[] System.ArraySegment`1::_array
	UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* ____array_0;
	// System.Int32 System.ArraySegment`1::_offset
	int32_t ____offset_1;
	// System.Int32 System.ArraySegment`1::_count
	int32_t ____count_2;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(ArraySegment_1_tE4B89ED4839E563DF2A25436675AC5C5E87B62B9, ____array_0)); }
	inline UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* get__array_0() const { return ____array_0; }
	inline UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____array_0), (void*)value);
	}

	inline static int32_t get_offset_of__offset_1() { return static_cast<int32_t>(offsetof(ArraySegment_1_tE4B89ED4839E563DF2A25436675AC5C5E87B62B9, ____offset_1)); }
	inline int32_t get__offset_1() const { return ____offset_1; }
	inline int32_t* get_address_of__offset_1() { return &____offset_1; }
	inline void set__offset_1(int32_t value)
	{
		____offset_1 = value;
	}

	inline static int32_t get_offset_of__count_2() { return static_cast<int32_t>(offsetof(ArraySegment_1_tE4B89ED4839E563DF2A25436675AC5C5E87B62B9, ____count_2)); }
	inline int32_t get__count_2() const { return ____count_2; }
	inline int32_t* get_address_of__count_2() { return &____count_2; }
	inline void set__count_2(int32_t value)
	{
		____count_2 = value;
	}
};


// System.ArraySegment`1<UniGLTF.UShort4>
struct ArraySegment_1_t9487D5EA4323A4B2C1F7F94C91363EBEE33F3462 
{
public:
	// T[] System.ArraySegment`1::_array
	UShort4U5BU5D_tBDA4C224D509794003F0CCB10B200496F61CEA35* ____array_0;
	// System.Int32 System.ArraySegment`1::_offset
	int32_t ____offset_1;
	// System.Int32 System.ArraySegment`1::_count
	int32_t ____count_2;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(ArraySegment_1_t9487D5EA4323A4B2C1F7F94C91363EBEE33F3462, ____array_0)); }
	inline UShort4U5BU5D_tBDA4C224D509794003F0CCB10B200496F61CEA35* get__array_0() const { return ____array_0; }
	inline UShort4U5BU5D_tBDA4C224D509794003F0CCB10B200496F61CEA35** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(UShort4U5BU5D_tBDA4C224D509794003F0CCB10B200496F61CEA35* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____array_0), (void*)value);
	}

	inline static int32_t get_offset_of__offset_1() { return static_cast<int32_t>(offsetof(ArraySegment_1_t9487D5EA4323A4B2C1F7F94C91363EBEE33F3462, ____offset_1)); }
	inline int32_t get__offset_1() const { return ____offset_1; }
	inline int32_t* get_address_of__offset_1() { return &____offset_1; }
	inline void set__offset_1(int32_t value)
	{
		____offset_1 = value;
	}

	inline static int32_t get_offset_of__count_2() { return static_cast<int32_t>(offsetof(ArraySegment_1_t9487D5EA4323A4B2C1F7F94C91363EBEE33F3462, ____count_2)); }
	inline int32_t get__count_2() const { return ____count_2; }
	inline int32_t* get_address_of__count_2() { return &____count_2; }
	inline void set__count_2(int32_t value)
	{
		____count_2 = value;
	}
};


// System.ArraySegment`1<UnityEngine.Vector2>
struct ArraySegment_1_t73AEFBC798515A51F93CF5816E332A0654AE698D 
{
public:
	// T[] System.ArraySegment`1::_array
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ____array_0;
	// System.Int32 System.ArraySegment`1::_offset
	int32_t ____offset_1;
	// System.Int32 System.ArraySegment`1::_count
	int32_t ____count_2;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(ArraySegment_1_t73AEFBC798515A51F93CF5816E332A0654AE698D, ____array_0)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get__array_0() const { return ____array_0; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____array_0), (void*)value);
	}

	inline static int32_t get_offset_of__offset_1() { return static_cast<int32_t>(offsetof(ArraySegment_1_t73AEFBC798515A51F93CF5816E332A0654AE698D, ____offset_1)); }
	inline int32_t get__offset_1() const { return ____offset_1; }
	inline int32_t* get_address_of__offset_1() { return &____offset_1; }
	inline void set__offset_1(int32_t value)
	{
		____offset_1 = value;
	}

	inline static int32_t get_offset_of__count_2() { return static_cast<int32_t>(offsetof(ArraySegment_1_t73AEFBC798515A51F93CF5816E332A0654AE698D, ____count_2)); }
	inline int32_t get__count_2() const { return ____count_2; }
	inline int32_t* get_address_of__count_2() { return &____count_2; }
	inline void set__count_2(int32_t value)
	{
		____count_2 = value;
	}
};


// System.ArraySegment`1<UnityEngine.Vector3>
struct ArraySegment_1_t12E2F3BC867255EB056CA1F106A8D1E85142A3F1 
{
public:
	// T[] System.ArraySegment`1::_array
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ____array_0;
	// System.Int32 System.ArraySegment`1::_offset
	int32_t ____offset_1;
	// System.Int32 System.ArraySegment`1::_count
	int32_t ____count_2;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(ArraySegment_1_t12E2F3BC867255EB056CA1F106A8D1E85142A3F1, ____array_0)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get__array_0() const { return ____array_0; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____array_0), (void*)value);
	}

	inline static int32_t get_offset_of__offset_1() { return static_cast<int32_t>(offsetof(ArraySegment_1_t12E2F3BC867255EB056CA1F106A8D1E85142A3F1, ____offset_1)); }
	inline int32_t get__offset_1() const { return ____offset_1; }
	inline int32_t* get_address_of__offset_1() { return &____offset_1; }
	inline void set__offset_1(int32_t value)
	{
		____offset_1 = value;
	}

	inline static int32_t get_offset_of__count_2() { return static_cast<int32_t>(offsetof(ArraySegment_1_t12E2F3BC867255EB056CA1F106A8D1E85142A3F1, ____count_2)); }
	inline int32_t get__count_2() const { return ____count_2; }
	inline int32_t* get_address_of__count_2() { return &____count_2; }
	inline void set__count_2(int32_t value)
	{
		____count_2 = value;
	}
};


// System.ArraySegment`1<UnityEngine.Vector4>
struct ArraySegment_1_t3BCE22176B44ACEB25236AD7501F226112F6E07A 
{
public:
	// T[] System.ArraySegment`1::_array
	Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871* ____array_0;
	// System.Int32 System.ArraySegment`1::_offset
	int32_t ____offset_1;
	// System.Int32 System.ArraySegment`1::_count
	int32_t ____count_2;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(ArraySegment_1_t3BCE22176B44ACEB25236AD7501F226112F6E07A, ____array_0)); }
	inline Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871* get__array_0() const { return ____array_0; }
	inline Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____array_0), (void*)value);
	}

	inline static int32_t get_offset_of__offset_1() { return static_cast<int32_t>(offsetof(ArraySegment_1_t3BCE22176B44ACEB25236AD7501F226112F6E07A, ____offset_1)); }
	inline int32_t get__offset_1() const { return ____offset_1; }
	inline int32_t* get_address_of__offset_1() { return &____offset_1; }
	inline void set__offset_1(int32_t value)
	{
		____offset_1 = value;
	}

	inline static int32_t get_offset_of__count_2() { return static_cast<int32_t>(offsetof(ArraySegment_1_t3BCE22176B44ACEB25236AD7501F226112F6E07A, ____count_2)); }
	inline int32_t get__count_2() const { return ____count_2; }
	inline int32_t* get_address_of__count_2() { return &____count_2; }
	inline void set__count_2(int32_t value)
	{
		____count_2 = value;
	}
};


// UnityEngine.InputSystem.Utilities.InlinedArray`1<System.Action`1<System.Object>>
struct InlinedArray_1_tCA120CEA444FAF14E58313503668909AB8450BB2 
{
public:
	// System.Int32 UnityEngine.InputSystem.Utilities.InlinedArray`1::length
	int32_t ___length_0;
	// TValue UnityEngine.InputSystem.Utilities.InlinedArray`1::firstValue
	Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * ___firstValue_1;
	// TValue[] UnityEngine.InputSystem.Utilities.InlinedArray`1::additionalValues
	Action_1U5BU5D_t45AFA72AB9C17482E4A448FF460696B2CD7F5B30* ___additionalValues_2;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(InlinedArray_1_tCA120CEA444FAF14E58313503668909AB8450BB2, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_firstValue_1() { return static_cast<int32_t>(offsetof(InlinedArray_1_tCA120CEA444FAF14E58313503668909AB8450BB2, ___firstValue_1)); }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * get_firstValue_1() const { return ___firstValue_1; }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC ** get_address_of_firstValue_1() { return &___firstValue_1; }
	inline void set_firstValue_1(Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * value)
	{
		___firstValue_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___firstValue_1), (void*)value);
	}

	inline static int32_t get_offset_of_additionalValues_2() { return static_cast<int32_t>(offsetof(InlinedArray_1_tCA120CEA444FAF14E58313503668909AB8450BB2, ___additionalValues_2)); }
	inline Action_1U5BU5D_t45AFA72AB9C17482E4A448FF460696B2CD7F5B30* get_additionalValues_2() const { return ___additionalValues_2; }
	inline Action_1U5BU5D_t45AFA72AB9C17482E4A448FF460696B2CD7F5B30** get_address_of_additionalValues_2() { return &___additionalValues_2; }
	inline void set_additionalValues_2(Action_1U5BU5D_t45AFA72AB9C17482E4A448FF460696B2CD7F5B30* value)
	{
		___additionalValues_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___additionalValues_2), (void*)value);
	}
};


// UnityEngine.InputSystem.Utilities.InlinedArray`1<System.Action`2<System.Object,UnityEngine.InputSystem.InputActionChange>>
struct InlinedArray_1_t6ABF81F82684BB96455201090C17BCA8242E85BD 
{
public:
	// System.Int32 UnityEngine.InputSystem.Utilities.InlinedArray`1::length
	int32_t ___length_0;
	// TValue UnityEngine.InputSystem.Utilities.InlinedArray`1::firstValue
	Action_2_tACB2F8D6B2F70F78DC65A6BC64A3108B0B1AC539 * ___firstValue_1;
	// TValue[] UnityEngine.InputSystem.Utilities.InlinedArray`1::additionalValues
	Action_2U5BU5D_t459BDBFC4C24F188864A38C4CD1D0E76071FEF6D* ___additionalValues_2;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(InlinedArray_1_t6ABF81F82684BB96455201090C17BCA8242E85BD, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_firstValue_1() { return static_cast<int32_t>(offsetof(InlinedArray_1_t6ABF81F82684BB96455201090C17BCA8242E85BD, ___firstValue_1)); }
	inline Action_2_tACB2F8D6B2F70F78DC65A6BC64A3108B0B1AC539 * get_firstValue_1() const { return ___firstValue_1; }
	inline Action_2_tACB2F8D6B2F70F78DC65A6BC64A3108B0B1AC539 ** get_address_of_firstValue_1() { return &___firstValue_1; }
	inline void set_firstValue_1(Action_2_tACB2F8D6B2F70F78DC65A6BC64A3108B0B1AC539 * value)
	{
		___firstValue_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___firstValue_1), (void*)value);
	}

	inline static int32_t get_offset_of_additionalValues_2() { return static_cast<int32_t>(offsetof(InlinedArray_1_t6ABF81F82684BB96455201090C17BCA8242E85BD, ___additionalValues_2)); }
	inline Action_2U5BU5D_t459BDBFC4C24F188864A38C4CD1D0E76071FEF6D* get_additionalValues_2() const { return ___additionalValues_2; }
	inline Action_2U5BU5D_t459BDBFC4C24F188864A38C4CD1D0E76071FEF6D** get_address_of_additionalValues_2() { return &___additionalValues_2; }
	inline void set_additionalValues_2(Action_2U5BU5D_t459BDBFC4C24F188864A38C4CD1D0E76071FEF6D* value)
	{
		___additionalValues_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___additionalValues_2), (void*)value);
	}
};


// System.Nullable`1<System.Boolean>
struct Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3 
{
public:
	// T System.Nullable`1::value
	bool ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3, ___value_0)); }
	inline bool get_value_0() const { return ___value_0; }
	inline bool* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(bool value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};


// UnityEngine.SubsystemsImplementation.SubsystemProvider`1<UnityEngine.XR.ARSubsystems.XRPlaneSubsystem>
struct SubsystemProvider_1_tF2F3B0C041BDD07A00CD49B25AE6016B61F24816  : public SubsystemProvider_t39E89FB8DB1EF1D2B0AF93796AEDB19D76A665F9
{
public:

public:
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// UnityEngine.InputSystem.Utilities.FourCC
struct FourCC_tB18951639903964A6B5533C4BAFD8DA17AA8FD9D 
{
public:
	// System.Int32 UnityEngine.InputSystem.Utilities.FourCC::m_Code
	int32_t ___m_Code_0;

public:
	inline static int32_t get_offset_of_m_Code_0() { return static_cast<int32_t>(offsetof(FourCC_tB18951639903964A6B5533C4BAFD8DA17AA8FD9D, ___m_Code_0)); }
	inline int32_t get_m_Code_0() const { return ___m_Code_0; }
	inline int32_t* get_address_of_m_Code_0() { return &___m_Code_0; }
	inline void set_m_Code_0(int32_t value)
	{
		___m_Code_0 = value;
	}
};


// System.Runtime.InteropServices.GCHandle
struct GCHandle_t757890BC4BBBEDE5A623A3C110013EDD24613603 
{
public:
	// System.Int32 System.Runtime.InteropServices.GCHandle::handle
	int32_t ___handle_0;

public:
	inline static int32_t get_offset_of_handle_0() { return static_cast<int32_t>(offsetof(GCHandle_t757890BC4BBBEDE5A623A3C110013EDD24613603, ___handle_0)); }
	inline int32_t get_handle_0() const { return ___handle_0; }
	inline int32_t* get_address_of_handle_0() { return &___handle_0; }
	inline void set_handle_0(int32_t value)
	{
		___handle_0 = value;
	}
};


// UnityEngine.InputSystem.InputControlScheme
struct InputControlScheme_tEA1C7C6A6509E7AE956DDBBA6C2E288AD437ED9A 
{
public:
	// System.String UnityEngine.InputSystem.InputControlScheme::m_Name
	String_t* ___m_Name_0;
	// System.String UnityEngine.InputSystem.InputControlScheme::m_BindingGroup
	String_t* ___m_BindingGroup_1;
	// UnityEngine.InputSystem.InputControlScheme/DeviceRequirement[] UnityEngine.InputSystem.InputControlScheme::m_DeviceRequirements
	DeviceRequirementU5BU5D_t78146FB21A5C410CBC1F66A8D380D1E95EB2C045* ___m_DeviceRequirements_2;

public:
	inline static int32_t get_offset_of_m_Name_0() { return static_cast<int32_t>(offsetof(InputControlScheme_tEA1C7C6A6509E7AE956DDBBA6C2E288AD437ED9A, ___m_Name_0)); }
	inline String_t* get_m_Name_0() const { return ___m_Name_0; }
	inline String_t** get_address_of_m_Name_0() { return &___m_Name_0; }
	inline void set_m_Name_0(String_t* value)
	{
		___m_Name_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Name_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_BindingGroup_1() { return static_cast<int32_t>(offsetof(InputControlScheme_tEA1C7C6A6509E7AE956DDBBA6C2E288AD437ED9A, ___m_BindingGroup_1)); }
	inline String_t* get_m_BindingGroup_1() const { return ___m_BindingGroup_1; }
	inline String_t** get_address_of_m_BindingGroup_1() { return &___m_BindingGroup_1; }
	inline void set_m_BindingGroup_1(String_t* value)
	{
		___m_BindingGroup_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_BindingGroup_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_DeviceRequirements_2() { return static_cast<int32_t>(offsetof(InputControlScheme_tEA1C7C6A6509E7AE956DDBBA6C2E288AD437ED9A, ___m_DeviceRequirements_2)); }
	inline DeviceRequirementU5BU5D_t78146FB21A5C410CBC1F66A8D380D1E95EB2C045* get_m_DeviceRequirements_2() const { return ___m_DeviceRequirements_2; }
	inline DeviceRequirementU5BU5D_t78146FB21A5C410CBC1F66A8D380D1E95EB2C045** get_address_of_m_DeviceRequirements_2() { return &___m_DeviceRequirements_2; }
	inline void set_m_DeviceRequirements_2(DeviceRequirementU5BU5D_t78146FB21A5C410CBC1F66A8D380D1E95EB2C045* value)
	{
		___m_DeviceRequirements_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DeviceRequirements_2), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.InputControlScheme
struct InputControlScheme_tEA1C7C6A6509E7AE956DDBBA6C2E288AD437ED9A_marshaled_pinvoke
{
	char* ___m_Name_0;
	char* ___m_BindingGroup_1;
	DeviceRequirement_t81B74F0AB5D81F4F19AB7A30E422908B6EF74ED3_marshaled_pinvoke* ___m_DeviceRequirements_2;
};
// Native definition for COM marshalling of UnityEngine.InputSystem.InputControlScheme
struct InputControlScheme_tEA1C7C6A6509E7AE956DDBBA6C2E288AD437ED9A_marshaled_com
{
	Il2CppChar* ___m_Name_0;
	Il2CppChar* ___m_BindingGroup_1;
	DeviceRequirement_t81B74F0AB5D81F4F19AB7A30E422908B6EF74ED3_marshaled_com* ___m_DeviceRequirements_2;
};

// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.InputSystem.Utilities.InternedString
struct InternedString_tD1138602E8B7EA0F5B4851812B13C7E0551E25C8 
{
public:
	// System.String UnityEngine.InputSystem.Utilities.InternedString::m_StringOriginalCase
	String_t* ___m_StringOriginalCase_0;
	// System.String UnityEngine.InputSystem.Utilities.InternedString::m_StringLowerCase
	String_t* ___m_StringLowerCase_1;

public:
	inline static int32_t get_offset_of_m_StringOriginalCase_0() { return static_cast<int32_t>(offsetof(InternedString_tD1138602E8B7EA0F5B4851812B13C7E0551E25C8, ___m_StringOriginalCase_0)); }
	inline String_t* get_m_StringOriginalCase_0() const { return ___m_StringOriginalCase_0; }
	inline String_t** get_address_of_m_StringOriginalCase_0() { return &___m_StringOriginalCase_0; }
	inline void set_m_StringOriginalCase_0(String_t* value)
	{
		___m_StringOriginalCase_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_StringOriginalCase_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_StringLowerCase_1() { return static_cast<int32_t>(offsetof(InternedString_tD1138602E8B7EA0F5B4851812B13C7E0551E25C8, ___m_StringLowerCase_1)); }
	inline String_t* get_m_StringLowerCase_1() const { return ___m_StringLowerCase_1; }
	inline String_t** get_address_of_m_StringLowerCase_1() { return &___m_StringLowerCase_1; }
	inline void set_m_StringLowerCase_1(String_t* value)
	{
		___m_StringLowerCase_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_StringLowerCase_1), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.Utilities.InternedString
struct InternedString_tD1138602E8B7EA0F5B4851812B13C7E0551E25C8_marshaled_pinvoke
{
	char* ___m_StringOriginalCase_0;
	char* ___m_StringLowerCase_1;
};
// Native definition for COM marshalling of UnityEngine.InputSystem.Utilities.InternedString
struct InternedString_tD1138602E8B7EA0F5B4851812B13C7E0551E25C8_marshaled_com
{
	Il2CppChar* ___m_StringOriginalCase_0;
	Il2CppChar* ___m_StringLowerCase_1;
};

// System.Linq.Expressions.LambdaExpression
struct LambdaExpression_t26BF905E97E6D94F81F17D60F4F4F47F8E93B474  : public Expression_t30A004209C10C2D9A9785B2F74EEED431A4D4660
{
public:
	// System.Linq.Expressions.Expression System.Linq.Expressions.LambdaExpression::_body
	Expression_t30A004209C10C2D9A9785B2F74EEED431A4D4660 * ____body_3;

public:
	inline static int32_t get_offset_of__body_3() { return static_cast<int32_t>(offsetof(LambdaExpression_t26BF905E97E6D94F81F17D60F4F4F47F8E93B474, ____body_3)); }
	inline Expression_t30A004209C10C2D9A9785B2F74EEED431A4D4660 * get__body_3() const { return ____body_3; }
	inline Expression_t30A004209C10C2D9A9785B2F74EEED431A4D4660 ** get_address_of__body_3() { return &____body_3; }
	inline void set__body_3(Expression_t30A004209C10C2D9A9785B2F74EEED431A4D4660 * value)
	{
		____body_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____body_3), (void*)value);
	}
};


// UnityEngine.Matrix4x4
struct Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  value)
	{
		___identityMatrix_17 = value;
	}
};


// System.Linq.Expressions.ParameterExpression
struct ParameterExpression_tA7B24F1DE0F013DA4BD55F76DB43B06DB33D8BEE  : public Expression_t30A004209C10C2D9A9785B2F74EEED431A4D4660
{
public:
	// System.String System.Linq.Expressions.ParameterExpression::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ParameterExpression_tA7B24F1DE0F013DA4BD55F76DB43B06DB33D8BEE, ___U3CNameU3Ek__BackingField_3)); }
	inline String_t* get_U3CNameU3Ek__BackingField_3() const { return ___U3CNameU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_3() { return &___U3CNameU3Ek__BackingField_3; }
	inline void set_U3CNameU3Ek__BackingField_3(String_t* value)
	{
		___U3CNameU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CNameU3Ek__BackingField_3), (void*)value);
	}
};


// UnityEngine.Quaternion
struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___identityQuaternion_4 = value;
	}
};


// System.Single
struct Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.InputSystem.Utilities.TypeTable
struct TypeTable_tE58155618A277C8EB10C9B8FB3040A6731E46BFA 
{
public:
	// System.Collections.Generic.Dictionary`2<UnityEngine.InputSystem.Utilities.InternedString,System.Type> UnityEngine.InputSystem.Utilities.TypeTable::table
	Dictionary_2_t9AFCC08689B78B2E9999C1E77045DE7FE2E38939 * ___table_0;

public:
	inline static int32_t get_offset_of_table_0() { return static_cast<int32_t>(offsetof(TypeTable_tE58155618A277C8EB10C9B8FB3040A6731E46BFA, ___table_0)); }
	inline Dictionary_2_t9AFCC08689B78B2E9999C1E77045DE7FE2E38939 * get_table_0() const { return ___table_0; }
	inline Dictionary_2_t9AFCC08689B78B2E9999C1E77045DE7FE2E38939 ** get_address_of_table_0() { return &___table_0; }
	inline void set_table_0(Dictionary_2_t9AFCC08689B78B2E9999C1E77045DE7FE2E38939 * value)
	{
		___table_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___table_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.Utilities.TypeTable
struct TypeTable_tE58155618A277C8EB10C9B8FB3040A6731E46BFA_marshaled_pinvoke
{
	Dictionary_2_t9AFCC08689B78B2E9999C1E77045DE7FE2E38939 * ___table_0;
};
// Native definition for COM marshalling of UnityEngine.InputSystem.Utilities.TypeTable
struct TypeTable_tE58155618A277C8EB10C9B8FB3040A6731E46BFA_marshaled_com
{
	Dictionary_2_t9AFCC08689B78B2E9999C1E77045DE7FE2E38939 * ___table_0;
};

// System.UInt32
struct UInt32_tE60352A06233E4E69DD198BCC67142159F686B15 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_tE60352A06233E4E69DD198BCC67142159F686B15, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};


// UniGLTF.UShort4
#pragma pack(push, tp, 1)
struct UShort4_tB19A4ECD972B8600C9515187BC86B572B83920DB 
{
public:
	// System.UInt16 UniGLTF.UShort4::x
	uint16_t ___x_0;
	// System.UInt16 UniGLTF.UShort4::y
	uint16_t ___y_1;
	// System.UInt16 UniGLTF.UShort4::z
	uint16_t ___z_2;
	// System.UInt16 UniGLTF.UShort4::w
	uint16_t ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(UShort4_tB19A4ECD972B8600C9515187BC86B572B83920DB, ___x_0)); }
	inline uint16_t get_x_0() const { return ___x_0; }
	inline uint16_t* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(uint16_t value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(UShort4_tB19A4ECD972B8600C9515187BC86B572B83920DB, ___y_1)); }
	inline uint16_t get_y_1() const { return ___y_1; }
	inline uint16_t* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(uint16_t value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(UShort4_tB19A4ECD972B8600C9515187BC86B572B83920DB, ___z_2)); }
	inline uint16_t get_z_2() const { return ___z_2; }
	inline uint16_t* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(uint16_t value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(UShort4_tB19A4ECD972B8600C9515187BC86B572B83920DB, ___w_3)); }
	inline uint16_t get_w_3() const { return ___w_3; }
	inline uint16_t* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(uint16_t value)
	{
		___w_3 = value;
	}
};
#pragma pack(pop, tp)


// UnityEngine.Vector2
struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___zeroVector_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___oneVector_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___upVector_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___downVector_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___leftVector_6)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___rightVector_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// UnityEngine.Vector4
struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___zeroVector_5)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___oneVector_6)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___negativeInfinityVector_8 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// UniGLTF.glTF
struct glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17  : public JsonSerializableBase_t40934AAB77C4330D240EB9E990699A5DA3EF5C8F
{
public:
	// UniGLTF.glTFAssets UniGLTF.glTF::asset
	glTFAssets_t52C00CC462A2941625CCAB39CFC40E45E49A4986 * ___asset_0;
	// System.Collections.Generic.List`1<UniGLTF.glTFBuffer> UniGLTF.glTF::buffers
	List_1_t4BFB26128F5A4C3E19B9E2DEAB3A142C3EFF8F38 * ___buffers_1;
	// System.Collections.Generic.List`1<UniGLTF.glTFBufferView> UniGLTF.glTF::bufferViews
	List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 * ___bufferViews_2;
	// System.Collections.Generic.List`1<UniGLTF.glTFAccessor> UniGLTF.glTF::accessors
	List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC * ___accessors_3;
	// System.Collections.Generic.List`1<UniGLTF.glTFTexture> UniGLTF.glTF::textures
	List_1_t93D2AEDD3CACD14F2165FD425D3B53F7DC20C9C0 * ___textures_4;
	// System.Collections.Generic.List`1<UniGLTF.glTFTextureSampler> UniGLTF.glTF::samplers
	List_1_tD62CE7D871066E026CC03730FED6BDCE71EFCC90 * ___samplers_5;
	// System.Collections.Generic.List`1<UniGLTF.glTFImage> UniGLTF.glTF::images
	List_1_tD99B99E72B26A00EB46A7FD4B4CF8A7737F04CDD * ___images_6;
	// System.Collections.Generic.List`1<UniGLTF.glTFMaterial> UniGLTF.glTF::materials
	List_1_tBF560A6A99F5B82BE164503467D8684D672FC119 * ___materials_7;
	// System.Collections.Generic.List`1<UniGLTF.glTFMesh> UniGLTF.glTF::meshes
	List_1_t934B70DFA7B8E9278E8936524113CCE5A37F007C * ___meshes_8;
	// System.Collections.Generic.List`1<UniGLTF.glTFNode> UniGLTF.glTF::nodes
	List_1_tE044764DB1DBE4A4B6C5DD39C3237D73AD872853 * ___nodes_9;
	// System.Collections.Generic.List`1<UniGLTF.glTFSkin> UniGLTF.glTF::skins
	List_1_t24D9800C2C25EC55A7776132DF3A1D018CD28065 * ___skins_10;
	// System.Int32 UniGLTF.glTF::scene
	int32_t ___scene_11;
	// System.Collections.Generic.List`1<UniGLTF.gltfScene> UniGLTF.glTF::scenes
	List_1_tA6B2AA43D377CA57C52B77094F68B8394D39B3E7 * ___scenes_12;
	// System.Collections.Generic.List`1<UniGLTF.glTFAnimation> UniGLTF.glTF::animations
	List_1_t0ACBD66ACBCB928B2502E851848F2397C58315E5 * ___animations_13;
	// System.Collections.Generic.List`1<UniGLTF.glTFCamera> UniGLTF.glTF::cameras
	List_1_tB0D97651B850CC1988E2E5E04908E28BF5CEF39C * ___cameras_14;
	// System.Collections.Generic.List`1<System.String> UniGLTF.glTF::extensionsUsed
	List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ___extensionsUsed_15;
	// System.Collections.Generic.List`1<System.String> UniGLTF.glTF::extensionsRequired
	List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ___extensionsRequired_16;
	// UniGLTF.glTF_extensions UniGLTF.glTF::extensions
	glTF_extensions_t1CAC5D30CEDEEFCC3C3DDEBF83806FB88490B985 * ___extensions_17;
	// UniGLTF.gltf_extras UniGLTF.glTF::extras
	gltf_extras_tC3468176DD822D7D326DB819BA4F115B129E6C94 * ___extras_18;

public:
	inline static int32_t get_offset_of_asset_0() { return static_cast<int32_t>(offsetof(glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17, ___asset_0)); }
	inline glTFAssets_t52C00CC462A2941625CCAB39CFC40E45E49A4986 * get_asset_0() const { return ___asset_0; }
	inline glTFAssets_t52C00CC462A2941625CCAB39CFC40E45E49A4986 ** get_address_of_asset_0() { return &___asset_0; }
	inline void set_asset_0(glTFAssets_t52C00CC462A2941625CCAB39CFC40E45E49A4986 * value)
	{
		___asset_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___asset_0), (void*)value);
	}

	inline static int32_t get_offset_of_buffers_1() { return static_cast<int32_t>(offsetof(glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17, ___buffers_1)); }
	inline List_1_t4BFB26128F5A4C3E19B9E2DEAB3A142C3EFF8F38 * get_buffers_1() const { return ___buffers_1; }
	inline List_1_t4BFB26128F5A4C3E19B9E2DEAB3A142C3EFF8F38 ** get_address_of_buffers_1() { return &___buffers_1; }
	inline void set_buffers_1(List_1_t4BFB26128F5A4C3E19B9E2DEAB3A142C3EFF8F38 * value)
	{
		___buffers_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buffers_1), (void*)value);
	}

	inline static int32_t get_offset_of_bufferViews_2() { return static_cast<int32_t>(offsetof(glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17, ___bufferViews_2)); }
	inline List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 * get_bufferViews_2() const { return ___bufferViews_2; }
	inline List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 ** get_address_of_bufferViews_2() { return &___bufferViews_2; }
	inline void set_bufferViews_2(List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 * value)
	{
		___bufferViews_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bufferViews_2), (void*)value);
	}

	inline static int32_t get_offset_of_accessors_3() { return static_cast<int32_t>(offsetof(glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17, ___accessors_3)); }
	inline List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC * get_accessors_3() const { return ___accessors_3; }
	inline List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC ** get_address_of_accessors_3() { return &___accessors_3; }
	inline void set_accessors_3(List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC * value)
	{
		___accessors_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___accessors_3), (void*)value);
	}

	inline static int32_t get_offset_of_textures_4() { return static_cast<int32_t>(offsetof(glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17, ___textures_4)); }
	inline List_1_t93D2AEDD3CACD14F2165FD425D3B53F7DC20C9C0 * get_textures_4() const { return ___textures_4; }
	inline List_1_t93D2AEDD3CACD14F2165FD425D3B53F7DC20C9C0 ** get_address_of_textures_4() { return &___textures_4; }
	inline void set_textures_4(List_1_t93D2AEDD3CACD14F2165FD425D3B53F7DC20C9C0 * value)
	{
		___textures_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___textures_4), (void*)value);
	}

	inline static int32_t get_offset_of_samplers_5() { return static_cast<int32_t>(offsetof(glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17, ___samplers_5)); }
	inline List_1_tD62CE7D871066E026CC03730FED6BDCE71EFCC90 * get_samplers_5() const { return ___samplers_5; }
	inline List_1_tD62CE7D871066E026CC03730FED6BDCE71EFCC90 ** get_address_of_samplers_5() { return &___samplers_5; }
	inline void set_samplers_5(List_1_tD62CE7D871066E026CC03730FED6BDCE71EFCC90 * value)
	{
		___samplers_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___samplers_5), (void*)value);
	}

	inline static int32_t get_offset_of_images_6() { return static_cast<int32_t>(offsetof(glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17, ___images_6)); }
	inline List_1_tD99B99E72B26A00EB46A7FD4B4CF8A7737F04CDD * get_images_6() const { return ___images_6; }
	inline List_1_tD99B99E72B26A00EB46A7FD4B4CF8A7737F04CDD ** get_address_of_images_6() { return &___images_6; }
	inline void set_images_6(List_1_tD99B99E72B26A00EB46A7FD4B4CF8A7737F04CDD * value)
	{
		___images_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___images_6), (void*)value);
	}

	inline static int32_t get_offset_of_materials_7() { return static_cast<int32_t>(offsetof(glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17, ___materials_7)); }
	inline List_1_tBF560A6A99F5B82BE164503467D8684D672FC119 * get_materials_7() const { return ___materials_7; }
	inline List_1_tBF560A6A99F5B82BE164503467D8684D672FC119 ** get_address_of_materials_7() { return &___materials_7; }
	inline void set_materials_7(List_1_tBF560A6A99F5B82BE164503467D8684D672FC119 * value)
	{
		___materials_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___materials_7), (void*)value);
	}

	inline static int32_t get_offset_of_meshes_8() { return static_cast<int32_t>(offsetof(glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17, ___meshes_8)); }
	inline List_1_t934B70DFA7B8E9278E8936524113CCE5A37F007C * get_meshes_8() const { return ___meshes_8; }
	inline List_1_t934B70DFA7B8E9278E8936524113CCE5A37F007C ** get_address_of_meshes_8() { return &___meshes_8; }
	inline void set_meshes_8(List_1_t934B70DFA7B8E9278E8936524113CCE5A37F007C * value)
	{
		___meshes_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___meshes_8), (void*)value);
	}

	inline static int32_t get_offset_of_nodes_9() { return static_cast<int32_t>(offsetof(glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17, ___nodes_9)); }
	inline List_1_tE044764DB1DBE4A4B6C5DD39C3237D73AD872853 * get_nodes_9() const { return ___nodes_9; }
	inline List_1_tE044764DB1DBE4A4B6C5DD39C3237D73AD872853 ** get_address_of_nodes_9() { return &___nodes_9; }
	inline void set_nodes_9(List_1_tE044764DB1DBE4A4B6C5DD39C3237D73AD872853 * value)
	{
		___nodes_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___nodes_9), (void*)value);
	}

	inline static int32_t get_offset_of_skins_10() { return static_cast<int32_t>(offsetof(glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17, ___skins_10)); }
	inline List_1_t24D9800C2C25EC55A7776132DF3A1D018CD28065 * get_skins_10() const { return ___skins_10; }
	inline List_1_t24D9800C2C25EC55A7776132DF3A1D018CD28065 ** get_address_of_skins_10() { return &___skins_10; }
	inline void set_skins_10(List_1_t24D9800C2C25EC55A7776132DF3A1D018CD28065 * value)
	{
		___skins_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___skins_10), (void*)value);
	}

	inline static int32_t get_offset_of_scene_11() { return static_cast<int32_t>(offsetof(glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17, ___scene_11)); }
	inline int32_t get_scene_11() const { return ___scene_11; }
	inline int32_t* get_address_of_scene_11() { return &___scene_11; }
	inline void set_scene_11(int32_t value)
	{
		___scene_11 = value;
	}

	inline static int32_t get_offset_of_scenes_12() { return static_cast<int32_t>(offsetof(glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17, ___scenes_12)); }
	inline List_1_tA6B2AA43D377CA57C52B77094F68B8394D39B3E7 * get_scenes_12() const { return ___scenes_12; }
	inline List_1_tA6B2AA43D377CA57C52B77094F68B8394D39B3E7 ** get_address_of_scenes_12() { return &___scenes_12; }
	inline void set_scenes_12(List_1_tA6B2AA43D377CA57C52B77094F68B8394D39B3E7 * value)
	{
		___scenes_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___scenes_12), (void*)value);
	}

	inline static int32_t get_offset_of_animations_13() { return static_cast<int32_t>(offsetof(glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17, ___animations_13)); }
	inline List_1_t0ACBD66ACBCB928B2502E851848F2397C58315E5 * get_animations_13() const { return ___animations_13; }
	inline List_1_t0ACBD66ACBCB928B2502E851848F2397C58315E5 ** get_address_of_animations_13() { return &___animations_13; }
	inline void set_animations_13(List_1_t0ACBD66ACBCB928B2502E851848F2397C58315E5 * value)
	{
		___animations_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___animations_13), (void*)value);
	}

	inline static int32_t get_offset_of_cameras_14() { return static_cast<int32_t>(offsetof(glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17, ___cameras_14)); }
	inline List_1_tB0D97651B850CC1988E2E5E04908E28BF5CEF39C * get_cameras_14() const { return ___cameras_14; }
	inline List_1_tB0D97651B850CC1988E2E5E04908E28BF5CEF39C ** get_address_of_cameras_14() { return &___cameras_14; }
	inline void set_cameras_14(List_1_tB0D97651B850CC1988E2E5E04908E28BF5CEF39C * value)
	{
		___cameras_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cameras_14), (void*)value);
	}

	inline static int32_t get_offset_of_extensionsUsed_15() { return static_cast<int32_t>(offsetof(glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17, ___extensionsUsed_15)); }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * get_extensionsUsed_15() const { return ___extensionsUsed_15; }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 ** get_address_of_extensionsUsed_15() { return &___extensionsUsed_15; }
	inline void set_extensionsUsed_15(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * value)
	{
		___extensionsUsed_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___extensionsUsed_15), (void*)value);
	}

	inline static int32_t get_offset_of_extensionsRequired_16() { return static_cast<int32_t>(offsetof(glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17, ___extensionsRequired_16)); }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * get_extensionsRequired_16() const { return ___extensionsRequired_16; }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 ** get_address_of_extensionsRequired_16() { return &___extensionsRequired_16; }
	inline void set_extensionsRequired_16(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * value)
	{
		___extensionsRequired_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___extensionsRequired_16), (void*)value);
	}

	inline static int32_t get_offset_of_extensions_17() { return static_cast<int32_t>(offsetof(glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17, ___extensions_17)); }
	inline glTF_extensions_t1CAC5D30CEDEEFCC3C3DDEBF83806FB88490B985 * get_extensions_17() const { return ___extensions_17; }
	inline glTF_extensions_t1CAC5D30CEDEEFCC3C3DDEBF83806FB88490B985 ** get_address_of_extensions_17() { return &___extensions_17; }
	inline void set_extensions_17(glTF_extensions_t1CAC5D30CEDEEFCC3C3DDEBF83806FB88490B985 * value)
	{
		___extensions_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___extensions_17), (void*)value);
	}

	inline static int32_t get_offset_of_extras_18() { return static_cast<int32_t>(offsetof(glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17, ___extras_18)); }
	inline gltf_extras_tC3468176DD822D7D326DB819BA4F115B129E6C94 * get_extras_18() const { return ___extras_18; }
	inline gltf_extras_tC3468176DD822D7D326DB819BA4F115B129E6C94 ** get_address_of_extras_18() { return &___extras_18; }
	inline void set_extras_18(gltf_extras_tC3468176DD822D7D326DB819BA4F115B129E6C94 * value)
	{
		___extras_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___extras_18), (void*)value);
	}
};


// UniGLTF.glTFBuffer
struct glTFBuffer_t840FC6FB6032C82B50388CE51BB1F054475E4BD6  : public JsonSerializableBase_t40934AAB77C4330D240EB9E990699A5DA3EF5C8F
{
public:
	// UniGLTF.IBytesBuffer UniGLTF.glTFBuffer::Storage
	RuntimeObject* ___Storage_0;
	// System.String UniGLTF.glTFBuffer::uri
	String_t* ___uri_1;
	// System.Int32 UniGLTF.glTFBuffer::byteLength
	int32_t ___byteLength_2;
	// System.Object UniGLTF.glTFBuffer::extensions
	RuntimeObject * ___extensions_3;
	// System.Object UniGLTF.glTFBuffer::extras
	RuntimeObject * ___extras_4;
	// System.String UniGLTF.glTFBuffer::name
	String_t* ___name_5;

public:
	inline static int32_t get_offset_of_Storage_0() { return static_cast<int32_t>(offsetof(glTFBuffer_t840FC6FB6032C82B50388CE51BB1F054475E4BD6, ___Storage_0)); }
	inline RuntimeObject* get_Storage_0() const { return ___Storage_0; }
	inline RuntimeObject** get_address_of_Storage_0() { return &___Storage_0; }
	inline void set_Storage_0(RuntimeObject* value)
	{
		___Storage_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Storage_0), (void*)value);
	}

	inline static int32_t get_offset_of_uri_1() { return static_cast<int32_t>(offsetof(glTFBuffer_t840FC6FB6032C82B50388CE51BB1F054475E4BD6, ___uri_1)); }
	inline String_t* get_uri_1() const { return ___uri_1; }
	inline String_t** get_address_of_uri_1() { return &___uri_1; }
	inline void set_uri_1(String_t* value)
	{
		___uri_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___uri_1), (void*)value);
	}

	inline static int32_t get_offset_of_byteLength_2() { return static_cast<int32_t>(offsetof(glTFBuffer_t840FC6FB6032C82B50388CE51BB1F054475E4BD6, ___byteLength_2)); }
	inline int32_t get_byteLength_2() const { return ___byteLength_2; }
	inline int32_t* get_address_of_byteLength_2() { return &___byteLength_2; }
	inline void set_byteLength_2(int32_t value)
	{
		___byteLength_2 = value;
	}

	inline static int32_t get_offset_of_extensions_3() { return static_cast<int32_t>(offsetof(glTFBuffer_t840FC6FB6032C82B50388CE51BB1F054475E4BD6, ___extensions_3)); }
	inline RuntimeObject * get_extensions_3() const { return ___extensions_3; }
	inline RuntimeObject ** get_address_of_extensions_3() { return &___extensions_3; }
	inline void set_extensions_3(RuntimeObject * value)
	{
		___extensions_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___extensions_3), (void*)value);
	}

	inline static int32_t get_offset_of_extras_4() { return static_cast<int32_t>(offsetof(glTFBuffer_t840FC6FB6032C82B50388CE51BB1F054475E4BD6, ___extras_4)); }
	inline RuntimeObject * get_extras_4() const { return ___extras_4; }
	inline RuntimeObject ** get_address_of_extras_4() { return &___extras_4; }
	inline void set_extras_4(RuntimeObject * value)
	{
		___extras_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___extras_4), (void*)value);
	}

	inline static int32_t get_offset_of_name_5() { return static_cast<int32_t>(offsetof(glTFBuffer_t840FC6FB6032C82B50388CE51BB1F054475E4BD6, ___name_5)); }
	inline String_t* get_name_5() const { return ___name_5; }
	inline String_t** get_address_of_name_5() { return &___name_5; }
	inline void set_name_5(String_t* value)
	{
		___name_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_5), (void*)value);
	}
};


// UniGLTF.glTFSparse
struct glTFSparse_tF8621187A1982375A902F6D7B023E40B72886684  : public JsonSerializableBase_t40934AAB77C4330D240EB9E990699A5DA3EF5C8F
{
public:
	// System.Int32 UniGLTF.glTFSparse::count
	int32_t ___count_0;
	// UniGLTF.glTFSparseIndices UniGLTF.glTFSparse::indices
	glTFSparseIndices_t332206D5F85C3918BDB6880C545D1B59EF3DE614 * ___indices_1;
	// UniGLTF.glTFSparseValues UniGLTF.glTFSparse::values
	glTFSparseValues_t482F196AE1519B26241A4BB28B338F2977BC734F * ___values_2;
	// System.Object UniGLTF.glTFSparse::extensions
	RuntimeObject * ___extensions_3;
	// System.Object UniGLTF.glTFSparse::extras
	RuntimeObject * ___extras_4;

public:
	inline static int32_t get_offset_of_count_0() { return static_cast<int32_t>(offsetof(glTFSparse_tF8621187A1982375A902F6D7B023E40B72886684, ___count_0)); }
	inline int32_t get_count_0() const { return ___count_0; }
	inline int32_t* get_address_of_count_0() { return &___count_0; }
	inline void set_count_0(int32_t value)
	{
		___count_0 = value;
	}

	inline static int32_t get_offset_of_indices_1() { return static_cast<int32_t>(offsetof(glTFSparse_tF8621187A1982375A902F6D7B023E40B72886684, ___indices_1)); }
	inline glTFSparseIndices_t332206D5F85C3918BDB6880C545D1B59EF3DE614 * get_indices_1() const { return ___indices_1; }
	inline glTFSparseIndices_t332206D5F85C3918BDB6880C545D1B59EF3DE614 ** get_address_of_indices_1() { return &___indices_1; }
	inline void set_indices_1(glTFSparseIndices_t332206D5F85C3918BDB6880C545D1B59EF3DE614 * value)
	{
		___indices_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___indices_1), (void*)value);
	}

	inline static int32_t get_offset_of_values_2() { return static_cast<int32_t>(offsetof(glTFSparse_tF8621187A1982375A902F6D7B023E40B72886684, ___values_2)); }
	inline glTFSparseValues_t482F196AE1519B26241A4BB28B338F2977BC734F * get_values_2() const { return ___values_2; }
	inline glTFSparseValues_t482F196AE1519B26241A4BB28B338F2977BC734F ** get_address_of_values_2() { return &___values_2; }
	inline void set_values_2(glTFSparseValues_t482F196AE1519B26241A4BB28B338F2977BC734F * value)
	{
		___values_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___values_2), (void*)value);
	}

	inline static int32_t get_offset_of_extensions_3() { return static_cast<int32_t>(offsetof(glTFSparse_tF8621187A1982375A902F6D7B023E40B72886684, ___extensions_3)); }
	inline RuntimeObject * get_extensions_3() const { return ___extensions_3; }
	inline RuntimeObject ** get_address_of_extensions_3() { return &___extensions_3; }
	inline void set_extensions_3(RuntimeObject * value)
	{
		___extensions_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___extensions_3), (void*)value);
	}

	inline static int32_t get_offset_of_extras_4() { return static_cast<int32_t>(offsetof(glTFSparse_tF8621187A1982375A902F6D7B023E40B72886684, ___extras_4)); }
	inline RuntimeObject * get_extras_4() const { return ___extras_4; }
	inline RuntimeObject ** get_address_of_extras_4() { return &___extras_4; }
	inline void set_extras_4(RuntimeObject * value)
	{
		___extras_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___extras_4), (void*)value);
	}
};


// UniGLTF.glTFSparseValues
struct glTFSparseValues_t482F196AE1519B26241A4BB28B338F2977BC734F  : public JsonSerializableBase_t40934AAB77C4330D240EB9E990699A5DA3EF5C8F
{
public:
	// System.Int32 UniGLTF.glTFSparseValues::bufferView
	int32_t ___bufferView_0;
	// System.Int32 UniGLTF.glTFSparseValues::byteOffset
	int32_t ___byteOffset_1;
	// System.Object UniGLTF.glTFSparseValues::extensions
	RuntimeObject * ___extensions_2;
	// System.Object UniGLTF.glTFSparseValues::extras
	RuntimeObject * ___extras_3;

public:
	inline static int32_t get_offset_of_bufferView_0() { return static_cast<int32_t>(offsetof(glTFSparseValues_t482F196AE1519B26241A4BB28B338F2977BC734F, ___bufferView_0)); }
	inline int32_t get_bufferView_0() const { return ___bufferView_0; }
	inline int32_t* get_address_of_bufferView_0() { return &___bufferView_0; }
	inline void set_bufferView_0(int32_t value)
	{
		___bufferView_0 = value;
	}

	inline static int32_t get_offset_of_byteOffset_1() { return static_cast<int32_t>(offsetof(glTFSparseValues_t482F196AE1519B26241A4BB28B338F2977BC734F, ___byteOffset_1)); }
	inline int32_t get_byteOffset_1() const { return ___byteOffset_1; }
	inline int32_t* get_address_of_byteOffset_1() { return &___byteOffset_1; }
	inline void set_byteOffset_1(int32_t value)
	{
		___byteOffset_1 = value;
	}

	inline static int32_t get_offset_of_extensions_2() { return static_cast<int32_t>(offsetof(glTFSparseValues_t482F196AE1519B26241A4BB28B338F2977BC734F, ___extensions_2)); }
	inline RuntimeObject * get_extensions_2() const { return ___extensions_2; }
	inline RuntimeObject ** get_address_of_extensions_2() { return &___extensions_2; }
	inline void set_extensions_2(RuntimeObject * value)
	{
		___extensions_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___extensions_2), (void*)value);
	}

	inline static int32_t get_offset_of_extras_3() { return static_cast<int32_t>(offsetof(glTFSparseValues_t482F196AE1519B26241A4BB28B338F2977BC734F, ___extras_3)); }
	inline RuntimeObject * get_extras_3() const { return ___extras_3; }
	inline RuntimeObject ** get_address_of_extras_3() { return &___extras_3; }
	inline void set_extras_3(RuntimeObject * value)
	{
		___extras_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___extras_3), (void*)value);
	}
};


// UnityEngine.InputSystem.InputAction/CallbackContext
struct CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B 
{
public:
	// UnityEngine.InputSystem.InputActionState UnityEngine.InputSystem.InputAction/CallbackContext::m_State
	InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C * ___m_State_0;
	// System.Int32 UnityEngine.InputSystem.InputAction/CallbackContext::m_ActionIndex
	int32_t ___m_ActionIndex_1;

public:
	inline static int32_t get_offset_of_m_State_0() { return static_cast<int32_t>(offsetof(CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B, ___m_State_0)); }
	inline InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C * get_m_State_0() const { return ___m_State_0; }
	inline InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C ** get_address_of_m_State_0() { return &___m_State_0; }
	inline void set_m_State_0(InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C * value)
	{
		___m_State_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_State_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_ActionIndex_1() { return static_cast<int32_t>(offsetof(CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B, ___m_ActionIndex_1)); }
	inline int32_t get_m_ActionIndex_1() const { return ___m_ActionIndex_1; }
	inline int32_t* get_address_of_m_ActionIndex_1() { return &___m_ActionIndex_1; }
	inline void set_m_ActionIndex_1(int32_t value)
	{
		___m_ActionIndex_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.InputAction/CallbackContext
struct CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B_marshaled_pinvoke
{
	InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C * ___m_State_0;
	int32_t ___m_ActionIndex_1;
};
// Native definition for COM marshalling of UnityEngine.InputSystem.InputAction/CallbackContext
struct CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B_marshaled_com
{
	InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C * ___m_State_0;
	int32_t ___m_ActionIndex_1;
};

// UnityEngine.InputSystem.InputActionSetupExtensions/BindingSyntax
struct BindingSyntax_tEF6F07812AAB1AF06B3D36C9B1570C01997C8BF1 
{
public:
	// UnityEngine.InputSystem.InputActionMap UnityEngine.InputSystem.InputActionSetupExtensions/BindingSyntax::m_ActionMap
	InputActionMap_tBA45B784C5957091166E0007413402A3E83DD226 * ___m_ActionMap_0;
	// UnityEngine.InputSystem.InputAction UnityEngine.InputSystem.InputActionSetupExtensions/BindingSyntax::m_Action
	InputAction_tF0C77AF72E0C19608B020E77A2C36C04E9DCBE48 * ___m_Action_1;
	// System.Int32 UnityEngine.InputSystem.InputActionSetupExtensions/BindingSyntax::m_BindingIndex
	int32_t ___m_BindingIndex_2;

public:
	inline static int32_t get_offset_of_m_ActionMap_0() { return static_cast<int32_t>(offsetof(BindingSyntax_tEF6F07812AAB1AF06B3D36C9B1570C01997C8BF1, ___m_ActionMap_0)); }
	inline InputActionMap_tBA45B784C5957091166E0007413402A3E83DD226 * get_m_ActionMap_0() const { return ___m_ActionMap_0; }
	inline InputActionMap_tBA45B784C5957091166E0007413402A3E83DD226 ** get_address_of_m_ActionMap_0() { return &___m_ActionMap_0; }
	inline void set_m_ActionMap_0(InputActionMap_tBA45B784C5957091166E0007413402A3E83DD226 * value)
	{
		___m_ActionMap_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ActionMap_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Action_1() { return static_cast<int32_t>(offsetof(BindingSyntax_tEF6F07812AAB1AF06B3D36C9B1570C01997C8BF1, ___m_Action_1)); }
	inline InputAction_tF0C77AF72E0C19608B020E77A2C36C04E9DCBE48 * get_m_Action_1() const { return ___m_Action_1; }
	inline InputAction_tF0C77AF72E0C19608B020E77A2C36C04E9DCBE48 ** get_address_of_m_Action_1() { return &___m_Action_1; }
	inline void set_m_Action_1(InputAction_tF0C77AF72E0C19608B020E77A2C36C04E9DCBE48 * value)
	{
		___m_Action_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Action_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_BindingIndex_2() { return static_cast<int32_t>(offsetof(BindingSyntax_tEF6F07812AAB1AF06B3D36C9B1570C01997C8BF1, ___m_BindingIndex_2)); }
	inline int32_t get_m_BindingIndex_2() const { return ___m_BindingIndex_2; }
	inline int32_t* get_address_of_m_BindingIndex_2() { return &___m_BindingIndex_2; }
	inline void set_m_BindingIndex_2(int32_t value)
	{
		___m_BindingIndex_2 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.InputActionSetupExtensions/BindingSyntax
struct BindingSyntax_tEF6F07812AAB1AF06B3D36C9B1570C01997C8BF1_marshaled_pinvoke
{
	InputActionMap_tBA45B784C5957091166E0007413402A3E83DD226 * ___m_ActionMap_0;
	InputAction_tF0C77AF72E0C19608B020E77A2C36C04E9DCBE48 * ___m_Action_1;
	int32_t ___m_BindingIndex_2;
};
// Native definition for COM marshalling of UnityEngine.InputSystem.InputActionSetupExtensions/BindingSyntax
struct BindingSyntax_tEF6F07812AAB1AF06B3D36C9B1570C01997C8BF1_marshaled_com
{
	InputActionMap_tBA45B784C5957091166E0007413402A3E83DD226 * ___m_ActionMap_0;
	InputAction_tF0C77AF72E0C19608B020E77A2C36C04E9DCBE48 * ___m_Action_1;
	int32_t ___m_BindingIndex_2;
};

// UnityEngine.InputSystem.InputActionState/UnmanagedMemory
struct UnmanagedMemory_t5E2A7F3AF355F7A20E8D8B3D7BE388511DD17C16 
{
public:
	// System.Void* UnityEngine.InputSystem.InputActionState/UnmanagedMemory::basePtr
	void* ___basePtr_0;
	// System.Int32 UnityEngine.InputSystem.InputActionState/UnmanagedMemory::mapCount
	int32_t ___mapCount_1;
	// System.Int32 UnityEngine.InputSystem.InputActionState/UnmanagedMemory::actionCount
	int32_t ___actionCount_2;
	// System.Int32 UnityEngine.InputSystem.InputActionState/UnmanagedMemory::interactionCount
	int32_t ___interactionCount_3;
	// System.Int32 UnityEngine.InputSystem.InputActionState/UnmanagedMemory::bindingCount
	int32_t ___bindingCount_4;
	// System.Int32 UnityEngine.InputSystem.InputActionState/UnmanagedMemory::controlCount
	int32_t ___controlCount_5;
	// System.Int32 UnityEngine.InputSystem.InputActionState/UnmanagedMemory::compositeCount
	int32_t ___compositeCount_6;
	// UnityEngine.InputSystem.InputActionState/TriggerState* UnityEngine.InputSystem.InputActionState/UnmanagedMemory::actionStates
	TriggerState_tD1D00F0DE77AE9CA21DE7A2CEDA8D93EED009CBE * ___actionStates_7;
	// UnityEngine.InputSystem.InputActionState/BindingState* UnityEngine.InputSystem.InputActionState/UnmanagedMemory::bindingStates
	BindingState_tE715A30188652BA114CEB7CD0E12A25CFCEF8FC7 * ___bindingStates_8;
	// UnityEngine.InputSystem.InputActionState/InteractionState* UnityEngine.InputSystem.InputActionState/UnmanagedMemory::interactionStates
	InteractionState_tC8A885CD98436B1609B0988EFDCCD6BBBCFDD6B7 * ___interactionStates_9;
	// System.Single* UnityEngine.InputSystem.InputActionState/UnmanagedMemory::controlMagnitudes
	float* ___controlMagnitudes_10;
	// System.Single* UnityEngine.InputSystem.InputActionState/UnmanagedMemory::compositeMagnitudes
	float* ___compositeMagnitudes_11;
	// System.Int32* UnityEngine.InputSystem.InputActionState/UnmanagedMemory::enabledControls
	int32_t* ___enabledControls_12;
	// System.UInt16* UnityEngine.InputSystem.InputActionState/UnmanagedMemory::actionBindingIndicesAndCounts
	uint16_t* ___actionBindingIndicesAndCounts_13;
	// System.UInt16* UnityEngine.InputSystem.InputActionState/UnmanagedMemory::actionBindingIndices
	uint16_t* ___actionBindingIndices_14;
	// System.Int32* UnityEngine.InputSystem.InputActionState/UnmanagedMemory::controlIndexToBindingIndex
	int32_t* ___controlIndexToBindingIndex_15;
	// UnityEngine.InputSystem.InputActionState/ActionMapIndices* UnityEngine.InputSystem.InputActionState/UnmanagedMemory::mapIndices
	ActionMapIndices_t70234E0132647DCE386300AED2C964F629609245 * ___mapIndices_16;

public:
	inline static int32_t get_offset_of_basePtr_0() { return static_cast<int32_t>(offsetof(UnmanagedMemory_t5E2A7F3AF355F7A20E8D8B3D7BE388511DD17C16, ___basePtr_0)); }
	inline void* get_basePtr_0() const { return ___basePtr_0; }
	inline void** get_address_of_basePtr_0() { return &___basePtr_0; }
	inline void set_basePtr_0(void* value)
	{
		___basePtr_0 = value;
	}

	inline static int32_t get_offset_of_mapCount_1() { return static_cast<int32_t>(offsetof(UnmanagedMemory_t5E2A7F3AF355F7A20E8D8B3D7BE388511DD17C16, ___mapCount_1)); }
	inline int32_t get_mapCount_1() const { return ___mapCount_1; }
	inline int32_t* get_address_of_mapCount_1() { return &___mapCount_1; }
	inline void set_mapCount_1(int32_t value)
	{
		___mapCount_1 = value;
	}

	inline static int32_t get_offset_of_actionCount_2() { return static_cast<int32_t>(offsetof(UnmanagedMemory_t5E2A7F3AF355F7A20E8D8B3D7BE388511DD17C16, ___actionCount_2)); }
	inline int32_t get_actionCount_2() const { return ___actionCount_2; }
	inline int32_t* get_address_of_actionCount_2() { return &___actionCount_2; }
	inline void set_actionCount_2(int32_t value)
	{
		___actionCount_2 = value;
	}

	inline static int32_t get_offset_of_interactionCount_3() { return static_cast<int32_t>(offsetof(UnmanagedMemory_t5E2A7F3AF355F7A20E8D8B3D7BE388511DD17C16, ___interactionCount_3)); }
	inline int32_t get_interactionCount_3() const { return ___interactionCount_3; }
	inline int32_t* get_address_of_interactionCount_3() { return &___interactionCount_3; }
	inline void set_interactionCount_3(int32_t value)
	{
		___interactionCount_3 = value;
	}

	inline static int32_t get_offset_of_bindingCount_4() { return static_cast<int32_t>(offsetof(UnmanagedMemory_t5E2A7F3AF355F7A20E8D8B3D7BE388511DD17C16, ___bindingCount_4)); }
	inline int32_t get_bindingCount_4() const { return ___bindingCount_4; }
	inline int32_t* get_address_of_bindingCount_4() { return &___bindingCount_4; }
	inline void set_bindingCount_4(int32_t value)
	{
		___bindingCount_4 = value;
	}

	inline static int32_t get_offset_of_controlCount_5() { return static_cast<int32_t>(offsetof(UnmanagedMemory_t5E2A7F3AF355F7A20E8D8B3D7BE388511DD17C16, ___controlCount_5)); }
	inline int32_t get_controlCount_5() const { return ___controlCount_5; }
	inline int32_t* get_address_of_controlCount_5() { return &___controlCount_5; }
	inline void set_controlCount_5(int32_t value)
	{
		___controlCount_5 = value;
	}

	inline static int32_t get_offset_of_compositeCount_6() { return static_cast<int32_t>(offsetof(UnmanagedMemory_t5E2A7F3AF355F7A20E8D8B3D7BE388511DD17C16, ___compositeCount_6)); }
	inline int32_t get_compositeCount_6() const { return ___compositeCount_6; }
	inline int32_t* get_address_of_compositeCount_6() { return &___compositeCount_6; }
	inline void set_compositeCount_6(int32_t value)
	{
		___compositeCount_6 = value;
	}

	inline static int32_t get_offset_of_actionStates_7() { return static_cast<int32_t>(offsetof(UnmanagedMemory_t5E2A7F3AF355F7A20E8D8B3D7BE388511DD17C16, ___actionStates_7)); }
	inline TriggerState_tD1D00F0DE77AE9CA21DE7A2CEDA8D93EED009CBE * get_actionStates_7() const { return ___actionStates_7; }
	inline TriggerState_tD1D00F0DE77AE9CA21DE7A2CEDA8D93EED009CBE ** get_address_of_actionStates_7() { return &___actionStates_7; }
	inline void set_actionStates_7(TriggerState_tD1D00F0DE77AE9CA21DE7A2CEDA8D93EED009CBE * value)
	{
		___actionStates_7 = value;
	}

	inline static int32_t get_offset_of_bindingStates_8() { return static_cast<int32_t>(offsetof(UnmanagedMemory_t5E2A7F3AF355F7A20E8D8B3D7BE388511DD17C16, ___bindingStates_8)); }
	inline BindingState_tE715A30188652BA114CEB7CD0E12A25CFCEF8FC7 * get_bindingStates_8() const { return ___bindingStates_8; }
	inline BindingState_tE715A30188652BA114CEB7CD0E12A25CFCEF8FC7 ** get_address_of_bindingStates_8() { return &___bindingStates_8; }
	inline void set_bindingStates_8(BindingState_tE715A30188652BA114CEB7CD0E12A25CFCEF8FC7 * value)
	{
		___bindingStates_8 = value;
	}

	inline static int32_t get_offset_of_interactionStates_9() { return static_cast<int32_t>(offsetof(UnmanagedMemory_t5E2A7F3AF355F7A20E8D8B3D7BE388511DD17C16, ___interactionStates_9)); }
	inline InteractionState_tC8A885CD98436B1609B0988EFDCCD6BBBCFDD6B7 * get_interactionStates_9() const { return ___interactionStates_9; }
	inline InteractionState_tC8A885CD98436B1609B0988EFDCCD6BBBCFDD6B7 ** get_address_of_interactionStates_9() { return &___interactionStates_9; }
	inline void set_interactionStates_9(InteractionState_tC8A885CD98436B1609B0988EFDCCD6BBBCFDD6B7 * value)
	{
		___interactionStates_9 = value;
	}

	inline static int32_t get_offset_of_controlMagnitudes_10() { return static_cast<int32_t>(offsetof(UnmanagedMemory_t5E2A7F3AF355F7A20E8D8B3D7BE388511DD17C16, ___controlMagnitudes_10)); }
	inline float* get_controlMagnitudes_10() const { return ___controlMagnitudes_10; }
	inline float** get_address_of_controlMagnitudes_10() { return &___controlMagnitudes_10; }
	inline void set_controlMagnitudes_10(float* value)
	{
		___controlMagnitudes_10 = value;
	}

	inline static int32_t get_offset_of_compositeMagnitudes_11() { return static_cast<int32_t>(offsetof(UnmanagedMemory_t5E2A7F3AF355F7A20E8D8B3D7BE388511DD17C16, ___compositeMagnitudes_11)); }
	inline float* get_compositeMagnitudes_11() const { return ___compositeMagnitudes_11; }
	inline float** get_address_of_compositeMagnitudes_11() { return &___compositeMagnitudes_11; }
	inline void set_compositeMagnitudes_11(float* value)
	{
		___compositeMagnitudes_11 = value;
	}

	inline static int32_t get_offset_of_enabledControls_12() { return static_cast<int32_t>(offsetof(UnmanagedMemory_t5E2A7F3AF355F7A20E8D8B3D7BE388511DD17C16, ___enabledControls_12)); }
	inline int32_t* get_enabledControls_12() const { return ___enabledControls_12; }
	inline int32_t** get_address_of_enabledControls_12() { return &___enabledControls_12; }
	inline void set_enabledControls_12(int32_t* value)
	{
		___enabledControls_12 = value;
	}

	inline static int32_t get_offset_of_actionBindingIndicesAndCounts_13() { return static_cast<int32_t>(offsetof(UnmanagedMemory_t5E2A7F3AF355F7A20E8D8B3D7BE388511DD17C16, ___actionBindingIndicesAndCounts_13)); }
	inline uint16_t* get_actionBindingIndicesAndCounts_13() const { return ___actionBindingIndicesAndCounts_13; }
	inline uint16_t** get_address_of_actionBindingIndicesAndCounts_13() { return &___actionBindingIndicesAndCounts_13; }
	inline void set_actionBindingIndicesAndCounts_13(uint16_t* value)
	{
		___actionBindingIndicesAndCounts_13 = value;
	}

	inline static int32_t get_offset_of_actionBindingIndices_14() { return static_cast<int32_t>(offsetof(UnmanagedMemory_t5E2A7F3AF355F7A20E8D8B3D7BE388511DD17C16, ___actionBindingIndices_14)); }
	inline uint16_t* get_actionBindingIndices_14() const { return ___actionBindingIndices_14; }
	inline uint16_t** get_address_of_actionBindingIndices_14() { return &___actionBindingIndices_14; }
	inline void set_actionBindingIndices_14(uint16_t* value)
	{
		___actionBindingIndices_14 = value;
	}

	inline static int32_t get_offset_of_controlIndexToBindingIndex_15() { return static_cast<int32_t>(offsetof(UnmanagedMemory_t5E2A7F3AF355F7A20E8D8B3D7BE388511DD17C16, ___controlIndexToBindingIndex_15)); }
	inline int32_t* get_controlIndexToBindingIndex_15() const { return ___controlIndexToBindingIndex_15; }
	inline int32_t** get_address_of_controlIndexToBindingIndex_15() { return &___controlIndexToBindingIndex_15; }
	inline void set_controlIndexToBindingIndex_15(int32_t* value)
	{
		___controlIndexToBindingIndex_15 = value;
	}

	inline static int32_t get_offset_of_mapIndices_16() { return static_cast<int32_t>(offsetof(UnmanagedMemory_t5E2A7F3AF355F7A20E8D8B3D7BE388511DD17C16, ___mapIndices_16)); }
	inline ActionMapIndices_t70234E0132647DCE386300AED2C964F629609245 * get_mapIndices_16() const { return ___mapIndices_16; }
	inline ActionMapIndices_t70234E0132647DCE386300AED2C964F629609245 ** get_address_of_mapIndices_16() { return &___mapIndices_16; }
	inline void set_mapIndices_16(ActionMapIndices_t70234E0132647DCE386300AED2C964F629609245 * value)
	{
		___mapIndices_16 = value;
	}
};


// UnityEngine.InputSystem.Layouts.InputControlLayout/Cache
struct Cache_t9F49740128E8F04886120C99F392DC7CEBC6AA67 
{
public:
	// System.Collections.Generic.Dictionary`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Layouts.InputControlLayout> UnityEngine.InputSystem.Layouts.InputControlLayout/Cache::table
	Dictionary_2_tDCF55C892C8E630B68A32A8B68C04B47E837C8A7 * ___table_0;

public:
	inline static int32_t get_offset_of_table_0() { return static_cast<int32_t>(offsetof(Cache_t9F49740128E8F04886120C99F392DC7CEBC6AA67, ___table_0)); }
	inline Dictionary_2_tDCF55C892C8E630B68A32A8B68C04B47E837C8A7 * get_table_0() const { return ___table_0; }
	inline Dictionary_2_tDCF55C892C8E630B68A32A8B68C04B47E837C8A7 ** get_address_of_table_0() { return &___table_0; }
	inline void set_table_0(Dictionary_2_tDCF55C892C8E630B68A32A8B68C04B47E837C8A7 * value)
	{
		___table_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___table_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.Layouts.InputControlLayout/Cache
struct Cache_t9F49740128E8F04886120C99F392DC7CEBC6AA67_marshaled_pinvoke
{
	Dictionary_2_tDCF55C892C8E630B68A32A8B68C04B47E837C8A7 * ___table_0;
};
// Native definition for COM marshalling of UnityEngine.InputSystem.Layouts.InputControlLayout/Cache
struct Cache_t9F49740128E8F04886120C99F392DC7CEBC6AA67_marshaled_com
{
	Dictionary_2_tDCF55C892C8E630B68A32A8B68C04B47E837C8A7 * ___table_0;
};

// UnityEngine.InputSystem.Layouts.InputControlLayout/Collection
struct Collection_tDBA0BC36ACE448001ABFFED32EF785467D714D7B 
{
public:
	// System.Collections.Generic.Dictionary`2<UnityEngine.InputSystem.Utilities.InternedString,System.Type> UnityEngine.InputSystem.Layouts.InputControlLayout/Collection::layoutTypes
	Dictionary_2_t9AFCC08689B78B2E9999C1E77045DE7FE2E38939 * ___layoutTypes_1;
	// System.Collections.Generic.Dictionary`2<UnityEngine.InputSystem.Utilities.InternedString,System.String> UnityEngine.InputSystem.Layouts.InputControlLayout/Collection::layoutStrings
	Dictionary_2_tDA5E5646D36E737D207FA28F7AB4CE457B42B367 * ___layoutStrings_2;
	// System.Collections.Generic.Dictionary`2<UnityEngine.InputSystem.Utilities.InternedString,System.Func`1<UnityEngine.InputSystem.Layouts.InputControlLayout>> UnityEngine.InputSystem.Layouts.InputControlLayout/Collection::layoutBuilders
	Dictionary_2_t73E7A2CC7CD17B4CBC4DBC5B50FC3A085745875D * ___layoutBuilders_3;
	// System.Collections.Generic.Dictionary`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString> UnityEngine.InputSystem.Layouts.InputControlLayout/Collection::baseLayoutTable
	Dictionary_2_tBE5A6550789B79F0A18DA883DF48023D1D980096 * ___baseLayoutTable_4;
	// System.Collections.Generic.Dictionary`2<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString[]> UnityEngine.InputSystem.Layouts.InputControlLayout/Collection::layoutOverrides
	Dictionary_2_t6B8207956EB1FAAEB6DBD37E3FD84A30466F2BA8 * ___layoutOverrides_5;
	// System.Collections.Generic.HashSet`1<UnityEngine.InputSystem.Utilities.InternedString> UnityEngine.InputSystem.Layouts.InputControlLayout/Collection::layoutOverrideNames
	HashSet_1_t68A0DBD19F3F3043FF8AC0D854B63BAA7BEB4064 * ___layoutOverrideNames_6;
	// System.Collections.Generic.List`1<UnityEngine.InputSystem.Layouts.InputControlLayout/Collection/LayoutMatcher> UnityEngine.InputSystem.Layouts.InputControlLayout/Collection::layoutMatchers
	List_1_tE0D0016948365B54865C0A62B95211A71F2FB9A6 * ___layoutMatchers_7;

public:
	inline static int32_t get_offset_of_layoutTypes_1() { return static_cast<int32_t>(offsetof(Collection_tDBA0BC36ACE448001ABFFED32EF785467D714D7B, ___layoutTypes_1)); }
	inline Dictionary_2_t9AFCC08689B78B2E9999C1E77045DE7FE2E38939 * get_layoutTypes_1() const { return ___layoutTypes_1; }
	inline Dictionary_2_t9AFCC08689B78B2E9999C1E77045DE7FE2E38939 ** get_address_of_layoutTypes_1() { return &___layoutTypes_1; }
	inline void set_layoutTypes_1(Dictionary_2_t9AFCC08689B78B2E9999C1E77045DE7FE2E38939 * value)
	{
		___layoutTypes_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___layoutTypes_1), (void*)value);
	}

	inline static int32_t get_offset_of_layoutStrings_2() { return static_cast<int32_t>(offsetof(Collection_tDBA0BC36ACE448001ABFFED32EF785467D714D7B, ___layoutStrings_2)); }
	inline Dictionary_2_tDA5E5646D36E737D207FA28F7AB4CE457B42B367 * get_layoutStrings_2() const { return ___layoutStrings_2; }
	inline Dictionary_2_tDA5E5646D36E737D207FA28F7AB4CE457B42B367 ** get_address_of_layoutStrings_2() { return &___layoutStrings_2; }
	inline void set_layoutStrings_2(Dictionary_2_tDA5E5646D36E737D207FA28F7AB4CE457B42B367 * value)
	{
		___layoutStrings_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___layoutStrings_2), (void*)value);
	}

	inline static int32_t get_offset_of_layoutBuilders_3() { return static_cast<int32_t>(offsetof(Collection_tDBA0BC36ACE448001ABFFED32EF785467D714D7B, ___layoutBuilders_3)); }
	inline Dictionary_2_t73E7A2CC7CD17B4CBC4DBC5B50FC3A085745875D * get_layoutBuilders_3() const { return ___layoutBuilders_3; }
	inline Dictionary_2_t73E7A2CC7CD17B4CBC4DBC5B50FC3A085745875D ** get_address_of_layoutBuilders_3() { return &___layoutBuilders_3; }
	inline void set_layoutBuilders_3(Dictionary_2_t73E7A2CC7CD17B4CBC4DBC5B50FC3A085745875D * value)
	{
		___layoutBuilders_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___layoutBuilders_3), (void*)value);
	}

	inline static int32_t get_offset_of_baseLayoutTable_4() { return static_cast<int32_t>(offsetof(Collection_tDBA0BC36ACE448001ABFFED32EF785467D714D7B, ___baseLayoutTable_4)); }
	inline Dictionary_2_tBE5A6550789B79F0A18DA883DF48023D1D980096 * get_baseLayoutTable_4() const { return ___baseLayoutTable_4; }
	inline Dictionary_2_tBE5A6550789B79F0A18DA883DF48023D1D980096 ** get_address_of_baseLayoutTable_4() { return &___baseLayoutTable_4; }
	inline void set_baseLayoutTable_4(Dictionary_2_tBE5A6550789B79F0A18DA883DF48023D1D980096 * value)
	{
		___baseLayoutTable_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___baseLayoutTable_4), (void*)value);
	}

	inline static int32_t get_offset_of_layoutOverrides_5() { return static_cast<int32_t>(offsetof(Collection_tDBA0BC36ACE448001ABFFED32EF785467D714D7B, ___layoutOverrides_5)); }
	inline Dictionary_2_t6B8207956EB1FAAEB6DBD37E3FD84A30466F2BA8 * get_layoutOverrides_5() const { return ___layoutOverrides_5; }
	inline Dictionary_2_t6B8207956EB1FAAEB6DBD37E3FD84A30466F2BA8 ** get_address_of_layoutOverrides_5() { return &___layoutOverrides_5; }
	inline void set_layoutOverrides_5(Dictionary_2_t6B8207956EB1FAAEB6DBD37E3FD84A30466F2BA8 * value)
	{
		___layoutOverrides_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___layoutOverrides_5), (void*)value);
	}

	inline static int32_t get_offset_of_layoutOverrideNames_6() { return static_cast<int32_t>(offsetof(Collection_tDBA0BC36ACE448001ABFFED32EF785467D714D7B, ___layoutOverrideNames_6)); }
	inline HashSet_1_t68A0DBD19F3F3043FF8AC0D854B63BAA7BEB4064 * get_layoutOverrideNames_6() const { return ___layoutOverrideNames_6; }
	inline HashSet_1_t68A0DBD19F3F3043FF8AC0D854B63BAA7BEB4064 ** get_address_of_layoutOverrideNames_6() { return &___layoutOverrideNames_6; }
	inline void set_layoutOverrideNames_6(HashSet_1_t68A0DBD19F3F3043FF8AC0D854B63BAA7BEB4064 * value)
	{
		___layoutOverrideNames_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___layoutOverrideNames_6), (void*)value);
	}

	inline static int32_t get_offset_of_layoutMatchers_7() { return static_cast<int32_t>(offsetof(Collection_tDBA0BC36ACE448001ABFFED32EF785467D714D7B, ___layoutMatchers_7)); }
	inline List_1_tE0D0016948365B54865C0A62B95211A71F2FB9A6 * get_layoutMatchers_7() const { return ___layoutMatchers_7; }
	inline List_1_tE0D0016948365B54865C0A62B95211A71F2FB9A6 ** get_address_of_layoutMatchers_7() { return &___layoutMatchers_7; }
	inline void set_layoutMatchers_7(List_1_tE0D0016948365B54865C0A62B95211A71F2FB9A6 * value)
	{
		___layoutMatchers_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___layoutMatchers_7), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.Layouts.InputControlLayout/Collection
struct Collection_tDBA0BC36ACE448001ABFFED32EF785467D714D7B_marshaled_pinvoke
{
	Dictionary_2_t9AFCC08689B78B2E9999C1E77045DE7FE2E38939 * ___layoutTypes_1;
	Dictionary_2_tDA5E5646D36E737D207FA28F7AB4CE457B42B367 * ___layoutStrings_2;
	Dictionary_2_t73E7A2CC7CD17B4CBC4DBC5B50FC3A085745875D * ___layoutBuilders_3;
	Dictionary_2_tBE5A6550789B79F0A18DA883DF48023D1D980096 * ___baseLayoutTable_4;
	Dictionary_2_t6B8207956EB1FAAEB6DBD37E3FD84A30466F2BA8 * ___layoutOverrides_5;
	HashSet_1_t68A0DBD19F3F3043FF8AC0D854B63BAA7BEB4064 * ___layoutOverrideNames_6;
	List_1_tE0D0016948365B54865C0A62B95211A71F2FB9A6 * ___layoutMatchers_7;
};
// Native definition for COM marshalling of UnityEngine.InputSystem.Layouts.InputControlLayout/Collection
struct Collection_tDBA0BC36ACE448001ABFFED32EF785467D714D7B_marshaled_com
{
	Dictionary_2_t9AFCC08689B78B2E9999C1E77045DE7FE2E38939 * ___layoutTypes_1;
	Dictionary_2_tDA5E5646D36E737D207FA28F7AB4CE457B42B367 * ___layoutStrings_2;
	Dictionary_2_t73E7A2CC7CD17B4CBC4DBC5B50FC3A085745875D * ___layoutBuilders_3;
	Dictionary_2_tBE5A6550789B79F0A18DA883DF48023D1D980096 * ___baseLayoutTable_4;
	Dictionary_2_t6B8207956EB1FAAEB6DBD37E3FD84A30466F2BA8 * ___layoutOverrides_5;
	HashSet_1_t68A0DBD19F3F3043FF8AC0D854B63BAA7BEB4064 * ___layoutOverrideNames_6;
	List_1_tE0D0016948365B54865C0A62B95211A71F2FB9A6 * ___layoutMatchers_7;
};

// System.Linq.Expressions.Interpreter.LightCompiler/QuoteVisitor
struct QuoteVisitor_tFE404B4C826642C3FC245A108AEC9E94D691E627  : public ExpressionVisitor_tD098DE8A366FBBB58C498C4EFF8B003FCA726DF4
{
public:
	// System.Collections.Generic.Dictionary`2<System.Linq.Expressions.ParameterExpression,System.Int32> System.Linq.Expressions.Interpreter.LightCompiler/QuoteVisitor::_definedParameters
	Dictionary_2_t557635FBDBCB4F09E0827F01D69D76FF503D03A7 * ____definedParameters_0;
	// System.Collections.Generic.HashSet`1<System.Linq.Expressions.ParameterExpression> System.Linq.Expressions.Interpreter.LightCompiler/QuoteVisitor::_hoistedParameters
	HashSet_1_t42A3AC337CA15FAC250AA5DA438F909806C72CB0 * ____hoistedParameters_1;

public:
	inline static int32_t get_offset_of__definedParameters_0() { return static_cast<int32_t>(offsetof(QuoteVisitor_tFE404B4C826642C3FC245A108AEC9E94D691E627, ____definedParameters_0)); }
	inline Dictionary_2_t557635FBDBCB4F09E0827F01D69D76FF503D03A7 * get__definedParameters_0() const { return ____definedParameters_0; }
	inline Dictionary_2_t557635FBDBCB4F09E0827F01D69D76FF503D03A7 ** get_address_of__definedParameters_0() { return &____definedParameters_0; }
	inline void set__definedParameters_0(Dictionary_2_t557635FBDBCB4F09E0827F01D69D76FF503D03A7 * value)
	{
		____definedParameters_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____definedParameters_0), (void*)value);
	}

	inline static int32_t get_offset_of__hoistedParameters_1() { return static_cast<int32_t>(offsetof(QuoteVisitor_tFE404B4C826642C3FC245A108AEC9E94D691E627, ____hoistedParameters_1)); }
	inline HashSet_1_t42A3AC337CA15FAC250AA5DA438F909806C72CB0 * get__hoistedParameters_1() const { return ____hoistedParameters_1; }
	inline HashSet_1_t42A3AC337CA15FAC250AA5DA438F909806C72CB0 ** get_address_of__hoistedParameters_1() { return &____hoistedParameters_1; }
	inline void set__hoistedParameters_1(HashSet_1_t42A3AC337CA15FAC250AA5DA438F909806C72CB0 * value)
	{
		____hoistedParameters_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____hoistedParameters_1), (void*)value);
	}
};


// System.Linq.Expressions.Interpreter.QuoteInstruction/ExpressionQuoter
struct ExpressionQuoter_t174D328A07E522775BA6B19ADF09DBEAF13098FE  : public ExpressionVisitor_tD098DE8A366FBBB58C498C4EFF8B003FCA726DF4
{
public:
	// System.Collections.Generic.Dictionary`2<System.Linq.Expressions.ParameterExpression,System.Linq.Expressions.Interpreter.LocalVariable> System.Linq.Expressions.Interpreter.QuoteInstruction/ExpressionQuoter::_variables
	Dictionary_2_tAE9216CE6245A2FBEA94860E5D55598909B27352 * ____variables_0;
	// System.Linq.Expressions.Interpreter.InterpretedFrame System.Linq.Expressions.Interpreter.QuoteInstruction/ExpressionQuoter::_frame
	InterpretedFrame_tC7B57503A639148EB56B34F5464120D4B42627E2 * ____frame_1;
	// System.Collections.Generic.Stack`1<System.Collections.Generic.HashSet`1<System.Linq.Expressions.ParameterExpression>> System.Linq.Expressions.Interpreter.QuoteInstruction/ExpressionQuoter::_shadowedVars
	Stack_1_t438C22E9DF33740A9BDB48CF9504B6E044484958 * ____shadowedVars_2;

public:
	inline static int32_t get_offset_of__variables_0() { return static_cast<int32_t>(offsetof(ExpressionQuoter_t174D328A07E522775BA6B19ADF09DBEAF13098FE, ____variables_0)); }
	inline Dictionary_2_tAE9216CE6245A2FBEA94860E5D55598909B27352 * get__variables_0() const { return ____variables_0; }
	inline Dictionary_2_tAE9216CE6245A2FBEA94860E5D55598909B27352 ** get_address_of__variables_0() { return &____variables_0; }
	inline void set__variables_0(Dictionary_2_tAE9216CE6245A2FBEA94860E5D55598909B27352 * value)
	{
		____variables_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____variables_0), (void*)value);
	}

	inline static int32_t get_offset_of__frame_1() { return static_cast<int32_t>(offsetof(ExpressionQuoter_t174D328A07E522775BA6B19ADF09DBEAF13098FE, ____frame_1)); }
	inline InterpretedFrame_tC7B57503A639148EB56B34F5464120D4B42627E2 * get__frame_1() const { return ____frame_1; }
	inline InterpretedFrame_tC7B57503A639148EB56B34F5464120D4B42627E2 ** get_address_of__frame_1() { return &____frame_1; }
	inline void set__frame_1(InterpretedFrame_tC7B57503A639148EB56B34F5464120D4B42627E2 * value)
	{
		____frame_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____frame_1), (void*)value);
	}

	inline static int32_t get_offset_of__shadowedVars_2() { return static_cast<int32_t>(offsetof(ExpressionQuoter_t174D328A07E522775BA6B19ADF09DBEAF13098FE, ____shadowedVars_2)); }
	inline Stack_1_t438C22E9DF33740A9BDB48CF9504B6E044484958 * get__shadowedVars_2() const { return ____shadowedVars_2; }
	inline Stack_1_t438C22E9DF33740A9BDB48CF9504B6E044484958 ** get_address_of__shadowedVars_2() { return &____shadowedVars_2; }
	inline void set__shadowedVars_2(Stack_1_t438C22E9DF33740A9BDB48CF9504B6E044484958 * value)
	{
		____shadowedVars_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____shadowedVars_2), (void*)value);
	}
};


// System.Linq.Expressions.Expression`1<System.Object>
struct Expression_1_t01B093F396848A065BE827B0C58E6F20E760FB6F  : public LambdaExpression_t26BF905E97E6D94F81F17D60F4F4F47F8E93B474
{
public:

public:
};


// UnityEngine.InputSystem.Utilities.InlinedArray`1<System.Runtime.InteropServices.GCHandle>
struct InlinedArray_1_tA3967E760933F5DC1FD6C0934D223A5ACA0A2723 
{
public:
	// System.Int32 UnityEngine.InputSystem.Utilities.InlinedArray`1::length
	int32_t ___length_0;
	// TValue UnityEngine.InputSystem.Utilities.InlinedArray`1::firstValue
	GCHandle_t757890BC4BBBEDE5A623A3C110013EDD24613603  ___firstValue_1;
	// TValue[] UnityEngine.InputSystem.Utilities.InlinedArray`1::additionalValues
	GCHandleU5BU5D_tE15D27927DA8B3D667EF2723192CDC34B763D2F3* ___additionalValues_2;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(InlinedArray_1_tA3967E760933F5DC1FD6C0934D223A5ACA0A2723, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_firstValue_1() { return static_cast<int32_t>(offsetof(InlinedArray_1_tA3967E760933F5DC1FD6C0934D223A5ACA0A2723, ___firstValue_1)); }
	inline GCHandle_t757890BC4BBBEDE5A623A3C110013EDD24613603  get_firstValue_1() const { return ___firstValue_1; }
	inline GCHandle_t757890BC4BBBEDE5A623A3C110013EDD24613603 * get_address_of_firstValue_1() { return &___firstValue_1; }
	inline void set_firstValue_1(GCHandle_t757890BC4BBBEDE5A623A3C110013EDD24613603  value)
	{
		___firstValue_1 = value;
	}

	inline static int32_t get_offset_of_additionalValues_2() { return static_cast<int32_t>(offsetof(InlinedArray_1_tA3967E760933F5DC1FD6C0934D223A5ACA0A2723, ___additionalValues_2)); }
	inline GCHandleU5BU5D_tE15D27927DA8B3D667EF2723192CDC34B763D2F3* get_additionalValues_2() const { return ___additionalValues_2; }
	inline GCHandleU5BU5D_tE15D27927DA8B3D667EF2723192CDC34B763D2F3** get_address_of_additionalValues_2() { return &___additionalValues_2; }
	inline void set_additionalValues_2(GCHandleU5BU5D_tE15D27927DA8B3D667EF2723192CDC34B763D2F3* value)
	{
		___additionalValues_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___additionalValues_2), (void*)value);
	}
};


// UnityEngine.InputSystem.Utilities.InlinedArray`1<UnityEngine.InputSystem.Utilities.InternedString>
struct InlinedArray_1_t7B73159E4B6A8AD12C99ED22A9138E32D7408FA0 
{
public:
	// System.Int32 UnityEngine.InputSystem.Utilities.InlinedArray`1::length
	int32_t ___length_0;
	// TValue UnityEngine.InputSystem.Utilities.InlinedArray`1::firstValue
	InternedString_tD1138602E8B7EA0F5B4851812B13C7E0551E25C8  ___firstValue_1;
	// TValue[] UnityEngine.InputSystem.Utilities.InlinedArray`1::additionalValues
	InternedStringU5BU5D_t1B3BD9ED90129E67B58E1681B1944E72F8E0E648* ___additionalValues_2;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(InlinedArray_1_t7B73159E4B6A8AD12C99ED22A9138E32D7408FA0, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_firstValue_1() { return static_cast<int32_t>(offsetof(InlinedArray_1_t7B73159E4B6A8AD12C99ED22A9138E32D7408FA0, ___firstValue_1)); }
	inline InternedString_tD1138602E8B7EA0F5B4851812B13C7E0551E25C8  get_firstValue_1() const { return ___firstValue_1; }
	inline InternedString_tD1138602E8B7EA0F5B4851812B13C7E0551E25C8 * get_address_of_firstValue_1() { return &___firstValue_1; }
	inline void set_firstValue_1(InternedString_tD1138602E8B7EA0F5B4851812B13C7E0551E25C8  value)
	{
		___firstValue_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___firstValue_1))->___m_StringOriginalCase_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___firstValue_1))->___m_StringLowerCase_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_additionalValues_2() { return static_cast<int32_t>(offsetof(InlinedArray_1_t7B73159E4B6A8AD12C99ED22A9138E32D7408FA0, ___additionalValues_2)); }
	inline InternedStringU5BU5D_t1B3BD9ED90129E67B58E1681B1944E72F8E0E648* get_additionalValues_2() const { return ___additionalValues_2; }
	inline InternedStringU5BU5D_t1B3BD9ED90129E67B58E1681B1944E72F8E0E648** get_address_of_additionalValues_2() { return &___additionalValues_2; }
	inline void set_additionalValues_2(InternedStringU5BU5D_t1B3BD9ED90129E67B58E1681B1944E72F8E0E648* value)
	{
		___additionalValues_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___additionalValues_2), (void*)value);
	}
};


// Unity.Collections.Allocator
struct Allocator_t9888223DEF4F46F3419ECFCCD0753599BEE52A05 
{
public:
	// System.Int32 Unity.Collections.Allocator::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Allocator_t9888223DEF4F46F3419ECFCCD0753599BEE52A05, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Exception
struct Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// UnityEngine.InputSystem.InputActionPhase
struct InputActionPhase_t10B92A32A4518A0D9284B677640A971AB3C81BE4 
{
public:
	// System.Int32 UnityEngine.InputSystem.InputActionPhase::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InputActionPhase_t10B92A32A4518A0D9284B677640A971AB3C81BE4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.InputSystem.InputProcessor
struct InputProcessor_tAC24832406A406286CE4B162D9D2EF5E2E08765A  : public RuntimeObject
{
public:

public:
};

struct InputProcessor_tAC24832406A406286CE4B162D9D2EF5E2E08765A_StaticFields
{
public:
	// UnityEngine.InputSystem.Utilities.TypeTable UnityEngine.InputSystem.InputProcessor::s_Processors
	TypeTable_tE58155618A277C8EB10C9B8FB3040A6731E46BFA  ___s_Processors_0;

public:
	inline static int32_t get_offset_of_s_Processors_0() { return static_cast<int32_t>(offsetof(InputProcessor_tAC24832406A406286CE4B162D9D2EF5E2E08765A_StaticFields, ___s_Processors_0)); }
	inline TypeTable_tE58155618A277C8EB10C9B8FB3040A6731E46BFA  get_s_Processors_0() const { return ___s_Processors_0; }
	inline TypeTable_tE58155618A277C8EB10C9B8FB3040A6731E46BFA * get_address_of_s_Processors_0() { return &___s_Processors_0; }
	inline void set_s_Processors_0(TypeTable_tE58155618A277C8EB10C9B8FB3040A6731E46BFA  value)
	{
		___s_Processors_0 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___s_Processors_0))->___table_0), (void*)NULL);
	}
};


// Unity.Collections.NativeArrayOptions
struct NativeArrayOptions_t181E2A9B49F6D62868DE6428E4CDF148AEF558E3 
{
public:
	// System.Int32 Unity.Collections.NativeArrayOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NativeArrayOptions_t181E2A9B49F6D62868DE6428E4CDF148AEF558E3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// UniGLTF.glBufferTarget
struct glBufferTarget_tB423D07DD863D74DE230C8A0834CC935935916E3 
{
public:
	// System.Int32 UniGLTF.glBufferTarget::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(glBufferTarget_tB423D07DD863D74DE230C8A0834CC935935916E3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UniGLTF.glComponentType
struct glComponentType_tAAD2C29320D53B232DA1C6BA61D10A90816E3BE0 
{
public:
	// System.Int32 UniGLTF.glComponentType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(glComponentType_tAAD2C29320D53B232DA1C6BA61D10A90816E3BE0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.InputSystem.InputActionSetupExtensions/ControlSchemeSyntax
struct ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214 
{
public:
	// UnityEngine.InputSystem.InputActionAsset UnityEngine.InputSystem.InputActionSetupExtensions/ControlSchemeSyntax::m_Asset
	InputActionAsset_tB1114F77EFFAE61A28A8A559662A6FCCF444F70E * ___m_Asset_0;
	// System.Int32 UnityEngine.InputSystem.InputActionSetupExtensions/ControlSchemeSyntax::m_ControlSchemeIndex
	int32_t ___m_ControlSchemeIndex_1;
	// UnityEngine.InputSystem.InputControlScheme UnityEngine.InputSystem.InputActionSetupExtensions/ControlSchemeSyntax::m_ControlScheme
	InputControlScheme_tEA1C7C6A6509E7AE956DDBBA6C2E288AD437ED9A  ___m_ControlScheme_2;

public:
	inline static int32_t get_offset_of_m_Asset_0() { return static_cast<int32_t>(offsetof(ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214, ___m_Asset_0)); }
	inline InputActionAsset_tB1114F77EFFAE61A28A8A559662A6FCCF444F70E * get_m_Asset_0() const { return ___m_Asset_0; }
	inline InputActionAsset_tB1114F77EFFAE61A28A8A559662A6FCCF444F70E ** get_address_of_m_Asset_0() { return &___m_Asset_0; }
	inline void set_m_Asset_0(InputActionAsset_tB1114F77EFFAE61A28A8A559662A6FCCF444F70E * value)
	{
		___m_Asset_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Asset_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_ControlSchemeIndex_1() { return static_cast<int32_t>(offsetof(ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214, ___m_ControlSchemeIndex_1)); }
	inline int32_t get_m_ControlSchemeIndex_1() const { return ___m_ControlSchemeIndex_1; }
	inline int32_t* get_address_of_m_ControlSchemeIndex_1() { return &___m_ControlSchemeIndex_1; }
	inline void set_m_ControlSchemeIndex_1(int32_t value)
	{
		___m_ControlSchemeIndex_1 = value;
	}

	inline static int32_t get_offset_of_m_ControlScheme_2() { return static_cast<int32_t>(offsetof(ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214, ___m_ControlScheme_2)); }
	inline InputControlScheme_tEA1C7C6A6509E7AE956DDBBA6C2E288AD437ED9A  get_m_ControlScheme_2() const { return ___m_ControlScheme_2; }
	inline InputControlScheme_tEA1C7C6A6509E7AE956DDBBA6C2E288AD437ED9A * get_address_of_m_ControlScheme_2() { return &___m_ControlScheme_2; }
	inline void set_m_ControlScheme_2(InputControlScheme_tEA1C7C6A6509E7AE956DDBBA6C2E288AD437ED9A  value)
	{
		___m_ControlScheme_2 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_ControlScheme_2))->___m_Name_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_ControlScheme_2))->___m_BindingGroup_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_ControlScheme_2))->___m_DeviceRequirements_2), (void*)NULL);
		#endif
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.InputActionSetupExtensions/ControlSchemeSyntax
struct ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214_marshaled_pinvoke
{
	InputActionAsset_tB1114F77EFFAE61A28A8A559662A6FCCF444F70E * ___m_Asset_0;
	int32_t ___m_ControlSchemeIndex_1;
	InputControlScheme_tEA1C7C6A6509E7AE956DDBBA6C2E288AD437ED9A_marshaled_pinvoke ___m_ControlScheme_2;
};
// Native definition for COM marshalling of UnityEngine.InputSystem.InputActionSetupExtensions/ControlSchemeSyntax
struct ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214_marshaled_com
{
	InputActionAsset_tB1114F77EFFAE61A28A8A559662A6FCCF444F70E * ___m_Asset_0;
	int32_t ___m_ControlSchemeIndex_1;
	InputControlScheme_tEA1C7C6A6509E7AE956DDBBA6C2E288AD437ED9A_marshaled_com ___m_ControlScheme_2;
};

// UnityEngine.InputSystem.InputBinding/Flags
struct Flags_t96BD9B15406A59FB60DE4A1F11DF96FB70426BF5 
{
public:
	// System.Int32 UnityEngine.InputSystem.InputBinding/Flags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Flags_t96BD9B15406A59FB60DE4A1F11DF96FB70426BF5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.InputSystem.Layouts.InputControlLayout/Builder
struct Builder_t4C196179CE57554255EBFB4ECD4D6499ADA974FB  : public RuntimeObject
{
public:
	// System.String UnityEngine.InputSystem.Layouts.InputControlLayout/Builder::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
	// System.String UnityEngine.InputSystem.Layouts.InputControlLayout/Builder::<displayName>k__BackingField
	String_t* ___U3CdisplayNameU3Ek__BackingField_1;
	// System.Type UnityEngine.InputSystem.Layouts.InputControlLayout/Builder::<type>k__BackingField
	Type_t * ___U3CtypeU3Ek__BackingField_2;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.Layouts.InputControlLayout/Builder::<stateFormat>k__BackingField
	FourCC_tB18951639903964A6B5533C4BAFD8DA17AA8FD9D  ___U3CstateFormatU3Ek__BackingField_3;
	// System.Int32 UnityEngine.InputSystem.Layouts.InputControlLayout/Builder::<stateSizeInBytes>k__BackingField
	int32_t ___U3CstateSizeInBytesU3Ek__BackingField_4;
	// System.String UnityEngine.InputSystem.Layouts.InputControlLayout/Builder::<extendsLayout>k__BackingField
	String_t* ___U3CextendsLayoutU3Ek__BackingField_5;
	// System.Nullable`1<System.Boolean> UnityEngine.InputSystem.Layouts.InputControlLayout/Builder::<updateBeforeRender>k__BackingField
	Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3  ___U3CupdateBeforeRenderU3Ek__BackingField_6;
	// System.Int32 UnityEngine.InputSystem.Layouts.InputControlLayout/Builder::m_ControlCount
	int32_t ___m_ControlCount_7;
	// UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem[] UnityEngine.InputSystem.Layouts.InputControlLayout/Builder::m_Controls
	ControlItemU5BU5D_t5AFF1520667DFF9AA06E17887A3D0B77BEB173BB* ___m_Controls_8;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Builder_t4C196179CE57554255EBFB4ECD4D6499ADA974FB, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CnameU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CdisplayNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Builder_t4C196179CE57554255EBFB4ECD4D6499ADA974FB, ___U3CdisplayNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CdisplayNameU3Ek__BackingField_1() const { return ___U3CdisplayNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CdisplayNameU3Ek__BackingField_1() { return &___U3CdisplayNameU3Ek__BackingField_1; }
	inline void set_U3CdisplayNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CdisplayNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CdisplayNameU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CtypeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Builder_t4C196179CE57554255EBFB4ECD4D6499ADA974FB, ___U3CtypeU3Ek__BackingField_2)); }
	inline Type_t * get_U3CtypeU3Ek__BackingField_2() const { return ___U3CtypeU3Ek__BackingField_2; }
	inline Type_t ** get_address_of_U3CtypeU3Ek__BackingField_2() { return &___U3CtypeU3Ek__BackingField_2; }
	inline void set_U3CtypeU3Ek__BackingField_2(Type_t * value)
	{
		___U3CtypeU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CtypeU3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CstateFormatU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Builder_t4C196179CE57554255EBFB4ECD4D6499ADA974FB, ___U3CstateFormatU3Ek__BackingField_3)); }
	inline FourCC_tB18951639903964A6B5533C4BAFD8DA17AA8FD9D  get_U3CstateFormatU3Ek__BackingField_3() const { return ___U3CstateFormatU3Ek__BackingField_3; }
	inline FourCC_tB18951639903964A6B5533C4BAFD8DA17AA8FD9D * get_address_of_U3CstateFormatU3Ek__BackingField_3() { return &___U3CstateFormatU3Ek__BackingField_3; }
	inline void set_U3CstateFormatU3Ek__BackingField_3(FourCC_tB18951639903964A6B5533C4BAFD8DA17AA8FD9D  value)
	{
		___U3CstateFormatU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CstateSizeInBytesU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Builder_t4C196179CE57554255EBFB4ECD4D6499ADA974FB, ___U3CstateSizeInBytesU3Ek__BackingField_4)); }
	inline int32_t get_U3CstateSizeInBytesU3Ek__BackingField_4() const { return ___U3CstateSizeInBytesU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CstateSizeInBytesU3Ek__BackingField_4() { return &___U3CstateSizeInBytesU3Ek__BackingField_4; }
	inline void set_U3CstateSizeInBytesU3Ek__BackingField_4(int32_t value)
	{
		___U3CstateSizeInBytesU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CextendsLayoutU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Builder_t4C196179CE57554255EBFB4ECD4D6499ADA974FB, ___U3CextendsLayoutU3Ek__BackingField_5)); }
	inline String_t* get_U3CextendsLayoutU3Ek__BackingField_5() const { return ___U3CextendsLayoutU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CextendsLayoutU3Ek__BackingField_5() { return &___U3CextendsLayoutU3Ek__BackingField_5; }
	inline void set_U3CextendsLayoutU3Ek__BackingField_5(String_t* value)
	{
		___U3CextendsLayoutU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CextendsLayoutU3Ek__BackingField_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CupdateBeforeRenderU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Builder_t4C196179CE57554255EBFB4ECD4D6499ADA974FB, ___U3CupdateBeforeRenderU3Ek__BackingField_6)); }
	inline Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3  get_U3CupdateBeforeRenderU3Ek__BackingField_6() const { return ___U3CupdateBeforeRenderU3Ek__BackingField_6; }
	inline Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3 * get_address_of_U3CupdateBeforeRenderU3Ek__BackingField_6() { return &___U3CupdateBeforeRenderU3Ek__BackingField_6; }
	inline void set_U3CupdateBeforeRenderU3Ek__BackingField_6(Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3  value)
	{
		___U3CupdateBeforeRenderU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_m_ControlCount_7() { return static_cast<int32_t>(offsetof(Builder_t4C196179CE57554255EBFB4ECD4D6499ADA974FB, ___m_ControlCount_7)); }
	inline int32_t get_m_ControlCount_7() const { return ___m_ControlCount_7; }
	inline int32_t* get_address_of_m_ControlCount_7() { return &___m_ControlCount_7; }
	inline void set_m_ControlCount_7(int32_t value)
	{
		___m_ControlCount_7 = value;
	}

	inline static int32_t get_offset_of_m_Controls_8() { return static_cast<int32_t>(offsetof(Builder_t4C196179CE57554255EBFB4ECD4D6499ADA974FB, ___m_Controls_8)); }
	inline ControlItemU5BU5D_t5AFF1520667DFF9AA06E17887A3D0B77BEB173BB* get_m_Controls_8() const { return ___m_Controls_8; }
	inline ControlItemU5BU5D_t5AFF1520667DFF9AA06E17887A3D0B77BEB173BB** get_address_of_m_Controls_8() { return &___m_Controls_8; }
	inline void set_m_Controls_8(ControlItemU5BU5D_t5AFF1520667DFF9AA06E17887A3D0B77BEB173BB* value)
	{
		___m_Controls_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Controls_8), (void*)value);
	}
};


// UnityEngine.InputSystem.Layouts.InputControlLayout/Flags
struct Flags_tAEF279DC7A9D86C63534C8EB1562B90382307466 
{
public:
	// System.Int32 UnityEngine.InputSystem.Layouts.InputControlLayout/Flags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Flags_tAEF279DC7A9D86C63534C8EB1562B90382307466, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.ARSubsystems.XRPlaneSubsystem/Provider
struct Provider_t6CB5B1036B0AAED1379F3828D695A6942B72BA12  : public SubsystemProvider_1_tF2F3B0C041BDD07A00CD49B25AE6016B61F24816
{
public:

public:
};


// UnityEngine.InputSystem.InputActionRebindingExtensions/RebindingOperation/Flags
struct Flags_tBAEC42444F14751378E9D7B2009867CBBBEF3FE9 
{
public:
	// System.Int32 UnityEngine.InputSystem.InputActionRebindingExtensions/RebindingOperation/Flags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Flags_tBAEC42444F14751378E9D7B2009867CBBBEF3FE9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Unity.Collections.NativeArray`1<System.UInt64>
struct NativeArray_1_t9D118727A643E61710D0A4DF5B0C8CD1A918A40B 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_t9D118727A643E61710D0A4DF5B0C8CD1A918A40B, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_t9D118727A643E61710D0A4DF5B0C8CD1A918A40B, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_t9D118727A643E61710D0A4DF5B0C8CD1A918A40B, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};


// Unity.Collections.NativeArray`1<UnityEngine.Vector2>
struct NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};


// UnityEngine.InputSystem.InputActionState
struct InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C  : public RuntimeObject
{
public:
	// UnityEngine.InputSystem.InputActionMap[] UnityEngine.InputSystem.InputActionState::maps
	InputActionMapU5BU5D_tEE6F572B03BE03B28C7B701C19AE4FD4D5D2E580* ___maps_1;
	// UnityEngine.InputSystem.InputControl[] UnityEngine.InputSystem.InputActionState::controls
	InputControlU5BU5D_t4668D8411829C981759512FED1506B20F902CA67* ___controls_2;
	// UnityEngine.InputSystem.IInputInteraction[] UnityEngine.InputSystem.InputActionState::interactions
	IInputInteractionU5BU5D_tC7710034CF7D51B96E7C1E12F3C7E64AB0B6C3E8* ___interactions_3;
	// UnityEngine.InputSystem.InputProcessor[] UnityEngine.InputSystem.InputActionState::processors
	InputProcessorU5BU5D_tC2B97EE395F9CFBD17046BD90DB5A0A3B46EB9D3* ___processors_4;
	// UnityEngine.InputSystem.InputBindingComposite[] UnityEngine.InputSystem.InputActionState::composites
	InputBindingCompositeU5BU5D_tC0DEE76C22B9127A27E52487A6F2F5F9CCF483B6* ___composites_5;
	// System.Int32 UnityEngine.InputSystem.InputActionState::totalProcessorCount
	int32_t ___totalProcessorCount_6;
	// UnityEngine.InputSystem.InputActionState/UnmanagedMemory UnityEngine.InputSystem.InputActionState::memory
	UnmanagedMemory_t5E2A7F3AF355F7A20E8D8B3D7BE388511DD17C16  ___memory_7;
	// System.Boolean UnityEngine.InputSystem.InputActionState::m_OnBeforeUpdateHooked
	bool ___m_OnBeforeUpdateHooked_8;
	// System.Boolean UnityEngine.InputSystem.InputActionState::m_OnAfterUpdateHooked
	bool ___m_OnAfterUpdateHooked_9;
	// System.Action UnityEngine.InputSystem.InputActionState::m_OnBeforeUpdateDelegate
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___m_OnBeforeUpdateDelegate_10;
	// System.Action UnityEngine.InputSystem.InputActionState::m_OnAfterUpdateDelegate
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___m_OnAfterUpdateDelegate_11;

public:
	inline static int32_t get_offset_of_maps_1() { return static_cast<int32_t>(offsetof(InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C, ___maps_1)); }
	inline InputActionMapU5BU5D_tEE6F572B03BE03B28C7B701C19AE4FD4D5D2E580* get_maps_1() const { return ___maps_1; }
	inline InputActionMapU5BU5D_tEE6F572B03BE03B28C7B701C19AE4FD4D5D2E580** get_address_of_maps_1() { return &___maps_1; }
	inline void set_maps_1(InputActionMapU5BU5D_tEE6F572B03BE03B28C7B701C19AE4FD4D5D2E580* value)
	{
		___maps_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___maps_1), (void*)value);
	}

	inline static int32_t get_offset_of_controls_2() { return static_cast<int32_t>(offsetof(InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C, ___controls_2)); }
	inline InputControlU5BU5D_t4668D8411829C981759512FED1506B20F902CA67* get_controls_2() const { return ___controls_2; }
	inline InputControlU5BU5D_t4668D8411829C981759512FED1506B20F902CA67** get_address_of_controls_2() { return &___controls_2; }
	inline void set_controls_2(InputControlU5BU5D_t4668D8411829C981759512FED1506B20F902CA67* value)
	{
		___controls_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___controls_2), (void*)value);
	}

	inline static int32_t get_offset_of_interactions_3() { return static_cast<int32_t>(offsetof(InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C, ___interactions_3)); }
	inline IInputInteractionU5BU5D_tC7710034CF7D51B96E7C1E12F3C7E64AB0B6C3E8* get_interactions_3() const { return ___interactions_3; }
	inline IInputInteractionU5BU5D_tC7710034CF7D51B96E7C1E12F3C7E64AB0B6C3E8** get_address_of_interactions_3() { return &___interactions_3; }
	inline void set_interactions_3(IInputInteractionU5BU5D_tC7710034CF7D51B96E7C1E12F3C7E64AB0B6C3E8* value)
	{
		___interactions_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___interactions_3), (void*)value);
	}

	inline static int32_t get_offset_of_processors_4() { return static_cast<int32_t>(offsetof(InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C, ___processors_4)); }
	inline InputProcessorU5BU5D_tC2B97EE395F9CFBD17046BD90DB5A0A3B46EB9D3* get_processors_4() const { return ___processors_4; }
	inline InputProcessorU5BU5D_tC2B97EE395F9CFBD17046BD90DB5A0A3B46EB9D3** get_address_of_processors_4() { return &___processors_4; }
	inline void set_processors_4(InputProcessorU5BU5D_tC2B97EE395F9CFBD17046BD90DB5A0A3B46EB9D3* value)
	{
		___processors_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___processors_4), (void*)value);
	}

	inline static int32_t get_offset_of_composites_5() { return static_cast<int32_t>(offsetof(InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C, ___composites_5)); }
	inline InputBindingCompositeU5BU5D_tC0DEE76C22B9127A27E52487A6F2F5F9CCF483B6* get_composites_5() const { return ___composites_5; }
	inline InputBindingCompositeU5BU5D_tC0DEE76C22B9127A27E52487A6F2F5F9CCF483B6** get_address_of_composites_5() { return &___composites_5; }
	inline void set_composites_5(InputBindingCompositeU5BU5D_tC0DEE76C22B9127A27E52487A6F2F5F9CCF483B6* value)
	{
		___composites_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___composites_5), (void*)value);
	}

	inline static int32_t get_offset_of_totalProcessorCount_6() { return static_cast<int32_t>(offsetof(InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C, ___totalProcessorCount_6)); }
	inline int32_t get_totalProcessorCount_6() const { return ___totalProcessorCount_6; }
	inline int32_t* get_address_of_totalProcessorCount_6() { return &___totalProcessorCount_6; }
	inline void set_totalProcessorCount_6(int32_t value)
	{
		___totalProcessorCount_6 = value;
	}

	inline static int32_t get_offset_of_memory_7() { return static_cast<int32_t>(offsetof(InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C, ___memory_7)); }
	inline UnmanagedMemory_t5E2A7F3AF355F7A20E8D8B3D7BE388511DD17C16  get_memory_7() const { return ___memory_7; }
	inline UnmanagedMemory_t5E2A7F3AF355F7A20E8D8B3D7BE388511DD17C16 * get_address_of_memory_7() { return &___memory_7; }
	inline void set_memory_7(UnmanagedMemory_t5E2A7F3AF355F7A20E8D8B3D7BE388511DD17C16  value)
	{
		___memory_7 = value;
	}

	inline static int32_t get_offset_of_m_OnBeforeUpdateHooked_8() { return static_cast<int32_t>(offsetof(InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C, ___m_OnBeforeUpdateHooked_8)); }
	inline bool get_m_OnBeforeUpdateHooked_8() const { return ___m_OnBeforeUpdateHooked_8; }
	inline bool* get_address_of_m_OnBeforeUpdateHooked_8() { return &___m_OnBeforeUpdateHooked_8; }
	inline void set_m_OnBeforeUpdateHooked_8(bool value)
	{
		___m_OnBeforeUpdateHooked_8 = value;
	}

	inline static int32_t get_offset_of_m_OnAfterUpdateHooked_9() { return static_cast<int32_t>(offsetof(InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C, ___m_OnAfterUpdateHooked_9)); }
	inline bool get_m_OnAfterUpdateHooked_9() const { return ___m_OnAfterUpdateHooked_9; }
	inline bool* get_address_of_m_OnAfterUpdateHooked_9() { return &___m_OnAfterUpdateHooked_9; }
	inline void set_m_OnAfterUpdateHooked_9(bool value)
	{
		___m_OnAfterUpdateHooked_9 = value;
	}

	inline static int32_t get_offset_of_m_OnBeforeUpdateDelegate_10() { return static_cast<int32_t>(offsetof(InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C, ___m_OnBeforeUpdateDelegate_10)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_m_OnBeforeUpdateDelegate_10() const { return ___m_OnBeforeUpdateDelegate_10; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_m_OnBeforeUpdateDelegate_10() { return &___m_OnBeforeUpdateDelegate_10; }
	inline void set_m_OnBeforeUpdateDelegate_10(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___m_OnBeforeUpdateDelegate_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnBeforeUpdateDelegate_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnAfterUpdateDelegate_11() { return static_cast<int32_t>(offsetof(InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C, ___m_OnAfterUpdateDelegate_11)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_m_OnAfterUpdateDelegate_11() const { return ___m_OnAfterUpdateDelegate_11; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_m_OnAfterUpdateDelegate_11() { return &___m_OnAfterUpdateDelegate_11; }
	inline void set_m_OnAfterUpdateDelegate_11(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___m_OnAfterUpdateDelegate_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnAfterUpdateDelegate_11), (void*)value);
	}
};

struct InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C_StaticFields
{
public:
	// UnityEngine.InputSystem.Utilities.InlinedArray`1<System.Runtime.InteropServices.GCHandle> UnityEngine.InputSystem.InputActionState::s_GlobalList
	InlinedArray_1_tA3967E760933F5DC1FD6C0934D223A5ACA0A2723  ___s_GlobalList_12;
	// UnityEngine.InputSystem.Utilities.InlinedArray`1<System.Action`2<System.Object,UnityEngine.InputSystem.InputActionChange>> UnityEngine.InputSystem.InputActionState::s_OnActionChange
	InlinedArray_1_t6ABF81F82684BB96455201090C17BCA8242E85BD  ___s_OnActionChange_13;
	// UnityEngine.InputSystem.Utilities.InlinedArray`1<System.Action`1<System.Object>> UnityEngine.InputSystem.InputActionState::s_OnActionControlsChanged
	InlinedArray_1_tCA120CEA444FAF14E58313503668909AB8450BB2  ___s_OnActionControlsChanged_14;

public:
	inline static int32_t get_offset_of_s_GlobalList_12() { return static_cast<int32_t>(offsetof(InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C_StaticFields, ___s_GlobalList_12)); }
	inline InlinedArray_1_tA3967E760933F5DC1FD6C0934D223A5ACA0A2723  get_s_GlobalList_12() const { return ___s_GlobalList_12; }
	inline InlinedArray_1_tA3967E760933F5DC1FD6C0934D223A5ACA0A2723 * get_address_of_s_GlobalList_12() { return &___s_GlobalList_12; }
	inline void set_s_GlobalList_12(InlinedArray_1_tA3967E760933F5DC1FD6C0934D223A5ACA0A2723  value)
	{
		___s_GlobalList_12 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___s_GlobalList_12))->___additionalValues_2), (void*)NULL);
	}

	inline static int32_t get_offset_of_s_OnActionChange_13() { return static_cast<int32_t>(offsetof(InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C_StaticFields, ___s_OnActionChange_13)); }
	inline InlinedArray_1_t6ABF81F82684BB96455201090C17BCA8242E85BD  get_s_OnActionChange_13() const { return ___s_OnActionChange_13; }
	inline InlinedArray_1_t6ABF81F82684BB96455201090C17BCA8242E85BD * get_address_of_s_OnActionChange_13() { return &___s_OnActionChange_13; }
	inline void set_s_OnActionChange_13(InlinedArray_1_t6ABF81F82684BB96455201090C17BCA8242E85BD  value)
	{
		___s_OnActionChange_13 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___s_OnActionChange_13))->___firstValue_1), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___s_OnActionChange_13))->___additionalValues_2), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_s_OnActionControlsChanged_14() { return static_cast<int32_t>(offsetof(InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C_StaticFields, ___s_OnActionControlsChanged_14)); }
	inline InlinedArray_1_tCA120CEA444FAF14E58313503668909AB8450BB2  get_s_OnActionControlsChanged_14() const { return ___s_OnActionControlsChanged_14; }
	inline InlinedArray_1_tCA120CEA444FAF14E58313503668909AB8450BB2 * get_address_of_s_OnActionControlsChanged_14() { return &___s_OnActionControlsChanged_14; }
	inline void set_s_OnActionControlsChanged_14(InlinedArray_1_tCA120CEA444FAF14E58313503668909AB8450BB2  value)
	{
		___s_OnActionControlsChanged_14 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___s_OnActionControlsChanged_14))->___firstValue_1), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___s_OnActionControlsChanged_14))->___additionalValues_2), (void*)NULL);
		#endif
	}
};


// UnityEngine.InputSystem.InputBinding
struct InputBinding_t18B68FF9E7C08763BFC653E20E79B63389CF80CD 
{
public:
	// System.String UnityEngine.InputSystem.InputBinding::m_Name
	String_t* ___m_Name_2;
	// System.String UnityEngine.InputSystem.InputBinding::m_Id
	String_t* ___m_Id_3;
	// System.String UnityEngine.InputSystem.InputBinding::m_Path
	String_t* ___m_Path_4;
	// System.String UnityEngine.InputSystem.InputBinding::m_Interactions
	String_t* ___m_Interactions_5;
	// System.String UnityEngine.InputSystem.InputBinding::m_Processors
	String_t* ___m_Processors_6;
	// System.String UnityEngine.InputSystem.InputBinding::m_Groups
	String_t* ___m_Groups_7;
	// System.String UnityEngine.InputSystem.InputBinding::m_Action
	String_t* ___m_Action_8;
	// UnityEngine.InputSystem.InputBinding/Flags UnityEngine.InputSystem.InputBinding::m_Flags
	int32_t ___m_Flags_9;
	// System.String UnityEngine.InputSystem.InputBinding::m_OverridePath
	String_t* ___m_OverridePath_10;
	// System.String UnityEngine.InputSystem.InputBinding::m_OverrideInteractions
	String_t* ___m_OverrideInteractions_11;
	// System.String UnityEngine.InputSystem.InputBinding::m_OverrideProcessors
	String_t* ___m_OverrideProcessors_12;

public:
	inline static int32_t get_offset_of_m_Name_2() { return static_cast<int32_t>(offsetof(InputBinding_t18B68FF9E7C08763BFC653E20E79B63389CF80CD, ___m_Name_2)); }
	inline String_t* get_m_Name_2() const { return ___m_Name_2; }
	inline String_t** get_address_of_m_Name_2() { return &___m_Name_2; }
	inline void set_m_Name_2(String_t* value)
	{
		___m_Name_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Name_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_Id_3() { return static_cast<int32_t>(offsetof(InputBinding_t18B68FF9E7C08763BFC653E20E79B63389CF80CD, ___m_Id_3)); }
	inline String_t* get_m_Id_3() const { return ___m_Id_3; }
	inline String_t** get_address_of_m_Id_3() { return &___m_Id_3; }
	inline void set_m_Id_3(String_t* value)
	{
		___m_Id_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Id_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_Path_4() { return static_cast<int32_t>(offsetof(InputBinding_t18B68FF9E7C08763BFC653E20E79B63389CF80CD, ___m_Path_4)); }
	inline String_t* get_m_Path_4() const { return ___m_Path_4; }
	inline String_t** get_address_of_m_Path_4() { return &___m_Path_4; }
	inline void set_m_Path_4(String_t* value)
	{
		___m_Path_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Path_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_Interactions_5() { return static_cast<int32_t>(offsetof(InputBinding_t18B68FF9E7C08763BFC653E20E79B63389CF80CD, ___m_Interactions_5)); }
	inline String_t* get_m_Interactions_5() const { return ___m_Interactions_5; }
	inline String_t** get_address_of_m_Interactions_5() { return &___m_Interactions_5; }
	inline void set_m_Interactions_5(String_t* value)
	{
		___m_Interactions_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Interactions_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_Processors_6() { return static_cast<int32_t>(offsetof(InputBinding_t18B68FF9E7C08763BFC653E20E79B63389CF80CD, ___m_Processors_6)); }
	inline String_t* get_m_Processors_6() const { return ___m_Processors_6; }
	inline String_t** get_address_of_m_Processors_6() { return &___m_Processors_6; }
	inline void set_m_Processors_6(String_t* value)
	{
		___m_Processors_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Processors_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Groups_7() { return static_cast<int32_t>(offsetof(InputBinding_t18B68FF9E7C08763BFC653E20E79B63389CF80CD, ___m_Groups_7)); }
	inline String_t* get_m_Groups_7() const { return ___m_Groups_7; }
	inline String_t** get_address_of_m_Groups_7() { return &___m_Groups_7; }
	inline void set_m_Groups_7(String_t* value)
	{
		___m_Groups_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Groups_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_Action_8() { return static_cast<int32_t>(offsetof(InputBinding_t18B68FF9E7C08763BFC653E20E79B63389CF80CD, ___m_Action_8)); }
	inline String_t* get_m_Action_8() const { return ___m_Action_8; }
	inline String_t** get_address_of_m_Action_8() { return &___m_Action_8; }
	inline void set_m_Action_8(String_t* value)
	{
		___m_Action_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Action_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_Flags_9() { return static_cast<int32_t>(offsetof(InputBinding_t18B68FF9E7C08763BFC653E20E79B63389CF80CD, ___m_Flags_9)); }
	inline int32_t get_m_Flags_9() const { return ___m_Flags_9; }
	inline int32_t* get_address_of_m_Flags_9() { return &___m_Flags_9; }
	inline void set_m_Flags_9(int32_t value)
	{
		___m_Flags_9 = value;
	}

	inline static int32_t get_offset_of_m_OverridePath_10() { return static_cast<int32_t>(offsetof(InputBinding_t18B68FF9E7C08763BFC653E20E79B63389CF80CD, ___m_OverridePath_10)); }
	inline String_t* get_m_OverridePath_10() const { return ___m_OverridePath_10; }
	inline String_t** get_address_of_m_OverridePath_10() { return &___m_OverridePath_10; }
	inline void set_m_OverridePath_10(String_t* value)
	{
		___m_OverridePath_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OverridePath_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_OverrideInteractions_11() { return static_cast<int32_t>(offsetof(InputBinding_t18B68FF9E7C08763BFC653E20E79B63389CF80CD, ___m_OverrideInteractions_11)); }
	inline String_t* get_m_OverrideInteractions_11() const { return ___m_OverrideInteractions_11; }
	inline String_t** get_address_of_m_OverrideInteractions_11() { return &___m_OverrideInteractions_11; }
	inline void set_m_OverrideInteractions_11(String_t* value)
	{
		___m_OverrideInteractions_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OverrideInteractions_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_OverrideProcessors_12() { return static_cast<int32_t>(offsetof(InputBinding_t18B68FF9E7C08763BFC653E20E79B63389CF80CD, ___m_OverrideProcessors_12)); }
	inline String_t* get_m_OverrideProcessors_12() const { return ___m_OverrideProcessors_12; }
	inline String_t** get_address_of_m_OverrideProcessors_12() { return &___m_OverrideProcessors_12; }
	inline void set_m_OverrideProcessors_12(String_t* value)
	{
		___m_OverrideProcessors_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OverrideProcessors_12), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.InputBinding
struct InputBinding_t18B68FF9E7C08763BFC653E20E79B63389CF80CD_marshaled_pinvoke
{
	char* ___m_Name_2;
	char* ___m_Id_3;
	char* ___m_Path_4;
	char* ___m_Interactions_5;
	char* ___m_Processors_6;
	char* ___m_Groups_7;
	char* ___m_Action_8;
	int32_t ___m_Flags_9;
	char* ___m_OverridePath_10;
	char* ___m_OverrideInteractions_11;
	char* ___m_OverrideProcessors_12;
};
// Native definition for COM marshalling of UnityEngine.InputSystem.InputBinding
struct InputBinding_t18B68FF9E7C08763BFC653E20E79B63389CF80CD_marshaled_com
{
	Il2CppChar* ___m_Name_2;
	Il2CppChar* ___m_Id_3;
	Il2CppChar* ___m_Path_4;
	Il2CppChar* ___m_Interactions_5;
	Il2CppChar* ___m_Processors_6;
	Il2CppChar* ___m_Groups_7;
	Il2CppChar* ___m_Action_8;
	int32_t ___m_Flags_9;
	Il2CppChar* ___m_OverridePath_10;
	Il2CppChar* ___m_OverrideInteractions_11;
	Il2CppChar* ___m_OverrideProcessors_12;
};

// UnityEngine.InputSystem.Layouts.InputControlLayout
struct InputControlLayout_t012E8BC3C93CE8A2A35BFC6A70C52530FFB55917  : public RuntimeObject
{
public:
	// UnityEngine.InputSystem.Utilities.InternedString UnityEngine.InputSystem.Layouts.InputControlLayout::m_Name
	InternedString_tD1138602E8B7EA0F5B4851812B13C7E0551E25C8  ___m_Name_2;
	// System.Type UnityEngine.InputSystem.Layouts.InputControlLayout::m_Type
	Type_t * ___m_Type_3;
	// UnityEngine.InputSystem.Utilities.InternedString UnityEngine.InputSystem.Layouts.InputControlLayout::m_Variants
	InternedString_tD1138602E8B7EA0F5B4851812B13C7E0551E25C8  ___m_Variants_4;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.Layouts.InputControlLayout::m_StateFormat
	FourCC_tB18951639903964A6B5533C4BAFD8DA17AA8FD9D  ___m_StateFormat_5;
	// System.Int32 UnityEngine.InputSystem.Layouts.InputControlLayout::m_StateSizeInBytes
	int32_t ___m_StateSizeInBytes_6;
	// System.Nullable`1<System.Boolean> UnityEngine.InputSystem.Layouts.InputControlLayout::m_UpdateBeforeRender
	Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3  ___m_UpdateBeforeRender_7;
	// UnityEngine.InputSystem.Utilities.InlinedArray`1<UnityEngine.InputSystem.Utilities.InternedString> UnityEngine.InputSystem.Layouts.InputControlLayout::m_BaseLayouts
	InlinedArray_1_t7B73159E4B6A8AD12C99ED22A9138E32D7408FA0  ___m_BaseLayouts_8;
	// UnityEngine.InputSystem.Utilities.InlinedArray`1<UnityEngine.InputSystem.Utilities.InternedString> UnityEngine.InputSystem.Layouts.InputControlLayout::m_AppliedOverrides
	InlinedArray_1_t7B73159E4B6A8AD12C99ED22A9138E32D7408FA0  ___m_AppliedOverrides_9;
	// UnityEngine.InputSystem.Utilities.InternedString[] UnityEngine.InputSystem.Layouts.InputControlLayout::m_CommonUsages
	InternedStringU5BU5D_t1B3BD9ED90129E67B58E1681B1944E72F8E0E648* ___m_CommonUsages_10;
	// UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem[] UnityEngine.InputSystem.Layouts.InputControlLayout::m_Controls
	ControlItemU5BU5D_t5AFF1520667DFF9AA06E17887A3D0B77BEB173BB* ___m_Controls_11;
	// System.String UnityEngine.InputSystem.Layouts.InputControlLayout::m_DisplayName
	String_t* ___m_DisplayName_12;
	// System.String UnityEngine.InputSystem.Layouts.InputControlLayout::m_Description
	String_t* ___m_Description_13;
	// UnityEngine.InputSystem.Layouts.InputControlLayout/Flags UnityEngine.InputSystem.Layouts.InputControlLayout::m_Flags
	int32_t ___m_Flags_14;

public:
	inline static int32_t get_offset_of_m_Name_2() { return static_cast<int32_t>(offsetof(InputControlLayout_t012E8BC3C93CE8A2A35BFC6A70C52530FFB55917, ___m_Name_2)); }
	inline InternedString_tD1138602E8B7EA0F5B4851812B13C7E0551E25C8  get_m_Name_2() const { return ___m_Name_2; }
	inline InternedString_tD1138602E8B7EA0F5B4851812B13C7E0551E25C8 * get_address_of_m_Name_2() { return &___m_Name_2; }
	inline void set_m_Name_2(InternedString_tD1138602E8B7EA0F5B4851812B13C7E0551E25C8  value)
	{
		___m_Name_2 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Name_2))->___m_StringOriginalCase_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Name_2))->___m_StringLowerCase_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_Type_3() { return static_cast<int32_t>(offsetof(InputControlLayout_t012E8BC3C93CE8A2A35BFC6A70C52530FFB55917, ___m_Type_3)); }
	inline Type_t * get_m_Type_3() const { return ___m_Type_3; }
	inline Type_t ** get_address_of_m_Type_3() { return &___m_Type_3; }
	inline void set_m_Type_3(Type_t * value)
	{
		___m_Type_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_Variants_4() { return static_cast<int32_t>(offsetof(InputControlLayout_t012E8BC3C93CE8A2A35BFC6A70C52530FFB55917, ___m_Variants_4)); }
	inline InternedString_tD1138602E8B7EA0F5B4851812B13C7E0551E25C8  get_m_Variants_4() const { return ___m_Variants_4; }
	inline InternedString_tD1138602E8B7EA0F5B4851812B13C7E0551E25C8 * get_address_of_m_Variants_4() { return &___m_Variants_4; }
	inline void set_m_Variants_4(InternedString_tD1138602E8B7EA0F5B4851812B13C7E0551E25C8  value)
	{
		___m_Variants_4 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Variants_4))->___m_StringOriginalCase_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Variants_4))->___m_StringLowerCase_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_StateFormat_5() { return static_cast<int32_t>(offsetof(InputControlLayout_t012E8BC3C93CE8A2A35BFC6A70C52530FFB55917, ___m_StateFormat_5)); }
	inline FourCC_tB18951639903964A6B5533C4BAFD8DA17AA8FD9D  get_m_StateFormat_5() const { return ___m_StateFormat_5; }
	inline FourCC_tB18951639903964A6B5533C4BAFD8DA17AA8FD9D * get_address_of_m_StateFormat_5() { return &___m_StateFormat_5; }
	inline void set_m_StateFormat_5(FourCC_tB18951639903964A6B5533C4BAFD8DA17AA8FD9D  value)
	{
		___m_StateFormat_5 = value;
	}

	inline static int32_t get_offset_of_m_StateSizeInBytes_6() { return static_cast<int32_t>(offsetof(InputControlLayout_t012E8BC3C93CE8A2A35BFC6A70C52530FFB55917, ___m_StateSizeInBytes_6)); }
	inline int32_t get_m_StateSizeInBytes_6() const { return ___m_StateSizeInBytes_6; }
	inline int32_t* get_address_of_m_StateSizeInBytes_6() { return &___m_StateSizeInBytes_6; }
	inline void set_m_StateSizeInBytes_6(int32_t value)
	{
		___m_StateSizeInBytes_6 = value;
	}

	inline static int32_t get_offset_of_m_UpdateBeforeRender_7() { return static_cast<int32_t>(offsetof(InputControlLayout_t012E8BC3C93CE8A2A35BFC6A70C52530FFB55917, ___m_UpdateBeforeRender_7)); }
	inline Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3  get_m_UpdateBeforeRender_7() const { return ___m_UpdateBeforeRender_7; }
	inline Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3 * get_address_of_m_UpdateBeforeRender_7() { return &___m_UpdateBeforeRender_7; }
	inline void set_m_UpdateBeforeRender_7(Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3  value)
	{
		___m_UpdateBeforeRender_7 = value;
	}

	inline static int32_t get_offset_of_m_BaseLayouts_8() { return static_cast<int32_t>(offsetof(InputControlLayout_t012E8BC3C93CE8A2A35BFC6A70C52530FFB55917, ___m_BaseLayouts_8)); }
	inline InlinedArray_1_t7B73159E4B6A8AD12C99ED22A9138E32D7408FA0  get_m_BaseLayouts_8() const { return ___m_BaseLayouts_8; }
	inline InlinedArray_1_t7B73159E4B6A8AD12C99ED22A9138E32D7408FA0 * get_address_of_m_BaseLayouts_8() { return &___m_BaseLayouts_8; }
	inline void set_m_BaseLayouts_8(InlinedArray_1_t7B73159E4B6A8AD12C99ED22A9138E32D7408FA0  value)
	{
		___m_BaseLayouts_8 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_BaseLayouts_8))->___firstValue_1))->___m_StringOriginalCase_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_BaseLayouts_8))->___firstValue_1))->___m_StringLowerCase_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_BaseLayouts_8))->___additionalValues_2), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_AppliedOverrides_9() { return static_cast<int32_t>(offsetof(InputControlLayout_t012E8BC3C93CE8A2A35BFC6A70C52530FFB55917, ___m_AppliedOverrides_9)); }
	inline InlinedArray_1_t7B73159E4B6A8AD12C99ED22A9138E32D7408FA0  get_m_AppliedOverrides_9() const { return ___m_AppliedOverrides_9; }
	inline InlinedArray_1_t7B73159E4B6A8AD12C99ED22A9138E32D7408FA0 * get_address_of_m_AppliedOverrides_9() { return &___m_AppliedOverrides_9; }
	inline void set_m_AppliedOverrides_9(InlinedArray_1_t7B73159E4B6A8AD12C99ED22A9138E32D7408FA0  value)
	{
		___m_AppliedOverrides_9 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_AppliedOverrides_9))->___firstValue_1))->___m_StringOriginalCase_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_AppliedOverrides_9))->___firstValue_1))->___m_StringLowerCase_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_AppliedOverrides_9))->___additionalValues_2), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_CommonUsages_10() { return static_cast<int32_t>(offsetof(InputControlLayout_t012E8BC3C93CE8A2A35BFC6A70C52530FFB55917, ___m_CommonUsages_10)); }
	inline InternedStringU5BU5D_t1B3BD9ED90129E67B58E1681B1944E72F8E0E648* get_m_CommonUsages_10() const { return ___m_CommonUsages_10; }
	inline InternedStringU5BU5D_t1B3BD9ED90129E67B58E1681B1944E72F8E0E648** get_address_of_m_CommonUsages_10() { return &___m_CommonUsages_10; }
	inline void set_m_CommonUsages_10(InternedStringU5BU5D_t1B3BD9ED90129E67B58E1681B1944E72F8E0E648* value)
	{
		___m_CommonUsages_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CommonUsages_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_Controls_11() { return static_cast<int32_t>(offsetof(InputControlLayout_t012E8BC3C93CE8A2A35BFC6A70C52530FFB55917, ___m_Controls_11)); }
	inline ControlItemU5BU5D_t5AFF1520667DFF9AA06E17887A3D0B77BEB173BB* get_m_Controls_11() const { return ___m_Controls_11; }
	inline ControlItemU5BU5D_t5AFF1520667DFF9AA06E17887A3D0B77BEB173BB** get_address_of_m_Controls_11() { return &___m_Controls_11; }
	inline void set_m_Controls_11(ControlItemU5BU5D_t5AFF1520667DFF9AA06E17887A3D0B77BEB173BB* value)
	{
		___m_Controls_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Controls_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisplayName_12() { return static_cast<int32_t>(offsetof(InputControlLayout_t012E8BC3C93CE8A2A35BFC6A70C52530FFB55917, ___m_DisplayName_12)); }
	inline String_t* get_m_DisplayName_12() const { return ___m_DisplayName_12; }
	inline String_t** get_address_of_m_DisplayName_12() { return &___m_DisplayName_12; }
	inline void set_m_DisplayName_12(String_t* value)
	{
		___m_DisplayName_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DisplayName_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_Description_13() { return static_cast<int32_t>(offsetof(InputControlLayout_t012E8BC3C93CE8A2A35BFC6A70C52530FFB55917, ___m_Description_13)); }
	inline String_t* get_m_Description_13() const { return ___m_Description_13; }
	inline String_t** get_address_of_m_Description_13() { return &___m_Description_13; }
	inline void set_m_Description_13(String_t* value)
	{
		___m_Description_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Description_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_Flags_14() { return static_cast<int32_t>(offsetof(InputControlLayout_t012E8BC3C93CE8A2A35BFC6A70C52530FFB55917, ___m_Flags_14)); }
	inline int32_t get_m_Flags_14() const { return ___m_Flags_14; }
	inline int32_t* get_address_of_m_Flags_14() { return &___m_Flags_14; }
	inline void set_m_Flags_14(int32_t value)
	{
		___m_Flags_14 = value;
	}
};

struct InputControlLayout_t012E8BC3C93CE8A2A35BFC6A70C52530FFB55917_StaticFields
{
public:
	// UnityEngine.InputSystem.Utilities.InternedString UnityEngine.InputSystem.Layouts.InputControlLayout::s_DefaultVariant
	InternedString_tD1138602E8B7EA0F5B4851812B13C7E0551E25C8  ___s_DefaultVariant_0;
	// UnityEngine.InputSystem.Layouts.InputControlLayout/Collection UnityEngine.InputSystem.Layouts.InputControlLayout::s_Layouts
	Collection_tDBA0BC36ACE448001ABFFED32EF785467D714D7B  ___s_Layouts_15;
	// UnityEngine.InputSystem.Layouts.InputControlLayout/Cache UnityEngine.InputSystem.Layouts.InputControlLayout::s_CacheInstance
	Cache_t9F49740128E8F04886120C99F392DC7CEBC6AA67  ___s_CacheInstance_16;
	// System.Int32 UnityEngine.InputSystem.Layouts.InputControlLayout::s_CacheInstanceRef
	int32_t ___s_CacheInstanceRef_17;

public:
	inline static int32_t get_offset_of_s_DefaultVariant_0() { return static_cast<int32_t>(offsetof(InputControlLayout_t012E8BC3C93CE8A2A35BFC6A70C52530FFB55917_StaticFields, ___s_DefaultVariant_0)); }
	inline InternedString_tD1138602E8B7EA0F5B4851812B13C7E0551E25C8  get_s_DefaultVariant_0() const { return ___s_DefaultVariant_0; }
	inline InternedString_tD1138602E8B7EA0F5B4851812B13C7E0551E25C8 * get_address_of_s_DefaultVariant_0() { return &___s_DefaultVariant_0; }
	inline void set_s_DefaultVariant_0(InternedString_tD1138602E8B7EA0F5B4851812B13C7E0551E25C8  value)
	{
		___s_DefaultVariant_0 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___s_DefaultVariant_0))->___m_StringOriginalCase_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___s_DefaultVariant_0))->___m_StringLowerCase_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_s_Layouts_15() { return static_cast<int32_t>(offsetof(InputControlLayout_t012E8BC3C93CE8A2A35BFC6A70C52530FFB55917_StaticFields, ___s_Layouts_15)); }
	inline Collection_tDBA0BC36ACE448001ABFFED32EF785467D714D7B  get_s_Layouts_15() const { return ___s_Layouts_15; }
	inline Collection_tDBA0BC36ACE448001ABFFED32EF785467D714D7B * get_address_of_s_Layouts_15() { return &___s_Layouts_15; }
	inline void set_s_Layouts_15(Collection_tDBA0BC36ACE448001ABFFED32EF785467D714D7B  value)
	{
		___s_Layouts_15 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___s_Layouts_15))->___layoutTypes_1), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___s_Layouts_15))->___layoutStrings_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___s_Layouts_15))->___layoutBuilders_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___s_Layouts_15))->___baseLayoutTable_4), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___s_Layouts_15))->___layoutOverrides_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___s_Layouts_15))->___layoutOverrideNames_6), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___s_Layouts_15))->___layoutMatchers_7), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_s_CacheInstance_16() { return static_cast<int32_t>(offsetof(InputControlLayout_t012E8BC3C93CE8A2A35BFC6A70C52530FFB55917_StaticFields, ___s_CacheInstance_16)); }
	inline Cache_t9F49740128E8F04886120C99F392DC7CEBC6AA67  get_s_CacheInstance_16() const { return ___s_CacheInstance_16; }
	inline Cache_t9F49740128E8F04886120C99F392DC7CEBC6AA67 * get_address_of_s_CacheInstance_16() { return &___s_CacheInstance_16; }
	inline void set_s_CacheInstance_16(Cache_t9F49740128E8F04886120C99F392DC7CEBC6AA67  value)
	{
		___s_CacheInstance_16 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___s_CacheInstance_16))->___table_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_s_CacheInstanceRef_17() { return static_cast<int32_t>(offsetof(InputControlLayout_t012E8BC3C93CE8A2A35BFC6A70C52530FFB55917_StaticFields, ___s_CacheInstanceRef_17)); }
	inline int32_t get_s_CacheInstanceRef_17() const { return ___s_CacheInstanceRef_17; }
	inline int32_t* get_address_of_s_CacheInstanceRef_17() { return &___s_CacheInstanceRef_17; }
	inline void set_s_CacheInstanceRef_17(int32_t value)
	{
		___s_CacheInstanceRef_17 = value;
	}
};


// System.SystemException
struct SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62  : public Exception_t
{
public:

public:
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};


// UniGLTF.glTFAccessor
struct glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC  : public JsonSerializableBase_t40934AAB77C4330D240EB9E990699A5DA3EF5C8F
{
public:
	// System.Int32 UniGLTF.glTFAccessor::bufferView
	int32_t ___bufferView_0;
	// System.Int32 UniGLTF.glTFAccessor::byteOffset
	int32_t ___byteOffset_1;
	// System.String UniGLTF.glTFAccessor::type
	String_t* ___type_2;
	// UniGLTF.glComponentType UniGLTF.glTFAccessor::componentType
	int32_t ___componentType_3;
	// System.Int32 UniGLTF.glTFAccessor::count
	int32_t ___count_4;
	// System.Single[] UniGLTF.glTFAccessor::max
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___max_5;
	// System.Single[] UniGLTF.glTFAccessor::min
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___min_6;
	// System.Boolean UniGLTF.glTFAccessor::normalized
	bool ___normalized_7;
	// UniGLTF.glTFSparse UniGLTF.glTFAccessor::sparse
	glTFSparse_tF8621187A1982375A902F6D7B023E40B72886684 * ___sparse_8;
	// System.String UniGLTF.glTFAccessor::name
	String_t* ___name_9;
	// System.Object UniGLTF.glTFAccessor::extensions
	RuntimeObject * ___extensions_10;
	// System.Object UniGLTF.glTFAccessor::extras
	RuntimeObject * ___extras_11;

public:
	inline static int32_t get_offset_of_bufferView_0() { return static_cast<int32_t>(offsetof(glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC, ___bufferView_0)); }
	inline int32_t get_bufferView_0() const { return ___bufferView_0; }
	inline int32_t* get_address_of_bufferView_0() { return &___bufferView_0; }
	inline void set_bufferView_0(int32_t value)
	{
		___bufferView_0 = value;
	}

	inline static int32_t get_offset_of_byteOffset_1() { return static_cast<int32_t>(offsetof(glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC, ___byteOffset_1)); }
	inline int32_t get_byteOffset_1() const { return ___byteOffset_1; }
	inline int32_t* get_address_of_byteOffset_1() { return &___byteOffset_1; }
	inline void set_byteOffset_1(int32_t value)
	{
		___byteOffset_1 = value;
	}

	inline static int32_t get_offset_of_type_2() { return static_cast<int32_t>(offsetof(glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC, ___type_2)); }
	inline String_t* get_type_2() const { return ___type_2; }
	inline String_t** get_address_of_type_2() { return &___type_2; }
	inline void set_type_2(String_t* value)
	{
		___type_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___type_2), (void*)value);
	}

	inline static int32_t get_offset_of_componentType_3() { return static_cast<int32_t>(offsetof(glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC, ___componentType_3)); }
	inline int32_t get_componentType_3() const { return ___componentType_3; }
	inline int32_t* get_address_of_componentType_3() { return &___componentType_3; }
	inline void set_componentType_3(int32_t value)
	{
		___componentType_3 = value;
	}

	inline static int32_t get_offset_of_count_4() { return static_cast<int32_t>(offsetof(glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC, ___count_4)); }
	inline int32_t get_count_4() const { return ___count_4; }
	inline int32_t* get_address_of_count_4() { return &___count_4; }
	inline void set_count_4(int32_t value)
	{
		___count_4 = value;
	}

	inline static int32_t get_offset_of_max_5() { return static_cast<int32_t>(offsetof(glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC, ___max_5)); }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* get_max_5() const { return ___max_5; }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** get_address_of_max_5() { return &___max_5; }
	inline void set_max_5(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		___max_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___max_5), (void*)value);
	}

	inline static int32_t get_offset_of_min_6() { return static_cast<int32_t>(offsetof(glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC, ___min_6)); }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* get_min_6() const { return ___min_6; }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** get_address_of_min_6() { return &___min_6; }
	inline void set_min_6(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		___min_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___min_6), (void*)value);
	}

	inline static int32_t get_offset_of_normalized_7() { return static_cast<int32_t>(offsetof(glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC, ___normalized_7)); }
	inline bool get_normalized_7() const { return ___normalized_7; }
	inline bool* get_address_of_normalized_7() { return &___normalized_7; }
	inline void set_normalized_7(bool value)
	{
		___normalized_7 = value;
	}

	inline static int32_t get_offset_of_sparse_8() { return static_cast<int32_t>(offsetof(glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC, ___sparse_8)); }
	inline glTFSparse_tF8621187A1982375A902F6D7B023E40B72886684 * get_sparse_8() const { return ___sparse_8; }
	inline glTFSparse_tF8621187A1982375A902F6D7B023E40B72886684 ** get_address_of_sparse_8() { return &___sparse_8; }
	inline void set_sparse_8(glTFSparse_tF8621187A1982375A902F6D7B023E40B72886684 * value)
	{
		___sparse_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sparse_8), (void*)value);
	}

	inline static int32_t get_offset_of_name_9() { return static_cast<int32_t>(offsetof(glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC, ___name_9)); }
	inline String_t* get_name_9() const { return ___name_9; }
	inline String_t** get_address_of_name_9() { return &___name_9; }
	inline void set_name_9(String_t* value)
	{
		___name_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_9), (void*)value);
	}

	inline static int32_t get_offset_of_extensions_10() { return static_cast<int32_t>(offsetof(glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC, ___extensions_10)); }
	inline RuntimeObject * get_extensions_10() const { return ___extensions_10; }
	inline RuntimeObject ** get_address_of_extensions_10() { return &___extensions_10; }
	inline void set_extensions_10(RuntimeObject * value)
	{
		___extensions_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___extensions_10), (void*)value);
	}

	inline static int32_t get_offset_of_extras_11() { return static_cast<int32_t>(offsetof(glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC, ___extras_11)); }
	inline RuntimeObject * get_extras_11() const { return ___extras_11; }
	inline RuntimeObject ** get_address_of_extras_11() { return &___extras_11; }
	inline void set_extras_11(RuntimeObject * value)
	{
		___extras_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___extras_11), (void*)value);
	}
};


// UniGLTF.glTFBufferView
struct glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4  : public JsonSerializableBase_t40934AAB77C4330D240EB9E990699A5DA3EF5C8F
{
public:
	// System.Int32 UniGLTF.glTFBufferView::buffer
	int32_t ___buffer_0;
	// System.Int32 UniGLTF.glTFBufferView::byteOffset
	int32_t ___byteOffset_1;
	// System.Int32 UniGLTF.glTFBufferView::byteLength
	int32_t ___byteLength_2;
	// System.Int32 UniGLTF.glTFBufferView::byteStride
	int32_t ___byteStride_3;
	// UniGLTF.glBufferTarget UniGLTF.glTFBufferView::target
	int32_t ___target_4;
	// System.Object UniGLTF.glTFBufferView::extensions
	RuntimeObject * ___extensions_5;
	// System.Object UniGLTF.glTFBufferView::extras
	RuntimeObject * ___extras_6;
	// System.String UniGLTF.glTFBufferView::name
	String_t* ___name_7;

public:
	inline static int32_t get_offset_of_buffer_0() { return static_cast<int32_t>(offsetof(glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4, ___buffer_0)); }
	inline int32_t get_buffer_0() const { return ___buffer_0; }
	inline int32_t* get_address_of_buffer_0() { return &___buffer_0; }
	inline void set_buffer_0(int32_t value)
	{
		___buffer_0 = value;
	}

	inline static int32_t get_offset_of_byteOffset_1() { return static_cast<int32_t>(offsetof(glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4, ___byteOffset_1)); }
	inline int32_t get_byteOffset_1() const { return ___byteOffset_1; }
	inline int32_t* get_address_of_byteOffset_1() { return &___byteOffset_1; }
	inline void set_byteOffset_1(int32_t value)
	{
		___byteOffset_1 = value;
	}

	inline static int32_t get_offset_of_byteLength_2() { return static_cast<int32_t>(offsetof(glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4, ___byteLength_2)); }
	inline int32_t get_byteLength_2() const { return ___byteLength_2; }
	inline int32_t* get_address_of_byteLength_2() { return &___byteLength_2; }
	inline void set_byteLength_2(int32_t value)
	{
		___byteLength_2 = value;
	}

	inline static int32_t get_offset_of_byteStride_3() { return static_cast<int32_t>(offsetof(glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4, ___byteStride_3)); }
	inline int32_t get_byteStride_3() const { return ___byteStride_3; }
	inline int32_t* get_address_of_byteStride_3() { return &___byteStride_3; }
	inline void set_byteStride_3(int32_t value)
	{
		___byteStride_3 = value;
	}

	inline static int32_t get_offset_of_target_4() { return static_cast<int32_t>(offsetof(glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4, ___target_4)); }
	inline int32_t get_target_4() const { return ___target_4; }
	inline int32_t* get_address_of_target_4() { return &___target_4; }
	inline void set_target_4(int32_t value)
	{
		___target_4 = value;
	}

	inline static int32_t get_offset_of_extensions_5() { return static_cast<int32_t>(offsetof(glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4, ___extensions_5)); }
	inline RuntimeObject * get_extensions_5() const { return ___extensions_5; }
	inline RuntimeObject ** get_address_of_extensions_5() { return &___extensions_5; }
	inline void set_extensions_5(RuntimeObject * value)
	{
		___extensions_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___extensions_5), (void*)value);
	}

	inline static int32_t get_offset_of_extras_6() { return static_cast<int32_t>(offsetof(glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4, ___extras_6)); }
	inline RuntimeObject * get_extras_6() const { return ___extras_6; }
	inline RuntimeObject ** get_address_of_extras_6() { return &___extras_6; }
	inline void set_extras_6(RuntimeObject * value)
	{
		___extras_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___extras_6), (void*)value);
	}

	inline static int32_t get_offset_of_name_7() { return static_cast<int32_t>(offsetof(glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4, ___name_7)); }
	inline String_t* get_name_7() const { return ___name_7; }
	inline String_t** get_address_of_name_7() { return &___name_7; }
	inline void set_name_7(String_t* value)
	{
		___name_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_7), (void*)value);
	}
};


// UniGLTF.glTFSparseIndices
struct glTFSparseIndices_t332206D5F85C3918BDB6880C545D1B59EF3DE614  : public JsonSerializableBase_t40934AAB77C4330D240EB9E990699A5DA3EF5C8F
{
public:
	// System.Int32 UniGLTF.glTFSparseIndices::bufferView
	int32_t ___bufferView_0;
	// System.Int32 UniGLTF.glTFSparseIndices::byteOffset
	int32_t ___byteOffset_1;
	// UniGLTF.glComponentType UniGLTF.glTFSparseIndices::componentType
	int32_t ___componentType_2;
	// System.Object UniGLTF.glTFSparseIndices::extensions
	RuntimeObject * ___extensions_3;
	// System.Object UniGLTF.glTFSparseIndices::extras
	RuntimeObject * ___extras_4;

public:
	inline static int32_t get_offset_of_bufferView_0() { return static_cast<int32_t>(offsetof(glTFSparseIndices_t332206D5F85C3918BDB6880C545D1B59EF3DE614, ___bufferView_0)); }
	inline int32_t get_bufferView_0() const { return ___bufferView_0; }
	inline int32_t* get_address_of_bufferView_0() { return &___bufferView_0; }
	inline void set_bufferView_0(int32_t value)
	{
		___bufferView_0 = value;
	}

	inline static int32_t get_offset_of_byteOffset_1() { return static_cast<int32_t>(offsetof(glTFSparseIndices_t332206D5F85C3918BDB6880C545D1B59EF3DE614, ___byteOffset_1)); }
	inline int32_t get_byteOffset_1() const { return ___byteOffset_1; }
	inline int32_t* get_address_of_byteOffset_1() { return &___byteOffset_1; }
	inline void set_byteOffset_1(int32_t value)
	{
		___byteOffset_1 = value;
	}

	inline static int32_t get_offset_of_componentType_2() { return static_cast<int32_t>(offsetof(glTFSparseIndices_t332206D5F85C3918BDB6880C545D1B59EF3DE614, ___componentType_2)); }
	inline int32_t get_componentType_2() const { return ___componentType_2; }
	inline int32_t* get_address_of_componentType_2() { return &___componentType_2; }
	inline void set_componentType_2(int32_t value)
	{
		___componentType_2 = value;
	}

	inline static int32_t get_offset_of_extensions_3() { return static_cast<int32_t>(offsetof(glTFSparseIndices_t332206D5F85C3918BDB6880C545D1B59EF3DE614, ___extensions_3)); }
	inline RuntimeObject * get_extensions_3() const { return ___extensions_3; }
	inline RuntimeObject ** get_address_of_extensions_3() { return &___extensions_3; }
	inline void set_extensions_3(RuntimeObject * value)
	{
		___extensions_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___extensions_3), (void*)value);
	}

	inline static int32_t get_offset_of_extras_4() { return static_cast<int32_t>(offsetof(glTFSparseIndices_t332206D5F85C3918BDB6880C545D1B59EF3DE614, ___extras_4)); }
	inline RuntimeObject * get_extras_4() const { return ___extras_4; }
	inline RuntimeObject ** get_address_of_extras_4() { return &___extras_4; }
	inline void set_extras_4(RuntimeObject * value)
	{
		___extras_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___extras_4), (void*)value);
	}
};


// UniGLTF.glTFExtensions/ComponentVec
struct ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 
{
public:
	// UniGLTF.glComponentType UniGLTF.glTFExtensions/ComponentVec::ComponentType
	int32_t ___ComponentType_0;
	// System.Int32 UniGLTF.glTFExtensions/ComponentVec::ElementCount
	int32_t ___ElementCount_1;

public:
	inline static int32_t get_offset_of_ComponentType_0() { return static_cast<int32_t>(offsetof(ComponentVec_t7A8C27B684980E106027B1173391609E39091C33, ___ComponentType_0)); }
	inline int32_t get_ComponentType_0() const { return ___ComponentType_0; }
	inline int32_t* get_address_of_ComponentType_0() { return &___ComponentType_0; }
	inline void set_ComponentType_0(int32_t value)
	{
		___ComponentType_0 = value;
	}

	inline static int32_t get_offset_of_ElementCount_1() { return static_cast<int32_t>(offsetof(ComponentVec_t7A8C27B684980E106027B1173391609E39091C33, ___ElementCount_1)); }
	inline int32_t get_ElementCount_1() const { return ___ElementCount_1; }
	inline int32_t* get_address_of_ElementCount_1() { return &___ElementCount_1; }
	inline void set_ElementCount_1(int32_t value)
	{
		___ElementCount_1 = value;
	}
};


// UnityEngine.InputSystem.InputControlList`1<UnityEngine.InputSystem.InputControl>
struct InputControlList_1_tC251485A8EBE20F32C1828D6F68B450DAB732E96 
{
public:
	// System.Int32 UnityEngine.InputSystem.InputControlList`1::m_Count
	int32_t ___m_Count_0;
	// Unity.Collections.NativeArray`1<System.UInt64> UnityEngine.InputSystem.InputControlList`1::m_Indices
	NativeArray_1_t9D118727A643E61710D0A4DF5B0C8CD1A918A40B  ___m_Indices_1;
	// Unity.Collections.Allocator UnityEngine.InputSystem.InputControlList`1::m_Allocator
	int32_t ___m_Allocator_2;

public:
	inline static int32_t get_offset_of_m_Count_0() { return static_cast<int32_t>(offsetof(InputControlList_1_tC251485A8EBE20F32C1828D6F68B450DAB732E96, ___m_Count_0)); }
	inline int32_t get_m_Count_0() const { return ___m_Count_0; }
	inline int32_t* get_address_of_m_Count_0() { return &___m_Count_0; }
	inline void set_m_Count_0(int32_t value)
	{
		___m_Count_0 = value;
	}

	inline static int32_t get_offset_of_m_Indices_1() { return static_cast<int32_t>(offsetof(InputControlList_1_tC251485A8EBE20F32C1828D6F68B450DAB732E96, ___m_Indices_1)); }
	inline NativeArray_1_t9D118727A643E61710D0A4DF5B0C8CD1A918A40B  get_m_Indices_1() const { return ___m_Indices_1; }
	inline NativeArray_1_t9D118727A643E61710D0A4DF5B0C8CD1A918A40B * get_address_of_m_Indices_1() { return &___m_Indices_1; }
	inline void set_m_Indices_1(NativeArray_1_t9D118727A643E61710D0A4DF5B0C8CD1A918A40B  value)
	{
		___m_Indices_1 = value;
	}

	inline static int32_t get_offset_of_m_Allocator_2() { return static_cast<int32_t>(offsetof(InputControlList_1_tC251485A8EBE20F32C1828D6F68B450DAB732E96, ___m_Allocator_2)); }
	inline int32_t get_m_Allocator_2() const { return ___m_Allocator_2; }
	inline int32_t* get_address_of_m_Allocator_2() { return &___m_Allocator_2; }
	inline void set_m_Allocator_2(int32_t value)
	{
		___m_Allocator_2 = value;
	}
};


// System.Nullable`1<UnityEngine.InputSystem.InputBinding>
struct Nullable_1_tAF2F42FC2F7DC5A356BAC77371758E4D7339F104 
{
public:
	// T System.Nullable`1::value
	InputBinding_t18B68FF9E7C08763BFC653E20E79B63389CF80CD  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tAF2F42FC2F7DC5A356BAC77371758E4D7339F104, ___value_0)); }
	inline InputBinding_t18B68FF9E7C08763BFC653E20E79B63389CF80CD  get_value_0() const { return ___value_0; }
	inline InputBinding_t18B68FF9E7C08763BFC653E20E79B63389CF80CD * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(InputBinding_t18B68FF9E7C08763BFC653E20E79B63389CF80CD  value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_0))->___m_Name_2), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_0))->___m_Id_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_0))->___m_Path_4), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_0))->___m_Interactions_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_0))->___m_Processors_6), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_0))->___m_Groups_7), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_0))->___m_Action_8), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_0))->___m_OverridePath_10), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_0))->___m_OverrideInteractions_11), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_0))->___m_OverrideProcessors_12), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tAF2F42FC2F7DC5A356BAC77371758E4D7339F104, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};


// System.NotImplementedException
struct NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:

public:
};


// System.NotSupportedException
struct NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:

public:
};


// UnityEngine.InputSystem.InputActionRebindingExtensions/RebindingOperation
struct RebindingOperation_t5DA0F3263991E7BED360B8A1ADE014B9B451F970  : public RuntimeObject
{
public:
	// UnityEngine.InputSystem.InputAction UnityEngine.InputSystem.InputActionRebindingExtensions/RebindingOperation::m_ActionToRebind
	InputAction_tF0C77AF72E0C19608B020E77A2C36C04E9DCBE48 * ___m_ActionToRebind_1;
	// System.Nullable`1<UnityEngine.InputSystem.InputBinding> UnityEngine.InputSystem.InputActionRebindingExtensions/RebindingOperation::m_BindingMask
	Nullable_1_tAF2F42FC2F7DC5A356BAC77371758E4D7339F104  ___m_BindingMask_2;
	// System.Type UnityEngine.InputSystem.InputActionRebindingExtensions/RebindingOperation::m_ControlType
	Type_t * ___m_ControlType_3;
	// UnityEngine.InputSystem.Utilities.InternedString UnityEngine.InputSystem.InputActionRebindingExtensions/RebindingOperation::m_ExpectedLayout
	InternedString_tD1138602E8B7EA0F5B4851812B13C7E0551E25C8  ___m_ExpectedLayout_4;
	// System.Int32 UnityEngine.InputSystem.InputActionRebindingExtensions/RebindingOperation::m_IncludePathCount
	int32_t ___m_IncludePathCount_5;
	// System.String[] UnityEngine.InputSystem.InputActionRebindingExtensions/RebindingOperation::m_IncludePaths
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___m_IncludePaths_6;
	// System.Int32 UnityEngine.InputSystem.InputActionRebindingExtensions/RebindingOperation::m_ExcludePathCount
	int32_t ___m_ExcludePathCount_7;
	// System.String[] UnityEngine.InputSystem.InputActionRebindingExtensions/RebindingOperation::m_ExcludePaths
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___m_ExcludePaths_8;
	// System.Int32 UnityEngine.InputSystem.InputActionRebindingExtensions/RebindingOperation::m_TargetBindingIndex
	int32_t ___m_TargetBindingIndex_9;
	// System.String UnityEngine.InputSystem.InputActionRebindingExtensions/RebindingOperation::m_BindingGroupForNewBinding
	String_t* ___m_BindingGroupForNewBinding_10;
	// System.String UnityEngine.InputSystem.InputActionRebindingExtensions/RebindingOperation::m_CancelBinding
	String_t* ___m_CancelBinding_11;
	// System.Single UnityEngine.InputSystem.InputActionRebindingExtensions/RebindingOperation::m_MagnitudeThreshold
	float ___m_MagnitudeThreshold_12;
	// System.Single[] UnityEngine.InputSystem.InputActionRebindingExtensions/RebindingOperation::m_Scores
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___m_Scores_13;
	// System.Single[] UnityEngine.InputSystem.InputActionRebindingExtensions/RebindingOperation::m_Magnitudes
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___m_Magnitudes_14;
	// System.Double UnityEngine.InputSystem.InputActionRebindingExtensions/RebindingOperation::m_LastMatchTime
	double ___m_LastMatchTime_15;
	// System.Double UnityEngine.InputSystem.InputActionRebindingExtensions/RebindingOperation::m_StartTime
	double ___m_StartTime_16;
	// System.Single UnityEngine.InputSystem.InputActionRebindingExtensions/RebindingOperation::m_Timeout
	float ___m_Timeout_17;
	// System.Single UnityEngine.InputSystem.InputActionRebindingExtensions/RebindingOperation::m_WaitSecondsAfterMatch
	float ___m_WaitSecondsAfterMatch_18;
	// UnityEngine.InputSystem.InputControlList`1<UnityEngine.InputSystem.InputControl> UnityEngine.InputSystem.InputActionRebindingExtensions/RebindingOperation::m_Candidates
	InputControlList_1_tC251485A8EBE20F32C1828D6F68B450DAB732E96  ___m_Candidates_19;
	// System.Action`1<UnityEngine.InputSystem.InputActionRebindingExtensions/RebindingOperation> UnityEngine.InputSystem.InputActionRebindingExtensions/RebindingOperation::m_OnComplete
	Action_1_t712E64B2FB6B72A4E947CBB2975DABB73A06E663 * ___m_OnComplete_20;
	// System.Action`1<UnityEngine.InputSystem.InputActionRebindingExtensions/RebindingOperation> UnityEngine.InputSystem.InputActionRebindingExtensions/RebindingOperation::m_OnCancel
	Action_1_t712E64B2FB6B72A4E947CBB2975DABB73A06E663 * ___m_OnCancel_21;
	// System.Action`1<UnityEngine.InputSystem.InputActionRebindingExtensions/RebindingOperation> UnityEngine.InputSystem.InputActionRebindingExtensions/RebindingOperation::m_OnPotentialMatch
	Action_1_t712E64B2FB6B72A4E947CBB2975DABB73A06E663 * ___m_OnPotentialMatch_22;
	// System.Func`2<UnityEngine.InputSystem.InputControl,System.String> UnityEngine.InputSystem.InputActionRebindingExtensions/RebindingOperation::m_OnGeneratePath
	Func_2_t256D379297E9972A2E59B56A5D0276E9ED789181 * ___m_OnGeneratePath_23;
	// System.Func`3<UnityEngine.InputSystem.InputControl,UnityEngine.InputSystem.LowLevel.InputEventPtr,System.Single> UnityEngine.InputSystem.InputActionRebindingExtensions/RebindingOperation::m_OnComputeScore
	Func_3_tB2180AFDCB89ECEDE3ED8A30CA81B8E923416ADC * ___m_OnComputeScore_24;
	// System.Action`2<UnityEngine.InputSystem.InputActionRebindingExtensions/RebindingOperation,System.String> UnityEngine.InputSystem.InputActionRebindingExtensions/RebindingOperation::m_OnApplyBinding
	Action_2_t1C13FAA0BE04A7EFAC5DDD87B23307535BE59B56 * ___m_OnApplyBinding_25;
	// System.Action`2<UnityEngine.InputSystem.LowLevel.InputEventPtr,UnityEngine.InputSystem.InputDevice> UnityEngine.InputSystem.InputActionRebindingExtensions/RebindingOperation::m_OnEventDelegate
	Action_2_t83E2B2D05A16B14F487969D4D187AEC66413E199 * ___m_OnEventDelegate_26;
	// System.Action UnityEngine.InputSystem.InputActionRebindingExtensions/RebindingOperation::m_OnAfterUpdateDelegate
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___m_OnAfterUpdateDelegate_27;
	// UnityEngine.InputSystem.Layouts.InputControlLayout/Cache UnityEngine.InputSystem.InputActionRebindingExtensions/RebindingOperation::m_LayoutCache
	Cache_t9F49740128E8F04886120C99F392DC7CEBC6AA67  ___m_LayoutCache_28;
	// System.Text.StringBuilder UnityEngine.InputSystem.InputActionRebindingExtensions/RebindingOperation::m_PathBuilder
	StringBuilder_t * ___m_PathBuilder_29;
	// UnityEngine.InputSystem.InputActionRebindingExtensions/RebindingOperation/Flags UnityEngine.InputSystem.InputActionRebindingExtensions/RebindingOperation::m_Flags
	int32_t ___m_Flags_30;
	// System.Int32 UnityEngine.InputSystem.InputActionRebindingExtensions/RebindingOperation::m_StartingActuationsCount
	int32_t ___m_StartingActuationsCount_31;
	// System.Single[] UnityEngine.InputSystem.InputActionRebindingExtensions/RebindingOperation::m_StaringActuationMagnitudes
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___m_StaringActuationMagnitudes_32;
	// UnityEngine.InputSystem.InputControl[] UnityEngine.InputSystem.InputActionRebindingExtensions/RebindingOperation::m_StartingActuationControls
	InputControlU5BU5D_t4668D8411829C981759512FED1506B20F902CA67* ___m_StartingActuationControls_33;

public:
	inline static int32_t get_offset_of_m_ActionToRebind_1() { return static_cast<int32_t>(offsetof(RebindingOperation_t5DA0F3263991E7BED360B8A1ADE014B9B451F970, ___m_ActionToRebind_1)); }
	inline InputAction_tF0C77AF72E0C19608B020E77A2C36C04E9DCBE48 * get_m_ActionToRebind_1() const { return ___m_ActionToRebind_1; }
	inline InputAction_tF0C77AF72E0C19608B020E77A2C36C04E9DCBE48 ** get_address_of_m_ActionToRebind_1() { return &___m_ActionToRebind_1; }
	inline void set_m_ActionToRebind_1(InputAction_tF0C77AF72E0C19608B020E77A2C36C04E9DCBE48 * value)
	{
		___m_ActionToRebind_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ActionToRebind_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_BindingMask_2() { return static_cast<int32_t>(offsetof(RebindingOperation_t5DA0F3263991E7BED360B8A1ADE014B9B451F970, ___m_BindingMask_2)); }
	inline Nullable_1_tAF2F42FC2F7DC5A356BAC77371758E4D7339F104  get_m_BindingMask_2() const { return ___m_BindingMask_2; }
	inline Nullable_1_tAF2F42FC2F7DC5A356BAC77371758E4D7339F104 * get_address_of_m_BindingMask_2() { return &___m_BindingMask_2; }
	inline void set_m_BindingMask_2(Nullable_1_tAF2F42FC2F7DC5A356BAC77371758E4D7339F104  value)
	{
		___m_BindingMask_2 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_BindingMask_2))->___value_0))->___m_Name_2), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_BindingMask_2))->___value_0))->___m_Id_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_BindingMask_2))->___value_0))->___m_Path_4), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_BindingMask_2))->___value_0))->___m_Interactions_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_BindingMask_2))->___value_0))->___m_Processors_6), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_BindingMask_2))->___value_0))->___m_Groups_7), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_BindingMask_2))->___value_0))->___m_Action_8), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_BindingMask_2))->___value_0))->___m_OverridePath_10), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_BindingMask_2))->___value_0))->___m_OverrideInteractions_11), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_BindingMask_2))->___value_0))->___m_OverrideProcessors_12), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_ControlType_3() { return static_cast<int32_t>(offsetof(RebindingOperation_t5DA0F3263991E7BED360B8A1ADE014B9B451F970, ___m_ControlType_3)); }
	inline Type_t * get_m_ControlType_3() const { return ___m_ControlType_3; }
	inline Type_t ** get_address_of_m_ControlType_3() { return &___m_ControlType_3; }
	inline void set_m_ControlType_3(Type_t * value)
	{
		___m_ControlType_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ControlType_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_ExpectedLayout_4() { return static_cast<int32_t>(offsetof(RebindingOperation_t5DA0F3263991E7BED360B8A1ADE014B9B451F970, ___m_ExpectedLayout_4)); }
	inline InternedString_tD1138602E8B7EA0F5B4851812B13C7E0551E25C8  get_m_ExpectedLayout_4() const { return ___m_ExpectedLayout_4; }
	inline InternedString_tD1138602E8B7EA0F5B4851812B13C7E0551E25C8 * get_address_of_m_ExpectedLayout_4() { return &___m_ExpectedLayout_4; }
	inline void set_m_ExpectedLayout_4(InternedString_tD1138602E8B7EA0F5B4851812B13C7E0551E25C8  value)
	{
		___m_ExpectedLayout_4 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_ExpectedLayout_4))->___m_StringOriginalCase_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_ExpectedLayout_4))->___m_StringLowerCase_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_IncludePathCount_5() { return static_cast<int32_t>(offsetof(RebindingOperation_t5DA0F3263991E7BED360B8A1ADE014B9B451F970, ___m_IncludePathCount_5)); }
	inline int32_t get_m_IncludePathCount_5() const { return ___m_IncludePathCount_5; }
	inline int32_t* get_address_of_m_IncludePathCount_5() { return &___m_IncludePathCount_5; }
	inline void set_m_IncludePathCount_5(int32_t value)
	{
		___m_IncludePathCount_5 = value;
	}

	inline static int32_t get_offset_of_m_IncludePaths_6() { return static_cast<int32_t>(offsetof(RebindingOperation_t5DA0F3263991E7BED360B8A1ADE014B9B451F970, ___m_IncludePaths_6)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_m_IncludePaths_6() const { return ___m_IncludePaths_6; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_m_IncludePaths_6() { return &___m_IncludePaths_6; }
	inline void set_m_IncludePaths_6(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___m_IncludePaths_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_IncludePaths_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_ExcludePathCount_7() { return static_cast<int32_t>(offsetof(RebindingOperation_t5DA0F3263991E7BED360B8A1ADE014B9B451F970, ___m_ExcludePathCount_7)); }
	inline int32_t get_m_ExcludePathCount_7() const { return ___m_ExcludePathCount_7; }
	inline int32_t* get_address_of_m_ExcludePathCount_7() { return &___m_ExcludePathCount_7; }
	inline void set_m_ExcludePathCount_7(int32_t value)
	{
		___m_ExcludePathCount_7 = value;
	}

	inline static int32_t get_offset_of_m_ExcludePaths_8() { return static_cast<int32_t>(offsetof(RebindingOperation_t5DA0F3263991E7BED360B8A1ADE014B9B451F970, ___m_ExcludePaths_8)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_m_ExcludePaths_8() const { return ___m_ExcludePaths_8; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_m_ExcludePaths_8() { return &___m_ExcludePaths_8; }
	inline void set_m_ExcludePaths_8(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___m_ExcludePaths_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ExcludePaths_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_TargetBindingIndex_9() { return static_cast<int32_t>(offsetof(RebindingOperation_t5DA0F3263991E7BED360B8A1ADE014B9B451F970, ___m_TargetBindingIndex_9)); }
	inline int32_t get_m_TargetBindingIndex_9() const { return ___m_TargetBindingIndex_9; }
	inline int32_t* get_address_of_m_TargetBindingIndex_9() { return &___m_TargetBindingIndex_9; }
	inline void set_m_TargetBindingIndex_9(int32_t value)
	{
		___m_TargetBindingIndex_9 = value;
	}

	inline static int32_t get_offset_of_m_BindingGroupForNewBinding_10() { return static_cast<int32_t>(offsetof(RebindingOperation_t5DA0F3263991E7BED360B8A1ADE014B9B451F970, ___m_BindingGroupForNewBinding_10)); }
	inline String_t* get_m_BindingGroupForNewBinding_10() const { return ___m_BindingGroupForNewBinding_10; }
	inline String_t** get_address_of_m_BindingGroupForNewBinding_10() { return &___m_BindingGroupForNewBinding_10; }
	inline void set_m_BindingGroupForNewBinding_10(String_t* value)
	{
		___m_BindingGroupForNewBinding_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_BindingGroupForNewBinding_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_CancelBinding_11() { return static_cast<int32_t>(offsetof(RebindingOperation_t5DA0F3263991E7BED360B8A1ADE014B9B451F970, ___m_CancelBinding_11)); }
	inline String_t* get_m_CancelBinding_11() const { return ___m_CancelBinding_11; }
	inline String_t** get_address_of_m_CancelBinding_11() { return &___m_CancelBinding_11; }
	inline void set_m_CancelBinding_11(String_t* value)
	{
		___m_CancelBinding_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CancelBinding_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_MagnitudeThreshold_12() { return static_cast<int32_t>(offsetof(RebindingOperation_t5DA0F3263991E7BED360B8A1ADE014B9B451F970, ___m_MagnitudeThreshold_12)); }
	inline float get_m_MagnitudeThreshold_12() const { return ___m_MagnitudeThreshold_12; }
	inline float* get_address_of_m_MagnitudeThreshold_12() { return &___m_MagnitudeThreshold_12; }
	inline void set_m_MagnitudeThreshold_12(float value)
	{
		___m_MagnitudeThreshold_12 = value;
	}

	inline static int32_t get_offset_of_m_Scores_13() { return static_cast<int32_t>(offsetof(RebindingOperation_t5DA0F3263991E7BED360B8A1ADE014B9B451F970, ___m_Scores_13)); }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* get_m_Scores_13() const { return ___m_Scores_13; }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** get_address_of_m_Scores_13() { return &___m_Scores_13; }
	inline void set_m_Scores_13(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		___m_Scores_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Scores_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_Magnitudes_14() { return static_cast<int32_t>(offsetof(RebindingOperation_t5DA0F3263991E7BED360B8A1ADE014B9B451F970, ___m_Magnitudes_14)); }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* get_m_Magnitudes_14() const { return ___m_Magnitudes_14; }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** get_address_of_m_Magnitudes_14() { return &___m_Magnitudes_14; }
	inline void set_m_Magnitudes_14(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		___m_Magnitudes_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Magnitudes_14), (void*)value);
	}

	inline static int32_t get_offset_of_m_LastMatchTime_15() { return static_cast<int32_t>(offsetof(RebindingOperation_t5DA0F3263991E7BED360B8A1ADE014B9B451F970, ___m_LastMatchTime_15)); }
	inline double get_m_LastMatchTime_15() const { return ___m_LastMatchTime_15; }
	inline double* get_address_of_m_LastMatchTime_15() { return &___m_LastMatchTime_15; }
	inline void set_m_LastMatchTime_15(double value)
	{
		___m_LastMatchTime_15 = value;
	}

	inline static int32_t get_offset_of_m_StartTime_16() { return static_cast<int32_t>(offsetof(RebindingOperation_t5DA0F3263991E7BED360B8A1ADE014B9B451F970, ___m_StartTime_16)); }
	inline double get_m_StartTime_16() const { return ___m_StartTime_16; }
	inline double* get_address_of_m_StartTime_16() { return &___m_StartTime_16; }
	inline void set_m_StartTime_16(double value)
	{
		___m_StartTime_16 = value;
	}

	inline static int32_t get_offset_of_m_Timeout_17() { return static_cast<int32_t>(offsetof(RebindingOperation_t5DA0F3263991E7BED360B8A1ADE014B9B451F970, ___m_Timeout_17)); }
	inline float get_m_Timeout_17() const { return ___m_Timeout_17; }
	inline float* get_address_of_m_Timeout_17() { return &___m_Timeout_17; }
	inline void set_m_Timeout_17(float value)
	{
		___m_Timeout_17 = value;
	}

	inline static int32_t get_offset_of_m_WaitSecondsAfterMatch_18() { return static_cast<int32_t>(offsetof(RebindingOperation_t5DA0F3263991E7BED360B8A1ADE014B9B451F970, ___m_WaitSecondsAfterMatch_18)); }
	inline float get_m_WaitSecondsAfterMatch_18() const { return ___m_WaitSecondsAfterMatch_18; }
	inline float* get_address_of_m_WaitSecondsAfterMatch_18() { return &___m_WaitSecondsAfterMatch_18; }
	inline void set_m_WaitSecondsAfterMatch_18(float value)
	{
		___m_WaitSecondsAfterMatch_18 = value;
	}

	inline static int32_t get_offset_of_m_Candidates_19() { return static_cast<int32_t>(offsetof(RebindingOperation_t5DA0F3263991E7BED360B8A1ADE014B9B451F970, ___m_Candidates_19)); }
	inline InputControlList_1_tC251485A8EBE20F32C1828D6F68B450DAB732E96  get_m_Candidates_19() const { return ___m_Candidates_19; }
	inline InputControlList_1_tC251485A8EBE20F32C1828D6F68B450DAB732E96 * get_address_of_m_Candidates_19() { return &___m_Candidates_19; }
	inline void set_m_Candidates_19(InputControlList_1_tC251485A8EBE20F32C1828D6F68B450DAB732E96  value)
	{
		___m_Candidates_19 = value;
	}

	inline static int32_t get_offset_of_m_OnComplete_20() { return static_cast<int32_t>(offsetof(RebindingOperation_t5DA0F3263991E7BED360B8A1ADE014B9B451F970, ___m_OnComplete_20)); }
	inline Action_1_t712E64B2FB6B72A4E947CBB2975DABB73A06E663 * get_m_OnComplete_20() const { return ___m_OnComplete_20; }
	inline Action_1_t712E64B2FB6B72A4E947CBB2975DABB73A06E663 ** get_address_of_m_OnComplete_20() { return &___m_OnComplete_20; }
	inline void set_m_OnComplete_20(Action_1_t712E64B2FB6B72A4E947CBB2975DABB73A06E663 * value)
	{
		___m_OnComplete_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnComplete_20), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnCancel_21() { return static_cast<int32_t>(offsetof(RebindingOperation_t5DA0F3263991E7BED360B8A1ADE014B9B451F970, ___m_OnCancel_21)); }
	inline Action_1_t712E64B2FB6B72A4E947CBB2975DABB73A06E663 * get_m_OnCancel_21() const { return ___m_OnCancel_21; }
	inline Action_1_t712E64B2FB6B72A4E947CBB2975DABB73A06E663 ** get_address_of_m_OnCancel_21() { return &___m_OnCancel_21; }
	inline void set_m_OnCancel_21(Action_1_t712E64B2FB6B72A4E947CBB2975DABB73A06E663 * value)
	{
		___m_OnCancel_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnCancel_21), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnPotentialMatch_22() { return static_cast<int32_t>(offsetof(RebindingOperation_t5DA0F3263991E7BED360B8A1ADE014B9B451F970, ___m_OnPotentialMatch_22)); }
	inline Action_1_t712E64B2FB6B72A4E947CBB2975DABB73A06E663 * get_m_OnPotentialMatch_22() const { return ___m_OnPotentialMatch_22; }
	inline Action_1_t712E64B2FB6B72A4E947CBB2975DABB73A06E663 ** get_address_of_m_OnPotentialMatch_22() { return &___m_OnPotentialMatch_22; }
	inline void set_m_OnPotentialMatch_22(Action_1_t712E64B2FB6B72A4E947CBB2975DABB73A06E663 * value)
	{
		___m_OnPotentialMatch_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnPotentialMatch_22), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnGeneratePath_23() { return static_cast<int32_t>(offsetof(RebindingOperation_t5DA0F3263991E7BED360B8A1ADE014B9B451F970, ___m_OnGeneratePath_23)); }
	inline Func_2_t256D379297E9972A2E59B56A5D0276E9ED789181 * get_m_OnGeneratePath_23() const { return ___m_OnGeneratePath_23; }
	inline Func_2_t256D379297E9972A2E59B56A5D0276E9ED789181 ** get_address_of_m_OnGeneratePath_23() { return &___m_OnGeneratePath_23; }
	inline void set_m_OnGeneratePath_23(Func_2_t256D379297E9972A2E59B56A5D0276E9ED789181 * value)
	{
		___m_OnGeneratePath_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnGeneratePath_23), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnComputeScore_24() { return static_cast<int32_t>(offsetof(RebindingOperation_t5DA0F3263991E7BED360B8A1ADE014B9B451F970, ___m_OnComputeScore_24)); }
	inline Func_3_tB2180AFDCB89ECEDE3ED8A30CA81B8E923416ADC * get_m_OnComputeScore_24() const { return ___m_OnComputeScore_24; }
	inline Func_3_tB2180AFDCB89ECEDE3ED8A30CA81B8E923416ADC ** get_address_of_m_OnComputeScore_24() { return &___m_OnComputeScore_24; }
	inline void set_m_OnComputeScore_24(Func_3_tB2180AFDCB89ECEDE3ED8A30CA81B8E923416ADC * value)
	{
		___m_OnComputeScore_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnComputeScore_24), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnApplyBinding_25() { return static_cast<int32_t>(offsetof(RebindingOperation_t5DA0F3263991E7BED360B8A1ADE014B9B451F970, ___m_OnApplyBinding_25)); }
	inline Action_2_t1C13FAA0BE04A7EFAC5DDD87B23307535BE59B56 * get_m_OnApplyBinding_25() const { return ___m_OnApplyBinding_25; }
	inline Action_2_t1C13FAA0BE04A7EFAC5DDD87B23307535BE59B56 ** get_address_of_m_OnApplyBinding_25() { return &___m_OnApplyBinding_25; }
	inline void set_m_OnApplyBinding_25(Action_2_t1C13FAA0BE04A7EFAC5DDD87B23307535BE59B56 * value)
	{
		___m_OnApplyBinding_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnApplyBinding_25), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnEventDelegate_26() { return static_cast<int32_t>(offsetof(RebindingOperation_t5DA0F3263991E7BED360B8A1ADE014B9B451F970, ___m_OnEventDelegate_26)); }
	inline Action_2_t83E2B2D05A16B14F487969D4D187AEC66413E199 * get_m_OnEventDelegate_26() const { return ___m_OnEventDelegate_26; }
	inline Action_2_t83E2B2D05A16B14F487969D4D187AEC66413E199 ** get_address_of_m_OnEventDelegate_26() { return &___m_OnEventDelegate_26; }
	inline void set_m_OnEventDelegate_26(Action_2_t83E2B2D05A16B14F487969D4D187AEC66413E199 * value)
	{
		___m_OnEventDelegate_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnEventDelegate_26), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnAfterUpdateDelegate_27() { return static_cast<int32_t>(offsetof(RebindingOperation_t5DA0F3263991E7BED360B8A1ADE014B9B451F970, ___m_OnAfterUpdateDelegate_27)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_m_OnAfterUpdateDelegate_27() const { return ___m_OnAfterUpdateDelegate_27; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_m_OnAfterUpdateDelegate_27() { return &___m_OnAfterUpdateDelegate_27; }
	inline void set_m_OnAfterUpdateDelegate_27(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___m_OnAfterUpdateDelegate_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnAfterUpdateDelegate_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_LayoutCache_28() { return static_cast<int32_t>(offsetof(RebindingOperation_t5DA0F3263991E7BED360B8A1ADE014B9B451F970, ___m_LayoutCache_28)); }
	inline Cache_t9F49740128E8F04886120C99F392DC7CEBC6AA67  get_m_LayoutCache_28() const { return ___m_LayoutCache_28; }
	inline Cache_t9F49740128E8F04886120C99F392DC7CEBC6AA67 * get_address_of_m_LayoutCache_28() { return &___m_LayoutCache_28; }
	inline void set_m_LayoutCache_28(Cache_t9F49740128E8F04886120C99F392DC7CEBC6AA67  value)
	{
		___m_LayoutCache_28 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_LayoutCache_28))->___table_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_m_PathBuilder_29() { return static_cast<int32_t>(offsetof(RebindingOperation_t5DA0F3263991E7BED360B8A1ADE014B9B451F970, ___m_PathBuilder_29)); }
	inline StringBuilder_t * get_m_PathBuilder_29() const { return ___m_PathBuilder_29; }
	inline StringBuilder_t ** get_address_of_m_PathBuilder_29() { return &___m_PathBuilder_29; }
	inline void set_m_PathBuilder_29(StringBuilder_t * value)
	{
		___m_PathBuilder_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PathBuilder_29), (void*)value);
	}

	inline static int32_t get_offset_of_m_Flags_30() { return static_cast<int32_t>(offsetof(RebindingOperation_t5DA0F3263991E7BED360B8A1ADE014B9B451F970, ___m_Flags_30)); }
	inline int32_t get_m_Flags_30() const { return ___m_Flags_30; }
	inline int32_t* get_address_of_m_Flags_30() { return &___m_Flags_30; }
	inline void set_m_Flags_30(int32_t value)
	{
		___m_Flags_30 = value;
	}

	inline static int32_t get_offset_of_m_StartingActuationsCount_31() { return static_cast<int32_t>(offsetof(RebindingOperation_t5DA0F3263991E7BED360B8A1ADE014B9B451F970, ___m_StartingActuationsCount_31)); }
	inline int32_t get_m_StartingActuationsCount_31() const { return ___m_StartingActuationsCount_31; }
	inline int32_t* get_address_of_m_StartingActuationsCount_31() { return &___m_StartingActuationsCount_31; }
	inline void set_m_StartingActuationsCount_31(int32_t value)
	{
		___m_StartingActuationsCount_31 = value;
	}

	inline static int32_t get_offset_of_m_StaringActuationMagnitudes_32() { return static_cast<int32_t>(offsetof(RebindingOperation_t5DA0F3263991E7BED360B8A1ADE014B9B451F970, ___m_StaringActuationMagnitudes_32)); }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* get_m_StaringActuationMagnitudes_32() const { return ___m_StaringActuationMagnitudes_32; }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** get_address_of_m_StaringActuationMagnitudes_32() { return &___m_StaringActuationMagnitudes_32; }
	inline void set_m_StaringActuationMagnitudes_32(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		___m_StaringActuationMagnitudes_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_StaringActuationMagnitudes_32), (void*)value);
	}

	inline static int32_t get_offset_of_m_StartingActuationControls_33() { return static_cast<int32_t>(offsetof(RebindingOperation_t5DA0F3263991E7BED360B8A1ADE014B9B451F970, ___m_StartingActuationControls_33)); }
	inline InputControlU5BU5D_t4668D8411829C981759512FED1506B20F902CA67* get_m_StartingActuationControls_33() const { return ___m_StartingActuationControls_33; }
	inline InputControlU5BU5D_t4668D8411829C981759512FED1506B20F902CA67** get_address_of_m_StartingActuationControls_33() { return &___m_StartingActuationControls_33; }
	inline void set_m_StartingActuationControls_33(InputControlU5BU5D_t4668D8411829C981759512FED1506B20F902CA67* value)
	{
		___m_StartingActuationControls_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_StartingActuationControls_33), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// UnityEngine.Matrix4x4[]
struct Matrix4x4U5BU5D_tE53F71E9C9110DD439281A6AB8B699F9F85D8F82  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  m_Items[1];

public:
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  value)
	{
		m_Items[index] = value;
	}
};
// System.UInt32[]
struct UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint32_t m_Items[1];

public:
	inline uint32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint32_t value)
	{
		m_Items[index] = value;
	}
};
// UniGLTF.UShort4[]
struct UShort4U5BU5D_tBDA4C224D509794003F0CCB10B200496F61CEA35  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) UShort4_tB19A4ECD972B8600C9515187BC86B572B83920DB  m_Items[1];

public:
	inline UShort4_tB19A4ECD972B8600C9515187BC86B572B83920DB  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline UShort4_tB19A4ECD972B8600C9515187BC86B572B83920DB * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, UShort4_tB19A4ECD972B8600C9515187BC86B572B83920DB  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline UShort4_tB19A4ECD972B8600C9515187BC86B572B83920DB  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline UShort4_tB19A4ECD972B8600C9515187BC86B572B83920DB * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, UShort4_tB19A4ECD972B8600C9515187BC86B572B83920DB  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  m_Items[1];

public:
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  m_Items[1];

public:
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Vector4[]
struct Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  m_Items[1];

public:
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		m_Items[index] = value;
	}
};
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// System.Linq.Expressions.ParameterExpression[]
struct ParameterExpressionU5BU5D_tCF3EAA6D8556513C4937E1126F65AA08DF4DB147  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) ParameterExpression_tA7B24F1DE0F013DA4BD55F76DB43B06DB33D8BEE * m_Items[1];

public:
	inline ParameterExpression_tA7B24F1DE0F013DA4BD55F76DB43B06DB33D8BEE * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ParameterExpression_tA7B24F1DE0F013DA4BD55F76DB43B06DB33D8BEE ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ParameterExpression_tA7B24F1DE0F013DA4BD55F76DB43B06DB33D8BEE * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline ParameterExpression_tA7B24F1DE0F013DA4BD55F76DB43B06DB33D8BEE * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ParameterExpression_tA7B24F1DE0F013DA4BD55F76DB43B06DB33D8BEE ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ParameterExpression_tA7B24F1DE0F013DA4BD55F76DB43B06DB33D8BEE * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// System.Void System.ArraySegment`1<UnityEngine.Matrix4x4>::.ctor(!0[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArraySegment_1__ctor_mE3508383112A119C6D1039270F35F365CC0763E5_gshared (ArraySegment_1_t7FCE6A956BF91EE65591716518285D293D3C5B6B * __this, Matrix4x4U5BU5D_tE53F71E9C9110DD439281A6AB8B699F9F85D8F82* ___array0, const RuntimeMethod* method);
// System.Int32 System.ArraySegment`1<System.UInt32>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ArraySegment_1_get_Count_mF79DF195C03504D335115BA66BC48FB0BB4D395C_gshared_inline (ArraySegment_1_tE4B89ED4839E563DF2A25436675AC5C5E87B62B9 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_mF15250BF947CA27BE9A23C08BAC6DB6F180B0EDD_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, RuntimeObject * ___item0, const RuntimeMethod* method);
// System.Void System.ArraySegment`1<System.UInt32>::.ctor(!0[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArraySegment_1__ctor_m8D3EA3869F4DD0072CF891BBB456B9DA390AE0A9_gshared (ArraySegment_1_tE4B89ED4839E563DF2A25436675AC5C5E87B62B9 * __this, UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* ___array0, const RuntimeMethod* method);
// System.Int32 System.ArraySegment`1<UniGLTF.UShort4>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ArraySegment_1_get_Count_mC78F0263F5BAC4CA823FA314F69DD18E7E4307D2_gshared_inline (ArraySegment_1_t9487D5EA4323A4B2C1F7F94C91363EBEE33F3462 * __this, const RuntimeMethod* method);
// System.Void System.ArraySegment`1<UniGLTF.UShort4>::.ctor(!0[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArraySegment_1__ctor_m7E66C7B5F5F7EA5DAA72B9D5F676B8CDBBA6EB06_gshared (ArraySegment_1_t9487D5EA4323A4B2C1F7F94C91363EBEE33F3462 * __this, UShort4U5BU5D_tBDA4C224D509794003F0CCB10B200496F61CEA35* ___array0, const RuntimeMethod* method);
// System.Int32 System.ArraySegment`1<UnityEngine.Vector2>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ArraySegment_1_get_Count_m038FD84B16BD0AC31B33B3B9D9237708DFC1AA32_gshared_inline (ArraySegment_1_t73AEFBC798515A51F93CF5816E332A0654AE698D * __this, const RuntimeMethod* method);
// System.Void System.ArraySegment`1<UnityEngine.Vector2>::.ctor(!0[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArraySegment_1__ctor_m93C686A91083A067A9AD065F4D76D36D1C80F444_gshared (ArraySegment_1_t73AEFBC798515A51F93CF5816E332A0654AE698D * __this, Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___array0, const RuntimeMethod* method);
// System.Int32 System.ArraySegment`1<UnityEngine.Vector3>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ArraySegment_1_get_Count_m03AF4BDAA1D5D96096A0072A85C43C2FBC7C03EE_gshared_inline (ArraySegment_1_t12E2F3BC867255EB056CA1F106A8D1E85142A3F1 * __this, const RuntimeMethod* method);
// System.Void System.ArraySegment`1<UnityEngine.Vector3>::.ctor(!0[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArraySegment_1__ctor_mDC6C24720C4540B4A9A00A8F03A5347729CBB745_gshared (ArraySegment_1_t12E2F3BC867255EB056CA1F106A8D1E85142A3F1 * __this, Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___array0, const RuntimeMethod* method);
// System.Int32 System.ArraySegment`1<UnityEngine.Vector4>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ArraySegment_1_get_Count_m86D4F7A8C6E66A89B03DB83E4F8F82019625E483_gshared_inline (ArraySegment_1_t3BCE22176B44ACEB25236AD7501F226112F6E07A * __this, const RuntimeMethod* method);
// System.Void System.ArraySegment`1<UnityEngine.Vector4>::.ctor(!0[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArraySegment_1__ctor_m1186B90A376A0AE42259FC310386A6BE3AFD6D40_gshared (ArraySegment_1_t3BCE22176B44ACEB25236AD7501F226112F6E07A * __this, Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871* ___array0, const RuntimeMethod* method);
// System.Int32 System.ArraySegment`1<UnityEngine.Color>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ArraySegment_1_get_Count_m313EE200EA1CB465EF2B1F8CFFA7E63FA3F408EF_gshared_inline (ArraySegment_1_t8A45E4F401BB0C4F9F8643CCA8706F98DC8057F6 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_m7B5E3383CB67492E573AC0D875ED82A51350F188_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Int32 System.ArraySegment`1<System.Int32>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ArraySegment_1_get_Count_mFB0298EF19B049FBF2C0C3D7FCA5E16DB8F80D56_gshared_inline (ArraySegment_1_t9FDA96F04294A918E0DBFCE7CF750DE23A898976 * __this, const RuntimeMethod* method);
// System.Void System.ArraySegment`1<System.Int32>::.ctor(!0[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArraySegment_1__ctor_m643D396633372662D1B12F94E9503026534D7F41_gshared (ArraySegment_1_t9FDA96F04294A918E0DBFCE7CF750DE23A898976 * __this, Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___array0, const RuntimeMethod* method);
// System.Int32 System.ArraySegment`1<UnityEngine.Matrix4x4>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ArraySegment_1_get_Count_m779174D975AA29188F9DFFC751199785A7F0ADBD_gshared_inline (ArraySegment_1_t7FCE6A956BF91EE65591716518285D293D3C5B6B * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UniGLTF.glTFExtensions/ComponentVec>::TryGetValue(!0,!1&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Dictionary_2_TryGetValue_m022664ECEA6FA0306333C8E8D40DA6D2CD70C12A_gshared (Dictionary_2_tB46DDF458408E887F684BF61B389BCF27B59F718 * __this, RuntimeObject * ___key0, ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 * ___value1, const RuntimeMethod* method);
// TValue UnityEngine.InputSystem.InputAction/CallbackContext::ReadValue<UnityEngine.Quaternion>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  CallbackContext_ReadValue_TisQuaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_mE7BC042F12AD2F6B0CC56EFAD37953575750E6C9_gshared (CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B * __this, const RuntimeMethod* method);
// TValue UnityEngine.InputSystem.InputAction/CallbackContext::ReadValue<System.Single>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float CallbackContext_ReadValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_m2BFB8C24FFB40E00E51EC8F4DA7CAC936508D419_gshared (CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B * __this, const RuntimeMethod* method);
// TValue UnityEngine.InputSystem.InputAction/CallbackContext::ReadValue<UnityEngine.Vector2>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  CallbackContext_ReadValue_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_m5979F5A5F9C48193420CD889B84D010F6373171B_gshared (CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B * __this, const RuntimeMethod* method);
// TValue UnityEngine.InputSystem.InputAction/CallbackContext::ReadValue<UnityEngine.Vector3>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  CallbackContext_ReadValue_TisVector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_m76A2A215A8ACD5B96AAAD484DD041DA960A11B81_gshared (CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B * __this, const RuntimeMethod* method);
// UnityEngine.InputSystem.InputActionSetupExtensions/BindingSyntax UnityEngine.InputSystem.InputActionSetupExtensions/BindingSyntax::WithInteraction<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR BindingSyntax_tEF6F07812AAB1AF06B3D36C9B1570C01997C8BF1  BindingSyntax_WithInteraction_TisRuntimeObject_mF004EAD1BEFB97AAECF7B3AD3C9856304B302EDC_gshared (BindingSyntax_tEF6F07812AAB1AF06B3D36C9B1570C01997C8BF1 * __this, const RuntimeMethod* method);
// UnityEngine.InputSystem.InputActionSetupExtensions/BindingSyntax UnityEngine.InputSystem.InputActionSetupExtensions/BindingSyntax::WithProcessor<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR BindingSyntax_tEF6F07812AAB1AF06B3D36C9B1570C01997C8BF1  BindingSyntax_WithProcessor_TisRuntimeObject_mE7F31DB067FE1D0D8B6CAE2F3A869BB60B4E15ED_gshared (BindingSyntax_tEF6F07812AAB1AF06B3D36C9B1570C01997C8BF1 * __this, const RuntimeMethod* method);
// System.String UnityEngine.InputSystem.InputActionSetupExtensions/ControlSchemeSyntax::DeviceTypeToControlPath<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* ControlSchemeSyntax_DeviceTypeToControlPath_TisRuntimeObject_mB700493B60AC1EDE0CA6A7C5B54091F0B1343D4F_gshared (ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214 * __this, const RuntimeMethod* method);
// UnityEngine.InputSystem.InputActionSetupExtensions/ControlSchemeSyntax UnityEngine.InputSystem.InputActionSetupExtensions/ControlSchemeSyntax::OrWithOptionalDevice<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214  ControlSchemeSyntax_OrWithOptionalDevice_TisRuntimeObject_mA966529367FEAF671DF7725154538CB7312C1D2F_gshared (ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214 * __this, const RuntimeMethod* method);
// UnityEngine.InputSystem.InputActionSetupExtensions/ControlSchemeSyntax UnityEngine.InputSystem.InputActionSetupExtensions/ControlSchemeSyntax::OrWithRequiredDevice<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214  ControlSchemeSyntax_OrWithRequiredDevice_TisRuntimeObject_m0F28425B0D706D1C1C43B7A30024ACBFC00A3518_gshared (ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214 * __this, const RuntimeMethod* method);
// UnityEngine.InputSystem.InputActionSetupExtensions/ControlSchemeSyntax UnityEngine.InputSystem.InputActionSetupExtensions/ControlSchemeSyntax::WithOptionalDevice<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214  ControlSchemeSyntax_WithOptionalDevice_TisRuntimeObject_mEEDD90A8BFEDAC21FF0B67F8F3BC50208F09D015_gshared (ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214 * __this, const RuntimeMethod* method);
// UnityEngine.InputSystem.InputActionSetupExtensions/ControlSchemeSyntax UnityEngine.InputSystem.InputActionSetupExtensions/ControlSchemeSyntax::WithRequiredDevice<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214  ControlSchemeSyntax_WithRequiredDevice_TisRuntimeObject_mC0CB0D601DCB446ACF2E02BAE15CDAAD58DAED10_gshared (ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214 * __this, const RuntimeMethod* method);
// !!0[] System.Array::Empty<System.Object>()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* Array_Empty_TisRuntimeObject_m002765312BF306B1B3B5BFAB9550C0A2A1820CDA_gshared_inline (const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_mFEB2301A6F28290A828A979BA9CC847B16B3D538_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, int32_t ___capacity0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.HashSet`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HashSet_1__ctor_m2CDA40DEC2900A9CB00F8348FF386DF44ABD0EC7_gshared (HashSet_1_t680119C7ED8D82AED56CDB83DF6F0E9149852A9B * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::Add(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool HashSet_1_Add_m8194125167FD7A887F1B39D342A71A0BD0C8BF5C_gshared (HashSet_1_t680119C7ED8D82AED56CDB83DF6F0E9149852A9B * __this, RuntimeObject * ___item0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Stack`1<System.Object>::Push(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Stack_1_Push_m8FFF613733BF88B54FCD4498B1E97DE296E27757_gshared (Stack_1_t92AC5F573A3C00899B24B775A71B4327D588E981 * __this, RuntimeObject * ___item0, const RuntimeMethod* method);
// !0 System.Collections.Generic.Stack`1<System.Object>::Pop()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Stack_1_Pop_m883C5DEF33CA2E556AC710A9BE26F8BD2D4B182C_gshared (Stack_1_t92AC5F573A3C00899B24B775A71B4327D588E981 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.Rendering.DynamicArray`1<System.Object>::get_size()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t DynamicArray_1_get_size_m0D404D0EDA77F50046B5592673CEADED3EB1DDF9_gshared_inline (DynamicArray_1_tBD159187A396D2634FDBCFD5A8F85F26FA3DF11D * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Rendering.DynamicArray`1<System.Object>::Resize(System.Int32,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DynamicArray_1_Resize_m3CEAE3B337E1361CC36C95D76BF653D9ADD630A2_gshared (DynamicArray_1_tBD159187A396D2634FDBCFD5A8F85F26FA3DF11D * __this, int32_t ___newSize0, bool ___keepContent1, const RuntimeMethod* method);
// T& UnityEngine.Rendering.DynamicArray`1<System.Object>::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject ** DynamicArray_1_get_Item_m8D97D96230A22384E91DC49B6855A34063768376_gshared (DynamicArray_1_tBD159187A396D2634FDBCFD5A8F85F26FA3DF11D * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Boolean Unity.Collections.NativeArray`1<UnityEngine.Vector2>::get_IsCreated()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NativeArray_1_get_IsCreated_mF82CF064C6050394DEAC11047A6308998101A4AE_gshared (NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0 * __this, const RuntimeMethod* method);
// System.Void Unity.Collections.NativeArray`1<UnityEngine.Vector2>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeArray_1_Dispose_mE737DADB8C5E4B02C2FB6CB36E86AC6371D806BF_gshared (NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0 * __this, const RuntimeMethod* method);
// System.Void Unity.Collections.NativeArray`1<UnityEngine.Vector2>::.ctor(System.Int32,Unity.Collections.Allocator,Unity.Collections.NativeArrayOptions)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeArray_1__ctor_m53A2E0E5DDF506B8508EAC444CAF3DB70F874729_gshared (NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0 * __this, int32_t ___length0, int32_t ___allocator1, int32_t ___options2, const RuntimeMethod* method);

// System.Void System.ArraySegment`1<UnityEngine.Matrix4x4>::.ctor(!0[])
inline void ArraySegment_1__ctor_mE3508383112A119C6D1039270F35F365CC0763E5 (ArraySegment_1_t7FCE6A956BF91EE65591716518285D293D3C5B6B * __this, Matrix4x4U5BU5D_tE53F71E9C9110DD439281A6AB8B699F9F85D8F82* ___array0, const RuntimeMethod* method)
{
	((  void (*) (ArraySegment_1_t7FCE6A956BF91EE65591716518285D293D3C5B6B *, Matrix4x4U5BU5D_tE53F71E9C9110DD439281A6AB8B699F9F85D8F82*, const RuntimeMethod*))ArraySegment_1__ctor_mE3508383112A119C6D1039270F35F365CC0763E5_gshared)(__this, ___array0, method);
}
// System.Int32 System.ArraySegment`1<System.UInt32>::get_Count()
inline int32_t ArraySegment_1_get_Count_mF79DF195C03504D335115BA66BC48FB0BB4D395C_inline (ArraySegment_1_tE4B89ED4839E563DF2A25436675AC5C5E87B62B9 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (ArraySegment_1_tE4B89ED4839E563DF2A25436675AC5C5E87B62B9 *, const RuntimeMethod*))ArraySegment_1_get_Count_mF79DF195C03504D335115BA66BC48FB0BB4D395C_gshared_inline)(__this, method);
}
// System.Int32 System.Collections.Generic.List`1<UniGLTF.glTFAccessor>::get_Count()
inline int32_t List_1_get_Count_mC221419694BDE6FF50C3F68C0964472743ACBA03_inline (List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC *, const RuntimeMethod*))List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline)(__this, method);
}
// System.Void UniGLTF.glTFAccessor::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void glTFAccessor__ctor_m0EF366F9EC3426182BDF6055B0B20661744327B8 (glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UniGLTF.glTFAccessor>::Add(!0)
inline void List_1_Add_m6189B75EF62756B468C403149F4AD081AC68C912 (List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC * __this, glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC * ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC *, glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC *, const RuntimeMethod*))List_1_Add_mF15250BF947CA27BE9A23C08BAC6DB6F180B0EDD_gshared)(__this, ___item0, method);
}
// System.Void System.ArraySegment`1<System.UInt32>::.ctor(!0[])
inline void ArraySegment_1__ctor_m8D3EA3869F4DD0072CF891BBB456B9DA390AE0A9 (ArraySegment_1_tE4B89ED4839E563DF2A25436675AC5C5E87B62B9 * __this, UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* ___array0, const RuntimeMethod* method)
{
	((  void (*) (ArraySegment_1_tE4B89ED4839E563DF2A25436675AC5C5E87B62B9 *, UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF*, const RuntimeMethod*))ArraySegment_1__ctor_m8D3EA3869F4DD0072CF891BBB456B9DA390AE0A9_gshared)(__this, ___array0, method);
}
// System.Int32 System.ArraySegment`1<UniGLTF.UShort4>::get_Count()
inline int32_t ArraySegment_1_get_Count_mC78F0263F5BAC4CA823FA314F69DD18E7E4307D2_inline (ArraySegment_1_t9487D5EA4323A4B2C1F7F94C91363EBEE33F3462 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (ArraySegment_1_t9487D5EA4323A4B2C1F7F94C91363EBEE33F3462 *, const RuntimeMethod*))ArraySegment_1_get_Count_mC78F0263F5BAC4CA823FA314F69DD18E7E4307D2_gshared_inline)(__this, method);
}
// System.Void System.ArraySegment`1<UniGLTF.UShort4>::.ctor(!0[])
inline void ArraySegment_1__ctor_m7E66C7B5F5F7EA5DAA72B9D5F676B8CDBBA6EB06 (ArraySegment_1_t9487D5EA4323A4B2C1F7F94C91363EBEE33F3462 * __this, UShort4U5BU5D_tBDA4C224D509794003F0CCB10B200496F61CEA35* ___array0, const RuntimeMethod* method)
{
	((  void (*) (ArraySegment_1_t9487D5EA4323A4B2C1F7F94C91363EBEE33F3462 *, UShort4U5BU5D_tBDA4C224D509794003F0CCB10B200496F61CEA35*, const RuntimeMethod*))ArraySegment_1__ctor_m7E66C7B5F5F7EA5DAA72B9D5F676B8CDBBA6EB06_gshared)(__this, ___array0, method);
}
// System.Int32 System.ArraySegment`1<UnityEngine.Vector2>::get_Count()
inline int32_t ArraySegment_1_get_Count_m038FD84B16BD0AC31B33B3B9D9237708DFC1AA32_inline (ArraySegment_1_t73AEFBC798515A51F93CF5816E332A0654AE698D * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (ArraySegment_1_t73AEFBC798515A51F93CF5816E332A0654AE698D *, const RuntimeMethod*))ArraySegment_1_get_Count_m038FD84B16BD0AC31B33B3B9D9237708DFC1AA32_gshared_inline)(__this, method);
}
// System.Void System.ArraySegment`1<UnityEngine.Vector2>::.ctor(!0[])
inline void ArraySegment_1__ctor_m93C686A91083A067A9AD065F4D76D36D1C80F444 (ArraySegment_1_t73AEFBC798515A51F93CF5816E332A0654AE698D * __this, Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___array0, const RuntimeMethod* method)
{
	((  void (*) (ArraySegment_1_t73AEFBC798515A51F93CF5816E332A0654AE698D *, Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA*, const RuntimeMethod*))ArraySegment_1__ctor_m93C686A91083A067A9AD065F4D76D36D1C80F444_gshared)(__this, ___array0, method);
}
// System.Int32 System.ArraySegment`1<UnityEngine.Vector3>::get_Count()
inline int32_t ArraySegment_1_get_Count_m03AF4BDAA1D5D96096A0072A85C43C2FBC7C03EE_inline (ArraySegment_1_t12E2F3BC867255EB056CA1F106A8D1E85142A3F1 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (ArraySegment_1_t12E2F3BC867255EB056CA1F106A8D1E85142A3F1 *, const RuntimeMethod*))ArraySegment_1_get_Count_m03AF4BDAA1D5D96096A0072A85C43C2FBC7C03EE_gshared_inline)(__this, method);
}
// System.Void System.ArraySegment`1<UnityEngine.Vector3>::.ctor(!0[])
inline void ArraySegment_1__ctor_mDC6C24720C4540B4A9A00A8F03A5347729CBB745 (ArraySegment_1_t12E2F3BC867255EB056CA1F106A8D1E85142A3F1 * __this, Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___array0, const RuntimeMethod* method)
{
	((  void (*) (ArraySegment_1_t12E2F3BC867255EB056CA1F106A8D1E85142A3F1 *, Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4*, const RuntimeMethod*))ArraySegment_1__ctor_mDC6C24720C4540B4A9A00A8F03A5347729CBB745_gshared)(__this, ___array0, method);
}
// System.Int32 System.ArraySegment`1<UnityEngine.Vector4>::get_Count()
inline int32_t ArraySegment_1_get_Count_m86D4F7A8C6E66A89B03DB83E4F8F82019625E483_inline (ArraySegment_1_t3BCE22176B44ACEB25236AD7501F226112F6E07A * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (ArraySegment_1_t3BCE22176B44ACEB25236AD7501F226112F6E07A *, const RuntimeMethod*))ArraySegment_1_get_Count_m86D4F7A8C6E66A89B03DB83E4F8F82019625E483_gshared_inline)(__this, method);
}
// System.Void System.ArraySegment`1<UnityEngine.Vector4>::.ctor(!0[])
inline void ArraySegment_1__ctor_m1186B90A376A0AE42259FC310386A6BE3AFD6D40 (ArraySegment_1_t3BCE22176B44ACEB25236AD7501F226112F6E07A * __this, Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871* ___array0, const RuntimeMethod* method)
{
	((  void (*) (ArraySegment_1_t3BCE22176B44ACEB25236AD7501F226112F6E07A *, Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871*, const RuntimeMethod*))ArraySegment_1__ctor_m1186B90A376A0AE42259FC310386A6BE3AFD6D40_gshared)(__this, ___array0, method);
}
// System.Int32 System.ArraySegment`1<UnityEngine.Color>::get_Count()
inline int32_t ArraySegment_1_get_Count_m313EE200EA1CB465EF2B1F8CFFA7E63FA3F408EF_inline (ArraySegment_1_t8A45E4F401BB0C4F9F8643CCA8706F98DC8057F6 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (ArraySegment_1_t8A45E4F401BB0C4F9F8643CCA8706F98DC8057F6 *, const RuntimeMethod*))ArraySegment_1_get_Count_m313EE200EA1CB465EF2B1F8CFFA7E63FA3F408EF_gshared_inline)(__this, method);
}
// !0 System.Collections.Generic.List`1<UniGLTF.glTFBuffer>::get_Item(System.Int32)
inline glTFBuffer_t840FC6FB6032C82B50388CE51BB1F054475E4BD6 * List_1_get_Item_mC4617D8E0A2C06E1168AC910BDB298A17C8F53FC_inline (List_1_t4BFB26128F5A4C3E19B9E2DEAB3A142C3EFF8F38 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  glTFBuffer_t840FC6FB6032C82B50388CE51BB1F054475E4BD6 * (*) (List_1_t4BFB26128F5A4C3E19B9E2DEAB3A142C3EFF8F38 *, int32_t, const RuntimeMethod*))List_1_get_Item_m7B5E3383CB67492E573AC0D875ED82A51350F188_gshared_inline)(__this, ___index0, method);
}
// System.Int32 System.Collections.Generic.List`1<UniGLTF.glTFBufferView>::get_Count()
inline int32_t List_1_get_Count_m3528BC05F270F79CD2DFF46FFDD7722124617F04_inline (List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 *, const RuntimeMethod*))List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline)(__this, method);
}
// System.Void System.Collections.Generic.List`1<UniGLTF.glTFBufferView>::Add(!0)
inline void List_1_Add_mDDEE5E9B13D0FE35D394C910EB8ADDF90244F666 (List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 * __this, glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4 * ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 *, glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4 *, const RuntimeMethod*))List_1_Add_mF15250BF947CA27BE9A23C08BAC6DB6F180B0EDD_gshared)(__this, ___item0, method);
}
// System.Int32 System.ArraySegment`1<System.Int32>::get_Count()
inline int32_t ArraySegment_1_get_Count_mFB0298EF19B049FBF2C0C3D7FCA5E16DB8F80D56_inline (ArraySegment_1_t9FDA96F04294A918E0DBFCE7CF750DE23A898976 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (ArraySegment_1_t9FDA96F04294A918E0DBFCE7CF750DE23A898976 *, const RuntimeMethod*))ArraySegment_1_get_Count_mFB0298EF19B049FBF2C0C3D7FCA5E16DB8F80D56_gshared_inline)(__this, method);
}
// System.Void System.ArraySegment`1<System.Int32>::.ctor(!0[])
inline void ArraySegment_1__ctor_m643D396633372662D1B12F94E9503026534D7F41 (ArraySegment_1_t9FDA96F04294A918E0DBFCE7CF750DE23A898976 * __this, Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___array0, const RuntimeMethod* method)
{
	((  void (*) (ArraySegment_1_t9FDA96F04294A918E0DBFCE7CF750DE23A898976 *, Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*, const RuntimeMethod*))ArraySegment_1__ctor_m643D396633372662D1B12F94E9503026534D7F41_gshared)(__this, ___array0, method);
}
// System.Int32 System.ArraySegment`1<UnityEngine.Matrix4x4>::get_Count()
inline int32_t ArraySegment_1_get_Count_m779174D975AA29188F9DFFC751199785A7F0ADBD_inline (ArraySegment_1_t7FCE6A956BF91EE65591716518285D293D3C5B6B * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (ArraySegment_1_t7FCE6A956BF91EE65591716518285D293D3C5B6B *, const RuntimeMethod*))ArraySegment_1_get_Count_m779174D975AA29188F9DFFC751199785A7F0ADBD_gshared_inline)(__this, method);
}
// System.Void UniGLTF.glTFSparse::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void glTFSparse__ctor_mF9AD5D5E7FC22FF264A96F90E0865BB31E739375 (glTFSparse_tF8621187A1982375A902F6D7B023E40B72886684 * __this, const RuntimeMethod* method);
// System.Void UniGLTF.glTFSparseIndices::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void glTFSparseIndices__ctor_m5F712EA16D41FA539BC70B661434283E3F9F2EC3 (glTFSparseIndices_t332206D5F85C3918BDB6880C545D1B59EF3DE614 * __this, const RuntimeMethod* method);
// System.Void UniGLTF.glTFSparseValues::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void glTFSparseValues__ctor_m9F5DAF602883EB7C62915DDCBECBE476D22FB829 (glTFSparseValues_t482F196AE1519B26241A4BB28B338F2977BC734F * __this, const RuntimeMethod* method);
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t * Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E (RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ___handle0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,UniGLTF.glTFExtensions/ComponentVec>::TryGetValue(!0,!1&)
inline bool Dictionary_2_TryGetValue_m5453E36663BAE277EFCD153F225808627F9570CD (Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E * __this, Type_t * ___key0, ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 * ___value1, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E *, Type_t *, ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 *, const RuntimeMethod*))Dictionary_2_TryGetValue_m022664ECEA6FA0306333C8E8D40DA6D2CD70C12A_gshared)(__this, ___key0, ___value1, method);
}
// System.Void System.Exception::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Exception__ctor_m0E9BEC861F6DBED197960E5BA23149543B1D7F5B (Exception_t * __this, const RuntimeMethod* method);
// System.Boolean System.Type::op_Equality(System.Type,System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Type_op_Equality_mA438719A1FDF103C7BBBB08AEF564E7FAEEA0046 (Type_t * ___left0, Type_t * ___right1, const RuntimeMethod* method);
// System.Void System.NotImplementedException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotImplementedException__ctor_m8A9AA4499614A5BC57DD21713D0720630C130AEB (NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6 * __this, String_t* ___message0, const RuntimeMethod* method);
// UnityEngine.InputSystem.InputActionPhase UnityEngine.InputSystem.InputAction/CallbackContext::get_phase()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t CallbackContext_get_phase_m1AA786B2FBD617BD9061126BF7442F08484F76E7 (CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.InputSystem.InputAction/CallbackContext::get_bindingIndex()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t CallbackContext_get_bindingIndex_mD26C42B8D97073283D42CB129F00E58AAF63C6EB (CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.InputSystem.InputAction/CallbackContext::get_controlIndex()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t CallbackContext_get_controlIndex_m56E2618DEAD8E55474756DFDCC261CCE1A4EF776 (CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B * __this, const RuntimeMethod* method);
// TValue UnityEngine.InputSystem.InputAction/CallbackContext::ReadValue<UnityEngine.Quaternion>()
inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  CallbackContext_ReadValue_TisQuaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_mE7BC042F12AD2F6B0CC56EFAD37953575750E6C9 (CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B * __this, const RuntimeMethod* method)
{
	return ((  Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  (*) (CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B *, const RuntimeMethod*))CallbackContext_ReadValue_TisQuaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_mE7BC042F12AD2F6B0CC56EFAD37953575750E6C9_gshared)(__this, method);
}
// TValue UnityEngine.InputSystem.InputAction/CallbackContext::ReadValue<System.Single>()
inline float CallbackContext_ReadValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_m2BFB8C24FFB40E00E51EC8F4DA7CAC936508D419 (CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B * __this, const RuntimeMethod* method)
{
	return ((  float (*) (CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B *, const RuntimeMethod*))CallbackContext_ReadValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_m2BFB8C24FFB40E00E51EC8F4DA7CAC936508D419_gshared)(__this, method);
}
// TValue UnityEngine.InputSystem.InputAction/CallbackContext::ReadValue<UnityEngine.Vector2>()
inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  CallbackContext_ReadValue_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_m5979F5A5F9C48193420CD889B84D010F6373171B (CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B * __this, const RuntimeMethod* method)
{
	return ((  Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  (*) (CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B *, const RuntimeMethod*))CallbackContext_ReadValue_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_m5979F5A5F9C48193420CD889B84D010F6373171B_gshared)(__this, method);
}
// TValue UnityEngine.InputSystem.InputAction/CallbackContext::ReadValue<UnityEngine.Vector3>()
inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  CallbackContext_ReadValue_TisVector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_m76A2A215A8ACD5B96AAAD484DD041DA960A11B81 (CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B * __this, const RuntimeMethod* method)
{
	return ((  Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  (*) (CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B *, const RuntimeMethod*))CallbackContext_ReadValue_TisVector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_m76A2A215A8ACD5B96AAAD484DD041DA960A11B81_gshared)(__this, method);
}
// System.Void UnityEngine.InputSystem.InputActionRebindingExtensions/RebindingOperation::ThrowIfRebindInProgress()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RebindingOperation_ThrowIfRebindInProgress_m28D12CF785947DD24FACB7DAE3E7B887C03F4908 (RebindingOperation_t5DA0F3263991E7BED360B8A1ADE014B9B451F970 * __this, const RuntimeMethod* method);
// UnityEngine.InputSystem.InputActionRebindingExtensions/RebindingOperation UnityEngine.InputSystem.InputActionRebindingExtensions/RebindingOperation::WithExpectedControlType(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RebindingOperation_t5DA0F3263991E7BED360B8A1ADE014B9B451F970 * RebindingOperation_WithExpectedControlType_m705AE6A47C8C1CA4F3E240985B5D1B96A8AAD012 (RebindingOperation_t5DA0F3263991E7BED360B8A1ADE014B9B451F970 * __this, Type_t * ___type0, const RuntimeMethod* method);
// UnityEngine.InputSystem.Utilities.InternedString UnityEngine.InputSystem.Utilities.TypeTable::FindNameForType(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InternedString_tD1138602E8B7EA0F5B4851812B13C7E0551E25C8  TypeTable_FindNameForType_m70022A55F8ADA2D7F33B9262F7F926145B4B6E4D (TypeTable_tE58155618A277C8EB10C9B8FB3040A6731E46BFA * __this, Type_t * ___type0, const RuntimeMethod* method);
// System.Boolean UnityEngine.InputSystem.Utilities.InternedString::IsEmpty()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool InternedString_IsEmpty_mD8617A5FDADD3E05A5F6253D42EFCB8BDAD11EE8 (InternedString_tD1138602E8B7EA0F5B4851812B13C7E0551E25C8 * __this, const RuntimeMethod* method);
// System.String System.String::Format(System.String,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Format_mB3D38E5238C3164DB4D7D29339D9E225A4496D17 (String_t* ___format0, RuntimeObject * ___arg01, const RuntimeMethod* method);
// System.Void System.NotSupportedException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedException__ctor_m40BC57BDA6E0E119B73700CC809A14B57DC65A90 (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * __this, String_t* ___message0, const RuntimeMethod* method);
// System.String UnityEngine.InputSystem.Utilities.InternedString::op_Implicit(UnityEngine.InputSystem.Utilities.InternedString)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* InternedString_op_Implicit_mFA680218C45365E7ED57EA5EAA6ED59F15D8DFC6 (InternedString_tD1138602E8B7EA0F5B4851812B13C7E0551E25C8  ___str0, const RuntimeMethod* method);
// UnityEngine.InputSystem.InputActionSetupExtensions/BindingSyntax UnityEngine.InputSystem.InputActionSetupExtensions/BindingSyntax::WithInteraction(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR BindingSyntax_tEF6F07812AAB1AF06B3D36C9B1570C01997C8BF1  BindingSyntax_WithInteraction_m5B7B398C4193415876B4C31BAFDCDD9EE5107576 (BindingSyntax_tEF6F07812AAB1AF06B3D36C9B1570C01997C8BF1 * __this, String_t* ___interaction0, const RuntimeMethod* method);
// UnityEngine.InputSystem.InputActionSetupExtensions/BindingSyntax UnityEngine.InputSystem.InputActionSetupExtensions/BindingSyntax::WithInteraction<System.Object>()
inline BindingSyntax_tEF6F07812AAB1AF06B3D36C9B1570C01997C8BF1  BindingSyntax_WithInteraction_TisRuntimeObject_mF004EAD1BEFB97AAECF7B3AD3C9856304B302EDC (BindingSyntax_tEF6F07812AAB1AF06B3D36C9B1570C01997C8BF1 * __this, const RuntimeMethod* method)
{
	return ((  BindingSyntax_tEF6F07812AAB1AF06B3D36C9B1570C01997C8BF1  (*) (BindingSyntax_tEF6F07812AAB1AF06B3D36C9B1570C01997C8BF1 *, const RuntimeMethod*))BindingSyntax_WithInteraction_TisRuntimeObject_mF004EAD1BEFB97AAECF7B3AD3C9856304B302EDC_gshared)(__this, method);
}
// UnityEngine.InputSystem.InputActionSetupExtensions/BindingSyntax UnityEngine.InputSystem.InputActionSetupExtensions/BindingSyntax::WithProcessor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR BindingSyntax_tEF6F07812AAB1AF06B3D36C9B1570C01997C8BF1  BindingSyntax_WithProcessor_m0A13729474D2C44C0646703F5543B891132D6484 (BindingSyntax_tEF6F07812AAB1AF06B3D36C9B1570C01997C8BF1 * __this, String_t* ___processor0, const RuntimeMethod* method);
// UnityEngine.InputSystem.InputActionSetupExtensions/BindingSyntax UnityEngine.InputSystem.InputActionSetupExtensions/BindingSyntax::WithProcessor<System.Object>()
inline BindingSyntax_tEF6F07812AAB1AF06B3D36C9B1570C01997C8BF1  BindingSyntax_WithProcessor_TisRuntimeObject_mE7F31DB067FE1D0D8B6CAE2F3A869BB60B4E15ED (BindingSyntax_tEF6F07812AAB1AF06B3D36C9B1570C01997C8BF1 * __this, const RuntimeMethod* method)
{
	return ((  BindingSyntax_tEF6F07812AAB1AF06B3D36C9B1570C01997C8BF1  (*) (BindingSyntax_tEF6F07812AAB1AF06B3D36C9B1570C01997C8BF1 *, const RuntimeMethod*))BindingSyntax_WithProcessor_TisRuntimeObject_mE7F31DB067FE1D0D8B6CAE2F3A869BB60B4E15ED_gshared)(__this, method);
}
// UnityEngine.InputSystem.Utilities.InternedString UnityEngine.InputSystem.Layouts.InputControlLayout/Collection::TryFindLayoutForType(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InternedString_tD1138602E8B7EA0F5B4851812B13C7E0551E25C8  Collection_TryFindLayoutForType_mA1BA2AB384A930262B5B405EBA6136861A0D6A5E (Collection_tDBA0BC36ACE448001ABFFED32EF785467D714D7B * __this, Type_t * ___layoutType0, const RuntimeMethod* method);
// System.String UnityEngine.InputSystem.Utilities.InternedString::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* InternedString_ToString_m2FF8E64B30FADEBCC623E0EFF944C3EDA244F6D7 (InternedString_tD1138602E8B7EA0F5B4851812B13C7E0551E25C8 * __this, const RuntimeMethod* method);
// System.Boolean System.String::IsNullOrEmpty(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_IsNullOrEmpty_m9AFBB5335B441B94E884B8A9D4A27AD60E3D7F7C (String_t* ___value0, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m89EAB4C6A96B0E5C3F87300D6BE78D386B9EFC44 (String_t* ___str00, String_t* ___str11, String_t* ___str22, const RuntimeMethod* method);
// System.String UnityEngine.InputSystem.InputActionSetupExtensions/ControlSchemeSyntax::DeviceTypeToControlPath<System.Object>()
inline String_t* ControlSchemeSyntax_DeviceTypeToControlPath_TisRuntimeObject_mB700493B60AC1EDE0CA6A7C5B54091F0B1343D4F (ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214 * __this, const RuntimeMethod* method)
{
	return ((  String_t* (*) (ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214 *, const RuntimeMethod*))ControlSchemeSyntax_DeviceTypeToControlPath_TisRuntimeObject_mB700493B60AC1EDE0CA6A7C5B54091F0B1343D4F_gshared)(__this, method);
}
// UnityEngine.InputSystem.InputActionSetupExtensions/ControlSchemeSyntax UnityEngine.InputSystem.InputActionSetupExtensions/ControlSchemeSyntax::WithOptionalDevice(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214  ControlSchemeSyntax_WithOptionalDevice_m1EEB8035CC62D2A78AC932A7CA0440AF94126C39 (ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214 * __this, String_t* ___controlPath0, const RuntimeMethod* method);
// UnityEngine.InputSystem.InputActionSetupExtensions/ControlSchemeSyntax UnityEngine.InputSystem.InputActionSetupExtensions/ControlSchemeSyntax::OrWithOptionalDevice<System.Object>()
inline ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214  ControlSchemeSyntax_OrWithOptionalDevice_TisRuntimeObject_mA966529367FEAF671DF7725154538CB7312C1D2F (ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214 * __this, const RuntimeMethod* method)
{
	return ((  ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214  (*) (ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214 *, const RuntimeMethod*))ControlSchemeSyntax_OrWithOptionalDevice_TisRuntimeObject_mA966529367FEAF671DF7725154538CB7312C1D2F_gshared)(__this, method);
}
// UnityEngine.InputSystem.InputActionSetupExtensions/ControlSchemeSyntax UnityEngine.InputSystem.InputActionSetupExtensions/ControlSchemeSyntax::WithRequiredDevice(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214  ControlSchemeSyntax_WithRequiredDevice_m7D612CC028141089F3AB419967A9A7F1D9FC3353 (ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214 * __this, String_t* ___controlPath0, const RuntimeMethod* method);
// UnityEngine.InputSystem.InputActionSetupExtensions/ControlSchemeSyntax UnityEngine.InputSystem.InputActionSetupExtensions/ControlSchemeSyntax::OrWithRequiredDevice<System.Object>()
inline ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214  ControlSchemeSyntax_OrWithRequiredDevice_TisRuntimeObject_m0F28425B0D706D1C1C43B7A30024ACBFC00A3518 (ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214 * __this, const RuntimeMethod* method)
{
	return ((  ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214  (*) (ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214 *, const RuntimeMethod*))ControlSchemeSyntax_OrWithRequiredDevice_TisRuntimeObject_m0F28425B0D706D1C1C43B7A30024ACBFC00A3518_gshared)(__this, method);
}
// UnityEngine.InputSystem.InputActionSetupExtensions/ControlSchemeSyntax UnityEngine.InputSystem.InputActionSetupExtensions/ControlSchemeSyntax::WithOptionalDevice<System.Object>()
inline ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214  ControlSchemeSyntax_WithOptionalDevice_TisRuntimeObject_mEEDD90A8BFEDAC21FF0B67F8F3BC50208F09D015 (ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214 * __this, const RuntimeMethod* method)
{
	return ((  ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214  (*) (ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214 *, const RuntimeMethod*))ControlSchemeSyntax_WithOptionalDevice_TisRuntimeObject_mEEDD90A8BFEDAC21FF0B67F8F3BC50208F09D015_gshared)(__this, method);
}
// UnityEngine.InputSystem.InputActionSetupExtensions/ControlSchemeSyntax UnityEngine.InputSystem.InputActionSetupExtensions/ControlSchemeSyntax::WithRequiredDevice<System.Object>()
inline ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214  ControlSchemeSyntax_WithRequiredDevice_TisRuntimeObject_mC0CB0D601DCB446ACF2E02BAE15CDAAD58DAED10 (ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214 * __this, const RuntimeMethod* method)
{
	return ((  ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214  (*) (ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214 *, const RuntimeMethod*))ControlSchemeSyntax_WithRequiredDevice_TisRuntimeObject_mC0CB0D601DCB446ACF2E02BAE15CDAAD58DAED10_gshared)(__this, method);
}
// System.Void UnityEngine.InputSystem.Layouts.InputControlLayout/Builder::set_type(System.Type)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Builder_set_type_m31E443E368751EC60EBA1A39D670313269E50B74_inline (Builder_t4C196179CE57554255EBFB4ECD4D6499ADA974FB * __this, Type_t * ___value0, const RuntimeMethod* method);
// !!0[] System.Array::Empty<System.Linq.Expressions.ParameterExpression>()
inline ParameterExpressionU5BU5D_tCF3EAA6D8556513C4937E1126F65AA08DF4DB147* Array_Empty_TisParameterExpression_tA7B24F1DE0F013DA4BD55F76DB43B06DB33D8BEE_m5F471CFF3314A2643178F228536765874B6A83EF_inline (const RuntimeMethod* method)
{
	return ((  ParameterExpressionU5BU5D_tCF3EAA6D8556513C4937E1126F65AA08DF4DB147* (*) (const RuntimeMethod*))Array_Empty_TisRuntimeObject_m002765312BF306B1B3B5BFAB9550C0A2A1820CDA_gshared_inline)(method);
}
// System.Void System.Collections.Generic.List`1<System.Linq.Expressions.ParameterExpression>::.ctor(System.Int32)
inline void List_1__ctor_mE4C9B3F15E5D5168479F4E7227A000B97C871A30 (List_1_tCDDF33E8793E2DD752E38CC326B13F8F35B1493B * __this, int32_t ___capacity0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tCDDF33E8793E2DD752E38CC326B13F8F35B1493B *, int32_t, const RuntimeMethod*))List_1__ctor_mFEB2301A6F28290A828A979BA9CC847B16B3D538_gshared)(__this, ___capacity0, method);
}
// System.Void System.Collections.Generic.List`1<System.Linq.Expressions.ParameterExpression>::Add(!0)
inline void List_1_Add_m0B405996905BBAFA7ED5939184F2344559D3400C (List_1_tCDDF33E8793E2DD752E38CC326B13F8F35B1493B * __this, ParameterExpression_tA7B24F1DE0F013DA4BD55F76DB43B06DB33D8BEE * ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tCDDF33E8793E2DD752E38CC326B13F8F35B1493B *, ParameterExpression_tA7B24F1DE0F013DA4BD55F76DB43B06DB33D8BEE *, const RuntimeMethod*))List_1_Add_mF15250BF947CA27BE9A23C08BAC6DB6F180B0EDD_gshared)(__this, ___item0, method);
}
// System.Void System.Linq.Expressions.Interpreter.LightCompiler/QuoteVisitor::PushParameters(System.Collections.Generic.IEnumerable`1<System.Linq.Expressions.ParameterExpression>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void QuoteVisitor_PushParameters_m7AAC447E0627A0AD1A5EBAB7A7AFFD5F239CC0ED (QuoteVisitor_tFE404B4C826642C3FC245A108AEC9E94D691E627 * __this, RuntimeObject* ___parameters0, const RuntimeMethod* method);
// System.Void System.Linq.Expressions.Interpreter.LightCompiler/QuoteVisitor::PopParameters(System.Collections.Generic.IEnumerable`1<System.Linq.Expressions.ParameterExpression>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void QuoteVisitor_PopParameters_m88C9C499B152E0333072D63BFD2908F2D710495A (QuoteVisitor_tFE404B4C826642C3FC245A108AEC9E94D691E627 * __this, RuntimeObject* ___parameters0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.HashSet`1<System.Linq.Expressions.ParameterExpression>::.ctor()
inline void HashSet_1__ctor_m7E015D0E7832B3967403CAEE703C819D77AE741B (HashSet_1_t42A3AC337CA15FAC250AA5DA438F909806C72CB0 * __this, const RuntimeMethod* method)
{
	((  void (*) (HashSet_1_t42A3AC337CA15FAC250AA5DA438F909806C72CB0 *, const RuntimeMethod*))HashSet_1__ctor_m2CDA40DEC2900A9CB00F8348FF386DF44ABD0EC7_gshared)(__this, method);
}
// System.Boolean System.Collections.Generic.HashSet`1<System.Linq.Expressions.ParameterExpression>::Add(T)
inline bool HashSet_1_Add_m9CF730E2C32C0613D8089427318C09E55C350DBE (HashSet_1_t42A3AC337CA15FAC250AA5DA438F909806C72CB0 * __this, ParameterExpression_tA7B24F1DE0F013DA4BD55F76DB43B06DB33D8BEE * ___item0, const RuntimeMethod* method)
{
	return ((  bool (*) (HashSet_1_t42A3AC337CA15FAC250AA5DA438F909806C72CB0 *, ParameterExpression_tA7B24F1DE0F013DA4BD55F76DB43B06DB33D8BEE *, const RuntimeMethod*))HashSet_1_Add_m8194125167FD7A887F1B39D342A71A0BD0C8BF5C_gshared)(__this, ___item0, method);
}
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.HashSet`1<System.Linq.Expressions.ParameterExpression>>::Push(!0)
inline void Stack_1_Push_m40AB4A635E6FAFC5491A227935BF4EB1D070C8E1 (Stack_1_t438C22E9DF33740A9BDB48CF9504B6E044484958 * __this, HashSet_1_t42A3AC337CA15FAC250AA5DA438F909806C72CB0 * ___item0, const RuntimeMethod* method)
{
	((  void (*) (Stack_1_t438C22E9DF33740A9BDB48CF9504B6E044484958 *, HashSet_1_t42A3AC337CA15FAC250AA5DA438F909806C72CB0 *, const RuntimeMethod*))Stack_1_Push_m8FFF613733BF88B54FCD4498B1E97DE296E27757_gshared)(__this, ___item0, method);
}
// System.Linq.Expressions.Expression System.Linq.Expressions.LambdaExpression::get_Body()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Expression_t30A004209C10C2D9A9785B2F74EEED431A4D4660 * LambdaExpression_get_Body_m595A485419E2F0AA13FC2695DEBD99ED9712D042_inline (LambdaExpression_t26BF905E97E6D94F81F17D60F4F4F47F8E93B474 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.Stack`1<System.Collections.Generic.HashSet`1<System.Linq.Expressions.ParameterExpression>>::Pop()
inline HashSet_1_t42A3AC337CA15FAC250AA5DA438F909806C72CB0 * Stack_1_Pop_m1B41FE87FFACE09E92B93079328100A46D7A9550 (Stack_1_t438C22E9DF33740A9BDB48CF9504B6E044484958 * __this, const RuntimeMethod* method)
{
	return ((  HashSet_1_t42A3AC337CA15FAC250AA5DA438F909806C72CB0 * (*) (Stack_1_t438C22E9DF33740A9BDB48CF9504B6E044484958 *, const RuntimeMethod*))Stack_1_Pop_m883C5DEF33CA2E556AC710A9BE26F8BD2D4B182C_gshared)(__this, method);
}
// System.Int32 UnityEngine.Rendering.DynamicArray`1<UnityEngine.Experimental.Rendering.RenderGraphModule.IRenderGraphResource>::get_size()
inline int32_t DynamicArray_1_get_size_m45BF02C8D0C22798BED29BCA0C5470C01CB2A015_inline (DynamicArray_1_tE865ACD9B04344D2C5D88A7A1DDE805DDE7B5616 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (DynamicArray_1_tE865ACD9B04344D2C5D88A7A1DDE805DDE7B5616 *, const RuntimeMethod*))DynamicArray_1_get_size_m0D404D0EDA77F50046B5592673CEADED3EB1DDF9_gshared_inline)(__this, method);
}
// System.Void UnityEngine.Rendering.DynamicArray`1<UnityEngine.Experimental.Rendering.RenderGraphModule.IRenderGraphResource>::Resize(System.Int32,System.Boolean)
inline void DynamicArray_1_Resize_mF6CDBE3B7E4118B28D15C20B1CA3DE54A23AB9FC (DynamicArray_1_tE865ACD9B04344D2C5D88A7A1DDE805DDE7B5616 * __this, int32_t ___newSize0, bool ___keepContent1, const RuntimeMethod* method)
{
	((  void (*) (DynamicArray_1_tE865ACD9B04344D2C5D88A7A1DDE805DDE7B5616 *, int32_t, bool, const RuntimeMethod*))DynamicArray_1_Resize_m3CEAE3B337E1361CC36C95D76BF653D9ADD630A2_gshared)(__this, ___newSize0, ___keepContent1, method);
}
// T& UnityEngine.Rendering.DynamicArray`1<UnityEngine.Experimental.Rendering.RenderGraphModule.IRenderGraphResource>::get_Item(System.Int32)
inline IRenderGraphResource_tC6A193679EB59585903B3E0EFC7F5B937C68AAE7 ** DynamicArray_1_get_Item_mA0C22D4E0321EFABE0334B7074D3E0978CE46251 (DynamicArray_1_tE865ACD9B04344D2C5D88A7A1DDE805DDE7B5616 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  IRenderGraphResource_tC6A193679EB59585903B3E0EFC7F5B937C68AAE7 ** (*) (DynamicArray_1_tE865ACD9B04344D2C5D88A7A1DDE805DDE7B5616 *, int32_t, const RuntimeMethod*))DynamicArray_1_get_Item_m8D97D96230A22384E91DC49B6855A34063768376_gshared)(__this, ___index0, method);
}
// System.Boolean Unity.Collections.NativeArray`1<UnityEngine.Vector2>::get_IsCreated()
inline bool NativeArray_1_get_IsCreated_mF82CF064C6050394DEAC11047A6308998101A4AE (NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0 *, const RuntimeMethod*))NativeArray_1_get_IsCreated_mF82CF064C6050394DEAC11047A6308998101A4AE_gshared)(__this, method);
}
// System.Void Unity.Collections.NativeArray`1<UnityEngine.Vector2>::Dispose()
inline void NativeArray_1_Dispose_mE737DADB8C5E4B02C2FB6CB36E86AC6371D806BF (NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0 * __this, const RuntimeMethod* method)
{
	((  void (*) (NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0 *, const RuntimeMethod*))NativeArray_1_Dispose_mE737DADB8C5E4B02C2FB6CB36E86AC6371D806BF_gshared)(__this, method);
}
// System.Void Unity.Collections.NativeArray`1<UnityEngine.Vector2>::.ctor(System.Int32,Unity.Collections.Allocator,Unity.Collections.NativeArrayOptions)
inline void NativeArray_1__ctor_m53A2E0E5DDF506B8508EAC444CAF3DB70F874729 (NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0 * __this, int32_t ___length0, int32_t ___allocator1, int32_t ___options2, const RuntimeMethod* method)
{
	((  void (*) (NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0 *, int32_t, int32_t, int32_t, const RuntimeMethod*))NativeArray_1__ctor_m53A2E0E5DDF506B8508EAC444CAF3DB70F874729_gshared)(__this, ___length0, ___allocator1, ___options2, method);
}
// System.Void System.ThrowHelper::ThrowArgumentOutOfRangeException()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929 (const RuntimeMethod* method);
// System.Int32 UniGLTF.glTFExtensions::ExtendBufferAndGetAccessorIndex<UnityEngine.Matrix4x4>(UniGLTF.glTF,System.Int32,T[],UniGLTF.glBufferTarget)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t glTFExtensions_ExtendBufferAndGetAccessorIndex_TisMatrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461_m0995AE9A06B6FDA589D09A4C996DA6447E4E6895_gshared (glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * ___gltf0, int32_t ___bufferIndex1, Matrix4x4U5BU5D_tE53F71E9C9110DD439281A6AB8B699F9F85D8F82* ___array2, int32_t ___target3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return gltf.ExtendBufferAndGetAccessorIndex(bufferIndex, new ArraySegment<T>(array), target);
		glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * L_0 = ___gltf0;
		int32_t L_1 = ___bufferIndex1;
		Matrix4x4U5BU5D_tE53F71E9C9110DD439281A6AB8B699F9F85D8F82* L_2 = ___array2;
		ArraySegment_1_t7FCE6A956BF91EE65591716518285D293D3C5B6B  L_3;
		memset((&L_3), 0, sizeof(L_3));
		ArraySegment_1__ctor_mE3508383112A119C6D1039270F35F365CC0763E5((&L_3), (Matrix4x4U5BU5D_tE53F71E9C9110DD439281A6AB8B699F9F85D8F82*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		int32_t L_4 = ___target3;
		IL2CPP_RUNTIME_CLASS_INIT(glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		int32_t L_5;
		L_5 = ((  int32_t (*) (glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 *, int32_t, ArraySegment_1_t7FCE6A956BF91EE65591716518285D293D3C5B6B , int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)((glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 *)L_0, (int32_t)L_1, (ArraySegment_1_t7FCE6A956BF91EE65591716518285D293D3C5B6B )L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		return (int32_t)L_5;
	}
}
// System.Int32 UniGLTF.glTFExtensions::ExtendBufferAndGetAccessorIndex<System.UInt32>(UniGLTF.glTF,System.Int32,System.ArraySegment`1<T>,UniGLTF.glBufferTarget)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t glTFExtensions_ExtendBufferAndGetAccessorIndex_TisUInt32_tE60352A06233E4E69DD198BCC67142159F686B15_mE8B6D1205D2C2EDEF249B205C8B1561F2790A363_gshared (glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * ___gltf0, int32_t ___bufferIndex1, ArraySegment_1_tE4B89ED4839E563DF2A25436675AC5C5E87B62B9  ___array2, int32_t ___target3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m6189B75EF62756B468C403149F4AD081AC68C912_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mC221419694BDE6FF50C3F68C0964472743ACBA03_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// if (array.Count == 0)
		int32_t L_0;
		L_0 = ArraySegment_1_get_Count_mF79DF195C03504D335115BA66BC48FB0BB4D395C_inline((ArraySegment_1_tE4B89ED4839E563DF2A25436675AC5C5E87B62B9 *)(ArraySegment_1_tE4B89ED4839E563DF2A25436675AC5C5E87B62B9 *)(&___array2), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		// return -1;
		return (int32_t)(-1);
	}

IL_000b:
	{
		// var viewIndex = ExtendBufferAndGetViewIndex(gltf, bufferIndex, array, target);
		glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * L_1 = ___gltf0;
		int32_t L_2 = ___bufferIndex1;
		ArraySegment_1_tE4B89ED4839E563DF2A25436675AC5C5E87B62B9  L_3 = ___array2;
		int32_t L_4 = ___target3;
		IL2CPP_RUNTIME_CLASS_INIT(glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		int32_t L_5;
		L_5 = ((  int32_t (*) (glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 *, int32_t, ArraySegment_1_tE4B89ED4839E563DF2A25436675AC5C5E87B62B9 , int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 *)L_1, (int32_t)L_2, (ArraySegment_1_tE4B89ED4839E563DF2A25436675AC5C5E87B62B9 )L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		V_0 = (int32_t)L_5;
		// var accessorIndex = gltf.accessors.Count;
		glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * L_6 = ___gltf0;
		NullCheck(L_6);
		List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC * L_7 = (List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC *)L_6->get_accessors_3();
		NullCheck((List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC *)L_7);
		int32_t L_8;
		L_8 = List_1_get_Count_mC221419694BDE6FF50C3F68C0964472743ACBA03_inline((List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC *)L_7, /*hidden argument*/List_1_get_Count_mC221419694BDE6FF50C3F68C0964472743ACBA03_RuntimeMethod_var);
		// gltf.accessors.Add(new glTFAccessor
		// {
		//     bufferView = viewIndex,
		//     byteOffset = 0,
		//     componentType = GetComponentType<T>(),
		//     type = GetAccessorType<T>(),
		//     count = array.Count,
		// });
		glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * L_9 = ___gltf0;
		NullCheck(L_9);
		List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC * L_10 = (List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC *)L_9->get_accessors_3();
		glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC * L_11 = (glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC *)il2cpp_codegen_object_new(glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC_il2cpp_TypeInfo_var);
		glTFAccessor__ctor_m0EF366F9EC3426182BDF6055B0B20661744327B8(L_11, /*hidden argument*/NULL);
		glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC * L_12 = (glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC *)L_11;
		int32_t L_13 = V_0;
		NullCheck(L_12);
		L_12->set_bufferView_0(L_13);
		glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC * L_14 = (glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC *)L_12;
		NullCheck(L_14);
		L_14->set_byteOffset_1(0);
		glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC * L_15 = (glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC *)L_14;
		int32_t L_16;
		L_16 = ((  int32_t (*) (const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)(/*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		NullCheck(L_15);
		L_15->set_componentType_3(L_16);
		glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC * L_17 = (glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC *)L_15;
		String_t* L_18;
		L_18 = ((  String_t* (*) (const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)->methodPointer)(/*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3));
		NullCheck(L_17);
		L_17->set_type_2(L_18);
		glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC * L_19 = (glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC *)L_17;
		int32_t L_20;
		L_20 = ArraySegment_1_get_Count_mF79DF195C03504D335115BA66BC48FB0BB4D395C_inline((ArraySegment_1_tE4B89ED4839E563DF2A25436675AC5C5E87B62B9 *)(ArraySegment_1_tE4B89ED4839E563DF2A25436675AC5C5E87B62B9 *)(&___array2), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		NullCheck(L_19);
		L_19->set_count_4(L_20);
		NullCheck((List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC *)L_10);
		List_1_Add_m6189B75EF62756B468C403149F4AD081AC68C912((List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC *)L_10, (glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC *)L_19, /*hidden argument*/List_1_Add_m6189B75EF62756B468C403149F4AD081AC68C912_RuntimeMethod_var);
		// return accessorIndex;
		return (int32_t)L_8;
	}
}
// System.Int32 UniGLTF.glTFExtensions::ExtendBufferAndGetAccessorIndex<System.UInt32>(UniGLTF.glTF,System.Int32,T[],UniGLTF.glBufferTarget)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t glTFExtensions_ExtendBufferAndGetAccessorIndex_TisUInt32_tE60352A06233E4E69DD198BCC67142159F686B15_mE86C6AC4EED03A0BBCCAD4177B3262F1CD8BDD48_gshared (glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * ___gltf0, int32_t ___bufferIndex1, UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* ___array2, int32_t ___target3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return gltf.ExtendBufferAndGetAccessorIndex(bufferIndex, new ArraySegment<T>(array), target);
		glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * L_0 = ___gltf0;
		int32_t L_1 = ___bufferIndex1;
		UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* L_2 = ___array2;
		ArraySegment_1_tE4B89ED4839E563DF2A25436675AC5C5E87B62B9  L_3;
		memset((&L_3), 0, sizeof(L_3));
		ArraySegment_1__ctor_m8D3EA3869F4DD0072CF891BBB456B9DA390AE0A9((&L_3), (UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		int32_t L_4 = ___target3;
		IL2CPP_RUNTIME_CLASS_INIT(glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		int32_t L_5;
		L_5 = ((  int32_t (*) (glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 *, int32_t, ArraySegment_1_tE4B89ED4839E563DF2A25436675AC5C5E87B62B9 , int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)((glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 *)L_0, (int32_t)L_1, (ArraySegment_1_tE4B89ED4839E563DF2A25436675AC5C5E87B62B9 )L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		return (int32_t)L_5;
	}
}
// System.Int32 UniGLTF.glTFExtensions::ExtendBufferAndGetAccessorIndex<UniGLTF.UShort4>(UniGLTF.glTF,System.Int32,System.ArraySegment`1<T>,UniGLTF.glBufferTarget)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t glTFExtensions_ExtendBufferAndGetAccessorIndex_TisUShort4_tB19A4ECD972B8600C9515187BC86B572B83920DB_m7E7E4743942656E3731353C21BB8F1D255101021_gshared (glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * ___gltf0, int32_t ___bufferIndex1, ArraySegment_1_t9487D5EA4323A4B2C1F7F94C91363EBEE33F3462  ___array2, int32_t ___target3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m6189B75EF62756B468C403149F4AD081AC68C912_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mC221419694BDE6FF50C3F68C0964472743ACBA03_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// if (array.Count == 0)
		int32_t L_0;
		L_0 = ArraySegment_1_get_Count_mC78F0263F5BAC4CA823FA314F69DD18E7E4307D2_inline((ArraySegment_1_t9487D5EA4323A4B2C1F7F94C91363EBEE33F3462 *)(ArraySegment_1_t9487D5EA4323A4B2C1F7F94C91363EBEE33F3462 *)(&___array2), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		// return -1;
		return (int32_t)(-1);
	}

IL_000b:
	{
		// var viewIndex = ExtendBufferAndGetViewIndex(gltf, bufferIndex, array, target);
		glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * L_1 = ___gltf0;
		int32_t L_2 = ___bufferIndex1;
		ArraySegment_1_t9487D5EA4323A4B2C1F7F94C91363EBEE33F3462  L_3 = ___array2;
		int32_t L_4 = ___target3;
		IL2CPP_RUNTIME_CLASS_INIT(glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		int32_t L_5;
		L_5 = ((  int32_t (*) (glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 *, int32_t, ArraySegment_1_t9487D5EA4323A4B2C1F7F94C91363EBEE33F3462 , int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 *)L_1, (int32_t)L_2, (ArraySegment_1_t9487D5EA4323A4B2C1F7F94C91363EBEE33F3462 )L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		V_0 = (int32_t)L_5;
		// var accessorIndex = gltf.accessors.Count;
		glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * L_6 = ___gltf0;
		NullCheck(L_6);
		List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC * L_7 = (List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC *)L_6->get_accessors_3();
		NullCheck((List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC *)L_7);
		int32_t L_8;
		L_8 = List_1_get_Count_mC221419694BDE6FF50C3F68C0964472743ACBA03_inline((List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC *)L_7, /*hidden argument*/List_1_get_Count_mC221419694BDE6FF50C3F68C0964472743ACBA03_RuntimeMethod_var);
		// gltf.accessors.Add(new glTFAccessor
		// {
		//     bufferView = viewIndex,
		//     byteOffset = 0,
		//     componentType = GetComponentType<T>(),
		//     type = GetAccessorType<T>(),
		//     count = array.Count,
		// });
		glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * L_9 = ___gltf0;
		NullCheck(L_9);
		List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC * L_10 = (List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC *)L_9->get_accessors_3();
		glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC * L_11 = (glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC *)il2cpp_codegen_object_new(glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC_il2cpp_TypeInfo_var);
		glTFAccessor__ctor_m0EF366F9EC3426182BDF6055B0B20661744327B8(L_11, /*hidden argument*/NULL);
		glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC * L_12 = (glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC *)L_11;
		int32_t L_13 = V_0;
		NullCheck(L_12);
		L_12->set_bufferView_0(L_13);
		glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC * L_14 = (glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC *)L_12;
		NullCheck(L_14);
		L_14->set_byteOffset_1(0);
		glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC * L_15 = (glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC *)L_14;
		int32_t L_16;
		L_16 = ((  int32_t (*) (const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)(/*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		NullCheck(L_15);
		L_15->set_componentType_3(L_16);
		glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC * L_17 = (glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC *)L_15;
		String_t* L_18;
		L_18 = ((  String_t* (*) (const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)->methodPointer)(/*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3));
		NullCheck(L_17);
		L_17->set_type_2(L_18);
		glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC * L_19 = (glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC *)L_17;
		int32_t L_20;
		L_20 = ArraySegment_1_get_Count_mC78F0263F5BAC4CA823FA314F69DD18E7E4307D2_inline((ArraySegment_1_t9487D5EA4323A4B2C1F7F94C91363EBEE33F3462 *)(ArraySegment_1_t9487D5EA4323A4B2C1F7F94C91363EBEE33F3462 *)(&___array2), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		NullCheck(L_19);
		L_19->set_count_4(L_20);
		NullCheck((List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC *)L_10);
		List_1_Add_m6189B75EF62756B468C403149F4AD081AC68C912((List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC *)L_10, (glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC *)L_19, /*hidden argument*/List_1_Add_m6189B75EF62756B468C403149F4AD081AC68C912_RuntimeMethod_var);
		// return accessorIndex;
		return (int32_t)L_8;
	}
}
// System.Int32 UniGLTF.glTFExtensions::ExtendBufferAndGetAccessorIndex<UniGLTF.UShort4>(UniGLTF.glTF,System.Int32,T[],UniGLTF.glBufferTarget)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t glTFExtensions_ExtendBufferAndGetAccessorIndex_TisUShort4_tB19A4ECD972B8600C9515187BC86B572B83920DB_m11003A23FE1485C858747B6FB78B82124CF44DCE_gshared (glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * ___gltf0, int32_t ___bufferIndex1, UShort4U5BU5D_tBDA4C224D509794003F0CCB10B200496F61CEA35* ___array2, int32_t ___target3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return gltf.ExtendBufferAndGetAccessorIndex(bufferIndex, new ArraySegment<T>(array), target);
		glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * L_0 = ___gltf0;
		int32_t L_1 = ___bufferIndex1;
		UShort4U5BU5D_tBDA4C224D509794003F0CCB10B200496F61CEA35* L_2 = ___array2;
		ArraySegment_1_t9487D5EA4323A4B2C1F7F94C91363EBEE33F3462  L_3;
		memset((&L_3), 0, sizeof(L_3));
		ArraySegment_1__ctor_m7E66C7B5F5F7EA5DAA72B9D5F676B8CDBBA6EB06((&L_3), (UShort4U5BU5D_tBDA4C224D509794003F0CCB10B200496F61CEA35*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		int32_t L_4 = ___target3;
		IL2CPP_RUNTIME_CLASS_INIT(glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		int32_t L_5;
		L_5 = ((  int32_t (*) (glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 *, int32_t, ArraySegment_1_t9487D5EA4323A4B2C1F7F94C91363EBEE33F3462 , int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)((glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 *)L_0, (int32_t)L_1, (ArraySegment_1_t9487D5EA4323A4B2C1F7F94C91363EBEE33F3462 )L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		return (int32_t)L_5;
	}
}
// System.Int32 UniGLTF.glTFExtensions::ExtendBufferAndGetAccessorIndex<UnityEngine.Vector2>(UniGLTF.glTF,System.Int32,System.ArraySegment`1<T>,UniGLTF.glBufferTarget)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t glTFExtensions_ExtendBufferAndGetAccessorIndex_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_m0AB92D916019B2F1E57311E2C3AA90220CB7AE50_gshared (glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * ___gltf0, int32_t ___bufferIndex1, ArraySegment_1_t73AEFBC798515A51F93CF5816E332A0654AE698D  ___array2, int32_t ___target3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m6189B75EF62756B468C403149F4AD081AC68C912_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mC221419694BDE6FF50C3F68C0964472743ACBA03_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// if (array.Count == 0)
		int32_t L_0;
		L_0 = ArraySegment_1_get_Count_m038FD84B16BD0AC31B33B3B9D9237708DFC1AA32_inline((ArraySegment_1_t73AEFBC798515A51F93CF5816E332A0654AE698D *)(ArraySegment_1_t73AEFBC798515A51F93CF5816E332A0654AE698D *)(&___array2), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		// return -1;
		return (int32_t)(-1);
	}

IL_000b:
	{
		// var viewIndex = ExtendBufferAndGetViewIndex(gltf, bufferIndex, array, target);
		glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * L_1 = ___gltf0;
		int32_t L_2 = ___bufferIndex1;
		ArraySegment_1_t73AEFBC798515A51F93CF5816E332A0654AE698D  L_3 = ___array2;
		int32_t L_4 = ___target3;
		IL2CPP_RUNTIME_CLASS_INIT(glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		int32_t L_5;
		L_5 = ((  int32_t (*) (glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 *, int32_t, ArraySegment_1_t73AEFBC798515A51F93CF5816E332A0654AE698D , int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 *)L_1, (int32_t)L_2, (ArraySegment_1_t73AEFBC798515A51F93CF5816E332A0654AE698D )L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		V_0 = (int32_t)L_5;
		// var accessorIndex = gltf.accessors.Count;
		glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * L_6 = ___gltf0;
		NullCheck(L_6);
		List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC * L_7 = (List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC *)L_6->get_accessors_3();
		NullCheck((List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC *)L_7);
		int32_t L_8;
		L_8 = List_1_get_Count_mC221419694BDE6FF50C3F68C0964472743ACBA03_inline((List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC *)L_7, /*hidden argument*/List_1_get_Count_mC221419694BDE6FF50C3F68C0964472743ACBA03_RuntimeMethod_var);
		// gltf.accessors.Add(new glTFAccessor
		// {
		//     bufferView = viewIndex,
		//     byteOffset = 0,
		//     componentType = GetComponentType<T>(),
		//     type = GetAccessorType<T>(),
		//     count = array.Count,
		// });
		glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * L_9 = ___gltf0;
		NullCheck(L_9);
		List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC * L_10 = (List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC *)L_9->get_accessors_3();
		glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC * L_11 = (glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC *)il2cpp_codegen_object_new(glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC_il2cpp_TypeInfo_var);
		glTFAccessor__ctor_m0EF366F9EC3426182BDF6055B0B20661744327B8(L_11, /*hidden argument*/NULL);
		glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC * L_12 = (glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC *)L_11;
		int32_t L_13 = V_0;
		NullCheck(L_12);
		L_12->set_bufferView_0(L_13);
		glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC * L_14 = (glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC *)L_12;
		NullCheck(L_14);
		L_14->set_byteOffset_1(0);
		glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC * L_15 = (glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC *)L_14;
		int32_t L_16;
		L_16 = ((  int32_t (*) (const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)(/*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		NullCheck(L_15);
		L_15->set_componentType_3(L_16);
		glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC * L_17 = (glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC *)L_15;
		String_t* L_18;
		L_18 = ((  String_t* (*) (const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)->methodPointer)(/*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3));
		NullCheck(L_17);
		L_17->set_type_2(L_18);
		glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC * L_19 = (glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC *)L_17;
		int32_t L_20;
		L_20 = ArraySegment_1_get_Count_m038FD84B16BD0AC31B33B3B9D9237708DFC1AA32_inline((ArraySegment_1_t73AEFBC798515A51F93CF5816E332A0654AE698D *)(ArraySegment_1_t73AEFBC798515A51F93CF5816E332A0654AE698D *)(&___array2), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		NullCheck(L_19);
		L_19->set_count_4(L_20);
		NullCheck((List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC *)L_10);
		List_1_Add_m6189B75EF62756B468C403149F4AD081AC68C912((List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC *)L_10, (glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC *)L_19, /*hidden argument*/List_1_Add_m6189B75EF62756B468C403149F4AD081AC68C912_RuntimeMethod_var);
		// return accessorIndex;
		return (int32_t)L_8;
	}
}
// System.Int32 UniGLTF.glTFExtensions::ExtendBufferAndGetAccessorIndex<UnityEngine.Vector2>(UniGLTF.glTF,System.Int32,T[],UniGLTF.glBufferTarget)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t glTFExtensions_ExtendBufferAndGetAccessorIndex_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_m74E1D0FC612B6A740B65B92D428AEAE9932E1DD0_gshared (glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * ___gltf0, int32_t ___bufferIndex1, Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___array2, int32_t ___target3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return gltf.ExtendBufferAndGetAccessorIndex(bufferIndex, new ArraySegment<T>(array), target);
		glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * L_0 = ___gltf0;
		int32_t L_1 = ___bufferIndex1;
		Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* L_2 = ___array2;
		ArraySegment_1_t73AEFBC798515A51F93CF5816E332A0654AE698D  L_3;
		memset((&L_3), 0, sizeof(L_3));
		ArraySegment_1__ctor_m93C686A91083A067A9AD065F4D76D36D1C80F444((&L_3), (Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		int32_t L_4 = ___target3;
		IL2CPP_RUNTIME_CLASS_INIT(glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		int32_t L_5;
		L_5 = ((  int32_t (*) (glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 *, int32_t, ArraySegment_1_t73AEFBC798515A51F93CF5816E332A0654AE698D , int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)((glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 *)L_0, (int32_t)L_1, (ArraySegment_1_t73AEFBC798515A51F93CF5816E332A0654AE698D )L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		return (int32_t)L_5;
	}
}
// System.Int32 UniGLTF.glTFExtensions::ExtendBufferAndGetAccessorIndex<UnityEngine.Vector3>(UniGLTF.glTF,System.Int32,System.ArraySegment`1<T>,UniGLTF.glBufferTarget)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t glTFExtensions_ExtendBufferAndGetAccessorIndex_TisVector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_mC9B6735759C3BFCE2050078E7232DE92FE2B799D_gshared (glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * ___gltf0, int32_t ___bufferIndex1, ArraySegment_1_t12E2F3BC867255EB056CA1F106A8D1E85142A3F1  ___array2, int32_t ___target3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m6189B75EF62756B468C403149F4AD081AC68C912_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mC221419694BDE6FF50C3F68C0964472743ACBA03_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// if (array.Count == 0)
		int32_t L_0;
		L_0 = ArraySegment_1_get_Count_m03AF4BDAA1D5D96096A0072A85C43C2FBC7C03EE_inline((ArraySegment_1_t12E2F3BC867255EB056CA1F106A8D1E85142A3F1 *)(ArraySegment_1_t12E2F3BC867255EB056CA1F106A8D1E85142A3F1 *)(&___array2), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		// return -1;
		return (int32_t)(-1);
	}

IL_000b:
	{
		// var viewIndex = ExtendBufferAndGetViewIndex(gltf, bufferIndex, array, target);
		glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * L_1 = ___gltf0;
		int32_t L_2 = ___bufferIndex1;
		ArraySegment_1_t12E2F3BC867255EB056CA1F106A8D1E85142A3F1  L_3 = ___array2;
		int32_t L_4 = ___target3;
		IL2CPP_RUNTIME_CLASS_INIT(glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		int32_t L_5;
		L_5 = ((  int32_t (*) (glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 *, int32_t, ArraySegment_1_t12E2F3BC867255EB056CA1F106A8D1E85142A3F1 , int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 *)L_1, (int32_t)L_2, (ArraySegment_1_t12E2F3BC867255EB056CA1F106A8D1E85142A3F1 )L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		V_0 = (int32_t)L_5;
		// var accessorIndex = gltf.accessors.Count;
		glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * L_6 = ___gltf0;
		NullCheck(L_6);
		List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC * L_7 = (List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC *)L_6->get_accessors_3();
		NullCheck((List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC *)L_7);
		int32_t L_8;
		L_8 = List_1_get_Count_mC221419694BDE6FF50C3F68C0964472743ACBA03_inline((List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC *)L_7, /*hidden argument*/List_1_get_Count_mC221419694BDE6FF50C3F68C0964472743ACBA03_RuntimeMethod_var);
		// gltf.accessors.Add(new glTFAccessor
		// {
		//     bufferView = viewIndex,
		//     byteOffset = 0,
		//     componentType = GetComponentType<T>(),
		//     type = GetAccessorType<T>(),
		//     count = array.Count,
		// });
		glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * L_9 = ___gltf0;
		NullCheck(L_9);
		List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC * L_10 = (List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC *)L_9->get_accessors_3();
		glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC * L_11 = (glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC *)il2cpp_codegen_object_new(glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC_il2cpp_TypeInfo_var);
		glTFAccessor__ctor_m0EF366F9EC3426182BDF6055B0B20661744327B8(L_11, /*hidden argument*/NULL);
		glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC * L_12 = (glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC *)L_11;
		int32_t L_13 = V_0;
		NullCheck(L_12);
		L_12->set_bufferView_0(L_13);
		glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC * L_14 = (glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC *)L_12;
		NullCheck(L_14);
		L_14->set_byteOffset_1(0);
		glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC * L_15 = (glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC *)L_14;
		int32_t L_16;
		L_16 = ((  int32_t (*) (const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)(/*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		NullCheck(L_15);
		L_15->set_componentType_3(L_16);
		glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC * L_17 = (glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC *)L_15;
		String_t* L_18;
		L_18 = ((  String_t* (*) (const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)->methodPointer)(/*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3));
		NullCheck(L_17);
		L_17->set_type_2(L_18);
		glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC * L_19 = (glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC *)L_17;
		int32_t L_20;
		L_20 = ArraySegment_1_get_Count_m03AF4BDAA1D5D96096A0072A85C43C2FBC7C03EE_inline((ArraySegment_1_t12E2F3BC867255EB056CA1F106A8D1E85142A3F1 *)(ArraySegment_1_t12E2F3BC867255EB056CA1F106A8D1E85142A3F1 *)(&___array2), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		NullCheck(L_19);
		L_19->set_count_4(L_20);
		NullCheck((List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC *)L_10);
		List_1_Add_m6189B75EF62756B468C403149F4AD081AC68C912((List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC *)L_10, (glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC *)L_19, /*hidden argument*/List_1_Add_m6189B75EF62756B468C403149F4AD081AC68C912_RuntimeMethod_var);
		// return accessorIndex;
		return (int32_t)L_8;
	}
}
// System.Int32 UniGLTF.glTFExtensions::ExtendBufferAndGetAccessorIndex<UnityEngine.Vector3>(UniGLTF.glTF,System.Int32,T[],UniGLTF.glBufferTarget)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t glTFExtensions_ExtendBufferAndGetAccessorIndex_TisVector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_m735212DBAAFCACDBBE2A806856D89E0CEB343125_gshared (glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * ___gltf0, int32_t ___bufferIndex1, Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___array2, int32_t ___target3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return gltf.ExtendBufferAndGetAccessorIndex(bufferIndex, new ArraySegment<T>(array), target);
		glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * L_0 = ___gltf0;
		int32_t L_1 = ___bufferIndex1;
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_2 = ___array2;
		ArraySegment_1_t12E2F3BC867255EB056CA1F106A8D1E85142A3F1  L_3;
		memset((&L_3), 0, sizeof(L_3));
		ArraySegment_1__ctor_mDC6C24720C4540B4A9A00A8F03A5347729CBB745((&L_3), (Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		int32_t L_4 = ___target3;
		IL2CPP_RUNTIME_CLASS_INIT(glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		int32_t L_5;
		L_5 = ((  int32_t (*) (glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 *, int32_t, ArraySegment_1_t12E2F3BC867255EB056CA1F106A8D1E85142A3F1 , int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)((glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 *)L_0, (int32_t)L_1, (ArraySegment_1_t12E2F3BC867255EB056CA1F106A8D1E85142A3F1 )L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		return (int32_t)L_5;
	}
}
// System.Int32 UniGLTF.glTFExtensions::ExtendBufferAndGetAccessorIndex<UnityEngine.Vector4>(UniGLTF.glTF,System.Int32,System.ArraySegment`1<T>,UniGLTF.glBufferTarget)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t glTFExtensions_ExtendBufferAndGetAccessorIndex_TisVector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_m15ADCDD51B2C9C32EA610E898A08B0B1FFD12B5D_gshared (glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * ___gltf0, int32_t ___bufferIndex1, ArraySegment_1_t3BCE22176B44ACEB25236AD7501F226112F6E07A  ___array2, int32_t ___target3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m6189B75EF62756B468C403149F4AD081AC68C912_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mC221419694BDE6FF50C3F68C0964472743ACBA03_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// if (array.Count == 0)
		int32_t L_0;
		L_0 = ArraySegment_1_get_Count_m86D4F7A8C6E66A89B03DB83E4F8F82019625E483_inline((ArraySegment_1_t3BCE22176B44ACEB25236AD7501F226112F6E07A *)(ArraySegment_1_t3BCE22176B44ACEB25236AD7501F226112F6E07A *)(&___array2), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		// return -1;
		return (int32_t)(-1);
	}

IL_000b:
	{
		// var viewIndex = ExtendBufferAndGetViewIndex(gltf, bufferIndex, array, target);
		glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * L_1 = ___gltf0;
		int32_t L_2 = ___bufferIndex1;
		ArraySegment_1_t3BCE22176B44ACEB25236AD7501F226112F6E07A  L_3 = ___array2;
		int32_t L_4 = ___target3;
		IL2CPP_RUNTIME_CLASS_INIT(glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		int32_t L_5;
		L_5 = ((  int32_t (*) (glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 *, int32_t, ArraySegment_1_t3BCE22176B44ACEB25236AD7501F226112F6E07A , int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 *)L_1, (int32_t)L_2, (ArraySegment_1_t3BCE22176B44ACEB25236AD7501F226112F6E07A )L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		V_0 = (int32_t)L_5;
		// var accessorIndex = gltf.accessors.Count;
		glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * L_6 = ___gltf0;
		NullCheck(L_6);
		List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC * L_7 = (List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC *)L_6->get_accessors_3();
		NullCheck((List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC *)L_7);
		int32_t L_8;
		L_8 = List_1_get_Count_mC221419694BDE6FF50C3F68C0964472743ACBA03_inline((List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC *)L_7, /*hidden argument*/List_1_get_Count_mC221419694BDE6FF50C3F68C0964472743ACBA03_RuntimeMethod_var);
		// gltf.accessors.Add(new glTFAccessor
		// {
		//     bufferView = viewIndex,
		//     byteOffset = 0,
		//     componentType = GetComponentType<T>(),
		//     type = GetAccessorType<T>(),
		//     count = array.Count,
		// });
		glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * L_9 = ___gltf0;
		NullCheck(L_9);
		List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC * L_10 = (List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC *)L_9->get_accessors_3();
		glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC * L_11 = (glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC *)il2cpp_codegen_object_new(glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC_il2cpp_TypeInfo_var);
		glTFAccessor__ctor_m0EF366F9EC3426182BDF6055B0B20661744327B8(L_11, /*hidden argument*/NULL);
		glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC * L_12 = (glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC *)L_11;
		int32_t L_13 = V_0;
		NullCheck(L_12);
		L_12->set_bufferView_0(L_13);
		glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC * L_14 = (glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC *)L_12;
		NullCheck(L_14);
		L_14->set_byteOffset_1(0);
		glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC * L_15 = (glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC *)L_14;
		int32_t L_16;
		L_16 = ((  int32_t (*) (const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)(/*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		NullCheck(L_15);
		L_15->set_componentType_3(L_16);
		glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC * L_17 = (glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC *)L_15;
		String_t* L_18;
		L_18 = ((  String_t* (*) (const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)->methodPointer)(/*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3));
		NullCheck(L_17);
		L_17->set_type_2(L_18);
		glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC * L_19 = (glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC *)L_17;
		int32_t L_20;
		L_20 = ArraySegment_1_get_Count_m86D4F7A8C6E66A89B03DB83E4F8F82019625E483_inline((ArraySegment_1_t3BCE22176B44ACEB25236AD7501F226112F6E07A *)(ArraySegment_1_t3BCE22176B44ACEB25236AD7501F226112F6E07A *)(&___array2), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		NullCheck(L_19);
		L_19->set_count_4(L_20);
		NullCheck((List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC *)L_10);
		List_1_Add_m6189B75EF62756B468C403149F4AD081AC68C912((List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC *)L_10, (glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC *)L_19, /*hidden argument*/List_1_Add_m6189B75EF62756B468C403149F4AD081AC68C912_RuntimeMethod_var);
		// return accessorIndex;
		return (int32_t)L_8;
	}
}
// System.Int32 UniGLTF.glTFExtensions::ExtendBufferAndGetAccessorIndex<UnityEngine.Vector4>(UniGLTF.glTF,System.Int32,T[],UniGLTF.glBufferTarget)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t glTFExtensions_ExtendBufferAndGetAccessorIndex_TisVector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_mAEB456FB731C05381DCF6A68754411A8C24B54CD_gshared (glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * ___gltf0, int32_t ___bufferIndex1, Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871* ___array2, int32_t ___target3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return gltf.ExtendBufferAndGetAccessorIndex(bufferIndex, new ArraySegment<T>(array), target);
		glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * L_0 = ___gltf0;
		int32_t L_1 = ___bufferIndex1;
		Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871* L_2 = ___array2;
		ArraySegment_1_t3BCE22176B44ACEB25236AD7501F226112F6E07A  L_3;
		memset((&L_3), 0, sizeof(L_3));
		ArraySegment_1__ctor_m1186B90A376A0AE42259FC310386A6BE3AFD6D40((&L_3), (Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		int32_t L_4 = ___target3;
		IL2CPP_RUNTIME_CLASS_INIT(glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		int32_t L_5;
		L_5 = ((  int32_t (*) (glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 *, int32_t, ArraySegment_1_t3BCE22176B44ACEB25236AD7501F226112F6E07A , int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)((glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 *)L_0, (int32_t)L_1, (ArraySegment_1_t3BCE22176B44ACEB25236AD7501F226112F6E07A )L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		return (int32_t)L_5;
	}
}
// System.Int32 UniGLTF.glTFExtensions::ExtendBufferAndGetViewIndex<UnityEngine.Color>(UniGLTF.glTF,System.Int32,System.ArraySegment`1<T>,UniGLTF.glBufferTarget)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t glTFExtensions_ExtendBufferAndGetViewIndex_TisColor_tF40DAF76C04FFECF3FE6024F85A294741C9CC659_m002064E441E472CA475BB4AC0E9A112529DA0445_gshared (glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * ___gltf0, int32_t ___bufferIndex1, ArraySegment_1_t8A45E4F401BB0C4F9F8643CCA8706F98DC8057F6  ___array2, int32_t ___target3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mDDEE5E9B13D0FE35D394C910EB8ADDF90244F666_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m3528BC05F270F79CD2DFF46FFDD7722124617F04_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_mC4617D8E0A2C06E1168AC910BDB298A17C8F53FC_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4 * V_0 = NULL;
	{
		// if (array.Count == 0)
		int32_t L_0;
		L_0 = ArraySegment_1_get_Count_m313EE200EA1CB465EF2B1F8CFFA7E63FA3F408EF_inline((ArraySegment_1_t8A45E4F401BB0C4F9F8643CCA8706F98DC8057F6 *)(ArraySegment_1_t8A45E4F401BB0C4F9F8643CCA8706F98DC8057F6 *)(&___array2), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		// return -1;
		return (int32_t)(-1);
	}

IL_000b:
	{
		// var view = gltf.buffers[bufferIndex].Append(array, target);
		glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * L_1 = ___gltf0;
		NullCheck(L_1);
		List_1_t4BFB26128F5A4C3E19B9E2DEAB3A142C3EFF8F38 * L_2 = (List_1_t4BFB26128F5A4C3E19B9E2DEAB3A142C3EFF8F38 *)L_1->get_buffers_1();
		int32_t L_3 = ___bufferIndex1;
		NullCheck((List_1_t4BFB26128F5A4C3E19B9E2DEAB3A142C3EFF8F38 *)L_2);
		glTFBuffer_t840FC6FB6032C82B50388CE51BB1F054475E4BD6 * L_4;
		L_4 = List_1_get_Item_mC4617D8E0A2C06E1168AC910BDB298A17C8F53FC_inline((List_1_t4BFB26128F5A4C3E19B9E2DEAB3A142C3EFF8F38 *)L_2, (int32_t)L_3, /*hidden argument*/List_1_get_Item_mC4617D8E0A2C06E1168AC910BDB298A17C8F53FC_RuntimeMethod_var);
		ArraySegment_1_t8A45E4F401BB0C4F9F8643CCA8706F98DC8057F6  L_5 = ___array2;
		int32_t L_6 = ___target3;
		NullCheck((glTFBuffer_t840FC6FB6032C82B50388CE51BB1F054475E4BD6 *)L_4);
		glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4 * L_7;
		L_7 = ((  glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4 * (*) (glTFBuffer_t840FC6FB6032C82B50388CE51BB1F054475E4BD6 *, ArraySegment_1_t8A45E4F401BB0C4F9F8643CCA8706F98DC8057F6 , int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((glTFBuffer_t840FC6FB6032C82B50388CE51BB1F054475E4BD6 *)L_4, (ArraySegment_1_t8A45E4F401BB0C4F9F8643CCA8706F98DC8057F6 )L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		V_0 = (glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4 *)L_7;
		// var viewIndex = gltf.bufferViews.Count;
		glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * L_8 = ___gltf0;
		NullCheck(L_8);
		List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 * L_9 = (List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 *)L_8->get_bufferViews_2();
		NullCheck((List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 *)L_9);
		int32_t L_10;
		L_10 = List_1_get_Count_m3528BC05F270F79CD2DFF46FFDD7722124617F04_inline((List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 *)L_9, /*hidden argument*/List_1_get_Count_m3528BC05F270F79CD2DFF46FFDD7722124617F04_RuntimeMethod_var);
		// gltf.bufferViews.Add(view);
		glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * L_11 = ___gltf0;
		NullCheck(L_11);
		List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 * L_12 = (List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 *)L_11->get_bufferViews_2();
		glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4 * L_13 = V_0;
		NullCheck((List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 *)L_12);
		List_1_Add_mDDEE5E9B13D0FE35D394C910EB8ADDF90244F666((List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 *)L_12, (glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4 *)L_13, /*hidden argument*/List_1_Add_mDDEE5E9B13D0FE35D394C910EB8ADDF90244F666_RuntimeMethod_var);
		// return viewIndex;
		return (int32_t)L_10;
	}
}
// System.Int32 UniGLTF.glTFExtensions::ExtendBufferAndGetViewIndex<System.Int32>(UniGLTF.glTF,System.Int32,System.ArraySegment`1<T>,UniGLTF.glBufferTarget)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t glTFExtensions_ExtendBufferAndGetViewIndex_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_m2ED87AEAB259DDCE4C394B242AE8E566D633A5D3_gshared (glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * ___gltf0, int32_t ___bufferIndex1, ArraySegment_1_t9FDA96F04294A918E0DBFCE7CF750DE23A898976  ___array2, int32_t ___target3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mDDEE5E9B13D0FE35D394C910EB8ADDF90244F666_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m3528BC05F270F79CD2DFF46FFDD7722124617F04_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_mC4617D8E0A2C06E1168AC910BDB298A17C8F53FC_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4 * V_0 = NULL;
	{
		// if (array.Count == 0)
		int32_t L_0;
		L_0 = ArraySegment_1_get_Count_mFB0298EF19B049FBF2C0C3D7FCA5E16DB8F80D56_inline((ArraySegment_1_t9FDA96F04294A918E0DBFCE7CF750DE23A898976 *)(ArraySegment_1_t9FDA96F04294A918E0DBFCE7CF750DE23A898976 *)(&___array2), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		// return -1;
		return (int32_t)(-1);
	}

IL_000b:
	{
		// var view = gltf.buffers[bufferIndex].Append(array, target);
		glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * L_1 = ___gltf0;
		NullCheck(L_1);
		List_1_t4BFB26128F5A4C3E19B9E2DEAB3A142C3EFF8F38 * L_2 = (List_1_t4BFB26128F5A4C3E19B9E2DEAB3A142C3EFF8F38 *)L_1->get_buffers_1();
		int32_t L_3 = ___bufferIndex1;
		NullCheck((List_1_t4BFB26128F5A4C3E19B9E2DEAB3A142C3EFF8F38 *)L_2);
		glTFBuffer_t840FC6FB6032C82B50388CE51BB1F054475E4BD6 * L_4;
		L_4 = List_1_get_Item_mC4617D8E0A2C06E1168AC910BDB298A17C8F53FC_inline((List_1_t4BFB26128F5A4C3E19B9E2DEAB3A142C3EFF8F38 *)L_2, (int32_t)L_3, /*hidden argument*/List_1_get_Item_mC4617D8E0A2C06E1168AC910BDB298A17C8F53FC_RuntimeMethod_var);
		ArraySegment_1_t9FDA96F04294A918E0DBFCE7CF750DE23A898976  L_5 = ___array2;
		int32_t L_6 = ___target3;
		NullCheck((glTFBuffer_t840FC6FB6032C82B50388CE51BB1F054475E4BD6 *)L_4);
		glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4 * L_7;
		L_7 = ((  glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4 * (*) (glTFBuffer_t840FC6FB6032C82B50388CE51BB1F054475E4BD6 *, ArraySegment_1_t9FDA96F04294A918E0DBFCE7CF750DE23A898976 , int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((glTFBuffer_t840FC6FB6032C82B50388CE51BB1F054475E4BD6 *)L_4, (ArraySegment_1_t9FDA96F04294A918E0DBFCE7CF750DE23A898976 )L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		V_0 = (glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4 *)L_7;
		// var viewIndex = gltf.bufferViews.Count;
		glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * L_8 = ___gltf0;
		NullCheck(L_8);
		List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 * L_9 = (List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 *)L_8->get_bufferViews_2();
		NullCheck((List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 *)L_9);
		int32_t L_10;
		L_10 = List_1_get_Count_m3528BC05F270F79CD2DFF46FFDD7722124617F04_inline((List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 *)L_9, /*hidden argument*/List_1_get_Count_m3528BC05F270F79CD2DFF46FFDD7722124617F04_RuntimeMethod_var);
		// gltf.bufferViews.Add(view);
		glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * L_11 = ___gltf0;
		NullCheck(L_11);
		List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 * L_12 = (List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 *)L_11->get_bufferViews_2();
		glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4 * L_13 = V_0;
		NullCheck((List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 *)L_12);
		List_1_Add_mDDEE5E9B13D0FE35D394C910EB8ADDF90244F666((List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 *)L_12, (glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4 *)L_13, /*hidden argument*/List_1_Add_mDDEE5E9B13D0FE35D394C910EB8ADDF90244F666_RuntimeMethod_var);
		// return viewIndex;
		return (int32_t)L_10;
	}
}
// System.Int32 UniGLTF.glTFExtensions::ExtendBufferAndGetViewIndex<System.Int32>(UniGLTF.glTF,System.Int32,T[],UniGLTF.glBufferTarget)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t glTFExtensions_ExtendBufferAndGetViewIndex_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_m8D75D2D812DD1E3A822E7817F1CF891A351DE98A_gshared (glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * ___gltf0, int32_t ___bufferIndex1, Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___array2, int32_t ___target3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return ExtendBufferAndGetViewIndex(gltf, bufferIndex, new ArraySegment<T>(array), target);
		glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * L_0 = ___gltf0;
		int32_t L_1 = ___bufferIndex1;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_2 = ___array2;
		ArraySegment_1_t9FDA96F04294A918E0DBFCE7CF750DE23A898976  L_3;
		memset((&L_3), 0, sizeof(L_3));
		ArraySegment_1__ctor_m643D396633372662D1B12F94E9503026534D7F41((&L_3), (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		int32_t L_4 = ___target3;
		IL2CPP_RUNTIME_CLASS_INIT(glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		int32_t L_5;
		L_5 = ((  int32_t (*) (glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 *, int32_t, ArraySegment_1_t9FDA96F04294A918E0DBFCE7CF750DE23A898976 , int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)((glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 *)L_0, (int32_t)L_1, (ArraySegment_1_t9FDA96F04294A918E0DBFCE7CF750DE23A898976 )L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		return (int32_t)L_5;
	}
}
// System.Int32 UniGLTF.glTFExtensions::ExtendBufferAndGetViewIndex<UnityEngine.Matrix4x4>(UniGLTF.glTF,System.Int32,System.ArraySegment`1<T>,UniGLTF.glBufferTarget)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t glTFExtensions_ExtendBufferAndGetViewIndex_TisMatrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461_m53548EF533AD92F8219A5574879122950A313EF1_gshared (glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * ___gltf0, int32_t ___bufferIndex1, ArraySegment_1_t7FCE6A956BF91EE65591716518285D293D3C5B6B  ___array2, int32_t ___target3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mDDEE5E9B13D0FE35D394C910EB8ADDF90244F666_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m3528BC05F270F79CD2DFF46FFDD7722124617F04_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_mC4617D8E0A2C06E1168AC910BDB298A17C8F53FC_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4 * V_0 = NULL;
	{
		// if (array.Count == 0)
		int32_t L_0;
		L_0 = ArraySegment_1_get_Count_m779174D975AA29188F9DFFC751199785A7F0ADBD_inline((ArraySegment_1_t7FCE6A956BF91EE65591716518285D293D3C5B6B *)(ArraySegment_1_t7FCE6A956BF91EE65591716518285D293D3C5B6B *)(&___array2), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		// return -1;
		return (int32_t)(-1);
	}

IL_000b:
	{
		// var view = gltf.buffers[bufferIndex].Append(array, target);
		glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * L_1 = ___gltf0;
		NullCheck(L_1);
		List_1_t4BFB26128F5A4C3E19B9E2DEAB3A142C3EFF8F38 * L_2 = (List_1_t4BFB26128F5A4C3E19B9E2DEAB3A142C3EFF8F38 *)L_1->get_buffers_1();
		int32_t L_3 = ___bufferIndex1;
		NullCheck((List_1_t4BFB26128F5A4C3E19B9E2DEAB3A142C3EFF8F38 *)L_2);
		glTFBuffer_t840FC6FB6032C82B50388CE51BB1F054475E4BD6 * L_4;
		L_4 = List_1_get_Item_mC4617D8E0A2C06E1168AC910BDB298A17C8F53FC_inline((List_1_t4BFB26128F5A4C3E19B9E2DEAB3A142C3EFF8F38 *)L_2, (int32_t)L_3, /*hidden argument*/List_1_get_Item_mC4617D8E0A2C06E1168AC910BDB298A17C8F53FC_RuntimeMethod_var);
		ArraySegment_1_t7FCE6A956BF91EE65591716518285D293D3C5B6B  L_5 = ___array2;
		int32_t L_6 = ___target3;
		NullCheck((glTFBuffer_t840FC6FB6032C82B50388CE51BB1F054475E4BD6 *)L_4);
		glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4 * L_7;
		L_7 = ((  glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4 * (*) (glTFBuffer_t840FC6FB6032C82B50388CE51BB1F054475E4BD6 *, ArraySegment_1_t7FCE6A956BF91EE65591716518285D293D3C5B6B , int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((glTFBuffer_t840FC6FB6032C82B50388CE51BB1F054475E4BD6 *)L_4, (ArraySegment_1_t7FCE6A956BF91EE65591716518285D293D3C5B6B )L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		V_0 = (glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4 *)L_7;
		// var viewIndex = gltf.bufferViews.Count;
		glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * L_8 = ___gltf0;
		NullCheck(L_8);
		List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 * L_9 = (List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 *)L_8->get_bufferViews_2();
		NullCheck((List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 *)L_9);
		int32_t L_10;
		L_10 = List_1_get_Count_m3528BC05F270F79CD2DFF46FFDD7722124617F04_inline((List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 *)L_9, /*hidden argument*/List_1_get_Count_m3528BC05F270F79CD2DFF46FFDD7722124617F04_RuntimeMethod_var);
		// gltf.bufferViews.Add(view);
		glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * L_11 = ___gltf0;
		NullCheck(L_11);
		List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 * L_12 = (List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 *)L_11->get_bufferViews_2();
		glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4 * L_13 = V_0;
		NullCheck((List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 *)L_12);
		List_1_Add_mDDEE5E9B13D0FE35D394C910EB8ADDF90244F666((List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 *)L_12, (glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4 *)L_13, /*hidden argument*/List_1_Add_mDDEE5E9B13D0FE35D394C910EB8ADDF90244F666_RuntimeMethod_var);
		// return viewIndex;
		return (int32_t)L_10;
	}
}
// System.Int32 UniGLTF.glTFExtensions::ExtendBufferAndGetViewIndex<System.UInt32>(UniGLTF.glTF,System.Int32,System.ArraySegment`1<T>,UniGLTF.glBufferTarget)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t glTFExtensions_ExtendBufferAndGetViewIndex_TisUInt32_tE60352A06233E4E69DD198BCC67142159F686B15_mA31EF604FC9D4BAF0FA7BE86929B2DAC42984717_gshared (glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * ___gltf0, int32_t ___bufferIndex1, ArraySegment_1_tE4B89ED4839E563DF2A25436675AC5C5E87B62B9  ___array2, int32_t ___target3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mDDEE5E9B13D0FE35D394C910EB8ADDF90244F666_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m3528BC05F270F79CD2DFF46FFDD7722124617F04_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_mC4617D8E0A2C06E1168AC910BDB298A17C8F53FC_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4 * V_0 = NULL;
	{
		// if (array.Count == 0)
		int32_t L_0;
		L_0 = ArraySegment_1_get_Count_mF79DF195C03504D335115BA66BC48FB0BB4D395C_inline((ArraySegment_1_tE4B89ED4839E563DF2A25436675AC5C5E87B62B9 *)(ArraySegment_1_tE4B89ED4839E563DF2A25436675AC5C5E87B62B9 *)(&___array2), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		// return -1;
		return (int32_t)(-1);
	}

IL_000b:
	{
		// var view = gltf.buffers[bufferIndex].Append(array, target);
		glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * L_1 = ___gltf0;
		NullCheck(L_1);
		List_1_t4BFB26128F5A4C3E19B9E2DEAB3A142C3EFF8F38 * L_2 = (List_1_t4BFB26128F5A4C3E19B9E2DEAB3A142C3EFF8F38 *)L_1->get_buffers_1();
		int32_t L_3 = ___bufferIndex1;
		NullCheck((List_1_t4BFB26128F5A4C3E19B9E2DEAB3A142C3EFF8F38 *)L_2);
		glTFBuffer_t840FC6FB6032C82B50388CE51BB1F054475E4BD6 * L_4;
		L_4 = List_1_get_Item_mC4617D8E0A2C06E1168AC910BDB298A17C8F53FC_inline((List_1_t4BFB26128F5A4C3E19B9E2DEAB3A142C3EFF8F38 *)L_2, (int32_t)L_3, /*hidden argument*/List_1_get_Item_mC4617D8E0A2C06E1168AC910BDB298A17C8F53FC_RuntimeMethod_var);
		ArraySegment_1_tE4B89ED4839E563DF2A25436675AC5C5E87B62B9  L_5 = ___array2;
		int32_t L_6 = ___target3;
		NullCheck((glTFBuffer_t840FC6FB6032C82B50388CE51BB1F054475E4BD6 *)L_4);
		glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4 * L_7;
		L_7 = ((  glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4 * (*) (glTFBuffer_t840FC6FB6032C82B50388CE51BB1F054475E4BD6 *, ArraySegment_1_tE4B89ED4839E563DF2A25436675AC5C5E87B62B9 , int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((glTFBuffer_t840FC6FB6032C82B50388CE51BB1F054475E4BD6 *)L_4, (ArraySegment_1_tE4B89ED4839E563DF2A25436675AC5C5E87B62B9 )L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		V_0 = (glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4 *)L_7;
		// var viewIndex = gltf.bufferViews.Count;
		glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * L_8 = ___gltf0;
		NullCheck(L_8);
		List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 * L_9 = (List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 *)L_8->get_bufferViews_2();
		NullCheck((List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 *)L_9);
		int32_t L_10;
		L_10 = List_1_get_Count_m3528BC05F270F79CD2DFF46FFDD7722124617F04_inline((List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 *)L_9, /*hidden argument*/List_1_get_Count_m3528BC05F270F79CD2DFF46FFDD7722124617F04_RuntimeMethod_var);
		// gltf.bufferViews.Add(view);
		glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * L_11 = ___gltf0;
		NullCheck(L_11);
		List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 * L_12 = (List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 *)L_11->get_bufferViews_2();
		glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4 * L_13 = V_0;
		NullCheck((List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 *)L_12);
		List_1_Add_mDDEE5E9B13D0FE35D394C910EB8ADDF90244F666((List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 *)L_12, (glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4 *)L_13, /*hidden argument*/List_1_Add_mDDEE5E9B13D0FE35D394C910EB8ADDF90244F666_RuntimeMethod_var);
		// return viewIndex;
		return (int32_t)L_10;
	}
}
// System.Int32 UniGLTF.glTFExtensions::ExtendBufferAndGetViewIndex<UniGLTF.UShort4>(UniGLTF.glTF,System.Int32,System.ArraySegment`1<T>,UniGLTF.glBufferTarget)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t glTFExtensions_ExtendBufferAndGetViewIndex_TisUShort4_tB19A4ECD972B8600C9515187BC86B572B83920DB_m806F557C609EE1686F1318E9A29E8D342AE7E8A4_gshared (glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * ___gltf0, int32_t ___bufferIndex1, ArraySegment_1_t9487D5EA4323A4B2C1F7F94C91363EBEE33F3462  ___array2, int32_t ___target3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mDDEE5E9B13D0FE35D394C910EB8ADDF90244F666_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m3528BC05F270F79CD2DFF46FFDD7722124617F04_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_mC4617D8E0A2C06E1168AC910BDB298A17C8F53FC_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4 * V_0 = NULL;
	{
		// if (array.Count == 0)
		int32_t L_0;
		L_0 = ArraySegment_1_get_Count_mC78F0263F5BAC4CA823FA314F69DD18E7E4307D2_inline((ArraySegment_1_t9487D5EA4323A4B2C1F7F94C91363EBEE33F3462 *)(ArraySegment_1_t9487D5EA4323A4B2C1F7F94C91363EBEE33F3462 *)(&___array2), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		// return -1;
		return (int32_t)(-1);
	}

IL_000b:
	{
		// var view = gltf.buffers[bufferIndex].Append(array, target);
		glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * L_1 = ___gltf0;
		NullCheck(L_1);
		List_1_t4BFB26128F5A4C3E19B9E2DEAB3A142C3EFF8F38 * L_2 = (List_1_t4BFB26128F5A4C3E19B9E2DEAB3A142C3EFF8F38 *)L_1->get_buffers_1();
		int32_t L_3 = ___bufferIndex1;
		NullCheck((List_1_t4BFB26128F5A4C3E19B9E2DEAB3A142C3EFF8F38 *)L_2);
		glTFBuffer_t840FC6FB6032C82B50388CE51BB1F054475E4BD6 * L_4;
		L_4 = List_1_get_Item_mC4617D8E0A2C06E1168AC910BDB298A17C8F53FC_inline((List_1_t4BFB26128F5A4C3E19B9E2DEAB3A142C3EFF8F38 *)L_2, (int32_t)L_3, /*hidden argument*/List_1_get_Item_mC4617D8E0A2C06E1168AC910BDB298A17C8F53FC_RuntimeMethod_var);
		ArraySegment_1_t9487D5EA4323A4B2C1F7F94C91363EBEE33F3462  L_5 = ___array2;
		int32_t L_6 = ___target3;
		NullCheck((glTFBuffer_t840FC6FB6032C82B50388CE51BB1F054475E4BD6 *)L_4);
		glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4 * L_7;
		L_7 = ((  glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4 * (*) (glTFBuffer_t840FC6FB6032C82B50388CE51BB1F054475E4BD6 *, ArraySegment_1_t9487D5EA4323A4B2C1F7F94C91363EBEE33F3462 , int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((glTFBuffer_t840FC6FB6032C82B50388CE51BB1F054475E4BD6 *)L_4, (ArraySegment_1_t9487D5EA4323A4B2C1F7F94C91363EBEE33F3462 )L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		V_0 = (glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4 *)L_7;
		// var viewIndex = gltf.bufferViews.Count;
		glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * L_8 = ___gltf0;
		NullCheck(L_8);
		List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 * L_9 = (List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 *)L_8->get_bufferViews_2();
		NullCheck((List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 *)L_9);
		int32_t L_10;
		L_10 = List_1_get_Count_m3528BC05F270F79CD2DFF46FFDD7722124617F04_inline((List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 *)L_9, /*hidden argument*/List_1_get_Count_m3528BC05F270F79CD2DFF46FFDD7722124617F04_RuntimeMethod_var);
		// gltf.bufferViews.Add(view);
		glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * L_11 = ___gltf0;
		NullCheck(L_11);
		List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 * L_12 = (List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 *)L_11->get_bufferViews_2();
		glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4 * L_13 = V_0;
		NullCheck((List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 *)L_12);
		List_1_Add_mDDEE5E9B13D0FE35D394C910EB8ADDF90244F666((List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 *)L_12, (glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4 *)L_13, /*hidden argument*/List_1_Add_mDDEE5E9B13D0FE35D394C910EB8ADDF90244F666_RuntimeMethod_var);
		// return viewIndex;
		return (int32_t)L_10;
	}
}
// System.Int32 UniGLTF.glTFExtensions::ExtendBufferAndGetViewIndex<UnityEngine.Vector2>(UniGLTF.glTF,System.Int32,System.ArraySegment`1<T>,UniGLTF.glBufferTarget)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t glTFExtensions_ExtendBufferAndGetViewIndex_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_m0D9B3292E59A6F764796FE5964BABB91D0EE5609_gshared (glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * ___gltf0, int32_t ___bufferIndex1, ArraySegment_1_t73AEFBC798515A51F93CF5816E332A0654AE698D  ___array2, int32_t ___target3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mDDEE5E9B13D0FE35D394C910EB8ADDF90244F666_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m3528BC05F270F79CD2DFF46FFDD7722124617F04_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_mC4617D8E0A2C06E1168AC910BDB298A17C8F53FC_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4 * V_0 = NULL;
	{
		// if (array.Count == 0)
		int32_t L_0;
		L_0 = ArraySegment_1_get_Count_m038FD84B16BD0AC31B33B3B9D9237708DFC1AA32_inline((ArraySegment_1_t73AEFBC798515A51F93CF5816E332A0654AE698D *)(ArraySegment_1_t73AEFBC798515A51F93CF5816E332A0654AE698D *)(&___array2), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		// return -1;
		return (int32_t)(-1);
	}

IL_000b:
	{
		// var view = gltf.buffers[bufferIndex].Append(array, target);
		glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * L_1 = ___gltf0;
		NullCheck(L_1);
		List_1_t4BFB26128F5A4C3E19B9E2DEAB3A142C3EFF8F38 * L_2 = (List_1_t4BFB26128F5A4C3E19B9E2DEAB3A142C3EFF8F38 *)L_1->get_buffers_1();
		int32_t L_3 = ___bufferIndex1;
		NullCheck((List_1_t4BFB26128F5A4C3E19B9E2DEAB3A142C3EFF8F38 *)L_2);
		glTFBuffer_t840FC6FB6032C82B50388CE51BB1F054475E4BD6 * L_4;
		L_4 = List_1_get_Item_mC4617D8E0A2C06E1168AC910BDB298A17C8F53FC_inline((List_1_t4BFB26128F5A4C3E19B9E2DEAB3A142C3EFF8F38 *)L_2, (int32_t)L_3, /*hidden argument*/List_1_get_Item_mC4617D8E0A2C06E1168AC910BDB298A17C8F53FC_RuntimeMethod_var);
		ArraySegment_1_t73AEFBC798515A51F93CF5816E332A0654AE698D  L_5 = ___array2;
		int32_t L_6 = ___target3;
		NullCheck((glTFBuffer_t840FC6FB6032C82B50388CE51BB1F054475E4BD6 *)L_4);
		glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4 * L_7;
		L_7 = ((  glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4 * (*) (glTFBuffer_t840FC6FB6032C82B50388CE51BB1F054475E4BD6 *, ArraySegment_1_t73AEFBC798515A51F93CF5816E332A0654AE698D , int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((glTFBuffer_t840FC6FB6032C82B50388CE51BB1F054475E4BD6 *)L_4, (ArraySegment_1_t73AEFBC798515A51F93CF5816E332A0654AE698D )L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		V_0 = (glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4 *)L_7;
		// var viewIndex = gltf.bufferViews.Count;
		glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * L_8 = ___gltf0;
		NullCheck(L_8);
		List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 * L_9 = (List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 *)L_8->get_bufferViews_2();
		NullCheck((List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 *)L_9);
		int32_t L_10;
		L_10 = List_1_get_Count_m3528BC05F270F79CD2DFF46FFDD7722124617F04_inline((List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 *)L_9, /*hidden argument*/List_1_get_Count_m3528BC05F270F79CD2DFF46FFDD7722124617F04_RuntimeMethod_var);
		// gltf.bufferViews.Add(view);
		glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * L_11 = ___gltf0;
		NullCheck(L_11);
		List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 * L_12 = (List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 *)L_11->get_bufferViews_2();
		glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4 * L_13 = V_0;
		NullCheck((List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 *)L_12);
		List_1_Add_mDDEE5E9B13D0FE35D394C910EB8ADDF90244F666((List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 *)L_12, (glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4 *)L_13, /*hidden argument*/List_1_Add_mDDEE5E9B13D0FE35D394C910EB8ADDF90244F666_RuntimeMethod_var);
		// return viewIndex;
		return (int32_t)L_10;
	}
}
// System.Int32 UniGLTF.glTFExtensions::ExtendBufferAndGetViewIndex<UnityEngine.Vector3>(UniGLTF.glTF,System.Int32,System.ArraySegment`1<T>,UniGLTF.glBufferTarget)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t glTFExtensions_ExtendBufferAndGetViewIndex_TisVector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_mF7C12D5AC54EFACE1BE6361D440835AA4966E3E6_gshared (glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * ___gltf0, int32_t ___bufferIndex1, ArraySegment_1_t12E2F3BC867255EB056CA1F106A8D1E85142A3F1  ___array2, int32_t ___target3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mDDEE5E9B13D0FE35D394C910EB8ADDF90244F666_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m3528BC05F270F79CD2DFF46FFDD7722124617F04_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_mC4617D8E0A2C06E1168AC910BDB298A17C8F53FC_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4 * V_0 = NULL;
	{
		// if (array.Count == 0)
		int32_t L_0;
		L_0 = ArraySegment_1_get_Count_m03AF4BDAA1D5D96096A0072A85C43C2FBC7C03EE_inline((ArraySegment_1_t12E2F3BC867255EB056CA1F106A8D1E85142A3F1 *)(ArraySegment_1_t12E2F3BC867255EB056CA1F106A8D1E85142A3F1 *)(&___array2), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		// return -1;
		return (int32_t)(-1);
	}

IL_000b:
	{
		// var view = gltf.buffers[bufferIndex].Append(array, target);
		glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * L_1 = ___gltf0;
		NullCheck(L_1);
		List_1_t4BFB26128F5A4C3E19B9E2DEAB3A142C3EFF8F38 * L_2 = (List_1_t4BFB26128F5A4C3E19B9E2DEAB3A142C3EFF8F38 *)L_1->get_buffers_1();
		int32_t L_3 = ___bufferIndex1;
		NullCheck((List_1_t4BFB26128F5A4C3E19B9E2DEAB3A142C3EFF8F38 *)L_2);
		glTFBuffer_t840FC6FB6032C82B50388CE51BB1F054475E4BD6 * L_4;
		L_4 = List_1_get_Item_mC4617D8E0A2C06E1168AC910BDB298A17C8F53FC_inline((List_1_t4BFB26128F5A4C3E19B9E2DEAB3A142C3EFF8F38 *)L_2, (int32_t)L_3, /*hidden argument*/List_1_get_Item_mC4617D8E0A2C06E1168AC910BDB298A17C8F53FC_RuntimeMethod_var);
		ArraySegment_1_t12E2F3BC867255EB056CA1F106A8D1E85142A3F1  L_5 = ___array2;
		int32_t L_6 = ___target3;
		NullCheck((glTFBuffer_t840FC6FB6032C82B50388CE51BB1F054475E4BD6 *)L_4);
		glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4 * L_7;
		L_7 = ((  glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4 * (*) (glTFBuffer_t840FC6FB6032C82B50388CE51BB1F054475E4BD6 *, ArraySegment_1_t12E2F3BC867255EB056CA1F106A8D1E85142A3F1 , int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((glTFBuffer_t840FC6FB6032C82B50388CE51BB1F054475E4BD6 *)L_4, (ArraySegment_1_t12E2F3BC867255EB056CA1F106A8D1E85142A3F1 )L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		V_0 = (glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4 *)L_7;
		// var viewIndex = gltf.bufferViews.Count;
		glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * L_8 = ___gltf0;
		NullCheck(L_8);
		List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 * L_9 = (List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 *)L_8->get_bufferViews_2();
		NullCheck((List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 *)L_9);
		int32_t L_10;
		L_10 = List_1_get_Count_m3528BC05F270F79CD2DFF46FFDD7722124617F04_inline((List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 *)L_9, /*hidden argument*/List_1_get_Count_m3528BC05F270F79CD2DFF46FFDD7722124617F04_RuntimeMethod_var);
		// gltf.bufferViews.Add(view);
		glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * L_11 = ___gltf0;
		NullCheck(L_11);
		List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 * L_12 = (List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 *)L_11->get_bufferViews_2();
		glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4 * L_13 = V_0;
		NullCheck((List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 *)L_12);
		List_1_Add_mDDEE5E9B13D0FE35D394C910EB8ADDF90244F666((List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 *)L_12, (glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4 *)L_13, /*hidden argument*/List_1_Add_mDDEE5E9B13D0FE35D394C910EB8ADDF90244F666_RuntimeMethod_var);
		// return viewIndex;
		return (int32_t)L_10;
	}
}
// System.Int32 UniGLTF.glTFExtensions::ExtendBufferAndGetViewIndex<UnityEngine.Vector4>(UniGLTF.glTF,System.Int32,System.ArraySegment`1<T>,UniGLTF.glBufferTarget)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t glTFExtensions_ExtendBufferAndGetViewIndex_TisVector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_m149C68027591F6BFE99E953834D2A2012DFED035_gshared (glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * ___gltf0, int32_t ___bufferIndex1, ArraySegment_1_t3BCE22176B44ACEB25236AD7501F226112F6E07A  ___array2, int32_t ___target3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mDDEE5E9B13D0FE35D394C910EB8ADDF90244F666_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m3528BC05F270F79CD2DFF46FFDD7722124617F04_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_mC4617D8E0A2C06E1168AC910BDB298A17C8F53FC_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4 * V_0 = NULL;
	{
		// if (array.Count == 0)
		int32_t L_0;
		L_0 = ArraySegment_1_get_Count_m86D4F7A8C6E66A89B03DB83E4F8F82019625E483_inline((ArraySegment_1_t3BCE22176B44ACEB25236AD7501F226112F6E07A *)(ArraySegment_1_t3BCE22176B44ACEB25236AD7501F226112F6E07A *)(&___array2), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		// return -1;
		return (int32_t)(-1);
	}

IL_000b:
	{
		// var view = gltf.buffers[bufferIndex].Append(array, target);
		glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * L_1 = ___gltf0;
		NullCheck(L_1);
		List_1_t4BFB26128F5A4C3E19B9E2DEAB3A142C3EFF8F38 * L_2 = (List_1_t4BFB26128F5A4C3E19B9E2DEAB3A142C3EFF8F38 *)L_1->get_buffers_1();
		int32_t L_3 = ___bufferIndex1;
		NullCheck((List_1_t4BFB26128F5A4C3E19B9E2DEAB3A142C3EFF8F38 *)L_2);
		glTFBuffer_t840FC6FB6032C82B50388CE51BB1F054475E4BD6 * L_4;
		L_4 = List_1_get_Item_mC4617D8E0A2C06E1168AC910BDB298A17C8F53FC_inline((List_1_t4BFB26128F5A4C3E19B9E2DEAB3A142C3EFF8F38 *)L_2, (int32_t)L_3, /*hidden argument*/List_1_get_Item_mC4617D8E0A2C06E1168AC910BDB298A17C8F53FC_RuntimeMethod_var);
		ArraySegment_1_t3BCE22176B44ACEB25236AD7501F226112F6E07A  L_5 = ___array2;
		int32_t L_6 = ___target3;
		NullCheck((glTFBuffer_t840FC6FB6032C82B50388CE51BB1F054475E4BD6 *)L_4);
		glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4 * L_7;
		L_7 = ((  glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4 * (*) (glTFBuffer_t840FC6FB6032C82B50388CE51BB1F054475E4BD6 *, ArraySegment_1_t3BCE22176B44ACEB25236AD7501F226112F6E07A , int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((glTFBuffer_t840FC6FB6032C82B50388CE51BB1F054475E4BD6 *)L_4, (ArraySegment_1_t3BCE22176B44ACEB25236AD7501F226112F6E07A )L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		V_0 = (glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4 *)L_7;
		// var viewIndex = gltf.bufferViews.Count;
		glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * L_8 = ___gltf0;
		NullCheck(L_8);
		List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 * L_9 = (List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 *)L_8->get_bufferViews_2();
		NullCheck((List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 *)L_9);
		int32_t L_10;
		L_10 = List_1_get_Count_m3528BC05F270F79CD2DFF46FFDD7722124617F04_inline((List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 *)L_9, /*hidden argument*/List_1_get_Count_m3528BC05F270F79CD2DFF46FFDD7722124617F04_RuntimeMethod_var);
		// gltf.bufferViews.Add(view);
		glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * L_11 = ___gltf0;
		NullCheck(L_11);
		List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 * L_12 = (List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 *)L_11->get_bufferViews_2();
		glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4 * L_13 = V_0;
		NullCheck((List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 *)L_12);
		List_1_Add_mDDEE5E9B13D0FE35D394C910EB8ADDF90244F666((List_1_tA2AF529BC5EABAB533FB9494F9DC9DE2F1013A89 *)L_12, (glTFBufferView_tD64303522BB36E6D50AEB087DF4A8395655BBCB4 *)L_13, /*hidden argument*/List_1_Add_mDDEE5E9B13D0FE35D394C910EB8ADDF90244F666_RuntimeMethod_var);
		// return viewIndex;
		return (int32_t)L_10;
	}
}
// System.Int32 UniGLTF.glTFExtensions::ExtendSparseBufferAndGetAccessorIndex<UnityEngine.Vector3>(UniGLTF.glTF,System.Int32,System.Int32,System.ArraySegment`1<T>,System.Int32[],System.Int32,UniGLTF.glBufferTarget)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t glTFExtensions_ExtendSparseBufferAndGetAccessorIndex_TisVector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_m47261D29F9C4CE39E41019F02A3FE0ECC8C83FC1_gshared (glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * ___gltf0, int32_t ___bufferIndex1, int32_t ___accessorCount2, ArraySegment_1_t12E2F3BC867255EB056CA1F106A8D1E85142A3F1  ___sparseValues3, Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___sparseIndices4, int32_t ___sparseIndicesViewIndex5, int32_t ___target6, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m6189B75EF62756B468C403149F4AD081AC68C912_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mC221419694BDE6FF50C3F68C0964472743ACBA03_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&glTFSparseIndices_t332206D5F85C3918BDB6880C545D1B59EF3DE614_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&glTFSparseValues_t482F196AE1519B26241A4BB28B338F2977BC734F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&glTFSparse_tF8621187A1982375A902F6D7B023E40B72886684_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// if (sparseValues.Count == 0)
		int32_t L_0;
		L_0 = ArraySegment_1_get_Count_m03AF4BDAA1D5D96096A0072A85C43C2FBC7C03EE_inline((ArraySegment_1_t12E2F3BC867255EB056CA1F106A8D1E85142A3F1 *)(ArraySegment_1_t12E2F3BC867255EB056CA1F106A8D1E85142A3F1 *)(&___sparseValues3), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		// return -1;
		return (int32_t)(-1);
	}

IL_000b:
	{
		// var sparseValuesViewIndex = ExtendBufferAndGetViewIndex(gltf, bufferIndex, sparseValues, target);
		glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * L_1 = ___gltf0;
		int32_t L_2 = ___bufferIndex1;
		ArraySegment_1_t12E2F3BC867255EB056CA1F106A8D1E85142A3F1  L_3 = ___sparseValues3;
		int32_t L_4 = ___target6;
		IL2CPP_RUNTIME_CLASS_INIT(glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		int32_t L_5;
		L_5 = ((  int32_t (*) (glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 *, int32_t, ArraySegment_1_t12E2F3BC867255EB056CA1F106A8D1E85142A3F1 , int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->methodPointer)((glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 *)L_1, (int32_t)L_2, (ArraySegment_1_t12E2F3BC867255EB056CA1F106A8D1E85142A3F1 )L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		V_0 = (int32_t)L_5;
		// var accessorIndex = gltf.accessors.Count;
		glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * L_6 = ___gltf0;
		NullCheck(L_6);
		List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC * L_7 = (List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC *)L_6->get_accessors_3();
		NullCheck((List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC *)L_7);
		int32_t L_8;
		L_8 = List_1_get_Count_mC221419694BDE6FF50C3F68C0964472743ACBA03_inline((List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC *)L_7, /*hidden argument*/List_1_get_Count_mC221419694BDE6FF50C3F68C0964472743ACBA03_RuntimeMethod_var);
		// gltf.accessors.Add(new glTFAccessor
		// {
		//     byteOffset = 0,
		//     componentType = GetComponentType<T>(),
		//     type = GetAccessorType<T>(),
		//     count = accessorCount,
		// 
		//     sparse = new glTFSparse
		//     {
		//         count=sparseIndices.Length,
		//         indices = new glTFSparseIndices
		//         {
		//             bufferView = sparseIndicesViewIndex,
		//             componentType = glComponentType.UNSIGNED_INT
		//         },
		//         values = new glTFSparseValues
		//         {
		//             bufferView = sparseValuesViewIndex,
		//         }
		//     }
		// });
		glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * L_9 = ___gltf0;
		NullCheck(L_9);
		List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC * L_10 = (List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC *)L_9->get_accessors_3();
		glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC * L_11 = (glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC *)il2cpp_codegen_object_new(glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC_il2cpp_TypeInfo_var);
		glTFAccessor__ctor_m0EF366F9EC3426182BDF6055B0B20661744327B8(L_11, /*hidden argument*/NULL);
		glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC * L_12 = (glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC *)L_11;
		NullCheck(L_12);
		L_12->set_byteOffset_1(0);
		glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC * L_13 = (glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC *)L_12;
		int32_t L_14;
		L_14 = ((  int32_t (*) (const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)(/*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		NullCheck(L_13);
		L_13->set_componentType_3(L_14);
		glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC * L_15 = (glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC *)L_13;
		String_t* L_16;
		L_16 = ((  String_t* (*) (const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)->methodPointer)(/*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3));
		NullCheck(L_15);
		L_15->set_type_2(L_16);
		glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC * L_17 = (glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC *)L_15;
		int32_t L_18 = ___accessorCount2;
		NullCheck(L_17);
		L_17->set_count_4(L_18);
		glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC * L_19 = (glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC *)L_17;
		glTFSparse_tF8621187A1982375A902F6D7B023E40B72886684 * L_20 = (glTFSparse_tF8621187A1982375A902F6D7B023E40B72886684 *)il2cpp_codegen_object_new(glTFSparse_tF8621187A1982375A902F6D7B023E40B72886684_il2cpp_TypeInfo_var);
		glTFSparse__ctor_mF9AD5D5E7FC22FF264A96F90E0865BB31E739375(L_20, /*hidden argument*/NULL);
		glTFSparse_tF8621187A1982375A902F6D7B023E40B72886684 * L_21 = (glTFSparse_tF8621187A1982375A902F6D7B023E40B72886684 *)L_20;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_22 = ___sparseIndices4;
		NullCheck(L_22);
		NullCheck(L_21);
		L_21->set_count_0(((int32_t)((int32_t)(((RuntimeArray*)L_22)->max_length))));
		glTFSparse_tF8621187A1982375A902F6D7B023E40B72886684 * L_23 = (glTFSparse_tF8621187A1982375A902F6D7B023E40B72886684 *)L_21;
		glTFSparseIndices_t332206D5F85C3918BDB6880C545D1B59EF3DE614 * L_24 = (glTFSparseIndices_t332206D5F85C3918BDB6880C545D1B59EF3DE614 *)il2cpp_codegen_object_new(glTFSparseIndices_t332206D5F85C3918BDB6880C545D1B59EF3DE614_il2cpp_TypeInfo_var);
		glTFSparseIndices__ctor_m5F712EA16D41FA539BC70B661434283E3F9F2EC3(L_24, /*hidden argument*/NULL);
		glTFSparseIndices_t332206D5F85C3918BDB6880C545D1B59EF3DE614 * L_25 = (glTFSparseIndices_t332206D5F85C3918BDB6880C545D1B59EF3DE614 *)L_24;
		int32_t L_26 = ___sparseIndicesViewIndex5;
		NullCheck(L_25);
		L_25->set_bufferView_0(L_26);
		glTFSparseIndices_t332206D5F85C3918BDB6880C545D1B59EF3DE614 * L_27 = (glTFSparseIndices_t332206D5F85C3918BDB6880C545D1B59EF3DE614 *)L_25;
		NullCheck(L_27);
		L_27->set_componentType_2(((int32_t)5125));
		NullCheck(L_23);
		L_23->set_indices_1(L_27);
		glTFSparse_tF8621187A1982375A902F6D7B023E40B72886684 * L_28 = (glTFSparse_tF8621187A1982375A902F6D7B023E40B72886684 *)L_23;
		glTFSparseValues_t482F196AE1519B26241A4BB28B338F2977BC734F * L_29 = (glTFSparseValues_t482F196AE1519B26241A4BB28B338F2977BC734F *)il2cpp_codegen_object_new(glTFSparseValues_t482F196AE1519B26241A4BB28B338F2977BC734F_il2cpp_TypeInfo_var);
		glTFSparseValues__ctor_m9F5DAF602883EB7C62915DDCBECBE476D22FB829(L_29, /*hidden argument*/NULL);
		glTFSparseValues_t482F196AE1519B26241A4BB28B338F2977BC734F * L_30 = (glTFSparseValues_t482F196AE1519B26241A4BB28B338F2977BC734F *)L_29;
		int32_t L_31 = V_0;
		NullCheck(L_30);
		L_30->set_bufferView_0(L_31);
		NullCheck(L_28);
		L_28->set_values_2(L_30);
		NullCheck(L_19);
		L_19->set_sparse_8(L_28);
		NullCheck((List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC *)L_10);
		List_1_Add_m6189B75EF62756B468C403149F4AD081AC68C912((List_1_tD3A84FADFAD7490735433FE8B77D3EEF8F4E69AC *)L_10, (glTFAccessor_tF7B4A68378B1BCBED0C8B3510DCBC15341534EFC *)L_19, /*hidden argument*/List_1_Add_m6189B75EF62756B468C403149F4AD081AC68C912_RuntimeMethod_var);
		// return accessorIndex;
		return (int32_t)L_8;
	}
}
// System.Int32 UniGLTF.glTFExtensions::ExtendSparseBufferAndGetAccessorIndex<UnityEngine.Vector3>(UniGLTF.glTF,System.Int32,System.Int32,T[],System.Int32[],System.Int32,UniGLTF.glBufferTarget)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t glTFExtensions_ExtendSparseBufferAndGetAccessorIndex_TisVector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_m1178FE4808D2F44A86A8DE3523A45044043BB654_gshared (glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * ___gltf0, int32_t ___bufferIndex1, int32_t ___accessorCount2, Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___sparseValues3, Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___sparseIndices4, int32_t ___sparseViewIndex5, int32_t ___target6, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return ExtendSparseBufferAndGetAccessorIndex(gltf, bufferIndex,
		//     accessorCount,
		//     new ArraySegment<T>(sparseValues), sparseIndices, sparseViewIndex,
		//     target);
		glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 * L_0 = ___gltf0;
		int32_t L_1 = ___bufferIndex1;
		int32_t L_2 = ___accessorCount2;
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_3 = ___sparseValues3;
		ArraySegment_1_t12E2F3BC867255EB056CA1F106A8D1E85142A3F1  L_4;
		memset((&L_4), 0, sizeof(L_4));
		ArraySegment_1__ctor_mDC6C24720C4540B4A9A00A8F03A5347729CBB745((&L_4), (Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_5 = ___sparseIndices4;
		int32_t L_6 = ___sparseViewIndex5;
		int32_t L_7 = ___target6;
		IL2CPP_RUNTIME_CLASS_INIT(glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		int32_t L_8;
		L_8 = ((  int32_t (*) (glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 *, int32_t, int32_t, ArraySegment_1_t12E2F3BC867255EB056CA1F106A8D1E85142A3F1 , Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->methodPointer)((glTF_tA59248124AF32A6A52B036D982CFB4CF1FEBAA17 *)L_0, (int32_t)L_1, (int32_t)L_2, (ArraySegment_1_t12E2F3BC867255EB056CA1F106A8D1E85142A3F1 )L_4, (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)L_5, (int32_t)L_6, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		return (int32_t)L_8;
	}
}
// System.Int32 UniGLTF.glTFExtensions::GetAccessorElementCount<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t glTFExtensions_GetAccessorElementCount_TisRuntimeObject_m95BD59BC532BDA0C91D8CF0FFBF2B85A6BC9649B_gshared (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_TryGetValue_m5453E36663BAE277EFCD153F225808627F9570CD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	ComponentVec_t7A8C27B684980E106027B1173391609E39091C33  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// var cv = default(ComponentVec);
		il2cpp_codegen_initobj((&V_0), sizeof(ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 ));
		// if (ComponentTypeMap.TryGetValue(typeof(T), out cv))
		IL2CPP_RUNTIME_CLASS_INIT(glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E * L_0 = ((glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_StaticFields*)il2cpp_codegen_static_fields_for(glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var))->get_ComponentTypeMap_0();
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_1 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2;
		L_2 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_1, /*hidden argument*/NULL);
		NullCheck((Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E *)L_0);
		bool L_3;
		L_3 = Dictionary_2_TryGetValue_m5453E36663BAE277EFCD153F225808627F9570CD((Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E *)L_0, (Type_t *)L_2, (ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 *)(ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 *)(&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m5453E36663BAE277EFCD153F225808627F9570CD_RuntimeMethod_var);
		if (!L_3)
		{
			goto IL_0027;
		}
	}
	{
		// return cv.ElementCount;
		ComponentVec_t7A8C27B684980E106027B1173391609E39091C33  L_4 = V_0;
		int32_t L_5 = (int32_t)L_4.get_ElementCount_1();
		return (int32_t)L_5;
	}

IL_0027:
	{
		// return 1;
		return (int32_t)1;
	}
}
// System.String UniGLTF.glTFExtensions::GetAccessorType<UnityEngine.Color>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* glTFExtensions_GetAccessorType_TisColor_tF40DAF76C04FFECF3FE6024F85A294741C9CC659_mAC298AB0447CC9FB4D019F99B852974CA050283E_gshared (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_TryGetValue_m5453E36663BAE277EFCD153F225808627F9570CD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral02BB463928D4E15BD5ECA84DABC17D6F67D3A89A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6DA17EF0CFCB88F3EE460F1488CE83FBB4DE84A3);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7F5DF872FA6CA76650526A1B598A5AE1F16E1FFC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE3A5647F9D4D7321D0FB2267C5B9C147C222F636);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEAAF420AB23FFD94B4DF38C14A9A16F0BED2E0F7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	ComponentVec_t7A8C27B684980E106027B1173391609E39091C33  V_0;
	memset((&V_0), 0, sizeof(V_0));
	int32_t V_1 = 0;
	{
		// var cv = default(ComponentVec);
		il2cpp_codegen_initobj((&V_0), sizeof(ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 ));
		// if (ComponentTypeMap.TryGetValue(typeof(T), out cv))
		IL2CPP_RUNTIME_CLASS_INIT(glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E * L_0 = ((glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_StaticFields*)il2cpp_codegen_static_fields_for(glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var))->get_ComponentTypeMap_0();
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_1 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2;
		L_2 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_1, /*hidden argument*/NULL);
		NullCheck((Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E *)L_0);
		bool L_3;
		L_3 = Dictionary_2_TryGetValue_m5453E36663BAE277EFCD153F225808627F9570CD((Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E *)L_0, (Type_t *)L_2, (ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 *)(ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 *)(&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m5453E36663BAE277EFCD153F225808627F9570CD_RuntimeMethod_var);
		if (!L_3)
		{
			goto IL_0060;
		}
	}
	{
		// switch (cv.ElementCount)
		ComponentVec_t7A8C27B684980E106027B1173391609E39091C33  L_4 = V_0;
		int32_t L_5 = (int32_t)L_4.get_ElementCount_1();
		V_1 = (int32_t)L_5;
		int32_t L_6 = V_1;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_6, (int32_t)2)))
		{
			case 0:
			{
				goto IL_0042;
			}
			case 1:
			{
				goto IL_0048;
			}
			case 2:
			{
				goto IL_004e;
			}
		}
	}
	{
		int32_t L_7 = V_1;
		if ((((int32_t)L_7) == ((int32_t)((int32_t)16))))
		{
			goto IL_0054;
		}
	}
	{
		goto IL_005a;
	}

IL_0042:
	{
		// case 2: return "VEC2";
		return (String_t*)_stringLiteral02BB463928D4E15BD5ECA84DABC17D6F67D3A89A;
	}

IL_0048:
	{
		// case 3: return "VEC3";
		return (String_t*)_stringLiteralE3A5647F9D4D7321D0FB2267C5B9C147C222F636;
	}

IL_004e:
	{
		// case 4: return "VEC4";
		return (String_t*)_stringLiteral6DA17EF0CFCB88F3EE460F1488CE83FBB4DE84A3;
	}

IL_0054:
	{
		// case 16: return "MAT4";
		return (String_t*)_stringLiteralEAAF420AB23FFD94B4DF38C14A9A16F0BED2E0F7;
	}

IL_005a:
	{
		// default: throw new Exception();
		Exception_t * L_8 = (Exception_t *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)));
		Exception__ctor_m0E9BEC861F6DBED197960E5BA23149543B1D7F5B(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&glTFExtensions_GetAccessorType_TisColor_tF40DAF76C04FFECF3FE6024F85A294741C9CC659_mAC298AB0447CC9FB4D019F99B852974CA050283E_RuntimeMethod_var)));
	}

IL_0060:
	{
		// return "SCALAR";
		return (String_t*)_stringLiteral7F5DF872FA6CA76650526A1B598A5AE1F16E1FFC;
	}
}
// System.String UniGLTF.glTFExtensions::GetAccessorType<UnityEngine.Matrix4x4>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* glTFExtensions_GetAccessorType_TisMatrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461_mF397CF1D74549BA7A9BD045FEEB3E6A0831259BD_gshared (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_TryGetValue_m5453E36663BAE277EFCD153F225808627F9570CD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral02BB463928D4E15BD5ECA84DABC17D6F67D3A89A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6DA17EF0CFCB88F3EE460F1488CE83FBB4DE84A3);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7F5DF872FA6CA76650526A1B598A5AE1F16E1FFC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE3A5647F9D4D7321D0FB2267C5B9C147C222F636);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEAAF420AB23FFD94B4DF38C14A9A16F0BED2E0F7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	ComponentVec_t7A8C27B684980E106027B1173391609E39091C33  V_0;
	memset((&V_0), 0, sizeof(V_0));
	int32_t V_1 = 0;
	{
		// var cv = default(ComponentVec);
		il2cpp_codegen_initobj((&V_0), sizeof(ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 ));
		// if (ComponentTypeMap.TryGetValue(typeof(T), out cv))
		IL2CPP_RUNTIME_CLASS_INIT(glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E * L_0 = ((glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_StaticFields*)il2cpp_codegen_static_fields_for(glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var))->get_ComponentTypeMap_0();
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_1 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2;
		L_2 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_1, /*hidden argument*/NULL);
		NullCheck((Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E *)L_0);
		bool L_3;
		L_3 = Dictionary_2_TryGetValue_m5453E36663BAE277EFCD153F225808627F9570CD((Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E *)L_0, (Type_t *)L_2, (ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 *)(ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 *)(&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m5453E36663BAE277EFCD153F225808627F9570CD_RuntimeMethod_var);
		if (!L_3)
		{
			goto IL_0060;
		}
	}
	{
		// switch (cv.ElementCount)
		ComponentVec_t7A8C27B684980E106027B1173391609E39091C33  L_4 = V_0;
		int32_t L_5 = (int32_t)L_4.get_ElementCount_1();
		V_1 = (int32_t)L_5;
		int32_t L_6 = V_1;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_6, (int32_t)2)))
		{
			case 0:
			{
				goto IL_0042;
			}
			case 1:
			{
				goto IL_0048;
			}
			case 2:
			{
				goto IL_004e;
			}
		}
	}
	{
		int32_t L_7 = V_1;
		if ((((int32_t)L_7) == ((int32_t)((int32_t)16))))
		{
			goto IL_0054;
		}
	}
	{
		goto IL_005a;
	}

IL_0042:
	{
		// case 2: return "VEC2";
		return (String_t*)_stringLiteral02BB463928D4E15BD5ECA84DABC17D6F67D3A89A;
	}

IL_0048:
	{
		// case 3: return "VEC3";
		return (String_t*)_stringLiteralE3A5647F9D4D7321D0FB2267C5B9C147C222F636;
	}

IL_004e:
	{
		// case 4: return "VEC4";
		return (String_t*)_stringLiteral6DA17EF0CFCB88F3EE460F1488CE83FBB4DE84A3;
	}

IL_0054:
	{
		// case 16: return "MAT4";
		return (String_t*)_stringLiteralEAAF420AB23FFD94B4DF38C14A9A16F0BED2E0F7;
	}

IL_005a:
	{
		// default: throw new Exception();
		Exception_t * L_8 = (Exception_t *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)));
		Exception__ctor_m0E9BEC861F6DBED197960E5BA23149543B1D7F5B(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&glTFExtensions_GetAccessorType_TisMatrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461_mF397CF1D74549BA7A9BD045FEEB3E6A0831259BD_RuntimeMethod_var)));
	}

IL_0060:
	{
		// return "SCALAR";
		return (String_t*)_stringLiteral7F5DF872FA6CA76650526A1B598A5AE1F16E1FFC;
	}
}
// System.String UniGLTF.glTFExtensions::GetAccessorType<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* glTFExtensions_GetAccessorType_TisRuntimeObject_m86DD1A906539E3063A4EEB8AB82D74ABE1A3EA58_gshared (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_TryGetValue_m5453E36663BAE277EFCD153F225808627F9570CD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral02BB463928D4E15BD5ECA84DABC17D6F67D3A89A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6DA17EF0CFCB88F3EE460F1488CE83FBB4DE84A3);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7F5DF872FA6CA76650526A1B598A5AE1F16E1FFC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE3A5647F9D4D7321D0FB2267C5B9C147C222F636);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEAAF420AB23FFD94B4DF38C14A9A16F0BED2E0F7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	ComponentVec_t7A8C27B684980E106027B1173391609E39091C33  V_0;
	memset((&V_0), 0, sizeof(V_0));
	int32_t V_1 = 0;
	{
		// var cv = default(ComponentVec);
		il2cpp_codegen_initobj((&V_0), sizeof(ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 ));
		// if (ComponentTypeMap.TryGetValue(typeof(T), out cv))
		IL2CPP_RUNTIME_CLASS_INIT(glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E * L_0 = ((glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_StaticFields*)il2cpp_codegen_static_fields_for(glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var))->get_ComponentTypeMap_0();
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_1 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2;
		L_2 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_1, /*hidden argument*/NULL);
		NullCheck((Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E *)L_0);
		bool L_3;
		L_3 = Dictionary_2_TryGetValue_m5453E36663BAE277EFCD153F225808627F9570CD((Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E *)L_0, (Type_t *)L_2, (ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 *)(ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 *)(&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m5453E36663BAE277EFCD153F225808627F9570CD_RuntimeMethod_var);
		if (!L_3)
		{
			goto IL_0060;
		}
	}
	{
		// switch (cv.ElementCount)
		ComponentVec_t7A8C27B684980E106027B1173391609E39091C33  L_4 = V_0;
		int32_t L_5 = (int32_t)L_4.get_ElementCount_1();
		V_1 = (int32_t)L_5;
		int32_t L_6 = V_1;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_6, (int32_t)2)))
		{
			case 0:
			{
				goto IL_0042;
			}
			case 1:
			{
				goto IL_0048;
			}
			case 2:
			{
				goto IL_004e;
			}
		}
	}
	{
		int32_t L_7 = V_1;
		if ((((int32_t)L_7) == ((int32_t)((int32_t)16))))
		{
			goto IL_0054;
		}
	}
	{
		goto IL_005a;
	}

IL_0042:
	{
		// case 2: return "VEC2";
		return (String_t*)_stringLiteral02BB463928D4E15BD5ECA84DABC17D6F67D3A89A;
	}

IL_0048:
	{
		// case 3: return "VEC3";
		return (String_t*)_stringLiteralE3A5647F9D4D7321D0FB2267C5B9C147C222F636;
	}

IL_004e:
	{
		// case 4: return "VEC4";
		return (String_t*)_stringLiteral6DA17EF0CFCB88F3EE460F1488CE83FBB4DE84A3;
	}

IL_0054:
	{
		// case 16: return "MAT4";
		return (String_t*)_stringLiteralEAAF420AB23FFD94B4DF38C14A9A16F0BED2E0F7;
	}

IL_005a:
	{
		// default: throw new Exception();
		Exception_t * L_8 = (Exception_t *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)));
		Exception__ctor_m0E9BEC861F6DBED197960E5BA23149543B1D7F5B(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&glTFExtensions_GetAccessorType_TisRuntimeObject_m86DD1A906539E3063A4EEB8AB82D74ABE1A3EA58_RuntimeMethod_var)));
	}

IL_0060:
	{
		// return "SCALAR";
		return (String_t*)_stringLiteral7F5DF872FA6CA76650526A1B598A5AE1F16E1FFC;
	}
}
// System.String UniGLTF.glTFExtensions::GetAccessorType<System.UInt32>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* glTFExtensions_GetAccessorType_TisUInt32_tE60352A06233E4E69DD198BCC67142159F686B15_mABDD7BBAF69B1BEE6F36115F00EDA0DADAF017BB_gshared (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_TryGetValue_m5453E36663BAE277EFCD153F225808627F9570CD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral02BB463928D4E15BD5ECA84DABC17D6F67D3A89A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6DA17EF0CFCB88F3EE460F1488CE83FBB4DE84A3);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7F5DF872FA6CA76650526A1B598A5AE1F16E1FFC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE3A5647F9D4D7321D0FB2267C5B9C147C222F636);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEAAF420AB23FFD94B4DF38C14A9A16F0BED2E0F7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	ComponentVec_t7A8C27B684980E106027B1173391609E39091C33  V_0;
	memset((&V_0), 0, sizeof(V_0));
	int32_t V_1 = 0;
	{
		// var cv = default(ComponentVec);
		il2cpp_codegen_initobj((&V_0), sizeof(ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 ));
		// if (ComponentTypeMap.TryGetValue(typeof(T), out cv))
		IL2CPP_RUNTIME_CLASS_INIT(glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E * L_0 = ((glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_StaticFields*)il2cpp_codegen_static_fields_for(glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var))->get_ComponentTypeMap_0();
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_1 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2;
		L_2 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_1, /*hidden argument*/NULL);
		NullCheck((Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E *)L_0);
		bool L_3;
		L_3 = Dictionary_2_TryGetValue_m5453E36663BAE277EFCD153F225808627F9570CD((Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E *)L_0, (Type_t *)L_2, (ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 *)(ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 *)(&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m5453E36663BAE277EFCD153F225808627F9570CD_RuntimeMethod_var);
		if (!L_3)
		{
			goto IL_0060;
		}
	}
	{
		// switch (cv.ElementCount)
		ComponentVec_t7A8C27B684980E106027B1173391609E39091C33  L_4 = V_0;
		int32_t L_5 = (int32_t)L_4.get_ElementCount_1();
		V_1 = (int32_t)L_5;
		int32_t L_6 = V_1;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_6, (int32_t)2)))
		{
			case 0:
			{
				goto IL_0042;
			}
			case 1:
			{
				goto IL_0048;
			}
			case 2:
			{
				goto IL_004e;
			}
		}
	}
	{
		int32_t L_7 = V_1;
		if ((((int32_t)L_7) == ((int32_t)((int32_t)16))))
		{
			goto IL_0054;
		}
	}
	{
		goto IL_005a;
	}

IL_0042:
	{
		// case 2: return "VEC2";
		return (String_t*)_stringLiteral02BB463928D4E15BD5ECA84DABC17D6F67D3A89A;
	}

IL_0048:
	{
		// case 3: return "VEC3";
		return (String_t*)_stringLiteralE3A5647F9D4D7321D0FB2267C5B9C147C222F636;
	}

IL_004e:
	{
		// case 4: return "VEC4";
		return (String_t*)_stringLiteral6DA17EF0CFCB88F3EE460F1488CE83FBB4DE84A3;
	}

IL_0054:
	{
		// case 16: return "MAT4";
		return (String_t*)_stringLiteralEAAF420AB23FFD94B4DF38C14A9A16F0BED2E0F7;
	}

IL_005a:
	{
		// default: throw new Exception();
		Exception_t * L_8 = (Exception_t *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)));
		Exception__ctor_m0E9BEC861F6DBED197960E5BA23149543B1D7F5B(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&glTFExtensions_GetAccessorType_TisUInt32_tE60352A06233E4E69DD198BCC67142159F686B15_mABDD7BBAF69B1BEE6F36115F00EDA0DADAF017BB_RuntimeMethod_var)));
	}

IL_0060:
	{
		// return "SCALAR";
		return (String_t*)_stringLiteral7F5DF872FA6CA76650526A1B598A5AE1F16E1FFC;
	}
}
// System.String UniGLTF.glTFExtensions::GetAccessorType<UniGLTF.UShort4>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* glTFExtensions_GetAccessorType_TisUShort4_tB19A4ECD972B8600C9515187BC86B572B83920DB_mDCBE589FB99CC00854FCD9DE415B16F024B2129E_gshared (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_TryGetValue_m5453E36663BAE277EFCD153F225808627F9570CD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral02BB463928D4E15BD5ECA84DABC17D6F67D3A89A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6DA17EF0CFCB88F3EE460F1488CE83FBB4DE84A3);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7F5DF872FA6CA76650526A1B598A5AE1F16E1FFC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE3A5647F9D4D7321D0FB2267C5B9C147C222F636);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEAAF420AB23FFD94B4DF38C14A9A16F0BED2E0F7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	ComponentVec_t7A8C27B684980E106027B1173391609E39091C33  V_0;
	memset((&V_0), 0, sizeof(V_0));
	int32_t V_1 = 0;
	{
		// var cv = default(ComponentVec);
		il2cpp_codegen_initobj((&V_0), sizeof(ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 ));
		// if (ComponentTypeMap.TryGetValue(typeof(T), out cv))
		IL2CPP_RUNTIME_CLASS_INIT(glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E * L_0 = ((glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_StaticFields*)il2cpp_codegen_static_fields_for(glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var))->get_ComponentTypeMap_0();
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_1 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2;
		L_2 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_1, /*hidden argument*/NULL);
		NullCheck((Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E *)L_0);
		bool L_3;
		L_3 = Dictionary_2_TryGetValue_m5453E36663BAE277EFCD153F225808627F9570CD((Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E *)L_0, (Type_t *)L_2, (ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 *)(ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 *)(&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m5453E36663BAE277EFCD153F225808627F9570CD_RuntimeMethod_var);
		if (!L_3)
		{
			goto IL_0060;
		}
	}
	{
		// switch (cv.ElementCount)
		ComponentVec_t7A8C27B684980E106027B1173391609E39091C33  L_4 = V_0;
		int32_t L_5 = (int32_t)L_4.get_ElementCount_1();
		V_1 = (int32_t)L_5;
		int32_t L_6 = V_1;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_6, (int32_t)2)))
		{
			case 0:
			{
				goto IL_0042;
			}
			case 1:
			{
				goto IL_0048;
			}
			case 2:
			{
				goto IL_004e;
			}
		}
	}
	{
		int32_t L_7 = V_1;
		if ((((int32_t)L_7) == ((int32_t)((int32_t)16))))
		{
			goto IL_0054;
		}
	}
	{
		goto IL_005a;
	}

IL_0042:
	{
		// case 2: return "VEC2";
		return (String_t*)_stringLiteral02BB463928D4E15BD5ECA84DABC17D6F67D3A89A;
	}

IL_0048:
	{
		// case 3: return "VEC3";
		return (String_t*)_stringLiteralE3A5647F9D4D7321D0FB2267C5B9C147C222F636;
	}

IL_004e:
	{
		// case 4: return "VEC4";
		return (String_t*)_stringLiteral6DA17EF0CFCB88F3EE460F1488CE83FBB4DE84A3;
	}

IL_0054:
	{
		// case 16: return "MAT4";
		return (String_t*)_stringLiteralEAAF420AB23FFD94B4DF38C14A9A16F0BED2E0F7;
	}

IL_005a:
	{
		// default: throw new Exception();
		Exception_t * L_8 = (Exception_t *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)));
		Exception__ctor_m0E9BEC861F6DBED197960E5BA23149543B1D7F5B(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&glTFExtensions_GetAccessorType_TisUShort4_tB19A4ECD972B8600C9515187BC86B572B83920DB_mDCBE589FB99CC00854FCD9DE415B16F024B2129E_RuntimeMethod_var)));
	}

IL_0060:
	{
		// return "SCALAR";
		return (String_t*)_stringLiteral7F5DF872FA6CA76650526A1B598A5AE1F16E1FFC;
	}
}
// System.String UniGLTF.glTFExtensions::GetAccessorType<UnityEngine.Vector2>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* glTFExtensions_GetAccessorType_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_mCFFC4EBE31F11816D29C72308AEF1E51A37570EF_gshared (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_TryGetValue_m5453E36663BAE277EFCD153F225808627F9570CD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral02BB463928D4E15BD5ECA84DABC17D6F67D3A89A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6DA17EF0CFCB88F3EE460F1488CE83FBB4DE84A3);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7F5DF872FA6CA76650526A1B598A5AE1F16E1FFC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE3A5647F9D4D7321D0FB2267C5B9C147C222F636);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEAAF420AB23FFD94B4DF38C14A9A16F0BED2E0F7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	ComponentVec_t7A8C27B684980E106027B1173391609E39091C33  V_0;
	memset((&V_0), 0, sizeof(V_0));
	int32_t V_1 = 0;
	{
		// var cv = default(ComponentVec);
		il2cpp_codegen_initobj((&V_0), sizeof(ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 ));
		// if (ComponentTypeMap.TryGetValue(typeof(T), out cv))
		IL2CPP_RUNTIME_CLASS_INIT(glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E * L_0 = ((glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_StaticFields*)il2cpp_codegen_static_fields_for(glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var))->get_ComponentTypeMap_0();
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_1 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2;
		L_2 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_1, /*hidden argument*/NULL);
		NullCheck((Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E *)L_0);
		bool L_3;
		L_3 = Dictionary_2_TryGetValue_m5453E36663BAE277EFCD153F225808627F9570CD((Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E *)L_0, (Type_t *)L_2, (ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 *)(ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 *)(&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m5453E36663BAE277EFCD153F225808627F9570CD_RuntimeMethod_var);
		if (!L_3)
		{
			goto IL_0060;
		}
	}
	{
		// switch (cv.ElementCount)
		ComponentVec_t7A8C27B684980E106027B1173391609E39091C33  L_4 = V_0;
		int32_t L_5 = (int32_t)L_4.get_ElementCount_1();
		V_1 = (int32_t)L_5;
		int32_t L_6 = V_1;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_6, (int32_t)2)))
		{
			case 0:
			{
				goto IL_0042;
			}
			case 1:
			{
				goto IL_0048;
			}
			case 2:
			{
				goto IL_004e;
			}
		}
	}
	{
		int32_t L_7 = V_1;
		if ((((int32_t)L_7) == ((int32_t)((int32_t)16))))
		{
			goto IL_0054;
		}
	}
	{
		goto IL_005a;
	}

IL_0042:
	{
		// case 2: return "VEC2";
		return (String_t*)_stringLiteral02BB463928D4E15BD5ECA84DABC17D6F67D3A89A;
	}

IL_0048:
	{
		// case 3: return "VEC3";
		return (String_t*)_stringLiteralE3A5647F9D4D7321D0FB2267C5B9C147C222F636;
	}

IL_004e:
	{
		// case 4: return "VEC4";
		return (String_t*)_stringLiteral6DA17EF0CFCB88F3EE460F1488CE83FBB4DE84A3;
	}

IL_0054:
	{
		// case 16: return "MAT4";
		return (String_t*)_stringLiteralEAAF420AB23FFD94B4DF38C14A9A16F0BED2E0F7;
	}

IL_005a:
	{
		// default: throw new Exception();
		Exception_t * L_8 = (Exception_t *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)));
		Exception__ctor_m0E9BEC861F6DBED197960E5BA23149543B1D7F5B(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&glTFExtensions_GetAccessorType_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_mCFFC4EBE31F11816D29C72308AEF1E51A37570EF_RuntimeMethod_var)));
	}

IL_0060:
	{
		// return "SCALAR";
		return (String_t*)_stringLiteral7F5DF872FA6CA76650526A1B598A5AE1F16E1FFC;
	}
}
// System.String UniGLTF.glTFExtensions::GetAccessorType<UnityEngine.Vector3>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* glTFExtensions_GetAccessorType_TisVector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_m9CD508D0E1682A72CF0F38B496C8FBB2E4E0278C_gshared (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_TryGetValue_m5453E36663BAE277EFCD153F225808627F9570CD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral02BB463928D4E15BD5ECA84DABC17D6F67D3A89A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6DA17EF0CFCB88F3EE460F1488CE83FBB4DE84A3);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7F5DF872FA6CA76650526A1B598A5AE1F16E1FFC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE3A5647F9D4D7321D0FB2267C5B9C147C222F636);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEAAF420AB23FFD94B4DF38C14A9A16F0BED2E0F7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	ComponentVec_t7A8C27B684980E106027B1173391609E39091C33  V_0;
	memset((&V_0), 0, sizeof(V_0));
	int32_t V_1 = 0;
	{
		// var cv = default(ComponentVec);
		il2cpp_codegen_initobj((&V_0), sizeof(ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 ));
		// if (ComponentTypeMap.TryGetValue(typeof(T), out cv))
		IL2CPP_RUNTIME_CLASS_INIT(glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E * L_0 = ((glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_StaticFields*)il2cpp_codegen_static_fields_for(glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var))->get_ComponentTypeMap_0();
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_1 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2;
		L_2 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_1, /*hidden argument*/NULL);
		NullCheck((Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E *)L_0);
		bool L_3;
		L_3 = Dictionary_2_TryGetValue_m5453E36663BAE277EFCD153F225808627F9570CD((Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E *)L_0, (Type_t *)L_2, (ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 *)(ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 *)(&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m5453E36663BAE277EFCD153F225808627F9570CD_RuntimeMethod_var);
		if (!L_3)
		{
			goto IL_0060;
		}
	}
	{
		// switch (cv.ElementCount)
		ComponentVec_t7A8C27B684980E106027B1173391609E39091C33  L_4 = V_0;
		int32_t L_5 = (int32_t)L_4.get_ElementCount_1();
		V_1 = (int32_t)L_5;
		int32_t L_6 = V_1;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_6, (int32_t)2)))
		{
			case 0:
			{
				goto IL_0042;
			}
			case 1:
			{
				goto IL_0048;
			}
			case 2:
			{
				goto IL_004e;
			}
		}
	}
	{
		int32_t L_7 = V_1;
		if ((((int32_t)L_7) == ((int32_t)((int32_t)16))))
		{
			goto IL_0054;
		}
	}
	{
		goto IL_005a;
	}

IL_0042:
	{
		// case 2: return "VEC2";
		return (String_t*)_stringLiteral02BB463928D4E15BD5ECA84DABC17D6F67D3A89A;
	}

IL_0048:
	{
		// case 3: return "VEC3";
		return (String_t*)_stringLiteralE3A5647F9D4D7321D0FB2267C5B9C147C222F636;
	}

IL_004e:
	{
		// case 4: return "VEC4";
		return (String_t*)_stringLiteral6DA17EF0CFCB88F3EE460F1488CE83FBB4DE84A3;
	}

IL_0054:
	{
		// case 16: return "MAT4";
		return (String_t*)_stringLiteralEAAF420AB23FFD94B4DF38C14A9A16F0BED2E0F7;
	}

IL_005a:
	{
		// default: throw new Exception();
		Exception_t * L_8 = (Exception_t *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)));
		Exception__ctor_m0E9BEC861F6DBED197960E5BA23149543B1D7F5B(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&glTFExtensions_GetAccessorType_TisVector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_m9CD508D0E1682A72CF0F38B496C8FBB2E4E0278C_RuntimeMethod_var)));
	}

IL_0060:
	{
		// return "SCALAR";
		return (String_t*)_stringLiteral7F5DF872FA6CA76650526A1B598A5AE1F16E1FFC;
	}
}
// System.String UniGLTF.glTFExtensions::GetAccessorType<UnityEngine.Vector4>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* glTFExtensions_GetAccessorType_TisVector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_m6FCC0143CCE1E49205215AFB4A0C66D6697CEB4A_gshared (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_TryGetValue_m5453E36663BAE277EFCD153F225808627F9570CD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral02BB463928D4E15BD5ECA84DABC17D6F67D3A89A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6DA17EF0CFCB88F3EE460F1488CE83FBB4DE84A3);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7F5DF872FA6CA76650526A1B598A5AE1F16E1FFC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE3A5647F9D4D7321D0FB2267C5B9C147C222F636);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEAAF420AB23FFD94B4DF38C14A9A16F0BED2E0F7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	ComponentVec_t7A8C27B684980E106027B1173391609E39091C33  V_0;
	memset((&V_0), 0, sizeof(V_0));
	int32_t V_1 = 0;
	{
		// var cv = default(ComponentVec);
		il2cpp_codegen_initobj((&V_0), sizeof(ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 ));
		// if (ComponentTypeMap.TryGetValue(typeof(T), out cv))
		IL2CPP_RUNTIME_CLASS_INIT(glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E * L_0 = ((glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_StaticFields*)il2cpp_codegen_static_fields_for(glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var))->get_ComponentTypeMap_0();
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_1 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2;
		L_2 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_1, /*hidden argument*/NULL);
		NullCheck((Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E *)L_0);
		bool L_3;
		L_3 = Dictionary_2_TryGetValue_m5453E36663BAE277EFCD153F225808627F9570CD((Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E *)L_0, (Type_t *)L_2, (ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 *)(ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 *)(&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m5453E36663BAE277EFCD153F225808627F9570CD_RuntimeMethod_var);
		if (!L_3)
		{
			goto IL_0060;
		}
	}
	{
		// switch (cv.ElementCount)
		ComponentVec_t7A8C27B684980E106027B1173391609E39091C33  L_4 = V_0;
		int32_t L_5 = (int32_t)L_4.get_ElementCount_1();
		V_1 = (int32_t)L_5;
		int32_t L_6 = V_1;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_6, (int32_t)2)))
		{
			case 0:
			{
				goto IL_0042;
			}
			case 1:
			{
				goto IL_0048;
			}
			case 2:
			{
				goto IL_004e;
			}
		}
	}
	{
		int32_t L_7 = V_1;
		if ((((int32_t)L_7) == ((int32_t)((int32_t)16))))
		{
			goto IL_0054;
		}
	}
	{
		goto IL_005a;
	}

IL_0042:
	{
		// case 2: return "VEC2";
		return (String_t*)_stringLiteral02BB463928D4E15BD5ECA84DABC17D6F67D3A89A;
	}

IL_0048:
	{
		// case 3: return "VEC3";
		return (String_t*)_stringLiteralE3A5647F9D4D7321D0FB2267C5B9C147C222F636;
	}

IL_004e:
	{
		// case 4: return "VEC4";
		return (String_t*)_stringLiteral6DA17EF0CFCB88F3EE460F1488CE83FBB4DE84A3;
	}

IL_0054:
	{
		// case 16: return "MAT4";
		return (String_t*)_stringLiteralEAAF420AB23FFD94B4DF38C14A9A16F0BED2E0F7;
	}

IL_005a:
	{
		// default: throw new Exception();
		Exception_t * L_8 = (Exception_t *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)));
		Exception__ctor_m0E9BEC861F6DBED197960E5BA23149543B1D7F5B(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&glTFExtensions_GetAccessorType_TisVector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_m6FCC0143CCE1E49205215AFB4A0C66D6697CEB4A_RuntimeMethod_var)));
	}

IL_0060:
	{
		// return "SCALAR";
		return (String_t*)_stringLiteral7F5DF872FA6CA76650526A1B598A5AE1F16E1FFC;
	}
}
// UniGLTF.glComponentType UniGLTF.glTFExtensions::GetComponentType<UnityEngine.Color>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t glTFExtensions_GetComponentType_TisColor_tF40DAF76C04FFECF3FE6024F85A294741C9CC659_m31B80320403DC6AA259DE27AE6B9AB626B51AD84_gshared (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_TryGetValue_m5453E36663BAE277EFCD153F225808627F9570CD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UInt32_tE60352A06233E4E69DD198BCC67142159F686B15_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	ComponentVec_t7A8C27B684980E106027B1173391609E39091C33  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// var cv = default(ComponentVec);
		il2cpp_codegen_initobj((&V_0), sizeof(ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 ));
		// if (ComponentTypeMap.TryGetValue(typeof(T), out cv))
		IL2CPP_RUNTIME_CLASS_INIT(glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E * L_0 = ((glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_StaticFields*)il2cpp_codegen_static_fields_for(glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var))->get_ComponentTypeMap_0();
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_1 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2;
		L_2 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_1, /*hidden argument*/NULL);
		NullCheck((Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E *)L_0);
		bool L_3;
		L_3 = Dictionary_2_TryGetValue_m5453E36663BAE277EFCD153F225808627F9570CD((Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E *)L_0, (Type_t *)L_2, (ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 *)(ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 *)(&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m5453E36663BAE277EFCD153F225808627F9570CD_RuntimeMethod_var);
		if (!L_3)
		{
			goto IL_0027;
		}
	}
	{
		// return cv.ComponentType;
		ComponentVec_t7A8C27B684980E106027B1173391609E39091C33  L_4 = V_0;
		int32_t L_5 = (int32_t)L_4.get_ComponentType_0();
		return (int32_t)L_5;
	}

IL_0027:
	{
		// else if (typeof(T) == typeof(uint))
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_6 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7;
		L_7 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_6, /*hidden argument*/NULL);
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_8 = { reinterpret_cast<intptr_t> (UInt32_tE60352A06233E4E69DD198BCC67142159F686B15_0_0_0_var) };
		Type_t * L_9;
		L_9 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_8, /*hidden argument*/NULL);
		bool L_10;
		L_10 = Type_op_Equality_mA438719A1FDF103C7BBBB08AEF564E7FAEEA0046((Type_t *)L_7, (Type_t *)L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0048;
		}
	}
	{
		// return glComponentType.UNSIGNED_INT;
		return (int32_t)(((int32_t)5125));
	}

IL_0048:
	{
		// else if (typeof(T) == typeof(float))
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_11 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_12;
		L_12 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_11, /*hidden argument*/NULL);
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_13 = { reinterpret_cast<intptr_t> (Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_0_0_0_var) };
		Type_t * L_14;
		L_14 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_13, /*hidden argument*/NULL);
		bool L_15;
		L_15 = Type_op_Equality_mA438719A1FDF103C7BBBB08AEF564E7FAEEA0046((Type_t *)L_12, (Type_t *)L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0069;
		}
	}
	{
		// return glComponentType.FLOAT;
		return (int32_t)(((int32_t)5126));
	}

IL_0069:
	{
		// throw new NotImplementedException(typeof(T).Name);
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_16 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Type_t_il2cpp_TypeInfo_var)));
		Type_t * L_17;
		L_17 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_16, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_17);
		String_t* L_18;
		L_18 = VirtualFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_17);
		NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6 * L_19 = (NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6_il2cpp_TypeInfo_var)));
		NotImplementedException__ctor_m8A9AA4499614A5BC57DD21713D0720630C130AEB(L_19, (String_t*)L_18, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_19, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&glTFExtensions_GetComponentType_TisColor_tF40DAF76C04FFECF3FE6024F85A294741C9CC659_m31B80320403DC6AA259DE27AE6B9AB626B51AD84_RuntimeMethod_var)));
	}
}
// UniGLTF.glComponentType UniGLTF.glTFExtensions::GetComponentType<UnityEngine.Matrix4x4>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t glTFExtensions_GetComponentType_TisMatrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461_m2C2A005ACBC154DBAB81F23A91AEA84AC2144C08_gshared (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_TryGetValue_m5453E36663BAE277EFCD153F225808627F9570CD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UInt32_tE60352A06233E4E69DD198BCC67142159F686B15_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	ComponentVec_t7A8C27B684980E106027B1173391609E39091C33  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// var cv = default(ComponentVec);
		il2cpp_codegen_initobj((&V_0), sizeof(ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 ));
		// if (ComponentTypeMap.TryGetValue(typeof(T), out cv))
		IL2CPP_RUNTIME_CLASS_INIT(glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E * L_0 = ((glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_StaticFields*)il2cpp_codegen_static_fields_for(glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var))->get_ComponentTypeMap_0();
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_1 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2;
		L_2 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_1, /*hidden argument*/NULL);
		NullCheck((Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E *)L_0);
		bool L_3;
		L_3 = Dictionary_2_TryGetValue_m5453E36663BAE277EFCD153F225808627F9570CD((Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E *)L_0, (Type_t *)L_2, (ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 *)(ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 *)(&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m5453E36663BAE277EFCD153F225808627F9570CD_RuntimeMethod_var);
		if (!L_3)
		{
			goto IL_0027;
		}
	}
	{
		// return cv.ComponentType;
		ComponentVec_t7A8C27B684980E106027B1173391609E39091C33  L_4 = V_0;
		int32_t L_5 = (int32_t)L_4.get_ComponentType_0();
		return (int32_t)L_5;
	}

IL_0027:
	{
		// else if (typeof(T) == typeof(uint))
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_6 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7;
		L_7 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_6, /*hidden argument*/NULL);
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_8 = { reinterpret_cast<intptr_t> (UInt32_tE60352A06233E4E69DD198BCC67142159F686B15_0_0_0_var) };
		Type_t * L_9;
		L_9 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_8, /*hidden argument*/NULL);
		bool L_10;
		L_10 = Type_op_Equality_mA438719A1FDF103C7BBBB08AEF564E7FAEEA0046((Type_t *)L_7, (Type_t *)L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0048;
		}
	}
	{
		// return glComponentType.UNSIGNED_INT;
		return (int32_t)(((int32_t)5125));
	}

IL_0048:
	{
		// else if (typeof(T) == typeof(float))
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_11 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_12;
		L_12 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_11, /*hidden argument*/NULL);
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_13 = { reinterpret_cast<intptr_t> (Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_0_0_0_var) };
		Type_t * L_14;
		L_14 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_13, /*hidden argument*/NULL);
		bool L_15;
		L_15 = Type_op_Equality_mA438719A1FDF103C7BBBB08AEF564E7FAEEA0046((Type_t *)L_12, (Type_t *)L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0069;
		}
	}
	{
		// return glComponentType.FLOAT;
		return (int32_t)(((int32_t)5126));
	}

IL_0069:
	{
		// throw new NotImplementedException(typeof(T).Name);
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_16 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Type_t_il2cpp_TypeInfo_var)));
		Type_t * L_17;
		L_17 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_16, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_17);
		String_t* L_18;
		L_18 = VirtualFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_17);
		NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6 * L_19 = (NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6_il2cpp_TypeInfo_var)));
		NotImplementedException__ctor_m8A9AA4499614A5BC57DD21713D0720630C130AEB(L_19, (String_t*)L_18, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_19, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&glTFExtensions_GetComponentType_TisMatrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461_m2C2A005ACBC154DBAB81F23A91AEA84AC2144C08_RuntimeMethod_var)));
	}
}
// UniGLTF.glComponentType UniGLTF.glTFExtensions::GetComponentType<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t glTFExtensions_GetComponentType_TisRuntimeObject_m401F1575C5C6A2B9B7E42AB3412C6868C2F6F698_gshared (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_TryGetValue_m5453E36663BAE277EFCD153F225808627F9570CD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UInt32_tE60352A06233E4E69DD198BCC67142159F686B15_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	ComponentVec_t7A8C27B684980E106027B1173391609E39091C33  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// var cv = default(ComponentVec);
		il2cpp_codegen_initobj((&V_0), sizeof(ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 ));
		// if (ComponentTypeMap.TryGetValue(typeof(T), out cv))
		IL2CPP_RUNTIME_CLASS_INIT(glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E * L_0 = ((glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_StaticFields*)il2cpp_codegen_static_fields_for(glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var))->get_ComponentTypeMap_0();
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_1 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2;
		L_2 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_1, /*hidden argument*/NULL);
		NullCheck((Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E *)L_0);
		bool L_3;
		L_3 = Dictionary_2_TryGetValue_m5453E36663BAE277EFCD153F225808627F9570CD((Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E *)L_0, (Type_t *)L_2, (ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 *)(ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 *)(&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m5453E36663BAE277EFCD153F225808627F9570CD_RuntimeMethod_var);
		if (!L_3)
		{
			goto IL_0027;
		}
	}
	{
		// return cv.ComponentType;
		ComponentVec_t7A8C27B684980E106027B1173391609E39091C33  L_4 = V_0;
		int32_t L_5 = (int32_t)L_4.get_ComponentType_0();
		return (int32_t)L_5;
	}

IL_0027:
	{
		// else if (typeof(T) == typeof(uint))
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_6 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7;
		L_7 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_6, /*hidden argument*/NULL);
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_8 = { reinterpret_cast<intptr_t> (UInt32_tE60352A06233E4E69DD198BCC67142159F686B15_0_0_0_var) };
		Type_t * L_9;
		L_9 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_8, /*hidden argument*/NULL);
		bool L_10;
		L_10 = Type_op_Equality_mA438719A1FDF103C7BBBB08AEF564E7FAEEA0046((Type_t *)L_7, (Type_t *)L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0048;
		}
	}
	{
		// return glComponentType.UNSIGNED_INT;
		return (int32_t)(((int32_t)5125));
	}

IL_0048:
	{
		// else if (typeof(T) == typeof(float))
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_11 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_12;
		L_12 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_11, /*hidden argument*/NULL);
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_13 = { reinterpret_cast<intptr_t> (Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_0_0_0_var) };
		Type_t * L_14;
		L_14 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_13, /*hidden argument*/NULL);
		bool L_15;
		L_15 = Type_op_Equality_mA438719A1FDF103C7BBBB08AEF564E7FAEEA0046((Type_t *)L_12, (Type_t *)L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0069;
		}
	}
	{
		// return glComponentType.FLOAT;
		return (int32_t)(((int32_t)5126));
	}

IL_0069:
	{
		// throw new NotImplementedException(typeof(T).Name);
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_16 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Type_t_il2cpp_TypeInfo_var)));
		Type_t * L_17;
		L_17 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_16, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_17);
		String_t* L_18;
		L_18 = VirtualFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_17);
		NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6 * L_19 = (NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6_il2cpp_TypeInfo_var)));
		NotImplementedException__ctor_m8A9AA4499614A5BC57DD21713D0720630C130AEB(L_19, (String_t*)L_18, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_19, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&glTFExtensions_GetComponentType_TisRuntimeObject_m401F1575C5C6A2B9B7E42AB3412C6868C2F6F698_RuntimeMethod_var)));
	}
}
// UniGLTF.glComponentType UniGLTF.glTFExtensions::GetComponentType<System.UInt32>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t glTFExtensions_GetComponentType_TisUInt32_tE60352A06233E4E69DD198BCC67142159F686B15_mCB4C70ACCF174504B3EACE9F3249CEDBA3D1FD4A_gshared (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_TryGetValue_m5453E36663BAE277EFCD153F225808627F9570CD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UInt32_tE60352A06233E4E69DD198BCC67142159F686B15_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	ComponentVec_t7A8C27B684980E106027B1173391609E39091C33  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// var cv = default(ComponentVec);
		il2cpp_codegen_initobj((&V_0), sizeof(ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 ));
		// if (ComponentTypeMap.TryGetValue(typeof(T), out cv))
		IL2CPP_RUNTIME_CLASS_INIT(glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E * L_0 = ((glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_StaticFields*)il2cpp_codegen_static_fields_for(glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var))->get_ComponentTypeMap_0();
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_1 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2;
		L_2 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_1, /*hidden argument*/NULL);
		NullCheck((Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E *)L_0);
		bool L_3;
		L_3 = Dictionary_2_TryGetValue_m5453E36663BAE277EFCD153F225808627F9570CD((Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E *)L_0, (Type_t *)L_2, (ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 *)(ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 *)(&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m5453E36663BAE277EFCD153F225808627F9570CD_RuntimeMethod_var);
		if (!L_3)
		{
			goto IL_0027;
		}
	}
	{
		// return cv.ComponentType;
		ComponentVec_t7A8C27B684980E106027B1173391609E39091C33  L_4 = V_0;
		int32_t L_5 = (int32_t)L_4.get_ComponentType_0();
		return (int32_t)L_5;
	}

IL_0027:
	{
		// else if (typeof(T) == typeof(uint))
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_6 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7;
		L_7 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_6, /*hidden argument*/NULL);
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_8 = { reinterpret_cast<intptr_t> (UInt32_tE60352A06233E4E69DD198BCC67142159F686B15_0_0_0_var) };
		Type_t * L_9;
		L_9 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_8, /*hidden argument*/NULL);
		bool L_10;
		L_10 = Type_op_Equality_mA438719A1FDF103C7BBBB08AEF564E7FAEEA0046((Type_t *)L_7, (Type_t *)L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0048;
		}
	}
	{
		// return glComponentType.UNSIGNED_INT;
		return (int32_t)(((int32_t)5125));
	}

IL_0048:
	{
		// else if (typeof(T) == typeof(float))
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_11 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_12;
		L_12 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_11, /*hidden argument*/NULL);
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_13 = { reinterpret_cast<intptr_t> (Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_0_0_0_var) };
		Type_t * L_14;
		L_14 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_13, /*hidden argument*/NULL);
		bool L_15;
		L_15 = Type_op_Equality_mA438719A1FDF103C7BBBB08AEF564E7FAEEA0046((Type_t *)L_12, (Type_t *)L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0069;
		}
	}
	{
		// return glComponentType.FLOAT;
		return (int32_t)(((int32_t)5126));
	}

IL_0069:
	{
		// throw new NotImplementedException(typeof(T).Name);
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_16 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Type_t_il2cpp_TypeInfo_var)));
		Type_t * L_17;
		L_17 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_16, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_17);
		String_t* L_18;
		L_18 = VirtualFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_17);
		NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6 * L_19 = (NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6_il2cpp_TypeInfo_var)));
		NotImplementedException__ctor_m8A9AA4499614A5BC57DD21713D0720630C130AEB(L_19, (String_t*)L_18, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_19, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&glTFExtensions_GetComponentType_TisUInt32_tE60352A06233E4E69DD198BCC67142159F686B15_mCB4C70ACCF174504B3EACE9F3249CEDBA3D1FD4A_RuntimeMethod_var)));
	}
}
// UniGLTF.glComponentType UniGLTF.glTFExtensions::GetComponentType<UniGLTF.UShort4>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t glTFExtensions_GetComponentType_TisUShort4_tB19A4ECD972B8600C9515187BC86B572B83920DB_m09407A885CDDA3F95F5C8AAA85A3677E6F0F760B_gshared (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_TryGetValue_m5453E36663BAE277EFCD153F225808627F9570CD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UInt32_tE60352A06233E4E69DD198BCC67142159F686B15_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	ComponentVec_t7A8C27B684980E106027B1173391609E39091C33  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// var cv = default(ComponentVec);
		il2cpp_codegen_initobj((&V_0), sizeof(ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 ));
		// if (ComponentTypeMap.TryGetValue(typeof(T), out cv))
		IL2CPP_RUNTIME_CLASS_INIT(glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E * L_0 = ((glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_StaticFields*)il2cpp_codegen_static_fields_for(glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var))->get_ComponentTypeMap_0();
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_1 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2;
		L_2 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_1, /*hidden argument*/NULL);
		NullCheck((Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E *)L_0);
		bool L_3;
		L_3 = Dictionary_2_TryGetValue_m5453E36663BAE277EFCD153F225808627F9570CD((Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E *)L_0, (Type_t *)L_2, (ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 *)(ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 *)(&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m5453E36663BAE277EFCD153F225808627F9570CD_RuntimeMethod_var);
		if (!L_3)
		{
			goto IL_0027;
		}
	}
	{
		// return cv.ComponentType;
		ComponentVec_t7A8C27B684980E106027B1173391609E39091C33  L_4 = V_0;
		int32_t L_5 = (int32_t)L_4.get_ComponentType_0();
		return (int32_t)L_5;
	}

IL_0027:
	{
		// else if (typeof(T) == typeof(uint))
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_6 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7;
		L_7 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_6, /*hidden argument*/NULL);
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_8 = { reinterpret_cast<intptr_t> (UInt32_tE60352A06233E4E69DD198BCC67142159F686B15_0_0_0_var) };
		Type_t * L_9;
		L_9 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_8, /*hidden argument*/NULL);
		bool L_10;
		L_10 = Type_op_Equality_mA438719A1FDF103C7BBBB08AEF564E7FAEEA0046((Type_t *)L_7, (Type_t *)L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0048;
		}
	}
	{
		// return glComponentType.UNSIGNED_INT;
		return (int32_t)(((int32_t)5125));
	}

IL_0048:
	{
		// else if (typeof(T) == typeof(float))
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_11 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_12;
		L_12 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_11, /*hidden argument*/NULL);
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_13 = { reinterpret_cast<intptr_t> (Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_0_0_0_var) };
		Type_t * L_14;
		L_14 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_13, /*hidden argument*/NULL);
		bool L_15;
		L_15 = Type_op_Equality_mA438719A1FDF103C7BBBB08AEF564E7FAEEA0046((Type_t *)L_12, (Type_t *)L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0069;
		}
	}
	{
		// return glComponentType.FLOAT;
		return (int32_t)(((int32_t)5126));
	}

IL_0069:
	{
		// throw new NotImplementedException(typeof(T).Name);
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_16 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Type_t_il2cpp_TypeInfo_var)));
		Type_t * L_17;
		L_17 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_16, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_17);
		String_t* L_18;
		L_18 = VirtualFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_17);
		NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6 * L_19 = (NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6_il2cpp_TypeInfo_var)));
		NotImplementedException__ctor_m8A9AA4499614A5BC57DD21713D0720630C130AEB(L_19, (String_t*)L_18, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_19, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&glTFExtensions_GetComponentType_TisUShort4_tB19A4ECD972B8600C9515187BC86B572B83920DB_m09407A885CDDA3F95F5C8AAA85A3677E6F0F760B_RuntimeMethod_var)));
	}
}
// UniGLTF.glComponentType UniGLTF.glTFExtensions::GetComponentType<UnityEngine.Vector2>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t glTFExtensions_GetComponentType_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_mE765928E35770B5DF25B89EDEB0CCB98632364A4_gshared (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_TryGetValue_m5453E36663BAE277EFCD153F225808627F9570CD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UInt32_tE60352A06233E4E69DD198BCC67142159F686B15_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	ComponentVec_t7A8C27B684980E106027B1173391609E39091C33  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// var cv = default(ComponentVec);
		il2cpp_codegen_initobj((&V_0), sizeof(ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 ));
		// if (ComponentTypeMap.TryGetValue(typeof(T), out cv))
		IL2CPP_RUNTIME_CLASS_INIT(glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E * L_0 = ((glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_StaticFields*)il2cpp_codegen_static_fields_for(glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var))->get_ComponentTypeMap_0();
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_1 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2;
		L_2 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_1, /*hidden argument*/NULL);
		NullCheck((Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E *)L_0);
		bool L_3;
		L_3 = Dictionary_2_TryGetValue_m5453E36663BAE277EFCD153F225808627F9570CD((Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E *)L_0, (Type_t *)L_2, (ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 *)(ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 *)(&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m5453E36663BAE277EFCD153F225808627F9570CD_RuntimeMethod_var);
		if (!L_3)
		{
			goto IL_0027;
		}
	}
	{
		// return cv.ComponentType;
		ComponentVec_t7A8C27B684980E106027B1173391609E39091C33  L_4 = V_0;
		int32_t L_5 = (int32_t)L_4.get_ComponentType_0();
		return (int32_t)L_5;
	}

IL_0027:
	{
		// else if (typeof(T) == typeof(uint))
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_6 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7;
		L_7 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_6, /*hidden argument*/NULL);
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_8 = { reinterpret_cast<intptr_t> (UInt32_tE60352A06233E4E69DD198BCC67142159F686B15_0_0_0_var) };
		Type_t * L_9;
		L_9 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_8, /*hidden argument*/NULL);
		bool L_10;
		L_10 = Type_op_Equality_mA438719A1FDF103C7BBBB08AEF564E7FAEEA0046((Type_t *)L_7, (Type_t *)L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0048;
		}
	}
	{
		// return glComponentType.UNSIGNED_INT;
		return (int32_t)(((int32_t)5125));
	}

IL_0048:
	{
		// else if (typeof(T) == typeof(float))
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_11 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_12;
		L_12 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_11, /*hidden argument*/NULL);
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_13 = { reinterpret_cast<intptr_t> (Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_0_0_0_var) };
		Type_t * L_14;
		L_14 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_13, /*hidden argument*/NULL);
		bool L_15;
		L_15 = Type_op_Equality_mA438719A1FDF103C7BBBB08AEF564E7FAEEA0046((Type_t *)L_12, (Type_t *)L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0069;
		}
	}
	{
		// return glComponentType.FLOAT;
		return (int32_t)(((int32_t)5126));
	}

IL_0069:
	{
		// throw new NotImplementedException(typeof(T).Name);
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_16 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Type_t_il2cpp_TypeInfo_var)));
		Type_t * L_17;
		L_17 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_16, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_17);
		String_t* L_18;
		L_18 = VirtualFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_17);
		NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6 * L_19 = (NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6_il2cpp_TypeInfo_var)));
		NotImplementedException__ctor_m8A9AA4499614A5BC57DD21713D0720630C130AEB(L_19, (String_t*)L_18, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_19, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&glTFExtensions_GetComponentType_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_mE765928E35770B5DF25B89EDEB0CCB98632364A4_RuntimeMethod_var)));
	}
}
// UniGLTF.glComponentType UniGLTF.glTFExtensions::GetComponentType<UnityEngine.Vector3>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t glTFExtensions_GetComponentType_TisVector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_mE483B95CF6753D05452AD675D2ADF98800407FCD_gshared (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_TryGetValue_m5453E36663BAE277EFCD153F225808627F9570CD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UInt32_tE60352A06233E4E69DD198BCC67142159F686B15_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	ComponentVec_t7A8C27B684980E106027B1173391609E39091C33  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// var cv = default(ComponentVec);
		il2cpp_codegen_initobj((&V_0), sizeof(ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 ));
		// if (ComponentTypeMap.TryGetValue(typeof(T), out cv))
		IL2CPP_RUNTIME_CLASS_INIT(glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E * L_0 = ((glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_StaticFields*)il2cpp_codegen_static_fields_for(glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var))->get_ComponentTypeMap_0();
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_1 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2;
		L_2 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_1, /*hidden argument*/NULL);
		NullCheck((Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E *)L_0);
		bool L_3;
		L_3 = Dictionary_2_TryGetValue_m5453E36663BAE277EFCD153F225808627F9570CD((Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E *)L_0, (Type_t *)L_2, (ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 *)(ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 *)(&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m5453E36663BAE277EFCD153F225808627F9570CD_RuntimeMethod_var);
		if (!L_3)
		{
			goto IL_0027;
		}
	}
	{
		// return cv.ComponentType;
		ComponentVec_t7A8C27B684980E106027B1173391609E39091C33  L_4 = V_0;
		int32_t L_5 = (int32_t)L_4.get_ComponentType_0();
		return (int32_t)L_5;
	}

IL_0027:
	{
		// else if (typeof(T) == typeof(uint))
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_6 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7;
		L_7 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_6, /*hidden argument*/NULL);
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_8 = { reinterpret_cast<intptr_t> (UInt32_tE60352A06233E4E69DD198BCC67142159F686B15_0_0_0_var) };
		Type_t * L_9;
		L_9 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_8, /*hidden argument*/NULL);
		bool L_10;
		L_10 = Type_op_Equality_mA438719A1FDF103C7BBBB08AEF564E7FAEEA0046((Type_t *)L_7, (Type_t *)L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0048;
		}
	}
	{
		// return glComponentType.UNSIGNED_INT;
		return (int32_t)(((int32_t)5125));
	}

IL_0048:
	{
		// else if (typeof(T) == typeof(float))
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_11 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_12;
		L_12 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_11, /*hidden argument*/NULL);
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_13 = { reinterpret_cast<intptr_t> (Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_0_0_0_var) };
		Type_t * L_14;
		L_14 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_13, /*hidden argument*/NULL);
		bool L_15;
		L_15 = Type_op_Equality_mA438719A1FDF103C7BBBB08AEF564E7FAEEA0046((Type_t *)L_12, (Type_t *)L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0069;
		}
	}
	{
		// return glComponentType.FLOAT;
		return (int32_t)(((int32_t)5126));
	}

IL_0069:
	{
		// throw new NotImplementedException(typeof(T).Name);
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_16 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Type_t_il2cpp_TypeInfo_var)));
		Type_t * L_17;
		L_17 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_16, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_17);
		String_t* L_18;
		L_18 = VirtualFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_17);
		NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6 * L_19 = (NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6_il2cpp_TypeInfo_var)));
		NotImplementedException__ctor_m8A9AA4499614A5BC57DD21713D0720630C130AEB(L_19, (String_t*)L_18, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_19, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&glTFExtensions_GetComponentType_TisVector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_mE483B95CF6753D05452AD675D2ADF98800407FCD_RuntimeMethod_var)));
	}
}
// UniGLTF.glComponentType UniGLTF.glTFExtensions::GetComponentType<UnityEngine.Vector4>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t glTFExtensions_GetComponentType_TisVector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_m196048D2AC77FBEE9F8398F1564F730EBF41A46C_gshared (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_TryGetValue_m5453E36663BAE277EFCD153F225808627F9570CD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UInt32_tE60352A06233E4E69DD198BCC67142159F686B15_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	ComponentVec_t7A8C27B684980E106027B1173391609E39091C33  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// var cv = default(ComponentVec);
		il2cpp_codegen_initobj((&V_0), sizeof(ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 ));
		// if (ComponentTypeMap.TryGetValue(typeof(T), out cv))
		IL2CPP_RUNTIME_CLASS_INIT(glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var);
		Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E * L_0 = ((glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_StaticFields*)il2cpp_codegen_static_fields_for(glTFExtensions_t9AAF17B63D85C9C57DB2BD5F526FDC03F0DEBE8A_il2cpp_TypeInfo_var))->get_ComponentTypeMap_0();
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_1 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2;
		L_2 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_1, /*hidden argument*/NULL);
		NullCheck((Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E *)L_0);
		bool L_3;
		L_3 = Dictionary_2_TryGetValue_m5453E36663BAE277EFCD153F225808627F9570CD((Dictionary_2_t095EED19918A7FFBC069F0D6F70C71862D0DC68E *)L_0, (Type_t *)L_2, (ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 *)(ComponentVec_t7A8C27B684980E106027B1173391609E39091C33 *)(&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m5453E36663BAE277EFCD153F225808627F9570CD_RuntimeMethod_var);
		if (!L_3)
		{
			goto IL_0027;
		}
	}
	{
		// return cv.ComponentType;
		ComponentVec_t7A8C27B684980E106027B1173391609E39091C33  L_4 = V_0;
		int32_t L_5 = (int32_t)L_4.get_ComponentType_0();
		return (int32_t)L_5;
	}

IL_0027:
	{
		// else if (typeof(T) == typeof(uint))
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_6 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7;
		L_7 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_6, /*hidden argument*/NULL);
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_8 = { reinterpret_cast<intptr_t> (UInt32_tE60352A06233E4E69DD198BCC67142159F686B15_0_0_0_var) };
		Type_t * L_9;
		L_9 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_8, /*hidden argument*/NULL);
		bool L_10;
		L_10 = Type_op_Equality_mA438719A1FDF103C7BBBB08AEF564E7FAEEA0046((Type_t *)L_7, (Type_t *)L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0048;
		}
	}
	{
		// return glComponentType.UNSIGNED_INT;
		return (int32_t)(((int32_t)5125));
	}

IL_0048:
	{
		// else if (typeof(T) == typeof(float))
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_11 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_12;
		L_12 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_11, /*hidden argument*/NULL);
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_13 = { reinterpret_cast<intptr_t> (Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_0_0_0_var) };
		Type_t * L_14;
		L_14 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_13, /*hidden argument*/NULL);
		bool L_15;
		L_15 = Type_op_Equality_mA438719A1FDF103C7BBBB08AEF564E7FAEEA0046((Type_t *)L_12, (Type_t *)L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0069;
		}
	}
	{
		// return glComponentType.FLOAT;
		return (int32_t)(((int32_t)5126));
	}

IL_0069:
	{
		// throw new NotImplementedException(typeof(T).Name);
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_16 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Type_t_il2cpp_TypeInfo_var)));
		Type_t * L_17;
		L_17 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_16, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_17);
		String_t* L_18;
		L_18 = VirtualFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_17);
		NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6 * L_19 = (NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotImplementedException_t26260C4EE0444C5FA022994203060B3A42A3ADE6_il2cpp_TypeInfo_var)));
		NotImplementedException__ctor_m8A9AA4499614A5BC57DD21713D0720630C130AEB(L_19, (String_t*)L_18, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_19, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&glTFExtensions_GetComponentType_TisVector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_m196048D2AC77FBEE9F8398F1564F730EBF41A46C_RuntimeMethod_var)));
	}
}
// TValue UnityEngine.InputSystem.InputAction/CallbackContext::ReadValue<UnityEngine.Quaternion>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  CallbackContext_ReadValue_TisQuaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_mE7BC042F12AD2F6B0CC56EFAD37953575750E6C9_gshared (CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B * __this, const RuntimeMethod* method)
{
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// var value = default(TValue);
		il2cpp_codegen_initobj((&V_0), sizeof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 ));
		// if (m_State != null && phase != InputActionPhase.Canceled)
		InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C * L_0 = (InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C *)__this->get_m_State_0();
		if (!L_0)
		{
			goto IL_0032;
		}
	}
	{
		int32_t L_1;
		L_1 = CallbackContext_get_phase_m1AA786B2FBD617BD9061126BF7442F08484F76E7((CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B *)(CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)4)))
		{
			goto IL_0032;
		}
	}
	{
		// value = m_State.ReadValue<TValue>(bindingIndex, controlIndex);
		InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C * L_2 = (InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C *)__this->get_m_State_0();
		int32_t L_3;
		L_3 = CallbackContext_get_bindingIndex_mD26C42B8D97073283D42CB129F00E58AAF63C6EB((CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B *)(CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B *)__this, /*hidden argument*/NULL);
		int32_t L_4;
		L_4 = CallbackContext_get_controlIndex_m56E2618DEAD8E55474756DFDCC261CCE1A4EF776((CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B *)(CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B *)__this, /*hidden argument*/NULL);
		NullCheck((InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C *)L_2);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_5;
		L_5 = ((  Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  (*) (InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C *, int32_t, int32_t, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C *)L_2, (int32_t)L_3, (int32_t)L_4, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_0 = (Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 )L_5;
	}

IL_0032:
	{
		// return value;
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_6 = V_0;
		return (Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 )L_6;
	}
}
IL2CPP_EXTERN_C  Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  CallbackContext_ReadValue_TisQuaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_mE7BC042F12AD2F6B0CC56EFAD37953575750E6C9_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B * _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B *>(__this + _offset);
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  _returnValue;
	_returnValue = CallbackContext_ReadValue_TisQuaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_mE7BC042F12AD2F6B0CC56EFAD37953575750E6C9(_thisAdjusted, method);
	return _returnValue;
}
// TValue UnityEngine.InputSystem.InputAction/CallbackContext::ReadValue<System.Single>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float CallbackContext_ReadValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_m2BFB8C24FFB40E00E51EC8F4DA7CAC936508D419_gshared (CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		// var value = default(TValue);
		il2cpp_codegen_initobj((&V_0), sizeof(float));
		// if (m_State != null && phase != InputActionPhase.Canceled)
		InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C * L_0 = (InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C *)__this->get_m_State_0();
		if (!L_0)
		{
			goto IL_0032;
		}
	}
	{
		int32_t L_1;
		L_1 = CallbackContext_get_phase_m1AA786B2FBD617BD9061126BF7442F08484F76E7((CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B *)(CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)4)))
		{
			goto IL_0032;
		}
	}
	{
		// value = m_State.ReadValue<TValue>(bindingIndex, controlIndex);
		InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C * L_2 = (InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C *)__this->get_m_State_0();
		int32_t L_3;
		L_3 = CallbackContext_get_bindingIndex_mD26C42B8D97073283D42CB129F00E58AAF63C6EB((CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B *)(CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B *)__this, /*hidden argument*/NULL);
		int32_t L_4;
		L_4 = CallbackContext_get_controlIndex_m56E2618DEAD8E55474756DFDCC261CCE1A4EF776((CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B *)(CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B *)__this, /*hidden argument*/NULL);
		NullCheck((InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C *)L_2);
		float L_5;
		L_5 = ((  float (*) (InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C *, int32_t, int32_t, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C *)L_2, (int32_t)L_3, (int32_t)L_4, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_0 = (float)L_5;
	}

IL_0032:
	{
		// return value;
		float L_6 = V_0;
		return (float)L_6;
	}
}
IL2CPP_EXTERN_C  float CallbackContext_ReadValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_m2BFB8C24FFB40E00E51EC8F4DA7CAC936508D419_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B * _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B *>(__this + _offset);
	float _returnValue;
	_returnValue = CallbackContext_ReadValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_m2BFB8C24FFB40E00E51EC8F4DA7CAC936508D419(_thisAdjusted, method);
	return _returnValue;
}
// TValue UnityEngine.InputSystem.InputAction/CallbackContext::ReadValue<UnityEngine.Vector2>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  CallbackContext_ReadValue_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_m5979F5A5F9C48193420CD889B84D010F6373171B_gshared (CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B * __this, const RuntimeMethod* method)
{
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// var value = default(TValue);
		il2cpp_codegen_initobj((&V_0), sizeof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 ));
		// if (m_State != null && phase != InputActionPhase.Canceled)
		InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C * L_0 = (InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C *)__this->get_m_State_0();
		if (!L_0)
		{
			goto IL_0032;
		}
	}
	{
		int32_t L_1;
		L_1 = CallbackContext_get_phase_m1AA786B2FBD617BD9061126BF7442F08484F76E7((CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B *)(CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)4)))
		{
			goto IL_0032;
		}
	}
	{
		// value = m_State.ReadValue<TValue>(bindingIndex, controlIndex);
		InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C * L_2 = (InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C *)__this->get_m_State_0();
		int32_t L_3;
		L_3 = CallbackContext_get_bindingIndex_mD26C42B8D97073283D42CB129F00E58AAF63C6EB((CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B *)(CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B *)__this, /*hidden argument*/NULL);
		int32_t L_4;
		L_4 = CallbackContext_get_controlIndex_m56E2618DEAD8E55474756DFDCC261CCE1A4EF776((CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B *)(CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B *)__this, /*hidden argument*/NULL);
		NullCheck((InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C *)L_2);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_5;
		L_5 = ((  Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  (*) (InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C *, int32_t, int32_t, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C *)L_2, (int32_t)L_3, (int32_t)L_4, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_0 = (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 )L_5;
	}

IL_0032:
	{
		// return value;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_6 = V_0;
		return (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 )L_6;
	}
}
IL2CPP_EXTERN_C  Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  CallbackContext_ReadValue_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_m5979F5A5F9C48193420CD889B84D010F6373171B_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B * _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B *>(__this + _offset);
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  _returnValue;
	_returnValue = CallbackContext_ReadValue_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_m5979F5A5F9C48193420CD889B84D010F6373171B(_thisAdjusted, method);
	return _returnValue;
}
// TValue UnityEngine.InputSystem.InputAction/CallbackContext::ReadValue<UnityEngine.Vector3>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  CallbackContext_ReadValue_TisVector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_m76A2A215A8ACD5B96AAAD484DD041DA960A11B81_gshared (CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B * __this, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// var value = default(TValue);
		il2cpp_codegen_initobj((&V_0), sizeof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E ));
		// if (m_State != null && phase != InputActionPhase.Canceled)
		InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C * L_0 = (InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C *)__this->get_m_State_0();
		if (!L_0)
		{
			goto IL_0032;
		}
	}
	{
		int32_t L_1;
		L_1 = CallbackContext_get_phase_m1AA786B2FBD617BD9061126BF7442F08484F76E7((CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B *)(CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)4)))
		{
			goto IL_0032;
		}
	}
	{
		// value = m_State.ReadValue<TValue>(bindingIndex, controlIndex);
		InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C * L_2 = (InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C *)__this->get_m_State_0();
		int32_t L_3;
		L_3 = CallbackContext_get_bindingIndex_mD26C42B8D97073283D42CB129F00E58AAF63C6EB((CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B *)(CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B *)__this, /*hidden argument*/NULL);
		int32_t L_4;
		L_4 = CallbackContext_get_controlIndex_m56E2618DEAD8E55474756DFDCC261CCE1A4EF776((CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B *)(CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B *)__this, /*hidden argument*/NULL);
		NullCheck((InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C *)L_2);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5;
		L_5 = ((  Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  (*) (InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C *, int32_t, int32_t, bool, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((InputActionState_tE8F8D2439CDB9528D77707FB251FBE280C34664C *)L_2, (int32_t)L_3, (int32_t)L_4, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_0 = (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E )L_5;
	}

IL_0032:
	{
		// return value;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = V_0;
		return (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E )L_6;
	}
}
IL2CPP_EXTERN_C  Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  CallbackContext_ReadValue_TisVector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_m76A2A215A8ACD5B96AAAD484DD041DA960A11B81_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B * _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<CallbackContext_tFAD329A5B98475EEDBD898FFDA3378C44BC12E0B *>(__this + _offset);
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  _returnValue;
	_returnValue = CallbackContext_ReadValue_TisVector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_m76A2A215A8ACD5B96AAAD484DD041DA960A11B81(_thisAdjusted, method);
	return _returnValue;
}
// UnityEngine.InputSystem.InputActionRebindingExtensions/RebindingOperation UnityEngine.InputSystem.InputActionRebindingExtensions/RebindingOperation::WithExpectedControlType<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RebindingOperation_t5DA0F3263991E7BED360B8A1ADE014B9B451F970 * RebindingOperation_WithExpectedControlType_TisRuntimeObject_m04CD217329B40A5260B4520D5F17E7A9E04CC927_gshared (RebindingOperation_t5DA0F3263991E7BED360B8A1ADE014B9B451F970 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// ThrowIfRebindInProgress();
		NullCheck((RebindingOperation_t5DA0F3263991E7BED360B8A1ADE014B9B451F970 *)__this);
		RebindingOperation_ThrowIfRebindInProgress_m28D12CF785947DD24FACB7DAE3E7B887C03F4908((RebindingOperation_t5DA0F3263991E7BED360B8A1ADE014B9B451F970 *)__this, /*hidden argument*/NULL);
		// return WithExpectedControlType(typeof(TControl));
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_0 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1;
		L_1 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_0, /*hidden argument*/NULL);
		NullCheck((RebindingOperation_t5DA0F3263991E7BED360B8A1ADE014B9B451F970 *)__this);
		RebindingOperation_t5DA0F3263991E7BED360B8A1ADE014B9B451F970 * L_2;
		L_2 = RebindingOperation_WithExpectedControlType_m705AE6A47C8C1CA4F3E240985B5D1B96A8AAD012((RebindingOperation_t5DA0F3263991E7BED360B8A1ADE014B9B451F970 *)__this, (Type_t *)L_1, /*hidden argument*/NULL);
		return (RebindingOperation_t5DA0F3263991E7BED360B8A1ADE014B9B451F970 *)L_2;
	}
}
// UnityEngine.InputSystem.InputActionSetupExtensions/BindingSyntax UnityEngine.InputSystem.InputActionSetupExtensions/BindingSyntax::WithInteraction<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR BindingSyntax_tEF6F07812AAB1AF06B3D36C9B1570C01997C8BF1  BindingSyntax_WithInteraction_TisRuntimeObject_mF004EAD1BEFB97AAECF7B3AD3C9856304B302EDC_gshared (BindingSyntax_tEF6F07812AAB1AF06B3D36C9B1570C01997C8BF1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InputProcessor_tAC24832406A406286CE4B162D9D2EF5E2E08765A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	InternedString_tD1138602E8B7EA0F5B4851812B13C7E0551E25C8  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// var interactionName = InputProcessor.s_Processors.FindNameForType(typeof(TInteraction));
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_0 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1;
		L_1 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_0, /*hidden argument*/NULL);
		InternedString_tD1138602E8B7EA0F5B4851812B13C7E0551E25C8  L_2;
		L_2 = TypeTable_FindNameForType_m70022A55F8ADA2D7F33B9262F7F926145B4B6E4D((TypeTable_tE58155618A277C8EB10C9B8FB3040A6731E46BFA *)(TypeTable_tE58155618A277C8EB10C9B8FB3040A6731E46BFA *)(((InputProcessor_tAC24832406A406286CE4B162D9D2EF5E2E08765A_StaticFields*)il2cpp_codegen_static_fields_for(InputProcessor_tAC24832406A406286CE4B162D9D2EF5E2E08765A_il2cpp_TypeInfo_var))->get_address_of_s_Processors_0()), (Type_t *)L_1, /*hidden argument*/NULL);
		V_0 = (InternedString_tD1138602E8B7EA0F5B4851812B13C7E0551E25C8 )L_2;
		// if (interactionName.IsEmpty())
		bool L_3;
		L_3 = InternedString_IsEmpty_mD8617A5FDADD3E05A5F6253D42EFCB8BDAD11EE8((InternedString_tD1138602E8B7EA0F5B4851812B13C7E0551E25C8 *)(InternedString_tD1138602E8B7EA0F5B4851812B13C7E0551E25C8 *)(&V_0), /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0038;
		}
	}
	{
		// throw new NotSupportedException($"Type '{typeof(TInteraction)}' has not been registered as a processor");
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_4 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Type_t_il2cpp_TypeInfo_var)));
		Type_t * L_5;
		L_5 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_4, /*hidden argument*/NULL);
		String_t* L_6;
		L_6 = String_Format_mB3D38E5238C3164DB4D7D29339D9E225A4496D17((String_t*)((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralAFCE96C2E9CB5FEF65576BADEA096873577F2BF6)), (RuntimeObject *)L_5, /*hidden argument*/NULL);
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_7 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m40BC57BDA6E0E119B73700CC809A14B57DC65A90(L_7, (String_t*)L_6, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&BindingSyntax_WithInteraction_TisRuntimeObject_mF004EAD1BEFB97AAECF7B3AD3C9856304B302EDC_RuntimeMethod_var)));
	}

IL_0038:
	{
		// return WithInteraction(interactionName);
		InternedString_tD1138602E8B7EA0F5B4851812B13C7E0551E25C8  L_8 = V_0;
		String_t* L_9;
		L_9 = InternedString_op_Implicit_mFA680218C45365E7ED57EA5EAA6ED59F15D8DFC6((InternedString_tD1138602E8B7EA0F5B4851812B13C7E0551E25C8 )L_8, /*hidden argument*/NULL);
		BindingSyntax_tEF6F07812AAB1AF06B3D36C9B1570C01997C8BF1  L_10;
		L_10 = BindingSyntax_WithInteraction_m5B7B398C4193415876B4C31BAFDCDD9EE5107576((BindingSyntax_tEF6F07812AAB1AF06B3D36C9B1570C01997C8BF1 *)(BindingSyntax_tEF6F07812AAB1AF06B3D36C9B1570C01997C8BF1 *)__this, (String_t*)L_9, /*hidden argument*/NULL);
		return (BindingSyntax_tEF6F07812AAB1AF06B3D36C9B1570C01997C8BF1 )L_10;
	}
}
IL2CPP_EXTERN_C  BindingSyntax_tEF6F07812AAB1AF06B3D36C9B1570C01997C8BF1  BindingSyntax_WithInteraction_TisRuntimeObject_mF004EAD1BEFB97AAECF7B3AD3C9856304B302EDC_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	BindingSyntax_tEF6F07812AAB1AF06B3D36C9B1570C01997C8BF1 * _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<BindingSyntax_tEF6F07812AAB1AF06B3D36C9B1570C01997C8BF1 *>(__this + _offset);
	BindingSyntax_tEF6F07812AAB1AF06B3D36C9B1570C01997C8BF1  _returnValue;
	_returnValue = BindingSyntax_WithInteraction_TisRuntimeObject_mF004EAD1BEFB97AAECF7B3AD3C9856304B302EDC(_thisAdjusted, method);
	return _returnValue;
}
// UnityEngine.InputSystem.InputActionSetupExtensions/BindingSyntax UnityEngine.InputSystem.InputActionSetupExtensions/BindingSyntax::WithProcessor<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR BindingSyntax_tEF6F07812AAB1AF06B3D36C9B1570C01997C8BF1  BindingSyntax_WithProcessor_TisRuntimeObject_mE7F31DB067FE1D0D8B6CAE2F3A869BB60B4E15ED_gshared (BindingSyntax_tEF6F07812AAB1AF06B3D36C9B1570C01997C8BF1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InputProcessor_tAC24832406A406286CE4B162D9D2EF5E2E08765A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	InternedString_tD1138602E8B7EA0F5B4851812B13C7E0551E25C8  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// var processorName = InputProcessor.s_Processors.FindNameForType(typeof(TProcessor));
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_0 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1;
		L_1 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_0, /*hidden argument*/NULL);
		InternedString_tD1138602E8B7EA0F5B4851812B13C7E0551E25C8  L_2;
		L_2 = TypeTable_FindNameForType_m70022A55F8ADA2D7F33B9262F7F926145B4B6E4D((TypeTable_tE58155618A277C8EB10C9B8FB3040A6731E46BFA *)(TypeTable_tE58155618A277C8EB10C9B8FB3040A6731E46BFA *)(((InputProcessor_tAC24832406A406286CE4B162D9D2EF5E2E08765A_StaticFields*)il2cpp_codegen_static_fields_for(InputProcessor_tAC24832406A406286CE4B162D9D2EF5E2E08765A_il2cpp_TypeInfo_var))->get_address_of_s_Processors_0()), (Type_t *)L_1, /*hidden argument*/NULL);
		V_0 = (InternedString_tD1138602E8B7EA0F5B4851812B13C7E0551E25C8 )L_2;
		// if (processorName.IsEmpty())
		bool L_3;
		L_3 = InternedString_IsEmpty_mD8617A5FDADD3E05A5F6253D42EFCB8BDAD11EE8((InternedString_tD1138602E8B7EA0F5B4851812B13C7E0551E25C8 *)(InternedString_tD1138602E8B7EA0F5B4851812B13C7E0551E25C8 *)(&V_0), /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0038;
		}
	}
	{
		// throw new NotSupportedException($"Type '{typeof(TProcessor)}' has not been registered as a processor");
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_4 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Type_t_il2cpp_TypeInfo_var)));
		Type_t * L_5;
		L_5 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_4, /*hidden argument*/NULL);
		String_t* L_6;
		L_6 = String_Format_mB3D38E5238C3164DB4D7D29339D9E225A4496D17((String_t*)((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralAFCE96C2E9CB5FEF65576BADEA096873577F2BF6)), (RuntimeObject *)L_5, /*hidden argument*/NULL);
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_7 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m40BC57BDA6E0E119B73700CC809A14B57DC65A90(L_7, (String_t*)L_6, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&BindingSyntax_WithProcessor_TisRuntimeObject_mE7F31DB067FE1D0D8B6CAE2F3A869BB60B4E15ED_RuntimeMethod_var)));
	}

IL_0038:
	{
		// return WithProcessor(processorName);
		InternedString_tD1138602E8B7EA0F5B4851812B13C7E0551E25C8  L_8 = V_0;
		String_t* L_9;
		L_9 = InternedString_op_Implicit_mFA680218C45365E7ED57EA5EAA6ED59F15D8DFC6((InternedString_tD1138602E8B7EA0F5B4851812B13C7E0551E25C8 )L_8, /*hidden argument*/NULL);
		BindingSyntax_tEF6F07812AAB1AF06B3D36C9B1570C01997C8BF1  L_10;
		L_10 = BindingSyntax_WithProcessor_m0A13729474D2C44C0646703F5543B891132D6484((BindingSyntax_tEF6F07812AAB1AF06B3D36C9B1570C01997C8BF1 *)(BindingSyntax_tEF6F07812AAB1AF06B3D36C9B1570C01997C8BF1 *)__this, (String_t*)L_9, /*hidden argument*/NULL);
		return (BindingSyntax_tEF6F07812AAB1AF06B3D36C9B1570C01997C8BF1 )L_10;
	}
}
IL2CPP_EXTERN_C  BindingSyntax_tEF6F07812AAB1AF06B3D36C9B1570C01997C8BF1  BindingSyntax_WithProcessor_TisRuntimeObject_mE7F31DB067FE1D0D8B6CAE2F3A869BB60B4E15ED_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	BindingSyntax_tEF6F07812AAB1AF06B3D36C9B1570C01997C8BF1 * _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<BindingSyntax_tEF6F07812AAB1AF06B3D36C9B1570C01997C8BF1 *>(__this + _offset);
	BindingSyntax_tEF6F07812AAB1AF06B3D36C9B1570C01997C8BF1  _returnValue;
	_returnValue = BindingSyntax_WithProcessor_TisRuntimeObject_mE7F31DB067FE1D0D8B6CAE2F3A869BB60B4E15ED(_thisAdjusted, method);
	return _returnValue;
}
// System.String UnityEngine.InputSystem.InputActionSetupExtensions/ControlSchemeSyntax::DeviceTypeToControlPath<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* ControlSchemeSyntax_DeviceTypeToControlPath_TisRuntimeObject_mB700493B60AC1EDE0CA6A7C5B54091F0B1343D4F_gshared (ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InputControlLayout_t012E8BC3C93CE8A2A35BFC6A70C52530FFB55917_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral725B1CAFF9B49E1231FDA15B85166BBEFAA36A11);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEF8AE9E6CBCFDABA932FBEB4C85964F450F724F5);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	InternedString_tD1138602E8B7EA0F5B4851812B13C7E0551E25C8  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		// var layoutName = InputControlLayout.s_Layouts.TryFindLayoutForType(typeof(TDevice)).ToString();
		IL2CPP_RUNTIME_CLASS_INIT(InputControlLayout_t012E8BC3C93CE8A2A35BFC6A70C52530FFB55917_il2cpp_TypeInfo_var);
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_0 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1;
		L_1 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_0, /*hidden argument*/NULL);
		InternedString_tD1138602E8B7EA0F5B4851812B13C7E0551E25C8  L_2;
		L_2 = Collection_TryFindLayoutForType_mA1BA2AB384A930262B5B405EBA6136861A0D6A5E((Collection_tDBA0BC36ACE448001ABFFED32EF785467D714D7B *)(Collection_tDBA0BC36ACE448001ABFFED32EF785467D714D7B *)(((InputControlLayout_t012E8BC3C93CE8A2A35BFC6A70C52530FFB55917_StaticFields*)il2cpp_codegen_static_fields_for(InputControlLayout_t012E8BC3C93CE8A2A35BFC6A70C52530FFB55917_il2cpp_TypeInfo_var))->get_address_of_s_Layouts_15()), (Type_t *)L_1, /*hidden argument*/NULL);
		V_1 = (InternedString_tD1138602E8B7EA0F5B4851812B13C7E0551E25C8 )L_2;
		String_t* L_3;
		L_3 = InternedString_ToString_m2FF8E64B30FADEBCC623E0EFF944C3EDA244F6D7((InternedString_tD1138602E8B7EA0F5B4851812B13C7E0551E25C8 *)(InternedString_tD1138602E8B7EA0F5B4851812B13C7E0551E25C8 *)(&V_1), /*hidden argument*/NULL);
		V_0 = (String_t*)L_3;
		// if (string.IsNullOrEmpty(layoutName))
		String_t* L_4 = V_0;
		bool L_5;
		L_5 = String_IsNullOrEmpty_m9AFBB5335B441B94E884B8A9D4A27AD60E3D7F7C((String_t*)L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003b;
		}
	}
	{
		// layoutName = typeof(TDevice).Name;
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_6 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7;
		L_7 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_6, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_7);
		String_t* L_8;
		L_8 = VirtualFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_7);
		V_0 = (String_t*)L_8;
	}

IL_003b:
	{
		// return $"<{layoutName}>";
		String_t* L_9 = V_0;
		String_t* L_10;
		L_10 = String_Concat_m89EAB4C6A96B0E5C3F87300D6BE78D386B9EFC44((String_t*)_stringLiteral725B1CAFF9B49E1231FDA15B85166BBEFAA36A11, (String_t*)L_9, (String_t*)_stringLiteralEF8AE9E6CBCFDABA932FBEB4C85964F450F724F5, /*hidden argument*/NULL);
		return (String_t*)L_10;
	}
}
IL2CPP_EXTERN_C  String_t* ControlSchemeSyntax_DeviceTypeToControlPath_TisRuntimeObject_mB700493B60AC1EDE0CA6A7C5B54091F0B1343D4F_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214 * _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214 *>(__this + _offset);
	String_t* _returnValue;
	_returnValue = ControlSchemeSyntax_DeviceTypeToControlPath_TisRuntimeObject_mB700493B60AC1EDE0CA6A7C5B54091F0B1343D4F(_thisAdjusted, method);
	return _returnValue;
}
// UnityEngine.InputSystem.InputActionSetupExtensions/ControlSchemeSyntax UnityEngine.InputSystem.InputActionSetupExtensions/ControlSchemeSyntax::OrWithOptionalDevice<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214  ControlSchemeSyntax_OrWithOptionalDevice_TisRuntimeObject_mA966529367FEAF671DF7725154538CB7312C1D2F_gshared (ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214 * __this, const RuntimeMethod* method)
{
	{
		// return WithOptionalDevice(DeviceTypeToControlPath<TDevice>());
		String_t* L_0;
		L_0 = ControlSchemeSyntax_DeviceTypeToControlPath_TisRuntimeObject_mB700493B60AC1EDE0CA6A7C5B54091F0B1343D4F((ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214 *)(ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214  L_1;
		L_1 = ControlSchemeSyntax_WithOptionalDevice_m1EEB8035CC62D2A78AC932A7CA0440AF94126C39((ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214 *)(ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214 *)__this, (String_t*)L_0, /*hidden argument*/NULL);
		return (ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214 )L_1;
	}
}
IL2CPP_EXTERN_C  ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214  ControlSchemeSyntax_OrWithOptionalDevice_TisRuntimeObject_mA966529367FEAF671DF7725154538CB7312C1D2F_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214 * _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214 *>(__this + _offset);
	ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214  _returnValue;
	_returnValue = ControlSchemeSyntax_OrWithOptionalDevice_TisRuntimeObject_mA966529367FEAF671DF7725154538CB7312C1D2F(_thisAdjusted, method);
	return _returnValue;
}
// UnityEngine.InputSystem.InputActionSetupExtensions/ControlSchemeSyntax UnityEngine.InputSystem.InputActionSetupExtensions/ControlSchemeSyntax::OrWithRequiredDevice<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214  ControlSchemeSyntax_OrWithRequiredDevice_TisRuntimeObject_m0F28425B0D706D1C1C43B7A30024ACBFC00A3518_gshared (ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214 * __this, const RuntimeMethod* method)
{
	{
		// return WithRequiredDevice(DeviceTypeToControlPath<TDevice>());
		String_t* L_0;
		L_0 = ControlSchemeSyntax_DeviceTypeToControlPath_TisRuntimeObject_mB700493B60AC1EDE0CA6A7C5B54091F0B1343D4F((ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214 *)(ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214  L_1;
		L_1 = ControlSchemeSyntax_WithRequiredDevice_m7D612CC028141089F3AB419967A9A7F1D9FC3353((ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214 *)(ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214 *)__this, (String_t*)L_0, /*hidden argument*/NULL);
		return (ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214 )L_1;
	}
}
IL2CPP_EXTERN_C  ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214  ControlSchemeSyntax_OrWithRequiredDevice_TisRuntimeObject_m0F28425B0D706D1C1C43B7A30024ACBFC00A3518_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214 * _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214 *>(__this + _offset);
	ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214  _returnValue;
	_returnValue = ControlSchemeSyntax_OrWithRequiredDevice_TisRuntimeObject_m0F28425B0D706D1C1C43B7A30024ACBFC00A3518(_thisAdjusted, method);
	return _returnValue;
}
// UnityEngine.InputSystem.InputActionSetupExtensions/ControlSchemeSyntax UnityEngine.InputSystem.InputActionSetupExtensions/ControlSchemeSyntax::WithOptionalDevice<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214  ControlSchemeSyntax_WithOptionalDevice_TisRuntimeObject_mEEDD90A8BFEDAC21FF0B67F8F3BC50208F09D015_gshared (ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214 * __this, const RuntimeMethod* method)
{
	{
		// return WithOptionalDevice(DeviceTypeToControlPath<TDevice>());
		String_t* L_0;
		L_0 = ControlSchemeSyntax_DeviceTypeToControlPath_TisRuntimeObject_mB700493B60AC1EDE0CA6A7C5B54091F0B1343D4F((ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214 *)(ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214  L_1;
		L_1 = ControlSchemeSyntax_WithOptionalDevice_m1EEB8035CC62D2A78AC932A7CA0440AF94126C39((ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214 *)(ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214 *)__this, (String_t*)L_0, /*hidden argument*/NULL);
		return (ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214 )L_1;
	}
}
IL2CPP_EXTERN_C  ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214  ControlSchemeSyntax_WithOptionalDevice_TisRuntimeObject_mEEDD90A8BFEDAC21FF0B67F8F3BC50208F09D015_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214 * _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214 *>(__this + _offset);
	ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214  _returnValue;
	_returnValue = ControlSchemeSyntax_WithOptionalDevice_TisRuntimeObject_mEEDD90A8BFEDAC21FF0B67F8F3BC50208F09D015(_thisAdjusted, method);
	return _returnValue;
}
// UnityEngine.InputSystem.InputActionSetupExtensions/ControlSchemeSyntax UnityEngine.InputSystem.InputActionSetupExtensions/ControlSchemeSyntax::WithRequiredDevice<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214  ControlSchemeSyntax_WithRequiredDevice_TisRuntimeObject_mC0CB0D601DCB446ACF2E02BAE15CDAAD58DAED10_gshared (ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214 * __this, const RuntimeMethod* method)
{
	{
		// return WithRequiredDevice(DeviceTypeToControlPath<TDevice>());
		String_t* L_0;
		L_0 = ControlSchemeSyntax_DeviceTypeToControlPath_TisRuntimeObject_mB700493B60AC1EDE0CA6A7C5B54091F0B1343D4F((ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214 *)(ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214  L_1;
		L_1 = ControlSchemeSyntax_WithRequiredDevice_m7D612CC028141089F3AB419967A9A7F1D9FC3353((ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214 *)(ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214 *)__this, (String_t*)L_0, /*hidden argument*/NULL);
		return (ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214 )L_1;
	}
}
IL2CPP_EXTERN_C  ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214  ControlSchemeSyntax_WithRequiredDevice_TisRuntimeObject_mC0CB0D601DCB446ACF2E02BAE15CDAAD58DAED10_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214 * _thisAdjusted;
	int32_t _offset = 1;
	_thisAdjusted = reinterpret_cast<ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214 *>(__this + _offset);
	ControlSchemeSyntax_t8268F41B7394AFA87007DA20E920E39A975DA214  _returnValue;
	_returnValue = ControlSchemeSyntax_WithRequiredDevice_TisRuntimeObject_mC0CB0D601DCB446ACF2E02BAE15CDAAD58DAED10(_thisAdjusted, method);
	return _returnValue;
}
// UnityEngine.InputSystem.Layouts.InputControlLayout/Builder UnityEngine.InputSystem.Layouts.InputControlLayout/Builder::WithType<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Builder_t4C196179CE57554255EBFB4ECD4D6499ADA974FB * Builder_WithType_TisRuntimeObject_m83FE280551024B58C685344F4FDB38F91DA78443_gshared (Builder_t4C196179CE57554255EBFB4ECD4D6499ADA974FB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// type = typeof(T);
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_0 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1;
		L_1 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_0, /*hidden argument*/NULL);
		NullCheck((Builder_t4C196179CE57554255EBFB4ECD4D6499ADA974FB *)__this);
		Builder_set_type_m31E443E368751EC60EBA1A39D670313269E50B74_inline((Builder_t4C196179CE57554255EBFB4ECD4D6499ADA974FB *)__this, (Type_t *)L_1, /*hidden argument*/NULL);
		// return this;
		return (Builder_t4C196179CE57554255EBFB4ECD4D6499ADA974FB *)__this;
	}
}
// System.Linq.Expressions.Expression System.Linq.Expressions.Interpreter.LightCompiler/QuoteVisitor::VisitLambda<System.Object>(System.Linq.Expressions.Expression`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Expression_t30A004209C10C2D9A9785B2F74EEED431A4D4660 * QuoteVisitor_VisitLambda_TisRuntimeObject_m22B88C45A2F1E492CDBEC9231A4A49CCC6867901_gshared (QuoteVisitor_tFE404B4C826642C3FC245A108AEC9E94D691E627 * __this, Expression_1_t01B093F396848A065BE827B0C58E6F20E760FB6F * ___node0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Array_Empty_TisParameterExpression_tA7B24F1DE0F013DA4BD55F76DB43B06DB33D8BEE_m5F471CFF3314A2643178F228536765874B6A83EF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m0B405996905BBAFA7ED5939184F2344559D3400C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mE4C9B3F15E5D5168479F4E7227A000B97C871A30_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_tCDDF33E8793E2DD752E38CC326B13F8F35B1493B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	int32_t V_1 = 0;
	List_1_tCDDF33E8793E2DD752E38CC326B13F8F35B1493B * V_2 = NULL;
	int32_t V_3 = 0;
	{
		ParameterExpressionU5BU5D_tCF3EAA6D8556513C4937E1126F65AA08DF4DB147* L_0;
		L_0 = Array_Empty_TisParameterExpression_tA7B24F1DE0F013DA4BD55F76DB43B06DB33D8BEE_m5F471CFF3314A2643178F228536765874B6A83EF_inline(/*hidden argument*/Array_Empty_TisParameterExpression_tA7B24F1DE0F013DA4BD55F76DB43B06DB33D8BEE_m5F471CFF3314A2643178F228536765874B6A83EF_RuntimeMethod_var);
		V_0 = (RuntimeObject*)L_0;
		Expression_1_t01B093F396848A065BE827B0C58E6F20E760FB6F * L_1 = ___node0;
		NullCheck((LambdaExpression_t26BF905E97E6D94F81F17D60F4F4F47F8E93B474 *)L_1);
		int32_t L_2;
		L_2 = VirtualFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Linq.Expressions.LambdaExpression::get_ParameterCount() */, (LambdaExpression_t26BF905E97E6D94F81F17D60F4F4F47F8E93B474 *)L_1);
		V_1 = (int32_t)L_2;
		int32_t L_3 = V_1;
		if ((((int32_t)L_3) <= ((int32_t)0)))
		{
			goto IL_0033;
		}
	}
	{
		int32_t L_4 = V_1;
		List_1_tCDDF33E8793E2DD752E38CC326B13F8F35B1493B * L_5 = (List_1_tCDDF33E8793E2DD752E38CC326B13F8F35B1493B *)il2cpp_codegen_object_new(List_1_tCDDF33E8793E2DD752E38CC326B13F8F35B1493B_il2cpp_TypeInfo_var);
		List_1__ctor_mE4C9B3F15E5D5168479F4E7227A000B97C871A30(L_5, (int32_t)L_4, /*hidden argument*/List_1__ctor_mE4C9B3F15E5D5168479F4E7227A000B97C871A30_RuntimeMethod_var);
		V_2 = (List_1_tCDDF33E8793E2DD752E38CC326B13F8F35B1493B *)L_5;
		V_3 = (int32_t)0;
		goto IL_002d;
	}

IL_001c:
	{
		List_1_tCDDF33E8793E2DD752E38CC326B13F8F35B1493B * L_6 = V_2;
		Expression_1_t01B093F396848A065BE827B0C58E6F20E760FB6F * L_7 = ___node0;
		int32_t L_8 = V_3;
		NullCheck((LambdaExpression_t26BF905E97E6D94F81F17D60F4F4F47F8E93B474 *)L_7);
		ParameterExpression_tA7B24F1DE0F013DA4BD55F76DB43B06DB33D8BEE * L_9;
		L_9 = VirtualFuncInvoker1< ParameterExpression_tA7B24F1DE0F013DA4BD55F76DB43B06DB33D8BEE *, int32_t >::Invoke(16 /* System.Linq.Expressions.ParameterExpression System.Linq.Expressions.LambdaExpression::GetParameter(System.Int32) */, (LambdaExpression_t26BF905E97E6D94F81F17D60F4F4F47F8E93B474 *)L_7, (int32_t)L_8);
		NullCheck((List_1_tCDDF33E8793E2DD752E38CC326B13F8F35B1493B *)L_6);
		List_1_Add_m0B405996905BBAFA7ED5939184F2344559D3400C((List_1_tCDDF33E8793E2DD752E38CC326B13F8F35B1493B *)L_6, (ParameterExpression_tA7B24F1DE0F013DA4BD55F76DB43B06DB33D8BEE *)L_9, /*hidden argument*/List_1_Add_m0B405996905BBAFA7ED5939184F2344559D3400C_RuntimeMethod_var);
		int32_t L_10 = V_3;
		V_3 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
	}

IL_002d:
	{
		int32_t L_11 = V_3;
		int32_t L_12 = V_1;
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_001c;
		}
	}
	{
		List_1_tCDDF33E8793E2DD752E38CC326B13F8F35B1493B * L_13 = V_2;
		V_0 = (RuntimeObject*)L_13;
	}

IL_0033:
	{
		RuntimeObject* L_14 = V_0;
		NullCheck((QuoteVisitor_tFE404B4C826642C3FC245A108AEC9E94D691E627 *)__this);
		QuoteVisitor_PushParameters_m7AAC447E0627A0AD1A5EBAB7A7AFFD5F239CC0ED((QuoteVisitor_tFE404B4C826642C3FC245A108AEC9E94D691E627 *)__this, (RuntimeObject*)L_14, /*hidden argument*/NULL);
		Expression_1_t01B093F396848A065BE827B0C58E6F20E760FB6F * L_15 = ___node0;
		NullCheck((ExpressionVisitor_tD098DE8A366FBBB58C498C4EFF8B003FCA726DF4 *)__this);
		Expression_t30A004209C10C2D9A9785B2F74EEED431A4D4660 * L_16;
		L_16 = ((  Expression_t30A004209C10C2D9A9785B2F74EEED431A4D4660 * (*) (ExpressionVisitor_tD098DE8A366FBBB58C498C4EFF8B003FCA726DF4 *, Expression_1_t01B093F396848A065BE827B0C58E6F20E760FB6F *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)((ExpressionVisitor_tD098DE8A366FBBB58C498C4EFF8B003FCA726DF4 *)__this, (Expression_1_t01B093F396848A065BE827B0C58E6F20E760FB6F *)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		RuntimeObject* L_17 = V_0;
		NullCheck((QuoteVisitor_tFE404B4C826642C3FC245A108AEC9E94D691E627 *)__this);
		QuoteVisitor_PopParameters_m88C9C499B152E0333072D63BFD2908F2D710495A((QuoteVisitor_tFE404B4C826642C3FC245A108AEC9E94D691E627 *)__this, (RuntimeObject*)L_17, /*hidden argument*/NULL);
		Expression_1_t01B093F396848A065BE827B0C58E6F20E760FB6F * L_18 = ___node0;
		return (Expression_t30A004209C10C2D9A9785B2F74EEED431A4D4660 *)L_18;
	}
}
// System.Linq.Expressions.Expression System.Linq.Expressions.Interpreter.QuoteInstruction/ExpressionQuoter::VisitLambda<System.Object>(System.Linq.Expressions.Expression`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Expression_t30A004209C10C2D9A9785B2F74EEED431A4D4660 * ExpressionQuoter_VisitLambda_TisRuntimeObject_mCF1EE6E7FA8600F2A3EB1E318A1BD31337F453E5_gshared (ExpressionQuoter_t174D328A07E522775BA6B19ADF09DBEAF13098FE * __this, Expression_1_t01B093F396848A065BE827B0C58E6F20E760FB6F * ___node0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&HashSet_1_Add_m9CF730E2C32C0613D8089427318C09E55C350DBE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&HashSet_1__ctor_m7E015D0E7832B3967403CAEE703C819D77AE741B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&HashSet_1_t42A3AC337CA15FAC250AA5DA438F909806C72CB0_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Stack_1_Pop_m1B41FE87FFACE09E92B93079328100A46D7A9550_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Stack_1_Push_m40AB4A635E6FAFC5491A227935BF4EB1D070C8E1_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	Expression_t30A004209C10C2D9A9785B2F74EEED431A4D4660 * V_0 = NULL;
	HashSet_1_t42A3AC337CA15FAC250AA5DA438F909806C72CB0 * V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		Expression_1_t01B093F396848A065BE827B0C58E6F20E760FB6F * L_0 = ___node0;
		NullCheck((LambdaExpression_t26BF905E97E6D94F81F17D60F4F4F47F8E93B474 *)L_0);
		int32_t L_1;
		L_1 = VirtualFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Linq.Expressions.LambdaExpression::get_ParameterCount() */, (LambdaExpression_t26BF905E97E6D94F81F17D60F4F4F47F8E93B474 *)L_0);
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_003c;
		}
	}
	{
		HashSet_1_t42A3AC337CA15FAC250AA5DA438F909806C72CB0 * L_2 = (HashSet_1_t42A3AC337CA15FAC250AA5DA438F909806C72CB0 *)il2cpp_codegen_object_new(HashSet_1_t42A3AC337CA15FAC250AA5DA438F909806C72CB0_il2cpp_TypeInfo_var);
		HashSet_1__ctor_m7E015D0E7832B3967403CAEE703C819D77AE741B(L_2, /*hidden argument*/HashSet_1__ctor_m7E015D0E7832B3967403CAEE703C819D77AE741B_RuntimeMethod_var);
		V_1 = (HashSet_1_t42A3AC337CA15FAC250AA5DA438F909806C72CB0 *)L_2;
		V_2 = (int32_t)0;
		Expression_1_t01B093F396848A065BE827B0C58E6F20E760FB6F * L_3 = ___node0;
		NullCheck((LambdaExpression_t26BF905E97E6D94F81F17D60F4F4F47F8E93B474 *)L_3);
		int32_t L_4;
		L_4 = VirtualFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Linq.Expressions.LambdaExpression::get_ParameterCount() */, (LambdaExpression_t26BF905E97E6D94F81F17D60F4F4F47F8E93B474 *)L_3);
		V_3 = (int32_t)L_4;
		goto IL_002c;
	}

IL_001a:
	{
		HashSet_1_t42A3AC337CA15FAC250AA5DA438F909806C72CB0 * L_5 = V_1;
		Expression_1_t01B093F396848A065BE827B0C58E6F20E760FB6F * L_6 = ___node0;
		int32_t L_7 = V_2;
		NullCheck((LambdaExpression_t26BF905E97E6D94F81F17D60F4F4F47F8E93B474 *)L_6);
		ParameterExpression_tA7B24F1DE0F013DA4BD55F76DB43B06DB33D8BEE * L_8;
		L_8 = VirtualFuncInvoker1< ParameterExpression_tA7B24F1DE0F013DA4BD55F76DB43B06DB33D8BEE *, int32_t >::Invoke(16 /* System.Linq.Expressions.ParameterExpression System.Linq.Expressions.LambdaExpression::GetParameter(System.Int32) */, (LambdaExpression_t26BF905E97E6D94F81F17D60F4F4F47F8E93B474 *)L_6, (int32_t)L_7);
		NullCheck((HashSet_1_t42A3AC337CA15FAC250AA5DA438F909806C72CB0 *)L_5);
		bool L_9;
		L_9 = HashSet_1_Add_m9CF730E2C32C0613D8089427318C09E55C350DBE((HashSet_1_t42A3AC337CA15FAC250AA5DA438F909806C72CB0 *)L_5, (ParameterExpression_tA7B24F1DE0F013DA4BD55F76DB43B06DB33D8BEE *)L_8, /*hidden argument*/HashSet_1_Add_m9CF730E2C32C0613D8089427318C09E55C350DBE_RuntimeMethod_var);
		int32_t L_10 = V_2;
		V_2 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
	}

IL_002c:
	{
		int32_t L_11 = V_2;
		int32_t L_12 = V_3;
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_001a;
		}
	}
	{
		Stack_1_t438C22E9DF33740A9BDB48CF9504B6E044484958 * L_13 = (Stack_1_t438C22E9DF33740A9BDB48CF9504B6E044484958 *)__this->get__shadowedVars_2();
		HashSet_1_t42A3AC337CA15FAC250AA5DA438F909806C72CB0 * L_14 = V_1;
		NullCheck((Stack_1_t438C22E9DF33740A9BDB48CF9504B6E044484958 *)L_13);
		Stack_1_Push_m40AB4A635E6FAFC5491A227935BF4EB1D070C8E1((Stack_1_t438C22E9DF33740A9BDB48CF9504B6E044484958 *)L_13, (HashSet_1_t42A3AC337CA15FAC250AA5DA438F909806C72CB0 *)L_14, /*hidden argument*/Stack_1_Push_m40AB4A635E6FAFC5491A227935BF4EB1D070C8E1_RuntimeMethod_var);
	}

IL_003c:
	{
		Expression_1_t01B093F396848A065BE827B0C58E6F20E760FB6F * L_15 = ___node0;
		NullCheck((LambdaExpression_t26BF905E97E6D94F81F17D60F4F4F47F8E93B474 *)L_15);
		Expression_t30A004209C10C2D9A9785B2F74EEED431A4D4660 * L_16;
		L_16 = LambdaExpression_get_Body_m595A485419E2F0AA13FC2695DEBD99ED9712D042_inline((LambdaExpression_t26BF905E97E6D94F81F17D60F4F4F47F8E93B474 *)L_15, /*hidden argument*/NULL);
		NullCheck((ExpressionVisitor_tD098DE8A366FBBB58C498C4EFF8B003FCA726DF4 *)__this);
		Expression_t30A004209C10C2D9A9785B2F74EEED431A4D4660 * L_17;
		L_17 = VirtualFuncInvoker1< Expression_t30A004209C10C2D9A9785B2F74EEED431A4D4660 *, Expression_t30A004209C10C2D9A9785B2F74EEED431A4D4660 * >::Invoke(4 /* System.Linq.Expressions.Expression System.Linq.Expressions.ExpressionVisitor::Visit(System.Linq.Expressions.Expression) */, (ExpressionVisitor_tD098DE8A366FBBB58C498C4EFF8B003FCA726DF4 *)__this, (Expression_t30A004209C10C2D9A9785B2F74EEED431A4D4660 *)L_16);
		V_0 = (Expression_t30A004209C10C2D9A9785B2F74EEED431A4D4660 *)L_17;
		Expression_1_t01B093F396848A065BE827B0C58E6F20E760FB6F * L_18 = ___node0;
		NullCheck((LambdaExpression_t26BF905E97E6D94F81F17D60F4F4F47F8E93B474 *)L_18);
		int32_t L_19;
		L_19 = VirtualFuncInvoker0< int32_t >::Invoke(17 /* System.Int32 System.Linq.Expressions.LambdaExpression::get_ParameterCount() */, (LambdaExpression_t26BF905E97E6D94F81F17D60F4F4F47F8E93B474 *)L_18);
		if ((((int32_t)L_19) <= ((int32_t)0)))
		{
			goto IL_005e;
		}
	}
	{
		Stack_1_t438C22E9DF33740A9BDB48CF9504B6E044484958 * L_20 = (Stack_1_t438C22E9DF33740A9BDB48CF9504B6E044484958 *)__this->get__shadowedVars_2();
		NullCheck((Stack_1_t438C22E9DF33740A9BDB48CF9504B6E044484958 *)L_20);
		HashSet_1_t42A3AC337CA15FAC250AA5DA438F909806C72CB0 * L_21;
		L_21 = Stack_1_Pop_m1B41FE87FFACE09E92B93079328100A46D7A9550((Stack_1_t438C22E9DF33740A9BDB48CF9504B6E044484958 *)L_20, /*hidden argument*/Stack_1_Pop_m1B41FE87FFACE09E92B93079328100A46D7A9550_RuntimeMethod_var);
	}

IL_005e:
	{
		Expression_t30A004209C10C2D9A9785B2F74EEED431A4D4660 * L_22 = V_0;
		Expression_1_t01B093F396848A065BE827B0C58E6F20E760FB6F * L_23 = ___node0;
		NullCheck((LambdaExpression_t26BF905E97E6D94F81F17D60F4F4F47F8E93B474 *)L_23);
		Expression_t30A004209C10C2D9A9785B2F74EEED431A4D4660 * L_24;
		L_24 = LambdaExpression_get_Body_m595A485419E2F0AA13FC2695DEBD99ED9712D042_inline((LambdaExpression_t26BF905E97E6D94F81F17D60F4F4F47F8E93B474 *)L_23, /*hidden argument*/NULL);
		if ((!(((RuntimeObject*)(Expression_t30A004209C10C2D9A9785B2F74EEED431A4D4660 *)L_22) == ((RuntimeObject*)(Expression_t30A004209C10C2D9A9785B2F74EEED431A4D4660 *)L_24))))
		{
			goto IL_0069;
		}
	}
	{
		Expression_1_t01B093F396848A065BE827B0C58E6F20E760FB6F * L_25 = ___node0;
		return (Expression_t30A004209C10C2D9A9785B2F74EEED431A4D4660 *)L_25;
	}

IL_0069:
	{
		Expression_1_t01B093F396848A065BE827B0C58E6F20E760FB6F * L_26 = ___node0;
		Expression_t30A004209C10C2D9A9785B2F74EEED431A4D4660 * L_27 = V_0;
		NullCheck((Expression_1_t01B093F396848A065BE827B0C58E6F20E760FB6F *)L_26);
		Expression_1_t01B093F396848A065BE827B0C58E6F20E760FB6F * L_28;
		L_28 = VirtualFuncInvoker2< Expression_1_t01B093F396848A065BE827B0C58E6F20E760FB6F *, Expression_t30A004209C10C2D9A9785B2F74EEED431A4D4660 *, ParameterExpressionU5BU5D_tCF3EAA6D8556513C4937E1126F65AA08DF4DB147* >::Invoke(18 /* System.Linq.Expressions.Expression`1<TDelegate> System.Linq.Expressions.Expression`1<System.Object>::Rewrite(System.Linq.Expressions.Expression,System.Linq.Expressions.ParameterExpression[]) */, (Expression_1_t01B093F396848A065BE827B0C58E6F20E760FB6F *)L_26, (Expression_t30A004209C10C2D9A9785B2F74EEED431A4D4660 *)L_27, (ParameterExpressionU5BU5D_tCF3EAA6D8556513C4937E1126F65AA08DF4DB147*)(ParameterExpressionU5BU5D_tCF3EAA6D8556513C4937E1126F65AA08DF4DB147*)NULL);
		return (Expression_t30A004209C10C2D9A9785B2F74EEED431A4D4660 *)L_28;
	}
}
// System.Int32 UnityEngine.Experimental.Rendering.RenderGraphModule.RenderGraphResourceRegistry/RenderGraphResourcesData::AddNewRenderGraphResource<System.Object>(ResType&,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t RenderGraphResourcesData_AddNewRenderGraphResource_TisRuntimeObject_mA59628C41652BC72D757435F37CC9E75A7284E20_gshared (RenderGraphResourcesData_tF2FA3C3EF12CF61BE1A292070F6C8B355CEBAC8D * __this, RuntimeObject ** ___outRes0, bool ___pooledResource1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DynamicArray_1_Resize_mF6CDBE3B7E4118B28D15C20B1CA3DE54A23AB9FC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DynamicArray_1_get_Item_mA0C22D4E0321EFABE0334B7074D3E0978CE46251_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DynamicArray_1_get_size_m45BF02C8D0C22798BED29BCA0C5470C01CB2A015_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	RuntimeObject ** G_B4_0 = NULL;
	RuntimeObject ** G_B3_0 = NULL;
	IRenderGraphResourcePool_t4D3438D3ED4DDEC9E1881C5EEADCD82187BECC7D * G_B5_0 = NULL;
	RuntimeObject ** G_B5_1 = NULL;
	{
		// int result = resourceArray.size;
		DynamicArray_1_tE865ACD9B04344D2C5D88A7A1DDE805DDE7B5616 * L_0 = (DynamicArray_1_tE865ACD9B04344D2C5D88A7A1DDE805DDE7B5616 *)__this->get_resourceArray_0();
		NullCheck((DynamicArray_1_tE865ACD9B04344D2C5D88A7A1DDE805DDE7B5616 *)L_0);
		int32_t L_1;
		L_1 = DynamicArray_1_get_size_m45BF02C8D0C22798BED29BCA0C5470C01CB2A015_inline((DynamicArray_1_tE865ACD9B04344D2C5D88A7A1DDE805DDE7B5616 *)L_0, /*hidden argument*/DynamicArray_1_get_size_m45BF02C8D0C22798BED29BCA0C5470C01CB2A015_RuntimeMethod_var);
		V_0 = (int32_t)L_1;
		// resourceArray.Resize(resourceArray.size + 1, true);
		DynamicArray_1_tE865ACD9B04344D2C5D88A7A1DDE805DDE7B5616 * L_2 = (DynamicArray_1_tE865ACD9B04344D2C5D88A7A1DDE805DDE7B5616 *)__this->get_resourceArray_0();
		DynamicArray_1_tE865ACD9B04344D2C5D88A7A1DDE805DDE7B5616 * L_3 = (DynamicArray_1_tE865ACD9B04344D2C5D88A7A1DDE805DDE7B5616 *)__this->get_resourceArray_0();
		NullCheck((DynamicArray_1_tE865ACD9B04344D2C5D88A7A1DDE805DDE7B5616 *)L_3);
		int32_t L_4;
		L_4 = DynamicArray_1_get_size_m45BF02C8D0C22798BED29BCA0C5470C01CB2A015_inline((DynamicArray_1_tE865ACD9B04344D2C5D88A7A1DDE805DDE7B5616 *)L_3, /*hidden argument*/DynamicArray_1_get_size_m45BF02C8D0C22798BED29BCA0C5470C01CB2A015_RuntimeMethod_var);
		NullCheck((DynamicArray_1_tE865ACD9B04344D2C5D88A7A1DDE805DDE7B5616 *)L_2);
		DynamicArray_1_Resize_mF6CDBE3B7E4118B28D15C20B1CA3DE54A23AB9FC((DynamicArray_1_tE865ACD9B04344D2C5D88A7A1DDE805DDE7B5616 *)L_2, (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)1)), (bool)1, /*hidden argument*/DynamicArray_1_Resize_mF6CDBE3B7E4118B28D15C20B1CA3DE54A23AB9FC_RuntimeMethod_var);
		// if (resourceArray[result] == null)
		DynamicArray_1_tE865ACD9B04344D2C5D88A7A1DDE805DDE7B5616 * L_5 = (DynamicArray_1_tE865ACD9B04344D2C5D88A7A1DDE805DDE7B5616 *)__this->get_resourceArray_0();
		int32_t L_6 = V_0;
		NullCheck((DynamicArray_1_tE865ACD9B04344D2C5D88A7A1DDE805DDE7B5616 *)L_5);
		IRenderGraphResource_tC6A193679EB59585903B3E0EFC7F5B937C68AAE7 ** L_7;
		L_7 = DynamicArray_1_get_Item_mA0C22D4E0321EFABE0334B7074D3E0978CE46251((DynamicArray_1_tE865ACD9B04344D2C5D88A7A1DDE805DDE7B5616 *)L_5, (int32_t)L_6, /*hidden argument*/DynamicArray_1_get_Item_mA0C22D4E0321EFABE0334B7074D3E0978CE46251_RuntimeMethod_var);
		IRenderGraphResource_tC6A193679EB59585903B3E0EFC7F5B937C68AAE7 * L_8 = *((IRenderGraphResource_tC6A193679EB59585903B3E0EFC7F5B937C68AAE7 **)L_7);
		if (L_8)
		{
			goto IL_004b;
		}
	}
	{
		// resourceArray[result] = new ResType();
		DynamicArray_1_tE865ACD9B04344D2C5D88A7A1DDE805DDE7B5616 * L_9 = (DynamicArray_1_tE865ACD9B04344D2C5D88A7A1DDE805DDE7B5616 *)__this->get_resourceArray_0();
		int32_t L_10 = V_0;
		NullCheck((DynamicArray_1_tE865ACD9B04344D2C5D88A7A1DDE805DDE7B5616 *)L_9);
		IRenderGraphResource_tC6A193679EB59585903B3E0EFC7F5B937C68AAE7 ** L_11;
		L_11 = DynamicArray_1_get_Item_mA0C22D4E0321EFABE0334B7074D3E0978CE46251((DynamicArray_1_tE865ACD9B04344D2C5D88A7A1DDE805DDE7B5616 *)L_9, (int32_t)L_10, /*hidden argument*/DynamicArray_1_get_Item_mA0C22D4E0321EFABE0334B7074D3E0978CE46251_RuntimeMethod_var);
		RuntimeObject * L_12;
		L_12 = ((  RuntimeObject * (*) (const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->methodPointer)(/*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		*((RuntimeObject **)L_11) = (RuntimeObject *)L_12;
		Il2CppCodeGenWriteBarrier((void**)(RuntimeObject **)L_11, (void*)(RuntimeObject *)L_12);
	}

IL_004b:
	{
		// outRes = resourceArray[result] as ResType;
		RuntimeObject ** L_13 = ___outRes0;
		DynamicArray_1_tE865ACD9B04344D2C5D88A7A1DDE805DDE7B5616 * L_14 = (DynamicArray_1_tE865ACD9B04344D2C5D88A7A1DDE805DDE7B5616 *)__this->get_resourceArray_0();
		int32_t L_15 = V_0;
		NullCheck((DynamicArray_1_tE865ACD9B04344D2C5D88A7A1DDE805DDE7B5616 *)L_14);
		IRenderGraphResource_tC6A193679EB59585903B3E0EFC7F5B937C68AAE7 ** L_16;
		L_16 = DynamicArray_1_get_Item_mA0C22D4E0321EFABE0334B7074D3E0978CE46251((DynamicArray_1_tE865ACD9B04344D2C5D88A7A1DDE805DDE7B5616 *)L_14, (int32_t)L_15, /*hidden argument*/DynamicArray_1_get_Item_mA0C22D4E0321EFABE0334B7074D3E0978CE46251_RuntimeMethod_var);
		IRenderGraphResource_tC6A193679EB59585903B3E0EFC7F5B937C68AAE7 * L_17 = *((IRenderGraphResource_tC6A193679EB59585903B3E0EFC7F5B937C68AAE7 **)L_16);
		*(RuntimeObject **)L_13 = ((RuntimeObject *)Castclass((RuntimeObject*)((RuntimeObject *)IsInst((RuntimeObject*)L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 1))), IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
		Il2CppCodeGenWriteBarrier((void**)(RuntimeObject **)L_13, (void*)((RuntimeObject *)Castclass((RuntimeObject*)((RuntimeObject *)IsInst((RuntimeObject*)L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 1))), IL2CPP_RGCTX_DATA(method->rgctx_data, 1))));
		// outRes.Reset(pooledResource ? pool : null);
		RuntimeObject ** L_18 = ___outRes0;
		bool L_19 = ___pooledResource1;
		G_B3_0 = L_18;
		if (L_19)
		{
			G_B4_0 = L_18;
			goto IL_006f;
		}
	}
	{
		G_B5_0 = ((IRenderGraphResourcePool_t4D3438D3ED4DDEC9E1881C5EEADCD82187BECC7D *)(NULL));
		G_B5_1 = G_B3_0;
		goto IL_0075;
	}

IL_006f:
	{
		IRenderGraphResourcePool_t4D3438D3ED4DDEC9E1881C5EEADCD82187BECC7D * L_20 = (IRenderGraphResourcePool_t4D3438D3ED4DDEC9E1881C5EEADCD82187BECC7D *)__this->get_pool_2();
		G_B5_0 = L_20;
		G_B5_1 = G_B4_0;
	}

IL_0075:
	{
		NullCheck((IRenderGraphResource_tC6A193679EB59585903B3E0EFC7F5B937C68AAE7 *)(*G_B5_1));
		VirtualActionInvoker1< IRenderGraphResourcePool_t4D3438D3ED4DDEC9E1881C5EEADCD82187BECC7D * >::Invoke(4 /* System.Void UnityEngine.Experimental.Rendering.RenderGraphModule.IRenderGraphResource::Reset(UnityEngine.Experimental.Rendering.RenderGraphModule.IRenderGraphResourcePool) */, (IRenderGraphResource_tC6A193679EB59585903B3E0EFC7F5B937C68AAE7 *)(*G_B5_1), (IRenderGraphResourcePool_t4D3438D3ED4DDEC9E1881C5EEADCD82187BECC7D *)G_B5_0);
		// return result;
		int32_t L_21 = V_0;
		return (int32_t)L_21;
	}
}
// System.Void UnityEngine.XR.ARSubsystems.XRPlaneSubsystem/Provider::CreateOrResizeNativeArrayIfNecessary<UnityEngine.Vector2>(System.Int32,Unity.Collections.Allocator,Unity.Collections.NativeArray`1<T>&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Provider_CreateOrResizeNativeArrayIfNecessary_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_m1A2EEEF53CFF49C4078333BE7CBA476FEAC9535D_gshared (int32_t ___length0, int32_t ___allocator1, NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0 * ___array2, const RuntimeMethod* method)
{
	{
		// if (array.IsCreated)
		NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0 * L_0 = ___array2;
		bool L_1;
		L_1 = NativeArray_1_get_IsCreated_mF82CF064C6050394DEAC11047A6308998101A4AE((NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0 *)(NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		// if (array.Length != length)
		NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0 * L_2 = ___array2;
		int32_t L_3;
		L_3 = IL2CPP_NATIVEARRAY_GET_LENGTH(((NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0 *)(NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0 *)L_2)->___m_Length_1);
		int32_t L_4 = ___length0;
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0034;
		}
	}
	{
		// array.Dispose();
		NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0 * L_5 = ___array2;
		NativeArray_1_Dispose_mE737DADB8C5E4B02C2FB6CB36E86AC6371D806BF((NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0 *)(NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		// array = new NativeArray<T>(length, allocator);
		NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0 * L_6 = ___array2;
		int32_t L_7 = ___length0;
		int32_t L_8 = ___allocator1;
		NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0  L_9;
		memset((&L_9), 0, sizeof(L_9));
		NativeArray_1__ctor_m53A2E0E5DDF506B8508EAC444CAF3DB70F874729((&L_9), (int32_t)L_7, (int32_t)L_8, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4));
		*(NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0 *)L_6 = L_9;
		// }
		return;
	}

IL_0026:
	{
		// array = new NativeArray<T>(length, allocator);
		NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0 * L_10 = ___array2;
		int32_t L_11 = ___length0;
		int32_t L_12 = ___allocator1;
		NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0  L_13;
		memset((&L_13), 0, sizeof(L_13));
		NativeArray_1__ctor_m53A2E0E5DDF506B8508EAC444CAF3DB70F874729((&L_13), (int32_t)L_11, (int32_t)L_12, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4));
		*(NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0 *)L_10 = L_13;
	}

IL_0034:
	{
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Builder_set_type_m31E443E368751EC60EBA1A39D670313269E50B74_inline (Builder_t4C196179CE57554255EBFB4ECD4D6499ADA974FB * __this, Type_t * ___value0, const RuntimeMethod* method)
{
	{
		// public Type type { get; set; }
		Type_t * L_0 = ___value0;
		__this->set_U3CtypeU3Ek__BackingField_2(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Expression_t30A004209C10C2D9A9785B2F74EEED431A4D4660 * LambdaExpression_get_Body_m595A485419E2F0AA13FC2695DEBD99ED9712D042_inline (LambdaExpression_t26BF905E97E6D94F81F17D60F4F4F47F8E93B474 * __this, const RuntimeMethod* method)
{
	{
		Expression_t30A004209C10C2D9A9785B2F74EEED431A4D4660 * L_0 = __this->get__body_3();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ArraySegment_1_get_Count_mF79DF195C03504D335115BA66BC48FB0BB4D395C_gshared_inline (ArraySegment_1_tE4B89ED4839E563DF2A25436675AC5C5E87B62B9 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__count_2();
		return (int32_t)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return (int32_t)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ArraySegment_1_get_Count_mC78F0263F5BAC4CA823FA314F69DD18E7E4307D2_gshared_inline (ArraySegment_1_t9487D5EA4323A4B2C1F7F94C91363EBEE33F3462 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__count_2();
		return (int32_t)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ArraySegment_1_get_Count_m038FD84B16BD0AC31B33B3B9D9237708DFC1AA32_gshared_inline (ArraySegment_1_t73AEFBC798515A51F93CF5816E332A0654AE698D * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__count_2();
		return (int32_t)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ArraySegment_1_get_Count_m03AF4BDAA1D5D96096A0072A85C43C2FBC7C03EE_gshared_inline (ArraySegment_1_t12E2F3BC867255EB056CA1F106A8D1E85142A3F1 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__count_2();
		return (int32_t)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ArraySegment_1_get_Count_m86D4F7A8C6E66A89B03DB83E4F8F82019625E483_gshared_inline (ArraySegment_1_t3BCE22176B44ACEB25236AD7501F226112F6E07A * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__count_2();
		return (int32_t)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ArraySegment_1_get_Count_m313EE200EA1CB465EF2B1F8CFFA7E63FA3F408EF_gshared_inline (ArraySegment_1_t8A45E4F401BB0C4F9F8643CCA8706F98DC8057F6 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__count_2();
		return (int32_t)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_m7B5E3383CB67492E573AC0D875ED82A51350F188_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929(/*hidden argument*/NULL);
	}

IL_000e:
	{
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_2 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)__this->get__items_1();
		int32_t L_3 = ___index0;
		RuntimeObject * L_4;
		L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)L_2, (int32_t)L_3);
		return (RuntimeObject *)L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ArraySegment_1_get_Count_mFB0298EF19B049FBF2C0C3D7FCA5E16DB8F80D56_gshared_inline (ArraySegment_1_t9FDA96F04294A918E0DBFCE7CF750DE23A898976 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__count_2();
		return (int32_t)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ArraySegment_1_get_Count_m779174D975AA29188F9DFFC751199785A7F0ADBD_gshared_inline (ArraySegment_1_t7FCE6A956BF91EE65591716518285D293D3C5B6B * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__count_2();
		return (int32_t)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* Array_Empty_TisRuntimeObject_m002765312BF306B1B3B5BFAB9550C0A2A1820CDA_gshared_inline (const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_0 = ((EmptyArray_1_tBF73225DFA890366D579424FE8F40073BF9FBAD4_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(method->rgctx_data, 0)))->get_Value_0();
		return (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t DynamicArray_1_get_size_m0D404D0EDA77F50046B5592673CEADED3EB1DDF9_gshared_inline (DynamicArray_1_tBD159187A396D2634FDBCFD5A8F85F26FA3DF11D * __this, const RuntimeMethod* method)
{
	{
		// public int size { get; private set; }
		int32_t L_0 = (int32_t)__this->get_U3CsizeU3Ek__BackingField_1();
		return (int32_t)L_0;
	}
}
